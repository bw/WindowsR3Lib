#include "stdafx.h"
#include "Number.h"

using namespace Mrlee_R3;

// 将LONGLONG类型转换为十六进制字符串。
void CNumber::LONGLONGToHexStr(__in LONGLONG llData, __out CString *hexStr) {
	while (0 != llData) {
		if (-1 == llData) {
			int chazhi = (16 - hexStr->GetLength()) / 2;
			for (int i = 0; i < chazhi; i++)
			{
				//*hexStr = CString("FF") + *hexStr;
				*hexStr += "FF";
			}
			break;
		}
		CString temp;
		temp.Format(TEXT("%02X"), (BYTE)llData);
		*hexStr = temp + *hexStr;
		llData >>= 8;
	}
}

// 将ULONGLONG类型转换为十六进制字符串。
void CNumber::ULONGLONGToHexStr(__in ULONGLONG llData, __in CString *hexStr) {
	while (0 != llData) {
		CString temp;
		temp.Format(TEXT("%02X"), (BYTE)llData);
		*hexStr = temp + *hexStr;
		llData >>= 8;
	}
}

//  将十六进制数字数组转换成数字。
BOOL CNumber::HexBytesArrayToNumber(__in BYTE szHex[], __in SIZE_T length, __out LONGLONG *outData)
{
	if (length > sizeof(LONGLONG))
		return FALSE;
	LONGLONG num = 0;
	for (SIZE_T i = 0; i < length; i++)
	{
		// 这里的pow中的参数做了强制转换，如果不做这样的转换会报下面的错误：
		// “pow”: 对重载函数的调用不明确
		num += (LONGLONG)(szHex[i] * (pow((float)0x100, (int)i)));
	}
	*outData = num;
	return TRUE;
}

// 数字转换字节数组。
void CNumber::NumberToBytesArray(__in LONGLONG &hex, __out BYTE szHex[], __in SIZE_T length) {
	ULONGLONG ll_temp = (ULONGLONG)hex;
	ZeroMemory(szHex, length);
	for (SIZE_T i = 0; i < length && i < sizeof(ULONGLONG); i++)
	{
		szHex[i] = ll_temp & 0xFF;
		ll_temp >>= 8;
	}
}

// 判断一个数字是否是偶数。
BOOL CNumber::IsEvenNum(__in LONGLONG dwNum)
{
	LONGLONG n = dwNum % 2;
	if (n == 1)
		return FALSE;
	else
		return TRUE;
}