#include "stdafx.h"
#include "WinAPIForInject.h"

//////////////////////////////////////////////////////////////////////////
#pragma optimize( "", off )
#pragma check_stack( off )
#pragma runtime_checks("sc", off)	// 关闭基本运行时检查。

// 为注入所写的GetProcAddress()函数。
FARPROC __stdcall GetProcAddressForInject (InjectParams *pInjectParams)
{
	//return ((GetProcAddress_Ptr)(*((ULONG_PTR*)lpBuffer))) (*(HMODULE*)((ULONG_PTR)lpBuffer + sizeof(LPVOID)), *((LPCSTR*)((BYTE*)lpBuffer + sizeof(LPVOID) + sizeof(HMODULE))));
	return (pInjectParams->pGetProcAddress) (pInjectParams->hDllHandle, pInjectParams->pFunName);
}
int __stdcall AfterGetProcAddressForInject(void) {return 1;}

#pragma runtime_checks("sc", restore)
#pragma check_stack( on )
#pragma optimize( "", on )


//////////////////////////////////////////////////////////////////////////