#include "stdafx.h"
#include "IInject.h"

using namespace Mrlee_R3::Security::Inject;

IInject::IInject()
	: m_IsSuc(FALSE)
	, m_Pid(0)
{

}

IInject::~IInject() {
}

BOOL IInject::Eject(){return TRUE;}

// 获得注入类型字符串。
CString IInject::GetInjectTypeString() {return m_strInjectType;}

// 获得被注入进程的ID。
DWORD IInject::GetPID() {return m_Pid;}

// 注入是否成功。
BOOL IInject::IsInjectSuccess() {
	return m_IsSuc;
}