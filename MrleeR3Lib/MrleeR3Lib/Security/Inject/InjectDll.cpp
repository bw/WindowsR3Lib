#include "stdafx.h"
#include "InjectDll.h"

using namespace Mrlee_R3;
using namespace Mrlee_R3::Process;
using namespace Mrlee_R3::Security::Inject;

CInjectDll_RemoteLoad::CInjectDll_RemoteLoad()
	: m_hLibModule(NULL)
	, CInjectBase()
{
	m_strInjectType = TEXT("CInjectDll_RemoteLoad");
}

CInjectDll_RemoteLoad::~CInjectDll_RemoteLoad()
{
}

// 将Dll注入到进程中。
BOOL CInjectDll_RemoteLoad::Inject(
	__in		DWORD dwPID, 
	__in		CString& szDllPath, 
	__in		BOOL isWait /* = TRUE */, 
	__in_opt	TCHAR *szFunName /* = NULL */, 
	__in_opt	LPVOID lpParamBuffer /* = NULL */, 
	__in_opt	SIZE_T paramSize /* = 0 */, 
	__out_opt	LPDWORD pFunRet /* = NULL */ )
{
	HANDLE	hThread_LoadDll = NULL;	// 线程句柄。
	HANDLE	hProcess = NULL;	// 进程句柄。
	void	*pLibRemote = NULL;// 远程进程中dll路径字符串的首地址。
	PVOID	pFunNameRemote = NULL;
	HANDLE	hThread_GetFunAddr = NULL;
	PVOID	pGetProcAddrParamBuffer = NULL;
	BOOL	bRet = TRUE;// 返回注入是否成功。
	LPVOID	pFunAddrRemote = NULL;
	DWORD	dwFuncSize;
	SIZE_T	funNameNum;	// 函数名称所占字节数。
	CStringW szDllPathW(szDllPath);

	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}

	// 启动DEBUG特权。
	if (!SuperPrivileges_EnableDebugPrivilege(TRUE))
		return FALSE;

	dwFuncSize = WinSys::GetFuncSize(GetProcAddressForInject);
	if ( 0 == dwFuncSize ) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - GetProcAddressForInject函数的大小计算失败。");
		bRet = FALSE;
		goto _return;
	}

	SIZE_T pathBytesNum = szDllPathW.GetLength() * sizeof(wchar_t) + sizeof(wchar_t);	// 路径所占字节数。
	hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPID);
	if (hProcess == NULL) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 打开进程失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _return;
	}

	// Allocate memory in the remote process to store the szLibPath string
	pLibRemote = ::VirtualAllocEx(hProcess, NULL, pathBytesNum, MEM_COMMIT, PAGE_READWRITE);
	if (NULL == pLibRemote) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远程进程中分配内存失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _return;
	}

	// 将dll路径拷贝到远程进程中。
	if (!::WriteProcessMemory(hProcess, pLibRemote, (void*)szDllPathW.GetBuffer(), pathBytesNum, NULL)) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 写远程进程内存失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _return;
	}

	LPTHREAD_START_ROUTINE FunLoadLibraryWAddr = (LPTHREAD_START_ROUTINE)::GetProcAddress(::GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
	// 在远程进程中加载Dll。
	hThread_LoadDll = ::CreateRemoteThread(hProcess, 
		NULL, 
		0,	
		FunLoadLibraryWAddr, 
		pLibRemote, 
		0, 
		NULL);
	// Failed
	if(NULL == hThread_LoadDll) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远处进程中加载动态链接库失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _return;
	}

	// 等待线程中的 LoadLibrary() 函数执行完毕，线程返回。
	::WaitForSingleObject(hThread_LoadDll, INFINITE);
	::GetExitCodeThread(hThread_LoadDll, (LPDWORD)&m_hLibModule);
	// 判断远程调用 LoadLibrary() 是否成功。
	if (!m_hLibModule) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - LoadLibraryW()调用失败。请检查Dll路径是否正确。");
		bRet = FALSE;
		goto _return;
	}

	//////////////////////////////////////////////////////////////////////////
	// 调用dll中的函数。
	if ( NULL != szFunName ) {
		CStringA funName(szFunName);
		funNameNum = funName.GetLength() + 1;
		if ( 0 == funName.GetLength() )
			goto _return;

		// 将函数名拷贝到远程进程。
		pFunNameRemote = WriteProcessMemory_(hProcess, funName.GetBuffer(), funNameNum);
		if (!pFunNameRemote) {
			m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远程进程中写函数名失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
			bRet = FALSE;
			goto _return;
		}

		// 在远程进程中获得函数名所代表的函数的地址。
		InjectParams injectParams;
		HMODULE hKernel32 = ::GetModuleHandle(TEXT("Kernel32"));
		injectParams.pGetProcAddress = (GetProcAddress_Ptr)::GetProcAddress(hKernel32, "GetProcAddress");
		injectParams.hDllHandle = m_hLibModule;
		injectParams.pFunName = (char*)pFunNameRemote;
		//bRet = RemoteFunctionCall_CopyTo(hProcess, (LPTHREAD_START_ROUTINE)GetProcAddressForInject, (SIZE_T)AfterGetProcAddressForInject - (SIZE_T)GetProcAddressForInject, (LPDWORD)&pFunAddrRemote, &injectParams, sizeof(InjectParams));
		bRet = RemoteFunctionCall_CopyTo(hProcess, (LPTHREAD_START_ROUTINE)GetProcAddressForInject, dwFuncSize, TRUE, (LPDWORD)&pFunAddrRemote, &injectParams, sizeof(InjectParams));

		if ( FALSE == bRet ) {
			m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远程进程中调用GetProcAddressForInject()失败。%s"), 
							WinSys::Mrlee_ErrorMessage(GetLastError()));
			goto _return;
		}

		if ( NULL == pFunAddrRemote ) {
			m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远程进程中获得Dll的函数地址失败。");
			bRet = FALSE;
			goto _return;
		}

		bRet = RemoteFunctionCall(hProcess, (LPTHREAD_START_ROUTINE)pFunAddrRemote, isWait, pFunRet, lpParamBuffer, paramSize);
		if ( FALSE == bRet ) {
			m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 在远程进程中调用%s()函数失败。%s"), 
							CString(funName), WinSys::Mrlee_ErrorMessage(GetLastError()));
			goto _return;
		}
	}

	m_Pid = dwPID;
	m_DllPath = szDllPathW;
	m_IsSuc = TRUE;

_return:
	if (hThread_LoadDll)
		::CloseHandle(hThread_LoadDll);
	if (hProcess)
		::CloseHandle(hProcess);
	if (pLibRemote)
		::VirtualFreeEx(hProcess, pLibRemote, pathBytesNum, MEM_RELEASE);	// 释放为dll路径字符串分配的内存。
	if (pFunNameRemote)
		::VirtualFreeEx(hProcess, pFunNameRemote, funNameNum, MEM_RELEASE);
	if (hThread_GetFunAddr)
		::CloseHandle(hThread_GetFunAddr);
	SuperPrivileges_EnableDebugPrivilege(FALSE);	// 禁用DEBUG特权。
	return bRet;
}

// 将Dll注入到进程中。
BOOL CInjectDll_RemoteLoad::Inject(
	__in TCHAR *strWinName_Contain, 
	__in		CString& szDllPath, 
	__in		BOOL isWait /* = TRUE */, 
	__in_opt	TCHAR *szFunName /* = NULL */, 
	__in_opt	LPVOID lpParamBuffer /* = NULL */, 
	__in_opt	SIZE_T paramSize /* = 0 */, 
	__out_opt	LPDWORD pFunRet /* = NULL */ )
{
	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}
	HWND hWnd = FindTopWindow(strWinName_Contain, TRUE);
	if (hWnd) {
		DWORD dwPID;
		GetWindowThreadProcessId(hWnd, &dwPID);
		return CInjectDll_RemoteLoad::Inject(dwPID, szDllPath, isWait, szFunName, lpParamBuffer, paramSize, pFunRet);
	} else {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 获得窗口句柄失败。");
		return FALSE;
	}
}

// 将Dll注入到进程中。
BOOL CInjectDll_RemoteLoad::Inject(
	__in_opt	TCHAR *strWinName, 
	__in_opt	TCHAR *className, 
	__in		CString& szDllPath, 
	__in		BOOL isWait /* = TRUE */, 
	__in_opt	TCHAR *szFunName /* = NULL */, 
	__in_opt	LPVOID lpParamBuffer /* = NULL */, 
	__in_opt	SIZE_T paramSize /* = 0 */, 
	__out_opt	LPDWORD pFunRet /* = NULL */ )
{
	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}
	HWND hWnd = ::FindWindow(className, strWinName);
	if (hWnd) {
		DWORD dwPID;
		GetWindowThreadProcessId(hWnd, &dwPID);
		return CInjectDll_RemoteLoad::Inject(dwPID, szDllPath, isWait, szFunName, lpParamBuffer, paramSize, pFunRet);
	} else {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::InjectDll() - 获得窗口句柄失败。");
		return FALSE;
	}
}

// 卸载注入到进程中的Dll。
BOOL CInjectDll_RemoteLoad::Eject() {
	HANDLE	hThread_FreeDll;
	HANDLE	hProcess;
	DWORD	dwRetFreeLibrary;

	if (!IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::EjectDll() - 未曾进行注入。");
		SetLastError(MRLEE_ERROR_CALL_NOT_UNLOAD);	// 调用未卸载。
		return FALSE;
	}

	// 启动DEBUG权限。
	if (!SuperPrivileges_EnableDebugPrivilege(TRUE))
		return FALSE;
	BOOL bRet = TRUE;

	// 打开远程进程。
	hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_Pid);
	if (hProcess == NULL) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::EjectDll() - 打开进程失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _ret;
	}

	// 释放远程进程中加载的Dll。
	hThread_FreeDll = ::CreateRemoteThread(hProcess,
		NULL, 
		0,
		(LPTHREAD_START_ROUTINE)::GetProcAddress(::GetModuleHandle(TEXT("Kernel32")), "FreeLibrary"), 
		(void*)m_hLibModule, 
		0, 
		NULL );
	// Failed to unload
	if (hThread_FreeDll == NULL) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_RemoteLoad::EjectDll() - 在远程进程中调用FreeLibrary()函数失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		bRet = FALSE;
		goto _ret;
	}

	// 等待线程中的 FreeLibrary() 函数执行完毕，线程返回。
	::WaitForSingleObject(hThread_FreeDll, INFINITE);
	::GetExitCodeThread(hThread_FreeDll, &dwRetFreeLibrary);

	if (dwRetFreeLibrary) {
		m_Pid = 0;
		m_DllPath.Empty();	// 清空dll路径。
		m_hLibModule = NULL;
	} else {
		m_ErrorInfo = TEXT("【错误】CInjectDll_RemoteLoad::EjectDll() - 在远程进程中卸载Dll失败。");
		bRet = FALSE;
		goto _ret;
	}

	m_IsSuc = FALSE;
	
_ret:
	if (hThread_FreeDll)
		::CloseHandle(hThread_FreeDll);
	if (hProcess)
		::CloseHandle(hProcess);
	SuperPrivileges_EnableDebugPrivilege(FALSE);	// 禁用DEBUG权限。

	return bRet;
}

//////////////////////////////////////////////////////////////////////////
// CInject_SWHE

// 构造函数。
CInjectDll_SWHE::CInjectDll_SWHE()
	: m_hHook(NULL)
	, CInjectBase()
{
	m_strInjectType = TEXT("CInjectDll_SWHE");
}

// 析构函数。
CInjectDll_SWHE::~CInjectDll_SWHE() {}

// 安装钩子。
//BOOL CInjectDll_SWHE::Inject(
//	__in DWORD	dwThreadId, 
//	__in TCHAR* strDllPath, 
//	__in TCHAR* strDllFunName)
//{
//	
//}

// 安装线程钩子。将钩子安装到指定窗口所在的线程中。
BOOL CInjectDll_SWHE::Inject(
	 __in HWND	hWnd, 
	 __in TCHAR* strDllPath, 
	 __in TCHAR* strDllFunName)
{
	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_SWHE::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}

	// 判断窗口句柄是否有效。
	if (!IsWindow(hWnd)) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_SWHE::InjectDll() - 无效的窗口句柄。");
		return FALSE;
	}

	// 获得窗口所在的进程和主线程Id。
	DWORD dwProcessId = 0;
	DWORD dwThreadId = 0;

	dwThreadId = ::GetWindowThreadProcessId(hWnd, &dwProcessId);	// 获得被注入窗口所属线程Id。
	if ( 0 == dwThreadId ) {
		m_ErrorInfo.Format(TEXT("【错误】CInjectDll_SWHE::InjectDll() - 获得窗口所属线程ID失败。%s"), WinSys::Mrlee_ErrorMessage(GetLastError()));
		return FALSE;
	}

	HMODULE hDll = ::LoadLibrary(strDllPath);	// 获得dll句柄。
	if (NULL == hDll) {
		return FALSE;
	}

	CStringA funName(strDllFunName);
	DWORD dwLastError = ERROR_SUCCESS;
	FARPROC dllFun = ::GetProcAddress(hDll, funName.GetBuffer());	// 获得函数地址。
	// 获得失败。
	if (NULL == dllFun) {
		dwLastError = GetLastError();
		goto _ret;
	}
	m_hHook = ::SetWindowsHookEx(WH_GETMESSAGE, (HOOKPROC)dllFun, hDll, dwThreadId);

	if (m_hHook) {
		m_IsSuc = TRUE;
		GetWindowThreadProcessId(hWnd, &m_Pid);
	} else {
		dwLastError = GetLastError();
		goto _ret;
	}

_ret:
	::FreeLibrary(hDll);
	SetLastError(dwLastError);
	return m_IsSuc;
}

// 安装线程钩子。将钩子安装到指定窗口所在的线程中。
BOOL CInjectDll_SWHE::Inject(
	__in TCHAR* strWindowName_Contain, 
	__in TCHAR* strDllPath, 
	__in TCHAR* strDllFunName)
{
	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_SWHE::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}

	// 获得窗口句柄。
	HWND hWnd = FindTopWindow(strWindowName_Contain, TRUE);
	return Inject(hWnd, strDllPath, strDllFunName);
}

// 安装线程钩子。将钩子安装到指定窗口所在的线程中。
BOOL CInjectDll_SWHE::Inject(
	__in_opt TCHAR* strWindowName, 
	__in_opt TCHAR* strClassName, 
	__in TCHAR *strDllPath, 
	__in TCHAR* strDllFunName)
{
	if (IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_SWHE::InjectDll() - 已经注入，不能重复注入。");
		return FALSE;
	}
	if ( NULL == strClassName && NULL == strWindowName ) {
		TRACE("【错误】CInjectDll_SWHE::InjectDll() - 窗口名和窗口类名均为NULL。\n");
		SetLastError(ERROR_INVALID_WINDOW_HANDLE);	// 无效窗口句柄。
		return FALSE;
	}

	// 获得窗口句柄。
	HWND hWnd = ::FindWindow(strClassName, strWindowName);
	return Inject(hWnd, strDllPath, strDllFunName);
}

// 卸载注入到进程中的Dll。
BOOL CInjectDll_SWHE::Eject() {
	if (!IsInjectSuccess()) {
		m_ErrorInfo = TEXT("【错误】CInjectDll_SWHE::InjectDll() - 未曾进行注入。");
		return FALSE;
	}
	return UnhookWindowsHookEx(m_hHook);
}
