#include "stdafx.h"
#include "InjectExe.h"

using namespace Mrlee_R3::File;
using namespace Mrlee_R3::Process;
using namespace Mrlee_R3::Security::Inject;

CInjectExe::CInjectExe() {
	//ZeroMemory(&m_ProcInfo, sizeof(PROCESS_INFORMATION));
}

CInjectExe::~CInjectExe() {
}

// 开始注入。
BOOL CInjectExe::Inject(__in TCHAR* pDesFilePath, __in CPeLoader32 *pCPeLoaderEx) {
	// 打开文件映射成功
	STARTUPINFO si = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION pi = {0};
	CONTEXT context = {0};			// 保存线程上下文
	DWORD dwBuffer = NULL;			// 保存目标程序的映像基址
	DWORD dwImageFileSize = 0;		// 注入目标程序的程序内存映射大小
	LPVOID lpImageBase = NULL;		// 分配的基址
	DWORD dwOldProtect = 0;
	
	BOOL bRet = CreateProcess(NULL, pDesFilePath, NULL, NULL, 
		FALSE, CREATE_SUSPENDED, NULL, NULL, &si, &pi);
	// 创建目标进程
	if (bRet)
	{
		__try {
			context.ContextFlags = CONTEXT_INTEGER;
			// 获得目标进程的线程上下文。
			if (GetThreadContext(pi.hThread, &context) == 0) {
				OutputDebugString(TEXT("CInjectExe::InjectStart() - 获得线程上下文失败！\n"));
				__leave;
			}

			// 获得目标进程的映像基址。
			// context 中 EBX 的值指向的就是该进程的 PEB。
			// 这个 PEB 不是 Winternl.h 中的PEB，而是 ntdll!_PEB，可用 WinDbg 查看。
			// PEB + 8 是 ImageBaseAddress。即，映像基址。
			ReadProcessMemory_(pi.hProcess, (LPCVOID)((LONG_PTR)context.Ebx + 8), &dwBuffer, 4, NULL);
			if(dwBuffer == 0)
			{OutputDebugString(TEXT("CInjectExe::InjectStart() - 未能读取到映像基址！\n"));__leave;}

			HINSTANCE hinstLib = LoadLibrary(TEXT("ntdll.dll"));
			// 获得 ZwUnmapViewOfSection 函数的地址。
			ZwUnmapViewOfSection_Ptr ZwUnmapViewOfSection = 
				(ZwUnmapViewOfSection_Ptr)GetProcAddress(hinstLib,"ZwUnmapViewOfSection");
			// 释放目标进程的地址映射
			NTSTATUS ntStart = ZwUnmapViewOfSection(pi.hProcess, (PVOID)(LONG_PTR)dwBuffer);
			if (!NT_SUCCESS(ntStart))
			{OutputDebugString(TEXT("CInjectExe::InjectStart() - 释放目标地址映射失败！\n"));__leave;}

			dwImageFileSize = pCPeLoaderEx->GetImageFileSize();	// 获得文件映射入内存后的大小

			// 在目标进程中分配内存空间
			lpImageBase = VirtualAllocEx(pi.hProcess, 
				(LPVOID)((LONG_PTR)pCPeLoaderEx->GetImageBase()), 
				dwImageFileSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

			if (0 == lpImageBase)
			{OutputDebugString(TEXT("CInjectExe::InjectStart() - 在目标进程中分配内存空间失败！\n"));__leave;}

			// 将注入程序的PE头写入目标进程
			if (!WriteProcessMemory_(pi.hProcess, lpImageBase, pCPeLoaderEx->GetFilePoint(), 
				pCPeLoaderEx->GetImageOptionHeader()->SizeOfHeaders, NULL))
			{
				OutputDebugString(TEXT("CInjectExe::InjectStart() - 将注入程序的PE头写入目标进程失败！\n"));
				__leave;
			}

			// 获得区段个数
			DWORD dwNumberOfSections = pCPeLoaderEx->GetImageFileHeader()->NumberOfSections;
			for (DWORD i = 0; i < dwNumberOfSections; i++)
			{
				// 将节内容写入目标程序`
				if (!WriteProcessMemory_(pi.hProcess, 
					(LPVOID)((LONG_PTR)lpImageBase + pCPeLoaderEx->GetImageSectionHeader()[i].VirtualAddress), 
					(LPCVOID)(pCPeLoaderEx->GetImageSectionHeader()[i].PointerToRawData + (LONG_PTR)(pCPeLoaderEx->GetFilePoint())), 
					pCPeLoaderEx->GetImageSectionHeader()[i].SizeOfRawData, NULL))
				{
					//OutputDebugString(TEXT("CInjectExe::InjectStart() - 将节内容写入目标程序失败！\n"));
					TRACE("CInjectExe::InjectStart() - 将节内容写入目标程序失败。\n");
					__leave;
				}
				// 设置节的保护属性
				VirtualProtectEx(pi.hProcess, 
					(LPVOID)((LONG_PTR)lpImageBase + pCPeLoaderEx->GetImageSectionHeader()[i].VirtualAddress), 
					pCPeLoaderEx->GetImageSectionHeader()[i].Misc.VirtualSize, 
					pCPeLoaderEx->PageProtectType(pCPeLoaderEx->GetImageSectionHeader()[i].Characteristics), &dwOldProtect);
			}

			// 设置映像基址
			if (!WriteProcessMemory_(pi.hProcess, (LPVOID)((LONG_PTR)context.Ebx + 8), (LPCVOID)(&lpImageBase), 4, NULL))
			{
				//OutputDebugString(TEXT("CInjectExe::InjectStart() - 设置映像基址失败！\n"));
				TRACE("CInjectExe::InjectStart() - 设置映像基址失败。\n");
				__leave;
			}

			context.Eax = (LONG_PTR)lpImageBase + pCPeLoaderEx->GetOEPOffset();
			SetThreadContext(pi.hThread, &context);	// 设置线程环境变量
			ResumeThread(pi.hThread);	// 恢复线程运行
			m_IsSuc = TRUE;
		} __finally {
			CloseHandle(pi.hThread);	// 释放线程句柄
		}
		if (FALSE == m_IsSuc) {
			TerminateProcess(pi.hProcess, 0);	// 结束进程。
			CloseHandle(pi.hProcess);	// 释放进程句柄。
		} else {
			m_Pid = pi.dwProcessId;
			CloseHandle(pi.hProcess);	// 释放进程句柄。
		}
		return m_IsSuc;
	}
	else
	{
		OutputDebugString(TEXT("CInjectExe::InjectStart() - 创建目标程序的进程失败！\n"));
		return FALSE;
	}

	return FALSE;
}

// 终止注入。
BOOL CInjectExe::Eject() {
	if (m_IsSuc) {
		HANDLE handle = OpenProcess_(m_Pid);
		if ( handle != NULL ) {
			TerminateProcess(handle, 0);// 结束进程。
			CloseHandle(handle);			// 释放进程句柄。
		}
		m_IsSuc = FALSE;
	}
	return TRUE;
}