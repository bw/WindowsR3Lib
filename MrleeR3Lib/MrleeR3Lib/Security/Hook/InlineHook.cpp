#include "stdafx.h"
#include "InlineHook.h"

using namespace Mrlee_R3::Process;
using namespace Mrlee_R3::Security::Hook;

CInlineHook::CInlineHook(){}

// 构造函数。
CInlineHook::CInlineHook(__in DWORD dwPID, __in PVOID TargetAddr, __in PVOID ProxyFun, __in DWORD dwOffset /* = 0 */)
	: m_Pid(dwPID), 
	  m_TargetAddr(TargetAddr), 
	  m_Offset(dwOffset), 
	  m_ProxyFun(ProxyFun), 
	  m_RealInstsLength(0), 
	  m_IsSuc(FALSE)
{
	m_FinalAddr = (PVOID)((ULONG_PTR)TargetAddr + dwOffset);
	ZeroMemory(m_OrigInsts, 30);
}

// Hook。
BOOL CInlineHook::Hook_() {
	if (m_IsSuc)
		return TRUE;

	BYTE *addressTemp = (BYTE*)m_FinalAddr;
	INSTRUCTION inst = {0};
	while (1)
	{
		if (m_RealInstsLength < 5) {
			get_instruction(&inst, addressTemp, MODE_32);
			m_RealInstsLength += inst.length;
			addressTemp += inst.length;
		} else {
			break;
		}
	}

	// 拷贝指令。
	memcpy(m_OrigInsts, m_FinalAddr, m_RealInstsLength);
	// 计算跳转地址。
	LONG_PTR jmpAddr = (LONG_PTR)m_ProxyFun - ((LONG_PTR)m_FinalAddr + 5);
	BYTE jmpCode[5] = {0};
	jmpCode[0] = 0xE9;
	CopyMemory(&jmpCode[1],&jmpAddr,4);

	HANDLE hProcess = OpenProcess_(m_Pid);
	if (0 == hProcess) {
		return FALSE;
	}
	m_IsSuc = WriteProcessMemory_(hProcess, (LPVOID)m_FinalAddr, (LPCVOID)jmpCode, 5, NULL);
	CloseHandle(hProcess);
	return m_IsSuc;
}

BOOL CInlineHook::UnHook() {
	if (!m_IsSuc)
		return true;

	HANDLE hProcess = OpenProcess_(m_Pid);
	if (0 == hProcess) {
		return false;
	}
	BOOL bRet = WriteProcessMemory_(hProcess, (LPVOID)m_FinalAddr, (LPCVOID)m_OrigInsts, m_RealInstsLength, NULL);
	CloseHandle(hProcess);
	if (bRet)
		m_IsSuc = FALSE;
	return bRet;
}

//////////////////////////////////////////////////////////////////////////
// CInlineHookEx

CInlineHookEx::CInlineHookEx(){}

// 构造函数。
CInlineHookEx::CInlineHookEx(__in DWORD dwPID, __in PVOID TargetAddr, __in PVOID ProxyFun, __in DWORD dwOffset /* = 0 */)
	: m_Pid(dwPID), 
	  m_TargetAddr(TargetAddr), 
	  m_Offset(dwOffset), 
	  m_ProxyFun(ProxyFun), 
	  m_RealInstsLength(0), 
	  m_IsSuc(FALSE)
{
	m_FinalAddr = (PVOID)((ULONG_PTR)TargetAddr + dwOffset);
	ZeroMemory(m_OrigInsts, 30);
}

// Hook。
BOOL CInlineHookEx::Hook_() {
	if (m_IsSuc)
		return TRUE;

	BYTE *addressTemp = (BYTE*)m_FinalAddr;
	INSTRUCTION inst = {0};
	while (1)
	{
		if (m_RealInstsLength < 6) {
			get_instruction(&inst, addressTemp, MODE_32);
			m_RealInstsLength += inst.length;
			addressTemp += inst.length;
		} else {
			break;
		}
	}

	// 拷贝指令。
	memcpy(m_OrigInsts, m_FinalAddr, m_RealInstsLength);
	// 计算跳转地址。
	BYTE pushCode[6] = {0};
	pushCode[0] = 0x68;
	CopyMemory(&pushCode[1],&m_ProxyFun,4);
	pushCode[5] = 0xC3;

	HANDLE hProcess = OpenProcess_(m_Pid);
	if (0 == hProcess) {
		return FALSE;
	}
	m_IsSuc = WriteProcessMemory_(hProcess, (LPVOID)m_FinalAddr, (LPCVOID)pushCode, 6, NULL);
	CloseHandle(hProcess);
	return m_IsSuc;
}

BOOL CInlineHookEx::UnHook() {
	if (!m_IsSuc)
		return TRUE;

	HANDLE hProcess = OpenProcess_(m_Pid);
	if (0 == hProcess) {
		return FALSE;
	}
	BOOL bRet = WriteProcessMemory_(hProcess, (LPVOID)m_FinalAddr, (LPCVOID)m_OrigInsts, m_RealInstsLength, NULL);
	CloseHandle(hProcess);
	if (bRet)
		m_IsSuc = FALSE;
	return bRet;
}
