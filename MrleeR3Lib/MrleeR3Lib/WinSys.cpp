#include "stdafx.h"
#include "WinSys.h"

using namespace std;
using namespace Mrlee_R3;
using namespace Mrlee_R3::IO;
using namespace Mrlee_R3::File;

// 检测Tcp端口是否被占用。
bool WinSys::CheckTcpPortState( IN unsigned num )
{
	bool bRet = false;
	PMIB_TCPTABLE_OWNER_PID pTcpTable;
	pTcpTable = new MIB_TCPTABLE_OWNER_PID;

	//获取所需要的内存大小
	DWORD tmpSize = sizeof(MIB_TCPTABLE_OWNER_PID); 
	GetExtendedTcpTable( pTcpTable, &tmpSize,false , AF_INET,  TCP_TABLE_OWNER_PID_ALL, 0);

	//分配足够大小的内存并获取端口信息
	DWORD dwSize = tmpSize/sizeof(MIB_TCPTABLE_OWNER_PID);
	delete pTcpTable;
	pTcpTable = NULL;
	pTcpTable = new MIB_TCPTABLE_OWNER_PID[dwSize];
	GetExtendedTcpTable( pTcpTable, &tmpSize, true, AF_INET,  TCP_TABLE_OWNER_PID_ALL, 0);

	//判断端口是否被占用，并找出占用端口的进程，对于某些system权限的进程需要提权
	for (DWORD i = 0; i < pTcpTable->dwNumEntries; i++) {\
		u_short port = ntohs( (u_short)(pTcpTable->table[i].dwLocalPort) );
		if ( num == port )
		{
			bRet = true;
			break;
// 			HANDLE provileges = NULL;
// 			LUID Luid;
// 			//提权操作
// 			if ( !OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES| TOKEN_QUERY, &provileges) )
// 			{
// 				long res = GetLastError();
// 				cout<<"error code "<<res<<endl;
// 				if (pTcpTable != NULL)
// 				{
// 					delete []pTcpTable;
// 					pTcpTable = NULL;
// 				}
// 				return false;
// 			}
// 
// 			if (!LookupPrivilegeValue(NULL,SE_DEBUG_NAME,&Luid))
// 			{
// 				cout<<"LookupPrivilegeValue err!"<<endl;
// 				if (pTcpTable != NULL)
// 				{
// 					delete []pTcpTable;
// 					pTcpTable = NULL;
// 				}
// 				return false;
// 			}
// 
// 			TOKEN_PRIVILEGES tp;
// 			tp.PrivilegeCount=1;
// 			tp.Privileges[0].Attributes=SE_PRIVILEGE_ENABLED;
// 			tp.Privileges[0].Luid=Luid;
// 
// 			if (!AdjustTokenPrivileges(provileges,0,&tp,sizeof(TOKEN_PRIVILEGES),NULL,NULL))
// 			{
// 				cout<<"AdjustTokenPrivileges err!"<<endl;
// 				if (pTcpTable != NULL)
// 				{
// 					delete []pTcpTable;
// 					pTcpTable = NULL;
// 				}
// 				return false;
// 			}
// 
// 			HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, false, pTcpTable->table[i].dwOwningPid);
// 			if ( hProcess == NULL )
// 			{
// 				long res = GetLastError();
// 				cout<<"error code "<<res<<endl;
// 				if (pTcpTable != NULL)
// 				{
// 					delete []pTcpTable;
// 					pTcpTable = NULL;
// 				}
// 				return false;
// 			}
// 			wchar_t wsProcessName[MAX_PATH + 1] = {0};
// 			DWORD len = MAX_PATH;
// 			//if ( QueryFullProcessImageName(hProcess, 0, wsProcessName, &len) )
// 			if ( GetProcessImageFileName(hProcess, wsProcessName, &len) )
// 			{
// 				wcout<<L"Port["<<num<<L"] is occupied "<<L"by process["<<wsProcessName<<L"] PID["
// 					<<pTcpTable->table[i].dwOwningPid<<L"]"<<endl;
// 				CloseHandle(hProcess);
// 			}
// 			else
// 			{
// 				CloseHandle(hProcess);
// 				hProcess = NULL;
// 				if (pTcpTable != NULL)
// 				{
// 					delete []pTcpTable;
// 					pTcpTable = NULL;
// 				}
// 				return false;
// 			}
		}
	}

	if (pTcpTable != NULL)
	{
		delete []pTcpTable;
		pTcpTable = NULL;
	}

	return bRet;
}

// 检测Udp端口是否被占用。
bool WinSys::CheckUdpPortState( IN unsigned num )
{
	bool bRet = false;
	PMIB_UDPTABLE_OWNER_PID pUdpTable;
	pUdpTable = new MIB_UDPTABLE_OWNER_PID;

	//获取所需要的内存大小
	DWORD tmpSize = sizeof(MIB_UDPTABLE_OWNER_PID); 
	GetExtendedUdpTable( pUdpTable, &tmpSize,false , AF_INET,  UDP_TABLE_OWNER_PID, 0);

	//分配足够大小的内存并获取端口信息
	DWORD dwSize = tmpSize/sizeof(MIB_UDPTABLE_OWNER_PID);
	delete pUdpTable;
	pUdpTable = NULL;
	pUdpTable = new MIB_UDPTABLE_OWNER_PID[dwSize];
	GetExtendedUdpTable( pUdpTable, &tmpSize, true, AF_INET,  UDP_TABLE_OWNER_PID, 0);

// 	HANDLE provileges = NULL;
// 	LUID Luid;
// 	//提权操作
// 	if ( !OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES| TOKEN_QUERY, &provileges) )
// 	{
// 		long res = GetLastError();
// 		cout<<"error code "<<res<<endl;
// 		if (pUdpTable != NULL)
// 		{
// 			delete []pUdpTable;
// 			pUdpTable = NULL;
// 		}
// 		return false;
// 	}
// 
// 	if (!LookupPrivilegeValue(NULL,SE_DEBUG_NAME,&Luid))
// 	{
// 		cout<<"LookupPrivilegeValue err!"<<endl;
// 		if (pUdpTable != NULL)
// 		{
// 			delete []pUdpTable;
// 			pUdpTable = NULL;
// 		}
// 		return false;
// 	}
// 
// 	TOKEN_PRIVILEGES tp;
// 	tp.PrivilegeCount=1;
// 	tp.Privileges[0].Attributes=SE_PRIVILEGE_ENABLED;
// 	tp.Privileges[0].Luid=Luid;
// 
// 	if (!AdjustTokenPrivileges(provileges,0,&tp,sizeof(TOKEN_PRIVILEGES),NULL,NULL))
// 	{
// 		cout<<"AdjustTokenPrivileges err!"<<endl;
// 		if (pUdpTable != NULL)
// 		{
// 			delete []pUdpTable;
// 			pUdpTable = NULL;
// 		}
// 		return false;
// 	}

	//判断端口是否被占用，并找出占用端口的进程，对于某些system权限的进程需要提权
	for (DWORD i = 0; i < pUdpTable->dwNumEntries; i++) {
		u_short port = ntohs( (u_short)(pUdpTable->table[i].dwLocalPort) );
		if ( num == port )
		{
			bRet = true;
			break;
// 			HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, false, pUdpTable->table[i].dwOwningPid);
// 			if ( hProcess == NULL )
// 			{
// 				long res = GetLastError();
// 				cout<<"error code "<<res<<endl;
// 				if (pUdpTable != NULL)
// 				{
// 					delete []pUdpTable;
// 					pUdpTable = NULL;
// 				}
// 				return false;
// 			}
// 			wchar_t wsProcessName[MAX_PATH + 1] = {0};
// 			DWORD len = MAX_PATH;
// 			if ( QueryFullProcessImageName(hProcess, 0, wsProcessName, &len) )
// 			{
// 				wcout<<L"Port["<<num<<L"] is occupied "<<L"by process["<<wsProcessName<<L"] PID["
// 					<<pUdpTable->table[i].dwOwningPid<<L"]"<<endl;
// 				CloseHandle(hProcess);
// 			}
// 			else
// 			{
// 				CloseHandle(hProcess);
// 				hProcess = NULL;
// 				if (pUdpTable != NULL)
// 				{
// 					delete []pUdpTable;
// 					pUdpTable = NULL;
// 				}
// 				return false;
// 			}
		}
	}

	if (pUdpTable != NULL)
	{
		delete []pUdpTable;
		pUdpTable = NULL;
	}

	return bRet;
}

// 拷贝指定窗口中的文本。
void WinSys::CopyText (__in HWND hWnd) {
	::SendMessage (hWnd, EM_SETSEL, 0, -1);	// 全选。
	::SendMessage (hWnd, WM_COPY, 0, 0);	// 拷贝。
	::SendMessage (hWnd, EM_SETSEL, 0, 0);	// 取消全选。
}

// 延时。
void WinSys::Delay(__in LONGLONG delay, __in TimeUnit timeUnit) {
	LARGE_INTEGER int64 = {0};
	HANDLE hTimer;

	switch (timeUnit) {
		// 微秒。
	case TimeUnit_Microseconds:
		int64.QuadPart = MAKE_MICROSECOND * delay;
		break;
		// 毫秒。
	case TimeUnit_Millisecond:
		int64.QuadPart = MAKE_MILLISECOND * delay;
		break;
		// 秒。
	case TimeUnit_Second:
		int64.QuadPart = MAKE_SECOND * delay;
		break;
		// 分钟。
	case TimeUnit_Minute:
		int64.QuadPart = MAKE_MINUTE * delay;
		break;
		// 小时。
	case TimeUnit_Hour:
		int64.QuadPart = MAKE_HOUR * delay;
		break;
		// 天。
	case TimeUnit_Day:
		int64.QuadPart = MAKE_DAY * delay;
		break;
	default:
		return;
	}

	hTimer = CreateWaitableTimer (NULL, FALSE, NULL);
	SetWaitableTimer (hTimer, &int64, 0, NULL, NULL, FALSE);
	while (MsgWaitForMultipleObjects (1, &hTimer, FALSE, INFINITE, QS_ALLINPUT) != WAIT_OBJECT_0) {
		DoEvents();
	}
	CloseHandle (hTimer);

	return;
}

// 模拟VB的DoEvents()函数。
void WinSys::DoEvents() {
	/*MSG msg;
	while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) {
	if (msg.message == WM_QUIT) {
	break;
	}
	GetMessage(&msg, NULL, 0, 0);
	::TranslateMessage(&msg);
	::DispatchMessage(&msg);
	}*/

	MSG msg;
	::Sleep(0);
	while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == TRUE)
	{
		if (AfxGetApp()->PumpMessage() == FALSE)
		{
			AfxPostQuitMessage(0);
			// return FALSE;
			return;
		}
	}

	// let MFC do its idle processing
	LONG lIdle = 0;

	while (AfxGetApp()->OnIdle(lIdle++) == TRUE)
	{
		; // empty
	}

	return;
	//return TRUE;
}

// 资源数据信息。
typedef struct _ResDataInfo {
	PVOID Address;	// 资源数据的地址。
	DWORD Size;		// 资源数据的大小。
} ResItemInfo;

// 获得资源项信息。
void GetResItemInfo(__in const IMAGE_RESOURCE_DIRECTORY *resTable, __in const IMAGE_RESOURCE_DIRECTORY *lpResDir, __in DWORD dwLevel, __in CPeLoader32 &pe, vector<ResItemInfo> &resItemInfos) {
	if (dwLevel < 2) {
		dwLevel = 2;
	}

	//CStringW resName, strTmp;
	IMAGE_RESOURCE_DIRECTORY_ENTRY *resDirEntry = (IMAGE_RESOURCE_DIRECTORY_ENTRY *)((BYTE*)lpResDir + sizeof(IMAGE_RESOURCE_DIRECTORY));	// 获得数据项。
	DWORD dwResNum = lpResDir->NumberOfNamedEntries + lpResDir->NumberOfIdEntries;

	while (dwResNum > 0) {
		if (2 == dwLevel) {			// 二级资源目录。
			DWORD OffsetToDirectory = resDirEntry->OffsetToData & 0x7FFFFFFF;	// 求其他目录项。
			const IMAGE_RESOURCE_DIRECTORY *resDirTemp = (const IMAGE_RESOURCE_DIRECTORY *)((BYTE*)resTable + OffsetToDirectory);
			/*pe.GetResourceTypeName(resDirEntry, dwLevel, resName);
			strTmp.Format(L"|   |-- ID %s\n|   |     |\n", resName.GetBuffer());
			OutputDebugStringW(strTmp.GetBuffer());*/
			GetResItemInfo(resTable, resDirTemp, dwLevel + 1, pe, resItemInfos);
		} else if (3 == dwLevel) {	// 三级数据。
			const IMAGE_RESOURCE_DATA_ENTRY *resDataEntry = (const IMAGE_RESOURCE_DATA_ENTRY *)((BYTE*)resTable + resDirEntry->OffsetToData);
			//pe.GetResourceTypeName(resDirEntry, dwLevel, resName);
			long foa = pe.RVAToFOA(resDataEntry->OffsetToData);
			/*strTmp.Format(L"|   |     |-- 代码页：%s   资源所在位置：FOA-0x%08X,RVA-0x%08X  资源长度：%d  范围(RVA)：[0x%08X-0x%08X]\n", resName.GetBuffer(), pe.RVAToFOA(resDataEntry->OffsetToData), resDataEntry->OffsetToData, resDataEntry->Size, resDataEntry->OffsetToData, resDataEntry->OffsetToData + resDataEntry->Size - 1);
			OutputDebugStringW(strTmp.GetBuffer());*/

			ResItemInfo rii = {0};
			if (-1 == foa) {
				rii.Address = (PVOID)-1;
			} else {
				rii.Address = (BYTE*)pe.GetFilePoint() + (DWORD)foa;
			}
			rii.Size = resDataEntry->Size;
			resItemInfos.push_back(rii);

			//if (resParseFun) {
			//	OutputDebugString(TEXT("\n-----------------------------------------------------------------------------\n"));
			//	// 获得菜单头。
			//	MenuHeader *menuHeader = (MenuHeader*)((BYTE*)pe.GetFilePoint() +  (DWORD)pe.RVAToFOA(resDataEntry->OffsetToData));
			//	// 资源解析。
			//	resParseFun(pe, (PVOID)menuHeader, resDataEntry);
			//	OutputDebugString(TEXT("-----------------------------------------------------------------------------\n"));
			//}
		}
		resDirEntry++;
		dwResNum--;
	}
}

// 提取图标。
BOOL WinSys::ExtractIcon_(__in CString &savePath, __in CString &filePath) {
	CPeLoader32 pe;
	pe.LoadPe(filePath.GetBuffer());

	if (FALSE == pe.IsLoad()) {
		return FALSE;
	}

	// 图标资源输出。
	IMAGE_RESOURCE_DIRECTORY *resDirIcon2 = NULL;
	const IMAGE_RESOURCE_DIRECTORY_ENTRY *resDirEntryOfIcon = pe.GetImageResourceDirectoryEntryOfIcon(&resDirIcon2);
	// 输出第一层目录。
	/*CStringW resName, strTmp;
	pe.GetResourceTypeName(resDirEntryOfIcon, 1, resName);
	strTmp.Format(L"|-- %d-%s,Addr=0x%08X\n|   |\n", resDirEntryOfIcon->Name, resName.GetBuffer(), resDirEntryOfIcon);
	OutputDebugStringW(strTmp.GetBuffer());*/
	vector<ResItemInfo> IconResItemInfos;	// 图标资源项信息。
	GetResItemInfo(pe.GetImageResourceDirectory(), resDirIcon2, 2, pe, IconResItemInfos);

	// 图标组资源输出。
	IMAGE_RESOURCE_DIRECTORY *resDirIconGroup2 = NULL;
	const IMAGE_RESOURCE_DIRECTORY_ENTRY *resDirEntryOfIconGroup = pe.GetImageResourceDirectoryEntryOfIconGroup(&resDirIconGroup2);
	// 输出第一层目录。
	/*pe.GetResourceTypeName(resDirEntryOfIconGroup, 1, resName);
	strTmp.Format(L"|-- %d-%s,Addr=0x%08X\n|   |\n", resDirEntryOfIconGroup->Name, resName.GetBuffer(), resDirEntryOfIconGroup);
	OutputDebugStringW(strTmp.GetBuffer());*/
	vector<ResItemInfo> IconGroupResItemInfos;	// 图标资源项信息。
	GetResItemInfo(pe.GetImageResourceDirectory(), resDirIconGroup2, 2, pe, IconGroupResItemInfos);

	// 提取图标。
	for (size_t i = 0; i < IconGroupResItemInfos.size(); i++)
	{
		// 获得图标目录地址。
		ICON_DIR_HEADER *iconDirHeader = (ICON_DIR_HEADER *)IconGroupResItemInfos[i].Address;
		// 获得Exe文件中的图标目录项。
		vector<ICON_DIR_ENTRY_InExe> icoDirEntryInExes;
		for (int j = 0; j < iconDirHeader->Count; j++)
		{
			// 获得图标目录项信息。
			ICON_DIR_ENTRY_InExe *iconDirEntryInExe = (ICON_DIR_ENTRY_InExe *)((BYTE*)IconGroupResItemInfos[i].Address + sizeof(ICON_DIR_HEADER) + j * sizeof(ICON_DIR_ENTRY_InExe));
			icoDirEntryInExes.push_back(*iconDirEntryInExe);
		}

		// 计算图标文件的大小。
		int iconFileSize = sizeof(ICON_DIR_HEADER) + iconDirHeader->Count * sizeof(ICON_DIR_ENTRY);
		for (int j = 0; j < iconDirHeader->Count; j++)
		{
			iconFileSize += IconResItemInfos[j].Size;
		}

		// 在设置在icon文件中ICON_DIR_ENTRY结构的数据。
		int count = 0;
		vector<ICON_DIR_ENTRY> icoDirEntrys;
		for (size_t j = 0; j < icoDirEntryInExes.size(); j++)
		{
			// 获得图标目录项信息。
			ICON_DIR_ENTRY iconDirEntryTemp = {0};
			iconDirEntryTemp.Reserved = 0;
			CopyMemory(&iconDirEntryTemp, &icoDirEntryInExes[j], sizeof(ICON_DIR_ENTRY_InExe));
			iconDirEntryTemp.ImageOffset = 0;
			// 图标数据在文件中的偏移。
			iconDirEntryTemp.ImageOffset = sizeof(ICON_DIR_HEADER) + iconDirHeader->Count * sizeof(ICON_DIR_ENTRY) + count;
			count += IconResItemInfos[j].Size;	// 累计图标数据。

			icoDirEntrys.push_back(iconDirEntryTemp);
		}

		DWORD bufferOffset = 0;	// 偏移。
		BYTE *buffer = (BYTE *)calloc(iconFileSize, 1);
		CopyMemory(buffer + bufferOffset, iconDirHeader, sizeof(ICON_DIR_HEADER));	// 拷贝图标目录头。
		bufferOffset += sizeof(ICON_DIR_HEADER);
		// 拷贝图标目录项。
		for (size_t j = 0; j < icoDirEntrys.size(); j++)
		{
			CopyMemory(buffer + bufferOffset, &icoDirEntrys[j], sizeof(ICON_DIR_ENTRY));
			bufferOffset += sizeof(ICON_DIR_ENTRY);
		}
		// 拷贝图标数据。
		for (size_t j = 0; j < icoDirEntryInExes.size(); j++)
		{
			int index = icoDirEntryInExes[j].Id - 1;	// 计算索引。经过观察，发现资源ID都是按照顺序进行排列的。
			CopyMemory(buffer + bufferOffset, IconResItemInfos[index].Address, IconResItemInfos[index].Size);
			bufferOffset += IconResItemInfos[index].Size;
		}
		// 写图标文件。
		CFile file;
		CString path;
		path.Format(TEXT("\\MrleeExtIcoX%d.ico"), i);
		path = savePath + path;
		file.Open(path, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary, NULL);
		file.Write(buffer, iconFileSize);
		file.Close();
		delete buffer;
	}

	pe.UnLoadPe();

	return TRUE;
}

// 获得剪贴板文本。
BOOL WinSys::GetClipboardText(__in HWND hWnd, __out CString &text) {
	if ( ::OpenClipboard(hWnd) ) {
		HANDLE hData = ::GetClipboardData(CF_TEXT);
		char * buffer = (char*)::GlobalLock(hData);
		text = buffer;
		::GlobalUnlock(hData);
		::CloseClipboard();

		return TRUE;
	} else {
		TRACE("【错误】WinSys::GetClipboardText() - 打开剪贴板失败。\n");
		return FALSE;
	}
}

// 获得CPU序列号。
CString WinSys::GetCPUID() {
	CString CPUID;

	unsigned long s1,s2;

	unsigned char vendor_id[]="------------";

	char sel;

	sel='1';

	CString VernderID;

	CString MyCpuID,CPUID1,CPUID2;

	switch(sel)

	{

	case '1':

		__asm{

			xor eax,eax      //eax=0:取Vendor信息

				cpuid    //取cpu id指令，可在Ring3级使用

				mov dword ptr vendor_id,ebx

				mov dword ptr vendor_id[+4],edx

				mov dword ptr vendor_id[+8],ecx

		}

		VernderID.Format(TEXT("%s-"),vendor_id);

		__asm{

			mov eax,01h   //eax=1:取CPU序列号

			xor edx,edx

			cpuid

			mov s1,edx

			mov s2,eax

		}

		CPUID1.Format(TEXT("%08X%08X"),s1,s2);

		__asm{

			mov eax,03h

			xor ecx,ecx

			xor edx,edx

			cpuid

			mov s1,edx

			mov s2,ecx

		}

		CPUID2.Format(TEXT("%08X%08X"),s1,s2);

		break;

	case '2':

		{

			__asm{

				mov ecx,119h

				rdmsr

				or eax,00200000h

				wrmsr

			}

		}

		//AfxMessageBox("CPU id is disabled.");

		break;

	}

	MyCpuID = CPUID1+CPUID2;

	CPUID = MyCpuID;

	return CPUID;
}

// 获得当前桌面路径。
CString WinSys::GetDesktopPath() {
	CString DesktopPath;
	GetDesktopPath(DesktopPath);
	return DesktopPath;
}

// 获得当前桌面路径。
void WinSys::GetDesktopPath(__out CString &DesktopPath) {
	TCHAR MyDir[_MAX_PATH] = {TEXT('\0')};
	SHGetSpecialFolderPath(NULL,DesktopPath.GetBuffer(MAX_PATH),CSIDL_DESKTOP,0);
	DesktopPath.ReleaseBuffer();
}

// 计算函数大小。
DWORD WinSys::GetFuncSize (__in PVOID pFunc, __in_opt DWORD dwMax /* = 0x1000 */) {
	DWORD i = 0;
	BYTE *pc = NULL;

	if ( NULL == pFunc )
		return 0;

	// 获得函数实际地址。
	pc = (BYTE *)GetJmpTableFuncAddr(pFunc);

	// 防止逻辑写的有问题而造成死循环。
	while ( i <= dwMax ) {
		i++;
		//函数返回指令(ret = 0xC3 )
		if ( 0xC3 == pc[ i ] ) {
			i++;
			break;
		}
		//函数返回指令(ret xx = 0xC2 )
		if ( 0xC2 == pc[ i ] ) {
			i += 2;
			i++;
			break;
		}
	}

	if ( 0 == i ) {
		SetLastError(MRLEE_ERROR_CALC_FUN_SIZE_FAILURE);
	}
	return i;
}

// 获得跳转表的实际函数地址。
PVOID WinSys::GetJmpTableFuncAddr (__in PVOID pFunc) {
	BYTE *pc = NULL;

	if ( NULL == pFunc )
		return 0;


	pc = ( BYTE *)pFunc;

	// 函数跳转指令( jmp = 0xE9 )
	// 之所以判断函数起始是否为jmp，这是为了判断这个函数地址是否是跳转表。
	if ( 0xE9 == pc[ 0 ] )
	{
		//获取真实的函数地址
		ULONG_PTR numTemp;
		INSTRUCTION inst = {0};
		get_instruction(&inst, pc, MODE_32);	// 获得指令长度。
		// 两个字节长的指令是短跳转。
		if ( 2 == inst.length ) {
			numTemp = pc[1];
		} else {	// 长跳转。
			numTemp = *((ULONG_PTR *)(pc + 1));
		}

		// 计算出函数实际地址。
		pc = (BYTE *)( ( pc + inst.length ) + numTemp );
	}

	return pc;
}

// 当前操作系统是否是64位。
BOOL WinSys::Is64bitSystem()
{
	SYSTEM_INFO si;
	GetNativeSystemInfo(&si);

	if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 || 
		si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64 )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	} 
}

// Windows注销。
void WinSys::LogOff(__in bool isForced) {
	ExitWindowsEx(isForced ? (EWX_LOGOFF | EWX_FORCE) : EWX_LOGOFF, 0);
}

// 根据错误信息码，获得错误信息。
CString WinSys::Mrlee_ErrorMessage (__in DWORD dwLastError) {
	CString errorMessage;
	Mrlee_ErrorMessage(dwLastError, errorMessage);
	return errorMessage;
}

// 根据错误信息码，获得错误信息。
void WinSys::Mrlee_ErrorMessage (__in DWORD dwLastError, __out CString &message) {
	if ( IsMrleeError(dwLastError) ) {
		switch ( dwLastError )
		{
		case MRLEE_ERROR_CALL_REPEATED:
			message = TEXT("重复调用。");
			return;

		case MRLEE_ERROR_CALL_NOT_UNLOAD:
			message = TEXT("调用未卸载。");
			return;

		case MRLEE_ERROR_IS_DIRECTORY:
			message = TEXT("路径是目录。");
			return;

		case MRLEE_ERROR_PROCESS_NOT_FOUND:
			message = TEXT("进程未找到。");
			return;
			
		case MRLEE_ERROR_CHARACTER_NOT_FOUND:
			message = TEXT("字符未找到。");
			return;

		case MRLEE_ERROR_MEMORY_ALLOTTED_FAILURE:
			message = TEXT("内存分配失败。");
			return;

		case MRLEE_ERROR_REMOTE_CALL_FAILURE:
			message = TEXT("远程调用失败。");
			return;

		case MRLEE_ERROR_CALC_FUN_SIZE_FAILURE:
			message = TEXT("计算函数大小失败。");
			return;
		}
	} else {
		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dwLastError,
			0, // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
			);
		message.SetString((LPCTSTR)lpMsgBuf);
		LocalFree(lpMsgBuf);
	}
}

// 根据错误信息码，获得错误信息。
CString WinSys::Mrlee_ErrorMessage_MY (__in DWORD dwLastError) {
	switch ( dwLastError )
	{
	case ERROR_INVALID_WINDOW_HANDLE:
		return TEXT("未找到窗口。");

	case ERROR_PROC_NOT_FOUND:
		return TEXT("找不到指定的函数。");

	default:
		return Mrlee_ErrorMessage(dwLastError);
	}
}

// 根据错误信息码，获得错误信息。
void WinSys::Mrlee_ErrorMessage_MY(__in DWORD dwLastError, __out CString &message) {
	switch ( dwLastError )
	{
	case ERROR_INVALID_HANDLE:
		message = TEXT("未找到窗口。");
		return;

	default:
		Mrlee_ErrorMessage(dwLastError, message);
		return;
	}
}

// 将文本粘贴到指定窗口中。
void WinSys::PasteText (__in HWND hWnd) {
	::SendMessage (hWnd, WM_PASTE, 0, 0);	// 拷贝。
}

// 投递文本。
void WinSys::PostText (__in HWND hWnd, __in TCHAR *text) {
	//size_t length = _tcslen(text) * sizeof(TCHAR);
	size_t length = _tcslen(text);
	for (size_t i = 0; i < length; i++)
	{
		::PostMessage(hWnd, WM_CHAR, text[i], 0);
	}
}

// 电脑重启。
void WinSys::Reboot(__in bool isForced) {
	SuperPrivileges_EnablePrivilege(SE_SHUTDOWN_NAME, TRUE);
	// EWX_REBOOT | EWX_FORCE是强制重启。
	ExitWindowsEx(isForced ? (EWX_REBOOT | EWX_FORCE) : EWX_REBOOT, 0);
}

// 替换图标。
BOOL WinSys::ReplaceIcon(__in TCHAR *iconFilePath, __in TCHAR *filePath) {
	if ( (FALSE == IsFile(iconFilePath)) || (FALSE == IsFile(filePath)) ) {
		OutputDebugString(TEXT("【错误】WinSys::ReplaceIcon() - 图标路径或文件路径不存在。\n"));;
		return FALSE;
	}

	// 打开icon文件。
	HANDLE hFile = ::CreateFile(iconFilePath, GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hFile) {
		OutputDebugString(TEXT("【错误】WinSys::ReplaceIcon() - 打开icon文件失败。\n"));;
		return FALSE;
	}

	// 读取图标目录。
	ICON_DIR_HEADER iconDir = {0};
	DWORD dwReturn = 0;
	::ReadFile(hFile, &iconDir, sizeof(ICON_DIR_HEADER), &dwReturn, NULL);

	// 读取图标目录项。
	ICON_DIR_ENTRY iconDirEntry = {0};
	::ReadFile(hFile, &iconDirEntry, sizeof(ICON_DIR_ENTRY), &dwReturn, NULL);

	// 设置文件指针，读取图标数据。
	BYTE *iconBuffer = (BYTE*)calloc(iconDirEntry.BytesInRes, 1);		// 分配内存，大小为图标数据的大小。
	::SetFilePointer(hFile, iconDirEntry.ImageOffset, NULL, FILE_BEGIN);	// 设置文件指针指向图标数据起始位置。
	if (FALSE == ReadFile(hFile, iconBuffer, iconDirEntry.BytesInRes, &dwReturn, NULL)) {
		free(iconBuffer);
		CloseHandle(hFile);
		OutputDebugString(TEXT("【错误】WinSys::ReplaceIcon() - 读取图标数据失败。\n"));
		return FALSE;
	}

	BOOL bRet = FALSE;
	// 生成ICON_DIR_ENTRY_InExe数据。
	ICON_DIR_ENTRY_InExe iconDirEntryInExe = {0};
	MoveMemory(&iconDirEntryInExe, &iconDirEntry, sizeof(ICON_DIR_ENTRY_InExe));
	iconDirEntryInExe.Id = 0;
	// 生成图标组数据。
	int groupSize = sizeof(ICON_DIR_HEADER) + sizeof(ICON_DIR_ENTRY_InExe);	// 图标组数据大小。
	BYTE *iconGroupBuffer = (BYTE*)calloc(groupSize, 1);		// 分配内存，大小为图标组数据的大小。
	MoveMemory(iconGroupBuffer, &iconDir, sizeof(ICON_DIR_HEADER));
	MoveMemory(iconGroupBuffer + sizeof(ICON_DIR_HEADER), &iconDirEntryInExe, sizeof(ICON_DIR_ENTRY_InExe));

	HANDLE hUpdate = BeginUpdateResource(filePath, FALSE);
	UpdateResource(hUpdate, RT_GROUP_ICON, MAKEINTRESOURCE(1), LANG_CHINESE, iconGroupBuffer, groupSize);	// 更新图标组数据。
	UpdateResource(hUpdate, RT_ICON, MAKEINTRESOURCE(1), LANG_CHINESE, iconBuffer, iconDirEntry.BytesInRes);	// 更新图标数据。
	// 结束更新数据。
	if (FALSE == EndUpdateResource(hUpdate, FALSE)) {
		OutputDebugString(TEXT("【错误】WinSys::ReplaceIcon() - EndUpdateResource()调用失败。\n"));
		goto _ret;
	}

	bRet = TRUE;

_ret:
	free(iconBuffer);
	free(iconGroupBuffer);
	CloseHandle(hFile);
	return TRUE;
}

// 替换文本。
void WinSys::ReplaceText_ (__in HWND hWnd, __in TCHAR *text) {
	::SendMessage(hWnd, EM_SETSEL, 0, -1);	// 全选。
	::SendMessage(hWnd, EM_REPLACESEL, TRUE, (LPARAM)text);	// 发送文本。
}

// 发送文本。
void WinSys::SendText(__in HWND hWnd, __in TCHAR *text) {
	// 第三个参数传入TRUE，表明发送的文本是可撤销的。如果传入FALSE，表明传入的文本不可撤销。
	::SendMessage(hWnd, EM_REPLACESEL, TRUE, (LPARAM)text);	// 发送文本。
}

// 文本全选。
void WinSys::SelectText (__in HWND hWnd) {
	::SendMessage (hWnd, EM_SETSEL, 0, -1);
}

// 设置剪贴板文本。
BOOL WinSys::SetClipboardText(__in HWND hWnd, __in char *text) {
	//打开剪贴板
	if ( !::OpenClipboard(hWnd) )
		return FALSE;
	//empties the clipboard and frees handles to data in the clipboard
	if ( !EmptyClipboard() )
	{
		CloseClipboard();
		return FALSE;
	}
	//get text length
	size_t len=strlen(text);
	//After SetClipboardData is called, the system owns the object identified by the hMem parameter. 
	//The application can read the data, but must not free the handle or leave it locked. If the 
	//hMem parameter identifies a memory object, the object must have been allocated using the 
	//GlobalAlloc function with the GMEM_MOVEABLE and GMEM_DDESHARE flags. 
	HANDLE hClip=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,len+1);
	if (hClip==NULL)
	{
		CloseClipboard();
		return FALSE;
	}
	//locks a global memory object and returns a pointer to the first byte of the object's memory block
	char* pBuf=(char*)GlobalLock(hClip);
	if (pBuf==NULL)
	{
		GlobalFree(hClip);  
		CloseClipboard();
		return FALSE;
	}

	memcpy(pBuf,text,len);
	pBuf[len]=NULL;

	GlobalUnlock(hClip);
	if (NULL==SetClipboardData(CF_TEXT,hClip))
	{
		GlobalFree(hClip);  
		CloseClipboard();
		return FALSE;
	}

	CloseClipboard();
	return TRUE;
}

// 设置剪贴板文本。
BOOL WinSys::SetClipboardText(__in HWND hWnd, __in wchar_t *text) {
	char *tmp = NULL;
	BOOL bRet;
	StringHelper::WCharToChar(text, &tmp);
	bRet = SetClipboardText(hWnd, tmp);
	delete tmp;
	return bRet;
}

// 设置开机启动。
BOOL WinSys::SetToRunOnStartup(__in CString& strAppPath, __in CString& strAppName)
{
	if (0 == strAppPath.GetLength() || 0  == strAppName.GetLength())
	{
		throw "Win32.h - SetToRunOnStartup - 应用程序名或应用程序路径为空。";
	}
	CString regPath = TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
	// 写入注册表，设置开机启动。
	CRegistry::SetRegistryValue(
		HKEY_CURRENT_USER, regPath, 
		strAppPath, REG_SZ, (const BYTE*)(strAppPath.GetBuffer()), strAppPath.GetLength());
	return true;
}

// 通过GetLastError的返回值，获得错误信息。
void WinSys::ShowErrorInfo(IN DWORD errorCode, OUT LPTSTR lpInfo, IN int length) {
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&lpMsgBuf, 0, NULL);
	StringCbPrintf(lpInfo, length, TEXT("%s"), lpMsgBuf);
	LocalFree(lpMsgBuf);
}

// 关机。
void WinSys::Shutdown(__in bool isForced) {
	SuperPrivileges_EnablePrivilege(SE_SHUTDOWN_NAME, TRUE);
	// EWX_SHUTDOWN | EWX_FORCE是强制关机。
	ExitWindowsEx(isForced ? (EWX_SHUTDOWN | EWX_FORCE) : EWX_SHUTDOWN, 0);
}

// 获得鼠标点处的窗口标题。
BOOL WinSys::WindowCaptionFromPoint (__out CString &windowCaption) {
	POINT mousePoint = {0};
	TCHAR text[MAX_PATH] = {TEXT('\0')};
	if (FALSE == ::GetCursorPos(&mousePoint)) {
		TRACE ("【错误】WinSys::WindowCaptionFromPoint() - 获得鼠标位置失败。\n");
		return FALSE;
	}

	HWND hWnd = ::WindowFromPoint(mousePoint);
	if (NULL == hWnd) {
		TRACE ("【错误】WinSys::WindowCaptionFromPoint() - 在当前鼠标点处没有窗口。\n");
		return FALSE;
	}
	::SendMessage (hWnd, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)text);
	windowCaption.SetString(text);
	return TRUE;
}

void 鬼叫() {
	::Beep(1999, 1000);
}

//////////////////////////////////////////////////////////////////////////
// 提权函数。

// 启用/禁用特权。
BOOL SuperPrivileges_EnablePrivilege(__in LPTSTR lpszPrivilegeName, __in BOOL bEnable)
{
	HANDLE				hToken;
	TOKEN_PRIVILEGES	tp;
	LUID				luid;
	BOOL				ret;
	DWORD				dwLastError;

	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY | TOKEN_READ,
		&hToken))
		return FALSE;

	if (!LookupPrivilegeValue(NULL, lpszPrivilegeName, &luid))
	{
		dwLastError = GetLastError();
		CloseHandle(hToken);
		SetLastError(dwLastError);
		return FALSE;
	}
	tp.PrivilegeCount           = 1;
	tp.Privileges[0].Luid       = luid;
	tp.Privileges[0].Attributes = bEnable ? SE_PRIVILEGE_ENABLED : 0;

	ret = AdjustTokenPrivileges(hToken, FALSE, &tp, 0, NULL, NULL);
	if ( FALSE == ret ) {
		dwLastError = GetLastError();
		CloseHandle(hToken);
		SetLastError(dwLastError);
	} else {
		CloseHandle(hToken);
	}

	return ret;
}

// 提高进程权限。若不提高进程权限，在调用OpenProcess()函数时可能NULL。
BOOL SuperPrivileges_EnableDebugPrivilege(__in BOOL bEnable) {
	return SuperPrivileges_EnablePrivilege(SE_DEBUG_NAME, bEnable); 
}

// 设置映射路径。
BOOL SuperPrivileges_SetImagePath(__in HANDLE hProcess, __in UNICODE_STRING &szImagePath)
{
	PEB_* pPeb = FS_GetPEBPtr(hProcess);	// 获得PEB指针。

	if (pPeb) {
		//////////////////////////////////////////////////////////////////////////
		//ULONG_PTR addrArray[10] = {0};
		//addrArray[0] = (ULONG_PTR)pPeb + FIELD_OFFSET(PEB, ProcessParameters);	// 读取PPEB->ProcessParameters处的数据。
		//addrArray[1] = FIELD_OFFSET(RTL_USER_PROCESS_PARAMETERS_, ImagePathName) + FIELD_OFFSET(UNICODE_STRING, Buffer);// 读取[PPEB->ProcessParameters].ImagePathName.Buffer处的数据。
		//addrArray[2] = 0;	// 读取[[PPEB->ProcessParameters].ImagePathName.Buffer]处的数据。
		//wchar_t imagePath[MAX_PATH] = {L'\0'};
		//ReadProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 3, imagePath, sizeof(imagePath));
		//OutputDebugStringW(imagePath);

		ULONG_PTR addrArray1[10] = {0};
		addrArray1[0] = (ULONG_PTR)pPeb + FIELD_OFFSET(PEB_, ProcessParameters);	// 读取PPEB->ProcessParameters处的数据。
		addrArray1[1] = FIELD_OFFSET(RTL_USER_PROCESS_PARAMETERS_, ImagePathName);

		LPVOID lpBuffer = VirtualAllocEx_(hProcess, szImagePath.MaximumLength);
		WriteProcessMemory_(hProcess, lpBuffer, szImagePath.Buffer, szImagePath.MaximumLength, NULL);	// 将新映射路径写入目标进程。
		szImagePath.Buffer = (PWSTR)lpBuffer;
		WriteProcessMemory_MulLevelPtrBytes(hProcess, addrArray1, 2, (LPCVOID)&szImagePath, sizeof(UNICODE_STRING));
		//printf("lpBuffer=0x%08X\n", lpBuffer);
		//printf("ImagePathName Addr: 0x%08X\nszImagePath.Buffer: 0x%08X\n", &pPeb->ProcessParameters->ImagePathName, szImagePath.Buffer);
		return TRUE;
	} else {
		return FALSE;
	}
}

// 设置映射路径。
BOOL SuperPrivileges_SetImagePath(__in DWORD dwPID, __in UNICODE_STRING &szImagePath) {
	HANDLE hProcess = OpenProcess_(dwPID);
	if (hProcess) {
		BOOL bRet = SuperPrivileges_SetImagePath(hProcess, szImagePath);
		CloseHandle(hProcess);
		return bRet;
	} else {
		return FALSE;
	}
}

// 设置当前进程的映射路径。
BOOL SuperPrivileges_SetImagePath_CurProc(__in UNICODE_STRING &szImagePath) {
	PEB_* pPeb = FS_GetPEBPtr(GetCurrentProcess());	// 获得PEB指针。
	if (pPeb) {
		//printf("ImagePathName Addr: 0x%08X\nszImagePath.Buffer: 0x%08X\n", &pPeb->ProcessParameters->ImagePathName, szImagePath.Buffer);
		pPeb->ProcessParameters->ImagePathName.Buffer = szImagePath.Buffer;
		pPeb->ProcessParameters->ImagePathName.Length = szImagePath.Length;
		pPeb->ProcessParameters->ImagePathName.MaximumLength = szImagePath.MaximumLength;
		return TRUE;
	} else {
		return FALSE;
	}
}
