// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 头中排除极少使用的资料
#endif

// 如果您必须使用下列所指定的平台之前的平台，则修改下面的定义。
// 有关不同平台的相应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用特定于 Windows XP 或更高版本的功能。
#define WINVER 0x0501		// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif

#ifndef _WIN32_WINNT		// 允许使用特定于 Windows XP 或更高版本的功能。
#define _WIN32_WINNT 0x0501	// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用特定于 Windows 98 或更高版本的功能。
#define _WIN32_WINDOWS 0x0410 // 将它更改为适合 Windows Me 或更高版本的相应值。
#endif

#ifndef _WIN32_IE			// 允许使用特定于 IE 6.0 或更高版本的功能。
#define _WIN32_IE 0x0600	// 将此值更改为相应的值，以适用于 IE 的其他版本。值。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常可放心忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展





#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT


#define _MRLEE_EXT_CLASS


//////////////////////////////////////////////////////////////////////////
// 系统头文件。
#include <afx.h>
#include <afxwin.h>
#include <afxtempl.h>
#include <cmath>
#include <process.h>
#include <psapi.h>
#include <Sensapi.h>
#include <shlobj.h>
#include <shlwapi.h>
#include <string>
#include <strsafe.h>
#include <tlhelp32.h>
#include <Wininet.h>
#include <WinSock2.h>
#include <WinSvc.h>
#include <winbase.h>
#include <IPHlpApi.h>
#include <tcpmib.h>

#include <iostream>
// 容器。
#include <vector>
#include <map>

//#include <XTToolkitPro.h>    // Xtreme Toolkit Pro components

#pragma comment(lib,"Psapi.lib")
#pragma comment(lib,"Shlwapi.lib")
#pragma comment(lib, "version.lib")
#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "Sensapi.lib")
#pragma comment(lib, "Iphlpapi.lib")

//////////////////////////////////////////////////////////////////////////
// 
#include "tinyxml2.h"
#include "libdasm.h"
#include "commondef.h"
#include "ObjectBase.h"
#include "MrleeError.h"
#include "WinAPIForInject.h"
#include "CWindowBase.h"
#include "Number.h"
#include "StringHelper.h"	// 字符串帮助类。

// Pe。
#include "PE.h"
#include "PeLoaderBase.h"
#include "PeLoader32.h"

#include "WinSys.h"
#include "FileHelper.h"
#include "ProcessHelper.h"
#include "MemoryHelper.h"
#include "Registry.h"
#include "Keyboard.h"
#include "Network.h"
#include "Driver.h"

// 注入。
#include "IInject.h"

//////////////////////////////////////////////////////////////////////////