#include "stdafx.h"
#include "MemoryHelper.h"


// 在进程空间中分配内存，此函数分配的内存拥有完全权限并且分配了实际的物理内存。
LPVOID VirtualAllocEx_(__in HANDLE hProcess, __in DWORD dwSize)
{
	return ::VirtualAllocEx(hProcess, NULL, dwSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
}

// 释放在堆中的内存。按 VirtualAllocEx 审请时的大小全部释放。
BOOL VirtualAllocEx_(__in HANDLE hProcess, __in LPVOID lpAddress)
{
	return ::VirtualFreeEx(hProcess, lpAddress, 0, MEM_RELEASE);
}