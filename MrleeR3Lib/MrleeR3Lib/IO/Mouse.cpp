#include "stdafx.h"
#include "Mouse.h"


using namespace Mrlee_R3;
using namespace Mrlee_R3::IO;

// 鼠标移动。
void CMouse::MouseMove(__in LONG dx, __in LONG dy, __in bool isAbsoluteMove) {
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = MOUSEEVENTF_MOVE;	// 鼠标移动。
	if (isAbsoluteMove) {
		input.mi.dwFlags |= MOUSEEVENTF_ABSOLUTE;	// 绝对坐标。

		int cx_screen = ::GetSystemMetrics(SM_CXSCREEN);  //屏幕 宽
		int cy_screen = ::GetSystemMetrics(SM_CYSCREEN);  //     高

		input.mi.dx = 65535 * dx / cx_screen;	//转换后的 x
		input.mi.dy = 65535 * dy / cy_screen;	//         y
	} else {
		input.mi.dx = dx;
		input.mi.dy = dy;
	}
	input.mi.time = ::GetTickCount();

	SendInput(1, &input, sizeof(INPUT));
}

// 鼠标移动。
BOOL CMouse::MouseMove(__in HWND hWnd, __in LONG dx, __in LONG dy) {
	POINT point = {0};
	BOOL bRet;

	if (NULL != hWnd) {
		bRet = ::ClientToScreen(hWnd, &point);
		if (FALSE == bRet) {
			TRACE("【错误】CMouse::MouseMove(HWND, LONG, LONG) - ClientToScreen()执行失败。\n");
			return FALSE;
		}
	}

	return SetCursorPos(point.x + dx, point.y + dy);
}

// 鼠标左键弹起。
void CMouse::LeftButtonUp() {
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = MOUSEEVENTF_LEFTUP;	// 左键按下。
	input.mi.time = ::GetTickCount();

	SendInput(1, &input, sizeof(INPUT));
}

// 鼠标左键按下。
void CMouse::LeftButtonDown() {
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;	// 左键按下。
	input.mi.time = ::GetTickCount();

	SendInput(1, &input, sizeof(INPUT));
}

// 鼠标左键单击。
void CMouse::LeftButtonClick(__in LONG dx, __in LONG dy, __in bool isAbsoluteMove) {
	MouseMove(dx, dy, isAbsoluteMove);
	LeftButtonDown();
	LeftButtonUp();
}

// 鼠标捕获。
void CMouse::MouseCapture(__in HWND hWnd) {
	DWORD ThreadId;
	ThreadId = GetWindowThreadProcessId(hWnd, NULL);
	AttachThreadInput(GetCurrentThreadId(), ThreadId, TRUE);
	if (hWnd) {
		SetCapture(hWnd);
	} else {
		ReleaseCapture();
	}
	AttachThreadInput(GetCurrentThreadId(), ThreadId, FALSE);
}

// 鼠标活动范围信息。
typedef struct _MouseRangeInfo {
	RECT range;				// 范围。
	HANDLE hRestoreEvent;	// 复原事件。
} MouseRangeInfo;

// 鼠标范围线程。
unsigned __stdcall MouseRangeThread (void * lpVoid) {
	MouseRangeInfo *mri = (MouseRangeInfo *)lpVoid;
	while (1) {
		ClipCursor(&(mri->range));	// 限制鼠标范围。

		if (WAIT_OBJECT_0 == WaitForSingleObject(mri->hRestoreEvent, 50)) {
			ClipCursor(NULL);
			delete mri;
			break;
		}
	}
	
	return 1;
}


// 限制鼠标活动范围
HANDLE CMouse::限制鼠标活动范围 (__in LONG left, __in LONG top, __in LONG right, __in LONG bottom) {
	MouseRangeInfo *mri = new MouseRangeInfo;
	mri->range.left = left;
	mri->range.top = top;
	mri->range.right = right;
	mri->range.bottom = bottom;
	mri->hRestoreEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	unsigned  uiThread2ID;
	_beginthreadex(NULL, 0, MouseRangeThread, (void*)mri, 0, &uiThread2ID);

	return mri->hRestoreEvent;
}

// 恢复鼠标活动范围。
void CMouse::恢复鼠标活动范围 (__in HANDLE hRestoreEvent) {
	SetEvent(hRestoreEvent);
}

// 获得鼠标位置。
BOOL CMouse::鼠标位置相对于窗口顶点的坐标(__in HWND hWnd, __out POINT &point) {
	POINT p = {0};	// 初始值必须设置为0。否则ClientToScreen将转换错误。
	POINT mousePos;
	BOOL bRet;
	if (NULL != hWnd) {
		bRet = ClientToScreen(hWnd, &p);
		if (FALSE == bRet) {
			TRACE("【错误】CMouse::鼠标位置相对于窗口顶点的坐标() - 获得窗口顶点相对于屏幕的坐标失败。\n");
			return FALSE;
		}
	}

	bRet = GetCursorPos (&mousePos);	// 获得鼠标位置。
	if (FALSE == bRet) {
		TRACE("【错误】CMouse::鼠标位置相对于窗口顶点的坐标() - 获得鼠标位置失败。\n");
		return FALSE;
	}

	point.x = mousePos.x - p.x;
	point.y = mousePos.y - p.y;

	return TRUE;
}
