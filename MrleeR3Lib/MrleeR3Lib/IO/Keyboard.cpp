#include "stdafx.h"
#include "Keyboard.h"

using namespace Mrlee_R3;
using namespace Mrlee_R3::IO;

#define VKKEYSCANSHIFTON	0x01
#define VKKEYSCANCTRLON		0x02
#define VKKEYSCANALTON		0x04

const UINT_PTR CKeyboard::ExtendedVKeys[MaxExtendedVKeys] =
{
	VK_UP, 
	VK_DOWN,
	VK_LEFT,
	VK_RIGHT,
	VK_HOME,
	VK_END,
	VK_PRIOR, // PgUp
	VK_NEXT,  //  PgDn
	VK_INSERT,
	VK_DELETE
};

// 检查虚拟键值是否是一个扩展键。
bool CKeyboard::IsVkExtended(__in UINT_PTR VKey) {
	for (int i=0;i<MaxExtendedVKeys;i++)
	{
		if (ExtendedVKeys[i] == VKey)
			return true;
	}
	return false;
}

// 发送按键。
void CKeyboard::SendKey (__in BYTE bVk, __in_opt LONGLONG delay /* = 0 */) {
	if (VK_NUMLOCK == bVk) {
		模拟按键_SendInput(bVk, CKeyboardFlag_KeyDown);
		模拟按键_SendInput(bVk, CKeyboardFlag_KeyUp);
		return;
	}

	WORD MKey = ::VkKeyScan(bVk);
	BYTE Hi = HIBYTE(MKey);
	bool isShiftKeyUp = false;
	// A~Z
	if (bVk >= 0x41 && bVk <= 0x5A) {
		// CapsLock 是否亮灯。如果CapsLock灯未亮，则按下shift键。
		if (! ::GetKeyState(VK_CAPITAL)&0x01) {
			模拟按键_SendInput(VK_SHIFT, CKeyboardFlag_KeyDown);
			isShiftKeyUp = true;
		}
	} else if (bVk >= 0x61 && bVk <= 0x7A) {	// a~z
		// CapsLock 是否亮灯。如果CapsLock灯未亮，则按下shift键。
		if (::GetKeyState(VK_CAPITAL)&0x01) {
			模拟按键_SendInput(VK_SHIFT, CKeyboardFlag_KeyDown);
			isShiftKeyUp = true;
		}
	} else {
		// 判断虚拟键是否需要配合shift键。
		if (BitSet(Hi, VKKEYSCANSHIFTON))
			模拟按键_SendInput(VK_SHIFT, CKeyboardFlag_KeyDown);
	}

	if (BitSet(Hi, VKKEYSCANCTRLON))
		模拟按键_SendInput(VK_CONTROL, CKeyboardFlag_KeyDown);

	if (BitSet(Hi, VKKEYSCANALTON))
		模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyDown);

	模拟按键_SendInput(LOBYTE(MKey), CKeyboardFlag_KeyDelay, delay);

	// toggle up shift keys
	if (isShiftKeyUp) {
		模拟按键_SendInput(VK_SHIFT, CKeyboardFlag_KeyUp);
	} else {
		// 判断虚拟键是否需要配合shift键。
		if (BitSet(Hi, VKKEYSCANSHIFTON))
			模拟按键_SendInput(VK_SHIFT, CKeyboardFlag_KeyUp);
	}

	if (BitSet(Hi, VKKEYSCANCTRLON))
		模拟按键_SendInput(VK_CONTROL, CKeyboardFlag_KeyUp);

	if (BitSet(Hi, VKKEYSCANALTON))
		模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyUp);
}

// 弹起键。
void CKeyboard::SendKeyUp (__in BYTE bVk) {
	模拟按键_SendInput(bVk, CKeyboardFlag_KeyUp);
}

// 按下键。
void CKeyboard::SendKeyDown (__in BYTE bVk) {
	模拟按键_SendInput(bVk, CKeyboardFlag_KeyDown);
}

// 模拟按键。
//void CKeyboard::模拟按键_KeyEvent(__in BYTE bVk, __in CKeyboardFlag keyboardFlag, __in_opt LONGLONG delay /* = 0 */) {
//	// 判断键盘标记。
//	switch(keyboardFlag)
//	{
//	case CKeyboardFlag_KeyDown:
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0, 0);		// 按键按下。
//		break;
//	case CKeyboardFlag_KeyUp:
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP, 0);	// 按键弹起。
//		break;
//	case CKeyboardFlag_KeyDownUp:
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0, 0);		// 按键按下。
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP, 0);	// 按键弹起。
//		break;
//	case CKeyboardFlag_KeyDelay:
//		WinSys::Delay(delay, TimeUnit_Millisecond);
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0, 0);		// 按键按下。
//		keybd_event(bVk, MapVirtualKey(bVk, MAPVK_VK_TO_VSC), (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP, 0);	// 按键弹起。
//		break;
//	default:
//		break;
//	}
//}

// 模拟按键。
void CKeyboard::模拟按键_SendInput(__in BYTE bVk, __in CKeyboardFlag keyboardFlag, __in_opt LONGLONG delay /* = 0 */) {
	INPUT input = {0};
	
	input.type = INPUT_KEYBOARD;	// 输入类型为键盘方式。
	input.ki.wVk = bVk;				// 虚拟键值。
	input.ki.wScan = MapVirtualKey(bVk, MAPVK_VK_TO_VSC);	// 将虚拟键值转换为扫描码。
	input.ki.dwExtraInfo = 0;		// 扩展信息为0。

	// 判断键盘标记。
	switch(keyboardFlag)
	{
	case CKeyboardFlag_KeyDown:
		input.ki.dwFlags = IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0;
		input.ki.time = GetTickCount();		// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键按下。
		break;
	case CKeyboardFlag_KeyUp:
		input.ki.dwFlags = (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP;
		input.ki.time = GetTickCount();		// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键弹起。
		break;
	case CKeyboardFlag_KeyDownUp:
		input.ki.dwFlags = IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0;
		input.ki.time = GetTickCount();		// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键按下。
		input.ki.dwFlags = (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP;
		input.ki.time = GetTickCount();	// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键按下。
		break;
	case CKeyboardFlag_KeyDelay:
		WinSys::Delay(delay, TimeUnit_Millisecond);
		input.ki.dwFlags = IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0;
		input.ki.time = GetTickCount();		// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键按下。
		//WinSys::Delay(delay, TimeUnit_Millisecond);
		input.ki.dwFlags = (IsVkExtended(bVk) ? KEYEVENTF_EXTENDEDKEY : 0) | KEYEVENTF_KEYUP;
		input.ki.time = GetTickCount();	// 获得从操作系统启动到现在所经过（elapsed）的毫秒数，它的返回值是DWORD。
		SendInput(1, &input, sizeof(INPUT));// 按键按下。
		break;
	default:
		break;
	}
}

typedef struct _KeyboardMessage {
	int keyboardFlag;	// 按键状态。
	UINT_PTR virtualKey;// 虚拟键值。
	LPARAM lParam;		// 如果不是扩展键，则赋值为0。是扩展键，则赋值为1。它对应了PostMessage或PostMessage函数的LPARAM类型的参数。
} KeyboardMessage, *PKeyboardMessage;

// 键盘消息函数。
BOOL CALLBACK KeyboardMessageProc (HWND hWnd, LPARAM lParam) {
	KeyboardMessage *pKeyboardMessage = (KeyboardMessage *)lParam;
	// 窗口句柄不能为NULL。
	if (hWnd) {
		switch (pKeyboardMessage->keyboardFlag) {
			case CKeyboardFlag_KeyChar:
				::PostMessage(hWnd, WM_CHAR, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				break;
			case CKeyboardFlag_SYSKeyDownUp:
				::PostMessage(hWnd, WM_SYSKEYDOWN, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				::PostMessage(hWnd, WM_SYSKEYUP, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				break;
			case CKeyboardFlag_KeyDown:
				::PostMessage(hWnd, WM_KEYDOWN, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				break;
			case CKeyboardFlag_KeyUp:
				::PostMessage(hWnd, WM_KEYUP, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				break;
			case CKeyboardFlag_KeyDownUp:
				::PostMessage(hWnd, WM_KEYDOWN, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				::PostMessage(hWnd, WM_KEYUP, pKeyboardMessage->virtualKey, pKeyboardMessage->lParam);
				break;
			default:
				return FALSE;
		}
		return TRUE;
	} else {
		return FALSE;
	}
}

// 发送按键消息。
void CKeyboard::PostKeyMessage(__in HWND hWnd, __in UINT_PTR virtualKey, __in CKeyboardFlag keyboardFlag, __in_opt bool isPostToChildWindow /* = false */) {
	// 初始化结构体数据。
	KeyboardMessage keyboardMessage = {0};
	keyboardMessage.keyboardFlag = keyboardFlag;
	keyboardMessage.virtualKey = virtualKey;
	keyboardMessage.lParam = IsVkExtended(virtualKey) ? 1 : 0;

	if (isPostToChildWindow) {
		EnumChildWindows(hWnd, KeyboardMessageProc, (LPARAM)&keyboardMessage);		// 将按键消息发送到所有的子窗口。
	} else {
		KeyboardMessageProc(hWnd, (LPARAM)(&keyboardMessage));	// 键盘消息处理。
	}
}

// 按下或弹起控件。
void CKeyboard::PressOrReleaseControl(__in HWND hWnd, __in UINT flag) {
	switch (flag) {
		case 0:
			::PostMessage(hWnd, BM_CLICK, 0, 0);		// 单击控件。
			break;
		case 1:
			::PostMessage(hWnd, WM_LBUTTONDOWN, 0, 0);// 按下控件。
			break;
		case 2:
			::PostMessage(hWnd, WM_LBUTTONUP, 0, 0);	// 弹起控件。
			break;
		default:
			return;
	}
}

// 按组合键。
//void CKeyboard::按组合键_KeyEvent(__in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in UINT_PTR functionKey2 /* = 0 */, __in UINT_PTR functionKey3 /* = 0 */) {
//	if (functionKey3) {
//		keybd_event(functionKey3, MapVirtualKey(functionKey3, MAPVK_VK_TO_VSC), 0, 0);	// 按下功能键3。
//	}
//	if (functionKey2) {
//		keybd_event(functionKey2, MapVirtualKey(functionKey2, MAPVK_VK_TO_VSC), 0, 0);	// 按下功能键2。
//	}
//	keybd_event(functionKey1, MapVirtualKey(functionKey1, MAPVK_VK_TO_VSC), 0, 0);		// 按下功能键1。
//	keybd_event(virtualKey, MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC), 0, 0);		// 按下虚拟键。
//
//	if (functionKey3) {
//		keybd_event(functionKey3, MapVirtualKey(functionKey3, MAPVK_VK_TO_VSC), 2, 0);	// 弹起功能键3。
//	}
//	if (functionKey2) {
//		keybd_event(functionKey2, MapVirtualKey(functionKey2, MAPVK_VK_TO_VSC), 2, 0);	// 弹起功能键2。
//	}
//	keybd_event(functionKey1, MapVirtualKey(functionKey1, MAPVK_VK_TO_VSC), 2, 0);		// 弹起功能键1。
//	keybd_event(virtualKey, MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC), 2, 0);		// 弹起虚拟键。
//
//	//if (functionKey3) {
//	//	keybd_event(functionKey3, 0, 0, 0);	// 按下功能键3。
//	//}
//	//if (functionKey2) {
//	//	keybd_event(functionKey2, 0, 0, 0);	// 按下功能键2。
//	//}
//	//keybd_event(functionKey1, 0, 0, 0);		// 按下功能键1。
//	//keybd_event(virtualKey, 0, 0, 0);		// 按下虚拟键。
//
//	//if (functionKey3) {
//	//	keybd_event(functionKey3, 0, 2, 0);	// 弹起功能键3。
//	//}
//	//if (functionKey2) {
//	//	keybd_event(functionKey2, 0, 2, 0);	// 弹起功能键2。
//	//}
//	//keybd_event(functionKey1, 0, 2, 0);		// 弹起功能键1。
//	//keybd_event(virtualKey, 0, 2, 0);		// 弹起虚拟键。
//}

void CKeyboard::按组合键_KeyEvent(__in LONGLONG delay, __in UINT virtualKey, __in UINT functionKey1, __in_opt UINT functionKey2 /* = 0 */, __in_opt UINT functionKey3 /* = 0 */) {
	if (delay) {
		WinSys::Delay(delay, TimeUnit_Millisecond);
	}
	if (functionKey3) {
		keybd_event(functionKey3, MapVirtualKey(functionKey3, MAPVK_VK_TO_VSC), 0, 0);	// 按下功能键3。
	}
	if (functionKey2) {
		keybd_event(functionKey2, MapVirtualKey(functionKey2, MAPVK_VK_TO_VSC), 0, 0);	// 按下功能键2。
	}
	keybd_event(functionKey1, MapVirtualKey(functionKey1, MAPVK_VK_TO_VSC), 0, 0);		// 按下功能键1。
	keybd_event(virtualKey, MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC), 0, 0);		// 按下虚拟键。

	if (functionKey3) {
		keybd_event(functionKey3, MapVirtualKey(functionKey3, MAPVK_VK_TO_VSC), 2, 0);	// 弹起功能键3。
	}
	if (functionKey2) {
		keybd_event(functionKey2, MapVirtualKey(functionKey2, MAPVK_VK_TO_VSC), 2, 0);	// 弹起功能键2。
	}
	keybd_event(functionKey1, MapVirtualKey(functionKey1, MAPVK_VK_TO_VSC), 2, 0);		// 弹起功能键1。
	keybd_event(virtualKey, MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC), 2, 0);		// 弹起虚拟键。
}

// 组合键信息。
typedef struct _CompositeKeyInfo {
	LONGLONG delay;			// 延时。
	UINT_PTR virtualKey;	// 虚拟键码。
	UINT_PTR functionKey1;	// 功能键码1。
	UINT_PTR functionKey2;	// 功能键码2。
	UINT_PTR functionKey3;	// 功能键码3。
} CompositeKeyInfo;

// 组合按键消息处理。
BOOL CALLBACK CompositeKeyMessageProc (HWND hWnd, LPARAM lParam) {
	CompositeKeyInfo *pCompositeKeyInfo = (CompositeKeyInfo*)lParam;
	if (hWnd) {
		if (pCompositeKeyInfo->delay)
			WinSys::Delay(pCompositeKeyInfo->delay, TimeUnit_Millisecond);
		if (pCompositeKeyInfo->functionKey3) {
			::PostMessage(hWnd, WM_SYSKEYDOWN, pCompositeKeyInfo->functionKey3, 0);	// 按下功能键3。
		}
		if (pCompositeKeyInfo->functionKey2) {
			::PostMessage(hWnd, WM_SYSKEYDOWN, pCompositeKeyInfo->functionKey2, 0);	// 按下功能键2。
		}
		::PostMessage(hWnd, WM_SYSKEYDOWN, pCompositeKeyInfo->functionKey1, 0);
		::PostMessage(hWnd, WM_SYSKEYDOWN, pCompositeKeyInfo->virtualKey, 0);

		if (pCompositeKeyInfo->functionKey3) {
			::PostMessage(hWnd, WM_KEYUP, pCompositeKeyInfo->functionKey3, 0);	// 按下功能键3。
		}
		if (pCompositeKeyInfo->functionKey2) {
			::PostMessage(hWnd, WM_KEYUP, pCompositeKeyInfo->functionKey2, 0);	// 按下功能键2。
		}
		::PostMessage(hWnd, WM_KEYUP, pCompositeKeyInfo->functionKey1, 0);
		::PostMessage(hWnd, WM_KEYUP, pCompositeKeyInfo->virtualKey, 0);

		return TRUE;
	} else {
		return FALSE;
	}
}

// 按组合键。
//void CKeyboard::按组合键_KeyMessage(__in HWND hWnd, __in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in UINT_PTR functionKey2 /* = 0 */, __in UINT_PTR functionKey3 /* = 0 */, __in bool isPostToChildWindow /* = false */) {
//	按组合键_KeyMessage(hWnd, virtualKey, (LONGLONG)0, functionKey1, functionKey2, functionKey3, isPostToChildWindow);
//}

// 按组合键。
void CKeyboard::按组合键_KeyMessage(__in HWND hWnd, __in LONGLONG delay, __in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in_opt UINT_PTR functionKey2 /* = 0 */, __in_opt UINT_PTR functionKey3 /* = 0 */, __in bool isPostToChildWindow /* = false */) {
	if (isPostToChildWindow) {
		CompositeKeyInfo cki;
		cki.delay = delay;
		cki.virtualKey = virtualKey;
		cki.functionKey1 = functionKey1;
		cki.functionKey2 = functionKey2;
		cki.functionKey3 = functionKey3;
		EnumChildWindows(hWnd, CompositeKeyMessageProc, (LPARAM)&cki);
	} else {
		if (delay)
			WinSys::Delay(delay, TimeUnit_Millisecond);
		if (functionKey3) {
			::PostMessage(hWnd, WM_SYSKEYDOWN, functionKey3, 0);	// 按下功能键3。
		}
		if (functionKey2) {
			::PostMessage(hWnd, WM_SYSKEYDOWN, functionKey2, 0);	// 按下功能键2。
		}
		::PostMessage(hWnd, WM_SYSKEYDOWN, functionKey1, 0);
		::PostMessage(hWnd, WM_SYSKEYDOWN, virtualKey, 0);

		if (functionKey3) {
			::PostMessage(hWnd, WM_SYSKEYUP, functionKey3, 0);	// 按下功能键3。
		}
		if (functionKey2) {
			::PostMessage(hWnd, WM_SYSKEYUP, functionKey2, 0);	// 按下功能键2。
		}
		::PostMessage(hWnd, WM_SYSKEYUP, functionKey1, 0);
		::PostMessage(hWnd, WM_SYSKEYUP, virtualKey, 0);
	}
}

// 检查位设置。
bool CKeyboard::BitSet(__in BYTE BitTable, __in UINT BitMask) {
	return BitTable & BitMask ? true : false;
}