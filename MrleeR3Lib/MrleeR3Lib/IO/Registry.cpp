#include "stdafx.h"
#include "Registry.h"

using namespace Mrlee_R3::IO;

/**	设置注册表项中的值。
 *	@param[in]  一个已打开项的句柄，或指定一个标准项名。
 *				可参照RegOpenKey函数的第一个参数。
 *	@param[in]  子项名。可参照RegOpenKey函数的第二个参数。
 *	@param[in]  欲删除值的名字。可参照RegDeleteValue函数的第二个参数。
 *	@param[in]	欲删除值的类型。可参照RegQueryValueEx函数的第四个参数。
 *	@return 返回CRegistryFlag枚举类型。
 *  设置成功返回CRegistryFlag_SUCCESS。
 *  设置失败返回CRegistryFlag_ERROR。
 *	如果子项或欲删除的值不存在返回CRegistryFlag_NOT_FIND。
 */
CRegistryFlag CRegistry::DeleteRegistryValue(
	__in HKEY hKey, 
	__in CString& lpSubKey, 
	__in CString& lpValueName, 
	__in DWORD dwType)
{
	HKEY hkResult;
	// 打开注册表键。
	int ret=RegOpenKey(hKey,lpSubKey,&hkResult);
	if (ERROR_SUCCESS == ret)
	{
		__try
		{
			// 检查NeverShowExt值是否存在。
			ret = RegQueryValueEx(hkResult, lpValueName, 0, &dwType, NULL, NULL);
			if (ERROR_SUCCESS != ret)
			{
				if (ERROR_FILE_NOT_FOUND == ret)
				{
					// 未找到NeverShowExt的值，可以认为Exe扩展名并未单独的被隐藏了。
					// 所以直接返回TRUE。
					return CRegistryFlag_NOT_FIND;
				}
				return CRegistryFlag_ERROR;
			}
			// 删除注册表值。
			ret = RegDeleteValue(hkResult, lpValueName);
			if (ERROR_SUCCESS == ret)
			{
				// 删除注册表值成功。
				return CRegistryFlag_SUCCESS;
			}
			return CRegistryFlag_ERROR;
		}
		__finally
		{
			// 关闭打开的句柄。
			RegCloseKey(hkResult);
		}
	} 
	else
	{
		if (ERROR_FILE_NOT_FOUND == ret)
		{
			return CRegistryFlag_NOT_FIND;
		}
		return CRegistryFlag_ERROR;
	}
}

// 获得注册表项中的值。
CRegistryFlag CRegistry::GetRegistryValue(
	__in HKEY hKey,
	__in_opt LPCWSTR lpSubKey,
	__in_opt LPCWSTR lpValueName,
	__out_opt LPDWORD lpType,
	__out_bcount_opt(*lpcbData) LPBYTE lpData,
	__inout_opt LPDWORD lpcbData
	)
{
	HKEY hkResult = NULL;
	int ret;
	// 打开注册表键。
	ret=RegCreateKeyW(hKey,lpSubKey,&hkResult);
	if (ERROR_SUCCESS == ret)
	{
		__try
		{
			// 设置一个空的名叫NeverShowExt的字符串值，即可隐藏Exe文件的扩展名。
			ret = RegQueryValueExW(hkResult, lpValueName, 0, lpType, lpData, lpcbData);
			if (ERROR_SUCCESS != ret)
			{
				return CRegistryFlag_ERROR;
			}
			return CRegistryFlag_SUCCESS;
		}
		__finally
		{
			// 关闭打开的句柄。
			RegCloseKey(hkResult);
		}

	}
	else
	{
		return CRegistryFlag_ERROR;
	}
}

/**	设置注册表项中的值。
 *	@param[in]  一个已打开项的句柄，或指定一个标准项名。
 *				可参照RegCreateKey函数的第一个参数。
 *	@param[in]  【可选】欲创建的新子项。可同时创建多个项，只需用反斜杠将它们分隔开即可。例如level1\level2\newkey。
 *				可参照RegCreateKey函数的第二个参数。
 *	@param[in]  【可选】要设置值的名字。
 *				可参照RegSetValueEx函数的第二个参数。
 *	@param[in]	要设置的值的类型。
 *				可参照RegSetValueEx函数的第四个参数。
 *	@param[in]	【可选】包含数据的缓冲区中的第一个字节。
 *				可参照RegSetValueEx函数的第五个参数。
 *	@param[in]	【可选】lpData缓冲区的长度。
 *				可参照RegSetValueEx函数的第六个参数。
 *	@return 返回CRegistryFlag枚举类型。
 *  设置成功返回CRegistryFlag_SUCCESS。
 *  设置失败返回CRegistryFlag_ERROR。
 *	@note 如指定的项已经存在，那么函数会打开现有的项。如果指定的项不存在，将会创建一个新项。
 */
CRegistryFlag CRegistry::SetRegistryValue(
	__in HKEY hKey, 
	__in_opt CString& lpSubKey, 
	__in_opt CString& lpValueName, 
	__in DWORD dwType, 
	__in_bcount_opt(cbData) CONST BYTE* lpData, 
	__in DWORD cbData)
{
	HKEY hkResult = NULL;
	int ret;
	// 打开注册表键。
	ret=RegCreateKey(hKey,lpSubKey,&hkResult);
	if (ERROR_SUCCESS == ret)
	{
		__try
		{
			// 设置一个空的名叫NeverShowExt的字符串值，即可隐藏Exe文件的扩展名。
			ret = RegSetValueEx(hkResult, lpValueName, 0, dwType, lpData, cbData);
			if (ERROR_SUCCESS != ret)
			{
				return CRegistryFlag_ERROR;
			}
			return CRegistryFlag_SUCCESS;
		}
		__finally
		{
			// 关闭打开的句柄。
			RegCloseKey(hkResult);
		}

	}
	else
	{
		return CRegistryFlag_ERROR;
	}
}
