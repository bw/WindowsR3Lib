#include "stdafx.h"
#include "SendKeys.h"

using namespace Mrlee_R3::IO;

const WORD CSendKeys::INVALIDKEY = 0xFFFF;

struct key_desc_t
{
	LPCTSTR keyName;// 按键字符串。
	BYTE VKey;		// 对应的虚拟键码。
	bool normalkey; // a normal character or a VKEY ?
};

#define MaxSendKeysRecs 71

key_desc_t KeyNames[MaxSendKeysRecs] = 
{
	{_T("ADD"), VK_ADD}, 
	{_T("APPS"), VK_APPS},
	{_T("AT"), '@', true},
	{_T("BACKSPACE"), VK_BACK},
	{_T("BKSP"), VK_BACK},
	{_T("BREAK"), VK_CANCEL},
	{_T("BS"), VK_BACK},
	{_T("CAPSLOCK"), VK_CAPITAL},
	{_T("CARET"), '^', true},
	{_T("CLEAR"), VK_CLEAR},
	{_T("DECIMAL"), VK_DECIMAL}, 
	{_T("DEL"), VK_DELETE},
	{_T("DELETE"), VK_DELETE},
	{_T("DIVIDE"), VK_DIVIDE}, 
	{_T("DOWN"), VK_DOWN},
	{_T("END"), VK_END},
	{_T("ENTER"), VK_RETURN},
	{_T("ESC"), VK_ESCAPE},
	{_T("ESCAPE"), VK_ESCAPE},
	{_T("F1"), VK_F1},
	{_T("F10"), VK_F10},
	{_T("F11"), VK_F11},
	{_T("F12"), VK_F12},
	{_T("F13"), VK_F13},
	{_T("F14"), VK_F14},
	{_T("F15"), VK_F15},
	{_T("F16"), VK_F16},
	{_T("F2"), VK_F2},
	{_T("F3"), VK_F3},
	{_T("F4"), VK_F4},
	{_T("F5"), VK_F5},
	{_T("F6"), VK_F6},
	{_T("F7"), VK_F7},
	{_T("F8"), VK_F8},
	{_T("F9"), VK_F9},
	{_T("HELP"), VK_HELP},
	{_T("HOME"), VK_HOME},
	{_T("INS"), VK_INSERT},
	{_T("LEFT"), VK_LEFT},
	{_T("LEFTBRACE"), '{', true},
	{_T("LEFTPAREN"), '(', true},
	{_T("LWIN"), VK_LWIN},
	{_T("MULTIPLY"), VK_MULTIPLY}, 
	{_T("NUMLOCK"), VK_NUMLOCK},
	{_T("NUMPAD0"), VK_NUMPAD0}, 
	{_T("NUMPAD1"), VK_NUMPAD1}, 
	{_T("NUMPAD2"), VK_NUMPAD2}, 
	{_T("NUMPAD3"), VK_NUMPAD3}, 
	{_T("NUMPAD4"), VK_NUMPAD4}, 
	{_T("NUMPAD5"), VK_NUMPAD5}, 
	{_T("NUMPAD6"), VK_NUMPAD6}, 
	{_T("NUMPAD7"), VK_NUMPAD7}, 
	{_T("NUMPAD8"), VK_NUMPAD8}, 
	{_T("NUMPAD9"), VK_NUMPAD9}, 
	{_T("PERCENT"), '%', true},
	{_T("PGDN"), VK_NEXT},
	{_T("PGUP"), VK_PRIOR},
	{_T("PLUS"), '+', true},
	{_T("PRTSC"), VK_PRINT},
	{_T("RIGHT"), VK_RIGHT},
	{_T("RIGHTBRACE"), '}', true},
	{_T("RIGHTPAREN"), ')', true},
	{_T("RWIN"), VK_RWIN},
	{_T("SCROLL"), VK_SCROLL},
	{_T("SEPARATOR"), VK_SEPARATOR}, 
	{_T("SNAPSHOT"), VK_SNAPSHOT},
	{_T("SUBTRACT"), VK_SUBTRACT}, 
	{_T("TAB"), VK_TAB},
	{_T("TILDE"), '~', true}, 
	{_T("UP"), VK_UP},
	{_T("WIN"), VK_LWIN}
};

// 释放所有转移键。
void CSendKeys::PopUpShiftKeys() {
	if (!m_bUsingParens) {
		if (m_bShiftDown)
			CKeyboard::SendKeyUp(VK_SHIFT);	// 释放Shift键。
		if (m_bControlDown)
			CKeyboard::SendKeyUp(VK_CONTROL);	// 释放Ctrl键。
		if (m_bAltDown)
			CKeyboard::SendKeyUp(VK_MENU);		// 释放Alt键。
		if (m_bWinDown)
			CKeyboard::SendKeyUp(VK_LWIN);		// 释放左Win键。
		m_bWinDown = m_bShiftDown = m_bControlDown = m_bAltDown = false;
	}
}

// 应用程序激活。
bool CSendKeys::AppActivate(__in HWND hWnd) {
	if (hWnd) {
		::SendMessage(hWnd, WM_SYSCOMMAND, SC_HOTKEY, (LPARAM) hWnd);
		::SendMessage(hWnd, WM_SYSCOMMAND, SC_RESTORE, (LPARAM) hWnd);

		::ShowWindow(hWnd, SW_SHOW);
		::SetForegroundWindow(hWnd);
		::SetFocus(hWnd);

		return true;
	} else {
		return false;
	}
}

// 应用程序激活。
bool CSendKeys::AppActivate(__in_opt LPTSTR lpWindowName, __in_opt LPTSTR lpClassName /* = 0 */) {
	HWND hWnd = FindWin(lpClassName, lpWindowName);
	return AppActivate(hWnd);
}

// Implements a simple binary search to locate special key name strings
// 将字符串转换为虚拟键码。
WORD CSendKeys::StringToVKey(__in LPCTSTR KeyString, __out int &idx)
{
	bool Found = false, Collided;
	int  Bottom = 0, 
		Top = MaxSendKeysRecs,
		Middle = (Bottom + Top) / 2;
	WORD retval = INVALIDKEY;	// 虚拟键码。

	idx    = -1;

	do
	{
		Collided = (Bottom == Middle) || (Top == Middle);
		// 比较数组中的字符串与参数字符串是否相等。
		int cmp = _tcsnicmp(KeyNames[Middle].keyName, KeyString, _tcslen(KeyString));
		if (cmp == 0)
		{
			Found = true;
			retval = KeyNames[Middle].VKey;
			idx    = Middle;
			break;
		}
		else
		{
			if (cmp < 0)
				Bottom = Middle;
			else
				Top = Middle;
			Middle = (Bottom + Top) / 2;
		}
	} while (!(Found || Collided));

	return retval;	// 返回虚拟键码。
}
#include "WinSys.h"
// 将一个或多个按键消息发送到活动窗口。
bool CSendKeys::SendKeys(__in LPCTSTR KeysString, __in_opt bool Wait /* = false */) {
	int once = 1;
	LPTSTR pKey = (LPTSTR) KeysString;
	WORD MKey, NumTimes;
	TCHAR KeyString[300] = {0};
	int  keyIdx;
	TCHAR  chTemp;

	m_bWinDown = m_bShiftDown = m_bControlDown = m_bAltDown = m_bUsingParens = false;
	m_bWait = Wait;
	DWORD m_nDelayAlways = 0;
	DWORD m_nDelayNow = 0;
	while (chTemp = *pKey) {
		switch (chTemp) {
			case TEXT('('):
				m_bUsingParens = true;	// 使用了括号。
				break;
			case TEXT(')'):
				m_bUsingParens = false;
				PopUpShiftKeys();		// 释放所有转移键。
				break;

			// Alt键。
			case TEXT('%'):
				m_bAltDown = true;
				CKeyboard::SendKeyDown(VK_MENU);
				break;

			// SHIFT 键
			case TEXT('+'):
				m_bShiftDown = true;
				CKeyboard::SendKeyDown(VK_SHIFT);
				break;

			// CTRL 键
			case TEXT('^'):
				m_bControlDown = true;
				CKeyboard::SendKeyDown(VK_CONTROL);
				break;

			// WINKEY (左Win键)
			case TEXT('@'):
				m_bWinDown = true;
				CKeyboard::SendKeyDown(VK_LWIN);
				break;

			// enter
			case TEXT('~'):
				CKeyboard::SendKey(VK_RETURN);
				PopUpShiftKeys();		// 释放所有转移键。
				break;

			// 关键字。
			case TEXT('{'):
				{
					LPTSTR p = pKey + 1;	// 跳过 '{'。
					size_t t;	// 关键字长度。

					// 寻找结束符号。
					while (*p && *p != _TXCHAR('}'))
						p++;

					t = p - pKey;	// 计算关键字长度。
					// 字符串是否过大？
					if (t > sizeof(KeyString))
						return false;

					// 拷贝关键字。
					_tcsncpy_s(KeyString, pKey+1, t);

					KeyString[t-1] = _TXCHAR('\0');
					keyIdx = -1;

					pKey += t; // 跳过关键字。

					// 无效键。
					MKey = INVALIDKEY;

					// 是否发送一个虚拟键码？
					// 语法：{VKEY keycode}。keycode代表虚拟键的数值，如传入65，则代表要发送字符'a'。
					if (_tcsnicmp(KeyString, TEXT("VKEY"), 4) == 0)
					{
						p = KeyString + 4;
						MKey = _ttoi(p);
					} 
					// 是否发送一个蜂鸣音？
					// 语法：{BEEP frequency duration}。frequency，代表发声频率，单位为赫兹。duration，代表声音的持续时间，单位为毫秒。
					else if (_tcsnicmp(KeyString, _T("BEEP"), 4) == 0)
					{
						p = KeyString + 4 + 1;	// 跳过右括号。
						LPTSTR p1 = p;
						DWORD frequency, duration;	// 频率、延时。

						if ((p1 = _tcsstr(p, _T(" "))) != NULL) {
							*p1++ = _TXCHAR('\0');
							frequency = _ttoi(p);		// 频率，单位为赫兹。
							duration = _ttoi(p1);		// 声音的持续时间，单位为毫秒。
							::Beep(frequency, duration);// 发出声音。
						}
					}
					// 是否激活一个窗口。
					// 语法：{APPACTIVATE WindowTitle}。WindowTitle，代表窗口名，此窗口名可以使全部名称也可以时部分名称。
					else if (_tcsnicmp(KeyString, _T("APPACTIVATE"), 11) == 0)
					{
						p = KeyString + 11 + 1;
						AppActivate(p);
					}
					// 想要发送或设置延时？
					else if (_tcsnicmp(KeyString, _T("DELAY"), 5) == 0)
					{
						// Advance to parameters
						p = KeyString + 5;
						// 设置延时因子。
						if (*p == _TXCHAR('='))
							m_nDelayAlways = _ttoi(p + 1); // 数字在'='之后。
						else
							// 设置即刻延时。
							m_nDelayNow = _ttoi(p);
					}
					else
					{
						MKey = StringToVKey(KeyString, keyIdx);
						// Key found in table
						if (keyIdx != -1)
						{
							NumTimes = 1;

							// Does the key string have also count specifier?
							t = _tcslen(KeyNames[keyIdx].keyName);
							if (_tcslen(KeyString) > t)
							{
								p = KeyString + t;
								// Take the specified number of times
								NumTimes = _ttoi(p);
							}

							if (KeyNames[keyIdx].normalkey)
								MKey = ::VkKeyScan(KeyNames[keyIdx].VKey);
						}
					}
				}

				// 是否是一个有效键？
				if (MKey != INVALIDKEY)
				{
					CKeyboard::SendKey(MKey, m_nDelayNow);
					PopUpShiftKeys();
				}

				break;

			default:
				if (1 == once) {
					once--;
					// 延时10毫秒。作用是为了使按键成功发出。
					HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
					WaitForSingleObject(hEvent, 10);
					if (!m_nDelayNow)
						m_nDelayNow = m_nDelayAlways;
				}

				CKeyboard::SendKey(chTemp, m_nDelayNow);
				
				PopUpShiftKeys();		// 释放所有转移键。
				break;
		}

		pKey++;
	}

	m_bUsingParens = false;
	PopUpShiftKeys();
	return true;
}