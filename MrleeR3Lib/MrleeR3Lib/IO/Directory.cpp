#include "stdafx.h"
#include "Directory.h"

using namespace Mrlee_R3::IO;

BOOL CDirectory::Copy (__in CString &strDesPath, __in CString &strSrcPath, __in BOOL isOverlay) {
	if( !PathIsDirectory( strSrcPath ))
	{      
		return FALSE;
	}
	if ( !PathIsDirectory( strDesPath ) )//目的目录不存在，新建文件夹
	{
		CreateDirectory(strDesPath, NULL); 
	}

	if ( strSrcPath.GetAt(strSrcPath.GetLength()-1) != '\\' )
		strSrcPath += '\\';
	if ( strDesPath.GetAt(strDesPath.GetLength()-1) != '\\' )
		strDesPath += '\\';
	BOOL bRet = FALSE; // 因为源目录不可能为空，所以该值一定会被修改
	CFileFind ff; 
	BOOL bFound = ff.FindFile(strSrcPath+"*",   0); 
	while(bFound)      // 递归拷贝
	{ 
		bFound = ff.FindNextFile(); 
		if( ff.GetFileName() == "." || ff.GetFileName() == ".." ) 
			continue;
		CString strSubSrcPath = ff.GetFilePath();
		CString strSubDespath = strSubSrcPath;
		strSubDespath.Replace(strSrcPath, strDesPath);
		if( ff.IsDirectory() ) {
			bRet = Copy(strSubSrcPath, strSubDespath, isOverlay);     // 递归拷贝文件夹
		}
		else {
			bRet = CopyFile(strSubSrcPath, strSubDespath, !isOverlay);   // 拷贝文件
			// 如果不覆盖，则将返回值赋值为TRUE。
			if (FALSE == isOverlay) {
				bRet = TRUE;
			}
		}
		if ( !bRet )
			break;
	} 
	ff.Close();
	return bRet;
}

// 创建目录。
BOOL CDirectory::CreateDirectory_ (__in TCHAR *strPath) {
	SECURITY_ATTRIBUTES attrib;
	attrib.bInheritHandle = FALSE;
	attrib.lpSecurityDescriptor = NULL;
	attrib.nLength =sizeof(SECURITY_ATTRIBUTES);


	//上面定义的属性可以省略。 直接return ::CreateDirectory( path, NULL); 即可
	if(!CreateDirectory( strPath, &attrib))
	{
		TRACE("【错误】CDirectory::CreateDirectory_() - 创建目录失败。\n");
		return FALSE;

	}
	return TRUE;
}

// 创建多级目录。
BOOL CDirectory::CreatedMultipleDirectory (TCHAR* strPath)
{
	CString Directoryname = strPath;

	if (  Directoryname[Directoryname.GetLength() - 1] !=  TEXT('//') )
	{
		Directoryname.Append(TEXT("\\"));

	}
	std::vector<CString> vpath;
	CString strtemp;
	BOOL  bSuccess = FALSE;
	for ( int i = 0; i < Directoryname.GetLength(); i++ )
	{
		if ( Directoryname[i] != TEXT('\\'))
		{
			strtemp += Directoryname[i];  
		}
		else
		{
			vpath.push_back( strtemp );
			strtemp.Append( TEXT("\\") );
		}
	}
	size_t count = vpath.size();
	for (size_t i = 0; i < count; i++)
	{
		bSuccess = ::CreateDirectory( vpath[i].GetBuffer(), NULL );
	}

	return bSuccess; 
}

// 获得文件名。
void CDirectory::GetFileName (__in TCHAR *path, __out CString &fileName) {
	size_t count = _tcslen(path);
	size_t i = count - 1;
	for (; i >= 0; i--) {
		if (TEXT('\\') == path[i]) {
			break;
		}
	}

	if (i < 0) {
		fileName.SetString(path);
		return;
	} else {
		fileName.SetString(path + i + 1);
		return;
	}
}