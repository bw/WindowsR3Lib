#include "stdafx.h"
#include "StringHelper.h"

using namespace Mrlee_R3;

int add(__in int a, __in int b) {
	return a + b;
}

// char 转 wchar_t。
void StringHelper::CharToWChar (__in char *src, __in wchar_t **des) {
	size_t length = strlen(src) * 2 + 2;
	*des = (wchar_t *)calloc(length, 1);
	MultiByteToWideChar( CP_ACP, 0, src, (int)(strlen(src)+1), *des, (int)length);
}

// wchar_t 转 char。
void StringHelper::WCharToChar(__in wchar_t *src, __in char **des) {
	size_t length = wcslen(src) * 2 + 1;
	*des = (char *)malloc(length);
	WideCharToMultiByte( CP_ACP, 0, (const wchar_t *)src, -1, *des, (int)length, NULL, NULL ); 
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindString (__in TCHAR *src, __in TCHAR *des) {
#ifdef UNICODE
	return FindStringW(src, des);
#else
	return FindStringA(src, des);
#endif // UNICODE
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindStringA (__in char *src, __in char *des) {
	size_t lengthSrc = strlen (src);	// 获得字符串长度。
	// 如果字符串长度为0，则没有必要再比下去了。
	if (0 == lengthSrc) {
		return -1;
	}
	size_t lengthDes = strlen (des);	// 获得字符串长度。
	// 如果字符串长度为0，则没有必要再比下去了。
	if (0 == lengthDes) {
		return -1;
	}

	// 如果被比较的字符串长度小于要查找的字符串长度，则返回-1。
	if (lengthDes > lengthSrc) {
		return -1;
	}

	lengthDes--;
	// 开始查找。
	for (size_t i = 0; i < lengthSrc; i++)
	{
		size_t tempI = i;
		for (size_t j = 0; j <= lengthDes; j++)
		{
			if (src[tempI] != des[j]) {
				break;
			}
			if (src[tempI] == des[j]) {
				tempI++;
				if (j == lengthDes) {
					return (int)i;
				}
			}
		}
	}

	return -1;
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindStringW (__in wchar_t *src, __in wchar_t *des) {
	size_t lengthSrc = wcslen (src);	// 获得字符串长度。
	// 如果字符串长度为0，则没有必要再比下去了。
	if (0 == lengthSrc) {
		return -1;
	}
	size_t lengthDes = wcslen (des);	// 获得字符串长度。
	// 如果字符串长度为0，则没有必要再比下去了。
	if (0 == lengthDes) {
		return -1;
	}

	// 如果被比较的字符串长度小于要查找的字符串长度，则返回-1。
	if (lengthDes > lengthSrc) {
		return -1;
	}

	lengthDes--;
	// 开始查找。
	for (size_t i = 0; i < lengthSrc; i++)
	{
		size_t tempI = i;
		for (size_t j = 0; j <= lengthDes; j++)
		{
			if (src[tempI] != des[j]) {
				break;
			}
			if (src[tempI] == des[j]) {
				tempI++;
				if (j == lengthDes) {
					return (int)i;
				}
			}
		}
	}

	return -1;
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindStringI (__in TCHAR *src, __in TCHAR *des) {
#ifdef UNICODE
	return FindStringIW(src, des);
#else
	return FindStringIA(src, des);
#endif // UNICODE
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindStringIA (__in char *src, __in char *des) {
	size_t iSizeSrc = strlen(src) + 1;
	size_t iSizeDes = strlen(des) + 1;
	char *srcTmp = (char *)calloc(iSizeSrc, 1);
	char *desTmp = (char *)calloc(iSizeDes, 1);
	strcpy_s(srcTmp, iSizeSrc, src);
	strcpy_s(desTmp, iSizeDes, des);
	_strupr_s(srcTmp, iSizeSrc);
	_strupr_s(desTmp, iSizeDes);
	int result = FindStringA(srcTmp, desTmp);
	free(srcTmp);
	free(desTmp);
	return result;
}

// 从左边开始查找字符串。返回找到的第一个字符串的偏移。
int StringHelper::FindStringIW (__in wchar_t *src, __in wchar_t *des) {
	size_t iSizeSrc = wcslen(src) + 1;
	size_t iSizeDes = wcslen(des) + 1;
	wchar_t *srcTmp = (wchar_t *)calloc(iSizeSrc * 2, 1);
	wchar_t *desTmp = (wchar_t *)calloc(iSizeDes * 2, 1);
	wcscpy_s(srcTmp, iSizeSrc, src);
	wcscpy_s(desTmp, iSizeDes, des);
	_wcsupr_s(srcTmp, iSizeSrc);
	_wcsupr_s(desTmp, iSizeDes);
	int result = FindStringW(srcTmp, desTmp);
	free(srcTmp);
	free(desTmp);
	return result;
}

// 十六进制字符串转换成十六进制数组。
int StringHelper::HexStrToByteArray(__in CString& hexStr, __out BYTE outBytes[]) {
	CStringArray szList;
	INT_PTR arraySize = StringHelper::SplitString(hexStr.Trim(), TEXT(" "), szList, FALSE);	// 分割字符串。
	if (0 == arraySize) {
		return 0;
	}

	CStringA tempHex;
	if (1 == arraySize) {
		tempHex = hexStr;
	} else {
		for (int i = 0; i < arraySize; i++)
		{
			tempHex += szList[i];
		}
	}

	int len = tempHex.GetLength();
	if (1 == len % 2) {
		tempHex = "0" + tempHex;
		len++;
	}

	int count = 0;
	for (; count < len; count += 2)
	{
		char ch = tempHex[count];
		int temp1;
		// 如果是A到F。
		if (ch >= 65 && ch <= 70) {
			temp1 = (ch - 55) << 4;
		}
		// 如果是a到f。
		if (ch >= 97 && ch <= 102) {
			temp1 = (ch - 87) << 4;
		}
		if (ch >= 48 && ch <= 57) {
			temp1 = (ch - 48) << 4;
		}

		ch = tempHex[count + 1];
		int temp2;
		// 如果是A到F。
		if (ch >= 65 && ch <= 70) {
			temp2 = (ch - 55);
		}
		// 如果是a到f。
		if (ch >= 97 && ch <= 102) {
			temp2 = (ch - 87);
		}
		if (ch >= 48 && ch <= 57) {
			temp2 = (ch - 48);
		}

		outBytes[count / 2] = temp1 + temp2;
	}

	return len / 2;
}

// 十六进制字符串转换成LONGLONG类型。
BOOL StringHelper::HexStrToLONGLONG(__in CString& hexStr, __out LONGLONG *hex)
{
	LONGLONG dem=0;
	int length = hexStr.GetLength();
	if (!length)
		return FALSE;
	for(int i=0;i<length;i++)
	{
		dem=dem*16;
		if((hexStr[i]<=TEXT('9'))&&(hexStr[i]>=TEXT('0')))        //0~9之间的字符
			dem+=hexStr[i]-TEXT('0');
		else if((hexStr[i]<=TEXT('F'))&&(hexStr[i]>=TEXT('A')))   //A~F之间的字符
			dem+=hexStr[i]-TEXT('A')+10;
		else if((hexStr[i]<=TEXT('f'))&&(hexStr[i]>=TEXT('a')))   //a~f之间的字符
			dem+=hexStr[i]-TEXT('a')+10;
		else
			return FALSE;                          //出错时返回-1
	}
	*hex = dem;
	return TRUE;     
}

// 十六进制数组转换成十进制。
BOOL StringHelper::HexStrToLONGLONG(__in CStringArray& hexStrArray, __out LONGLONG *hex) {
	INT_PTR length = hexStrArray.GetCount();
	LONGLONG num = 0;
	int weiYi = 0;
	BOOL bRet = FALSE;
	for (INT_PTR i = 0; i < length; i++)
	{
		LONGLONG temp;
		bRet = HexStrToLONGLONG(hexStrArray[i], &temp);
		if (bRet) {
			num += temp<<weiYi * 4;
			weiYi += hexStrArray[i].GetLength();
		}
		else {
			return FALSE;
		}
	}
	*hex = num;
	return TRUE;
}

// 十六进制字符串转换成字节表现形式。
void StringHelper::HexStrToBytes(__in CString& hexStr, __out CString *bytes) {
	int length = hexStr.GetLength();
	*bytes = "";
	while (1)
	{
		if (length == 0) {
			break;
		}
		if (length == 1) {
			*bytes += TEXT('0');
			*bytes += hexStr.Mid(0, 1);
			break;
		}
		length -= 2;
		*bytes += hexStr.Mid(length, 2);
	}
}

// 分割字符串。
INT_PTR StringHelper::SplitString(__in LPCTSTR lpszStr, __in LPCTSTR lpszSplit, __out CStringArray& rArrString, __in BOOL bAllowNullString /* = FALSE */) {
	rArrString.RemoveAll();   // 删除数组中所有的元素。
	CString szStr = lpszStr;   
	szStr.Trim();
	// 原字符串长度为0。
	if(szStr.GetLength()==0)   
	{   
		return 0;   
	}   
	CString szSplit = lpszSplit;
	if(szSplit.GetLength() == 0)   
	{   
		rArrString.Add(szStr);   
		return 1;   
	}   
	// 分割字符串长度为0。
	/*if(szSplit.GetLength() == 0)   
	{  
		return 0;   
	} */
	CString s;   
	int n;
	int i = 1;
	do {   
		n = szStr.Find(szSplit);   
		if(n > 0)   
		{   
			rArrString.Add(szStr.Left(n));   
			szStr = szStr.Right(szStr.GetLength()-n-szSplit.GetLength());   
			szStr.TrimLeft();   
		}   
		else if(n==0)	// 分割字符串  
		{   
			if(bAllowNullString)
				rArrString.Add(_T(""));   
			szStr = szStr.Right(szStr.GetLength()-szSplit.GetLength());   
			szStr.TrimLeft();   
		}   
		else  // 未找到分割字符串。
		{   
			if((szStr.GetLength()>0)||bAllowNullString)   
				rArrString.Add(szStr);   
			break;   
			/*if( szStr.GetLength()>0 && i != 1 ) {
				rArrString.Add(szStr);
				break;
			} else {
				return 0;
			}*/
		}
		i++;
	} while(1);   
	return rArrString.GetCount();
}

// 分割字符串。
INT_PTR StringHelper::SplitCString( IN CString& strIn, IN TCHAR division, OUT CStringArray& strAryRe )
{
	strAryRe.RemoveAll();
	if (!strIn.IsEmpty())
	{
		int nCount = 0;
		int nPos = -1;
		nPos = strIn.Find(division);
		CString strTemp = strIn;
		while (nPos != -1)
		{
			CString strSubString = strTemp.Left(nPos);  
			strTemp = strTemp.Right(strTemp.GetLength() - nPos-1);  
			nPos = strTemp.Find(division); 
			nCount++;
			strAryRe.Add(strSubString);
		}

		if (nCount == strAryRe.GetSize())
		{
			CString str;
			int nSize = strIn.ReverseFind(division);
			str = strIn.Right(strIn.GetLength()-nSize-1);
			strAryRe.Add(str);
		}
	}

	return strAryRe.GetCount();
}

// 分割字符串。
INT_PTR StringHelper::SplitCString( IN CString& strIn, IN LPCTSTR division, OUT CStringArray& strAryRe )
{
	if (division == _T(""))
	{
		strAryRe.RemoveAll();
		return 0;
	}

	int nStart = 0;
	int nEnd = 0;
	CString strTmp;
	while ( (nEnd = strIn.Find(division, nStart)) != -1 )
	{
		strTmp = strIn.Mid(nStart, nEnd-nStart);
		strAryRe.Add(strTmp);
		nStart = nEnd+2;
	}
	strTmp = strIn.Mid(nStart, strIn.GetLength());
	strAryRe.Add(strTmp);

	return strAryRe.GetCount();
}

// 分割字符串，分割字符为重复的字符串。
INT_PTR StringHelper::SplitCStringForRepeat( IN CString& strIn, IN TCHAR division, OUT CStringArray& strAryRe ) {
	strAryRe.RemoveAll();
	if (!strIn.IsEmpty())
	{
		int len = strIn.GetLength();
		int str_start = -1;
		int str_len = 0;
		for (int i = 0; i < len; i++)
		{
			if ( division == strIn[i] ) {
				if ( -1 != str_start ) {
					strAryRe.Add(strIn.Mid(str_start, str_len));
					str_start = -1;
					str_len = 0;
				}
				continue;
			}

			if ( -1 == str_start ) {
				str_start = i;
			}

			str_len++;
			//strAryRe
		}
	}

	return strAryRe.GetCount();
}

// 以行为单位分隔字符串。
INT_PTR StringHelper::SplitCStringByLine( IN CString& strIn, OUT CStringArray& strAryRe ) {
	strAryRe.RemoveAll();
	if (!strIn.IsEmpty())
	{
		int len = strIn.GetLength();
		int str_start = -1;
		int str_len = 0;
		for (int i = 0; i < len; i++)
		{
			if ( TEXT('\r') == strIn[i] )
			{
				if ( ( i + 1 ) == len ) {
					str_len++;
				} else if ( TEXT('\n') != strIn[i + 1] ) {
					continue;
				}

				if ( -1 != str_start ) {
					strAryRe.Add(strIn.Mid(str_start, str_len));
					str_start = -1;
					str_len = 0;
				}
				continue;
			} else if ( TEXT('\n') == strIn[i] ) {
				if ( -1 != str_start ) {
					strAryRe.Add(strIn.Mid(str_start, str_len));
					str_start = -1;
					str_len = 0;
				}
				continue;
			}

			if ( -1 == str_start ) {
				str_start = i;
			}

			str_len++;
		}

		if ( -1 != str_start ) {
			strAryRe.Add(strIn.Mid(str_start, str_len));
			str_start = -1;
			str_len = 0;
		}
	}

	return strAryRe.GetCount();
}

// 通过空白符分割字符串。
INT_PTR StringHelper::SplitCStringBySpace( IN CString& strIn, OUT CStringArray& strAryRe ) {
	strAryRe.RemoveAll();
	if (!strIn.IsEmpty())
	{
		int len = strIn.GetLength();
		int str_start = -1;
		int str_len = 0;
		for (int i = 0; i < len; i++)
		{
			if ( (TEXT(' ') == strIn[i]) ||
				 (TEXT('\t') == strIn[i]) || 
				 (TEXT('\v') == strIn[i]) )
			{
				if ( -1 != str_start ) {
					strAryRe.Add(strIn.Mid(str_start, str_len));
					str_start = -1;
					str_len = 0;
				}
				continue;
			}

			if ( -1 == str_start ) {
				str_start = i;
			}

			str_len++;
		}

		if ( -1 != str_start ) {
			strAryRe.Add(strIn.Mid(str_start, str_len));
			str_start = -1;
			str_len = 0;
		}
	}

	return strAryRe.GetCount();
}

// 繁体转简体。
TCHAR* StringHelper::繁体转简体 (__in TCHAR *Traditional) {
	size_t size = _tcslen (Traditional) + 1;
	TCHAR *Simplified = (TCHAR *)calloc(size * sizeof(TCHAR), 1);	// 这里乘以2是因为一个中文需要占两个字节。如果不乘以2，将出错。
	LCMapString (LOCALE_SYSTEM_DEFAULT, LCMAP_SIMPLIFIED_CHINESE, Traditional, (int)size, Simplified, (int)size);
	return Simplified;
}

// 简体转简体。
TCHAR* StringHelper::简体转繁体 (__in TCHAR *Simplified) {
	size_t size = _tcslen (Simplified) + 1;
	TCHAR *Traditional = (TCHAR *)calloc(size * sizeof(TCHAR), 1);	// 这里乘以2是因为一个中文需要占两个字节。如果不乘以2，将出错。
	LCMapString (LOCALE_SYSTEM_DEFAULT, LCMAP_TRADITIONAL_CHINESE, Simplified, (int)size, Traditional, (int)size);
	return Traditional;
}
