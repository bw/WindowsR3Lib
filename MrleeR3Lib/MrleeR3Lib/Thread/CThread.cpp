#include "stdafx.h"
#include "CThread.h"

using namespace Mrlee_R3::Thread;

// 创建远程线程。
HANDLE CThread::CreateRemoteThread(
	__in HANDLE hProcess, __in LPTHREAD_START_ROUTINE lpStartAddress, 
	__in_opt LPVOID lpParameter /* = NULL */, __out_opt LPDWORD lpThreadId /* = NULL */)
{
	return ::CreateRemoteThread(hProcess, 0, 0, lpStartAddress, lpParameter, 0, lpThreadId);
}

// 创建线程。
HANDLE CThread::CreateThead_(__in LPTHREAD_START_ROUTINE lpStartAddress, __in_opt LPVOID lpParameter, __out_opt LPDWORD lpThreadId) {
	return ::CreateThread(NULL, 0, lpStartAddress, lpParameter, 0, lpThreadId);
}