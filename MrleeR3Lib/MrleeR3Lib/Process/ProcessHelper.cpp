#include "stdafx.h"
#include "ProcessHelper.h"

using namespace Mrlee_R3;
using namespace Mrlee_R3::Process;

HMODULE m_hNtdll = LoadLibrary(TEXT("ntdll.dll"));
NtQueryInformationProcess_Ptr m_NtQueryInformationProcess = (NtQueryInformationProcess_Ptr)GetProcAddress(m_hNtdll, "NtQueryInformationProcess");

// 创建进程。
BOOL CreateProcess_ (__in TCHAR *processPath, __out_opt LPPROCESS_INFORMATION lpProcessInformation) {
	PROCESS_INFORMATION pi;
	TCHAR chTemp[MAX_PATH] = {TEXT('\0')};
	STARTUPINFO si = {sizeof(STARTUPINFO)};
	_tcsncpy_s(chTemp, processPath, MAX_PATH);
	BOOL bRet = ::CreateProcess(NULL, chTemp, NULL, NULL, FALSE, 0, NULL, NULL, &si, (lpProcessInformation == NULL) ? &pi : lpProcessInformation);
	return bRet;
}

// 查找进程模块。
BOOL FindProcessModule (__in DWORD dwPID, __in TCHAR *strModulePath, __in_opt BOOL isName /* = FALSE */) {
	MODULEENTRY32 module32;
	HANDLE hSnapshot;
	BOOL bRet, isFind = FALSE;

	// 获得模块快照。
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID);
	if (INVALID_HANDLE_VALUE == hSnapshot) {
		return FALSE;
	}

	module32.dwSize = sizeof(module32);			// module32.dwSize是必须设置的。
	bRet = Module32First(hSnapshot, &module32);	// 获得第一个模块的快照信息。

	// 遍历进程中所有模块的快照信息。
	while (bRet)
	{
		if ( isName ) {
			// 判断模块名是否相等。
			if (0 == StrCmpI(module32.szModule, strModulePath)) {
				isFind = TRUE;	// 找到相应模块。
				break;
			}
		} else {
			// 判断模块完整路径是否相等。
			if (0 == StrCmpI(module32.szExePath, strModulePath)) {
				isFind = TRUE;	// 找到相应模块。
				break;
			}
		}
		bRet = Module32Next(hSnapshot, &module32);	// 如果还有进程没有被遍历，则返回TRUE，否则FALSE
	}

	CloseHandle(hSnapshot);	// 一定要释放句柄
	return isFind;
}

// 获得PEB结构指针。
PEB_* FS_GetPEBPtr(__in HANDLE hProcess) {
	if ( NULL == hProcess ) {
		hProcess = GetCurrentProcess();
	}
	PROCESS_BASIC_INFORMATION_ pbi;
	NTSTATUS status = m_NtQueryInformationProcess( 
		hProcess, ProcessBasicInformation,
		(PVOID)&pbi, sizeof(PROCESS_BASIC_INFORMATION), NULL );
	if (NT_SUCCESS(status)) {
		return pbi.PebBaseAddress;
	} else {
		return NULL;
	}
}

// 获得父进程Id。
HANDLE GetParentProcessId(__in DWORD dwPID)
{
	NTSTATUS status;
	PROCESS_BASIC_INFORMATION_ pbi;

	HANDLE hProcess = OpenProcess_(dwPID);
	status = m_NtQueryInformationProcess( 
		hProcess, ProcessBasicInformation,
		(PVOID)&pbi, sizeof(PROCESS_BASIC_INFORMATION), NULL );
	CloseHandle(hProcess);
	if (NT_SUCCESS(status)) {
		return (HANDLE)pbi.InheritedFromUniqueProcessId;	// 返回父进程Id。
	}

	return (HANDLE)-1;
}

// 获得进程ID。
long GetProcessIdOfProcessName (__in TCHAR *strProcessName) {
	// PROCESSENTRY32结构体
	// 用来存放快照进程信息的一个结构体（存放进程信息和调用成员输出进程信息）。
	// 用 Process32First 指向第一个进程信息，并将进程信息抽取到 PROCESSENTRY32 中。
	// 用 Process32Next 指向下一条进程信息。
	HANDLE			hSnapshot;	// 保存快照句柄
	PROCESSENTRY32	pe32;
	BOOL			bRes;
	long			pid = -1;	// 保存PID

	// CreateToolhelp32Snapshot函数
	// 参数1：要得到进程的信息，赋为TH32CS_SNAPPROCESS
	//		 要得到线程的信息，赋为TH32CS_SNAPTHREAD
	//		 要得到指定进程的堆列表，赋为TH32CS_SNAPHEAPLIST
	//		 要得到指定进程的模块列表，赋为TH32CS_SNAPMODULE
	//		 要获取其他标记代表意义，查看MSDN
	// 参数2：指定将要快照的进程ID。如果该参数为0表示快照当前进程。
	//		 该参数只有在设置了TH32CS_SNAPHEAPLIST或TH32CS_SNAPMOUDLE后才有效，在其他情况下该参数被忽略，所有的进程都会被快照。 
	// 返回值：调用成功，返回快照的句柄，调用失败，返回INVAID_HANDLE_VALUE。
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot)
		return -1;

	pe32.dwSize = sizeof(pe32);	// pe32.dwSize是必须设置的。
	bRes = Process32First(hSnapshot, &pe32);	// 获得第一个进程的快照信息

	// 遍历所有进程的快照信息
	while (bRes)
	{
		// 如果参数和遍历出的进程名相同，表示找到了进程，则返回指定进程的PID
		if (0 == StrCmpI(pe32.szExeFile, strProcessName))
		{
			pid = pe32.th32ProcessID;	// 获得PID
			break;
		}
		bRes = Process32Next(hSnapshot, &pe32);	// 如果还有进程没有被遍历，则返回TRUE，否则FALSE
	}

	CloseHandle(hSnapshot);	// 一定要释放句柄
	return pid;
}

// 获得进程ID。
long GetProcessId(__in TCHAR *strWinName_Contain) {
	if ( NULL == strWinName_Contain ) {
		return -1;
	}
	HWND hWnd = FindTopWindow(strWinName_Contain, TRUE);
	if ( hWnd ) {
		DWORD dwPID = 0;
		GetWindowThreadProcessId(hWnd, &dwPID);
		return dwPID;
	} else {
		return -1;
	}
}

// 获得进程ID。
long GetProcessId(__in_opt TCHAR *strWinName, __in_opt TCHAR *strClassName) {
	if ( NULL == strWinName && NULL == strClassName ) {
		SetLastError(ERROR_INVALID_WINDOW_HANDLE);
		return -1;
	}
	HWND hWnd = ::FindWindow(strClassName, strWinName);
	if ( hWnd ) {
		DWORD dwPID = 0;
		GetWindowThreadProcessId(hWnd, &dwPID);
		return dwPID;
	} else {
		return -1;
	}
}

// 获得进程名。
BOOL GetProcessName (__in DWORD dwPID, __out CString &processName) {
	HANDLE hSnapshot;
	PROCESSENTRY32	pe32;
	BOOL bRes;

	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot)
		return FALSE;

	pe32.dwSize = sizeof(pe32);	// pe32.dwSize是必须设置的。
	bRes = Process32First(hSnapshot, &pe32);	// 获得第一个进程的快照信息

	// 遍历所有进程的快照信息
	while (bRes)
	{
		// 如果参数和遍历出的进程名相同，表示找到了进程，则返回指定进程的PID
		if (dwPID == pe32.th32ProcessID) {
			processName.SetString(pe32.szExeFile);
			CloseHandle(hSnapshot);	// 一定要释放句柄
			return TRUE;
		}
		bRes = Process32Next(hSnapshot, &pe32);	// 如果还有进程没有被遍历，则返回TRUE，否则FALSE
	}

	CloseHandle(hSnapshot);	// 一定要释放句柄
	SetLastError(MRLEE_ERROR_PROCESS_NOT_FOUND);	// 进程未找到。
	return FALSE;
}

// 获得进程名。
BOOL GetProcessName(__in HWND hWnd, __out CString &processName) {
	DWORD ProcessId = 0;
	GetWindowThreadProcessId(hWnd, &ProcessId);	// 获得进程Id。
	if (0 == ProcessId) {
		TRACE("【错误】GetProcessName() - 获得进程Id失败。\n");
		return FALSE;
	}
	return GetProcessName(ProcessId, processName);
}

// 获得进程名。
BOOL GetProcessName (__in_opt TCHAR *strWinName, __in_opt TCHAR *strClassName, __out CString &processName) {
	if ( NULL == strWinName && NULL == strClassName ) {
		SetLastError(ERROR_INVALID_WINDOW_HANDLE);
		return FALSE;
	}

	return GetProcessName(FindWindow(strClassName, strWinName), processName);
}

// 获得进程路径。
DWORD GetProcessPath(__in DWORD dwPID, __out CString *procPath) {
	//HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwPID);
	HANDLE hProcess = OpenProcess_(dwPID);
	if (!hProcess)
		return FALSE;
	TCHAR* procName = new TCHAR[MAX_PATH];
	ZeroMemory(procName, MAX_PATH);
	DWORD dwResult = GetModuleFileNameEx(hProcess, NULL, procName, MAX_PATH);
	CloseHandle(hProcess);
	procPath->SetString(procName);
	delete procName;
	return dwResult;
}

// 隐藏模块。隐藏当前进程中的模块。
BOOL __stdcall HideModule(__in TCHAR *strModulePath) {
	// 不能比较模块句柄是否相等，因为GetModuleHandle函数看的是模块名，而不是模块完整路径。
	//HMODULE hMod = ::GetModuleHandle(strModulePath);
	CStringW strModulePathW(strModulePath);
	PLIST_ENTRY Head,Cur;
	PPEB_LDR_DATA_ ldr;
	PLDR_DATA_TABLE_ENTRY_ ldm;
	__asm
	{
		mov eax , fs:[0x30]
		mov ecx , [eax + 0x0c] //Ldr
		mov ldr , ecx
	}
	Head = &(ldr->InLoadOrderModuleList);
	Cur = Head->Flink;
	do
	{
		ldm = CONTAINING_RECORD( Cur, LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks);
		//if( hMod == ldm->DllBase)
		if ( strModulePathW.GetLength() == ldm->FullDllName.Length / 2 ) {
			if( 0 == StrCmpNIW(strModulePathW.GetBuffer(), ldm->FullDllName.Buffer, ldm->FullDllName.Length) ) {
				ldm->InLoadOrderLinks.Blink->Flink =
					ldm->InLoadOrderLinks.Flink;
				ldm->InLoadOrderLinks.Flink->Blink =
					ldm->InLoadOrderLinks.Blink;

				ldm->InInitializationOrderLinks.Blink->Flink =
					ldm->InInitializationOrderLinks.Flink;
				ldm->InInitializationOrderLinks.Flink->Blink =
					ldm->InInitializationOrderLinks.Blink;  

				ldm->InMemoryOrderLinks.Blink->Flink =
					ldm->InMemoryOrderLinks.Flink;
				ldm->InMemoryOrderLinks.Flink->Blink =
					ldm->InMemoryOrderLinks.Blink;  
				return TRUE;
			}
		}
		Cur= Cur->Flink;
	}while(Head != Cur);
	SetLastError(ERROR_MOD_NOT_FOUND);
	return FALSE;
}

// 隐藏模块。
BOOL __stdcall HideModule(__in DWORD dwPID, __in TCHAR *strModulePath) {
	HANDLE hProcess = OpenProcess_(dwPID);
	if ( NULL == hProcess )  {
		return FALSE;
	}
	PEB_*					pPEB;
	PLIST_ENTRY				pHead = NULL, pCur = NULL;
	PEB_LDR_DATA_			Ldr = {0};
	PPEB_LDR_DATA_			pLdr = NULL;
	SIZE_T					nNumberOfBytesRead = 0;
	PLDR_DATA_TABLE_ENTRY_	pldte = NULL;
	BOOL					bRet = FALSE;
	wchar_t*				strFullDllName = NULL;
	DWORD					dwErrorLast = ERROR_SUCCESS;
	CStringW				strModulePathW;

	pPEB = FS_GetPEBPtr(hProcess);

	// 读取PEB_LDR_DATA_。
	if ( ReadProcessMemory_(hProcess, (PVOID)((ULONG_PTR)pPEB + FIELD_OFFSET(PEB_, Ldr)), &pLdr, sizeof(pLdr), &nNumberOfBytesRead) ) {
		if ( sizeof(pLdr) != nNumberOfBytesRead ) {
			dwErrorLast = GetLastError();
			goto _ret;
		}
	} else {
		dwErrorLast = GetLastError();
		goto _ret;
	}

	pHead = (PLIST_ENTRY)((ULONG_PTR)pLdr + FIELD_OFFSET(PEB_LDR_DATA_, InLoadOrderModuleList));
	// pCur = pHead->Flink;
	if ( ReadProcessMemory_(hProcess, (PVOID)((ULONG_PTR)pHead + FIELD_OFFSET(LIST_ENTRY, Flink)), &pCur, sizeof(pCur), &nNumberOfBytesRead) ) {
		if ( sizeof(pCur) != nNumberOfBytesRead ) {
			dwErrorLast = GetLastError();
			goto _ret;
		}
	} else {
		dwErrorLast = GetLastError();
		goto _ret;
	}

	//pCur = pHead->Flink;
	strModulePathW = strModulePath;
	do
	{
		pldte = CONTAINING_RECORD( pCur, LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks);

		// us = pldte->FullDllName;
		UNICODE_STRING us = {0};
		if ( ReadProcessMemory_(hProcess, (PVOID)((ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, FullDllName)), &us, sizeof(us), &nNumberOfBytesRead) ) {
			if ( sizeof(us) != nNumberOfBytesRead ) {
				dwErrorLast = GetLastError();
				goto _ret;
			}
		} else {
			dwErrorLast = GetLastError();
			goto _ret;
		}

		// 读取UNICODE_STRING.Buffer所指的字符串。
		strFullDllName = new wchar_t[us.Length / 2 + 1];
		ZeroMemory(strFullDllName, us.Length + 2);
		if ( ReadProcessMemory_(hProcess, us.Buffer, strFullDllName, us.Length, &nNumberOfBytesRead) ) {
			if ( us.Length != nNumberOfBytesRead ) {
				dwErrorLast = GetLastError();
				goto _ret;
			}
		} else {
			dwErrorLast = GetLastError();
			goto _ret;
		}

		if ( strModulePathW.GetLength() == us.Length / 2 ) {
			if( 0 == StrCmpNIW(strModulePathW.GetBuffer(), strFullDllName, us.Length / 2) ) {
				ULONG_PTR addrArray_src[1];
				ULONG_PTR addrArray_des[2];

				//////////////////////////////////////////////////////////////////////////

				// pldte->InLoadOrderLinks.Blink->Flink。
				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Flink);
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				// pldte->InLoadOrderLinks.Blink->Flink = pldte->InLoadOrderLinks.Flink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InLoadOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				// pldte->InLoadOrderLinks.Flink->Blink = pldte->InLoadOrderLinks.Blink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				//////////////////////////////////////////////////////////////////////////

				// pldte->InInitializationOrderLinks.Blink->Flink。
				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InInitializationOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Flink);
				// pldte->InInitializationOrderLinks.Flink。
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InInitializationOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				// pldte->InInitializationOrderLinks.Blink->Flink = pldte->InInitializationOrderLinks.Flink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InInitializationOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InInitializationOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				// pldte->InInitializationOrderLinks.Blink->Flink = pldte->InInitializationOrderLinks.Flink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				//////////////////////////////////////////////////////////////////////////

				// pldte->InMemoryOrderLinks.Blink->Flink。
				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InMemoryOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Flink);
				// pldte->InMemoryOrderLinks.Flink。
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InMemoryOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				// pldte->InMemoryOrderLinks.Blink->Flink = pldte->InMemoryOrderLinks.Flink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				addrArray_des[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InMemoryOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Flink);
				addrArray_des[1] = FIELD_OFFSET(LIST_ENTRY, Blink);
				addrArray_src[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, InMemoryOrderLinks) + FIELD_OFFSET(LIST_ENTRY, Blink);
				// pldte->InMemoryOrderLinks.Blink->Flink = pldte->InMemoryOrderLinks.Flink;
				if ( FALSE == RemoteCopy(hProcess, addrArray_des, 2, addrArray_src, 1, sizeof(PLIST_ENTRY)) ){
					dwErrorLast = GetLastError();
					goto _ret;
				}

				//////////////////////////////////////////////////////////////////////////
				bRet = TRUE;
				goto _ret;
			}
		}
		delete []strFullDllName;
		strFullDllName = NULL;
		// pCur= pCur->Flink;
		if ( ReadProcessMemory_(hProcess, (PVOID)((ULONG_PTR)pCur + FIELD_OFFSET(LIST_ENTRY, Flink)), &pCur, sizeof(pCur), &nNumberOfBytesRead) ) {
			if ( sizeof(pCur) != nNumberOfBytesRead ) {
				goto _ret;
			}
		} else {
			goto _ret;
		}
	}while(pHead != pCur);

_ret:
	if ( strFullDllName ) {
		delete []strFullDllName;
	}
	CloseHandle(hProcess);
	SetLastError(dwErrorLast);
	return bRet;
}
//
// 打开进程。
HANDLE OpenProcess_(__in DWORD dwPID) {
	DWORD dwLastError;
	// 1. 提升进程权限为Debug权限。
	if (!SuperPrivileges_EnableDebugPrivilege(TRUE))
		return 0;
	// 2. 打开进程。
	HANDLE hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPID);
	if ( NULL == hProcess ) {
		dwLastError = GetLastError();
		// 3. 关闭权限。
		SuperPrivileges_EnableDebugPrivilege(FALSE);
		SetLastError(dwLastError);
		return NULL;
	} else {
		// 3. 关闭权限。
		SuperPrivileges_EnableDebugPrivilege(FALSE);
		return hProcess;
	}
}

// 通过进程名打开进程。
HANDLE OpenProcess_ (__in TCHAR *processName)
{
	DWORD dwPID = GetProcessId(processName);	// 获得PID
	if (!dwPID)
		return NULL;
	return OpenProcess_(dwPID);
}

// 读进程内存。
BOOL __stdcall ReadProcessMemory_(
								  __in      HANDLE hProcess,
								  __in      LPCVOID lpBaseAddress,
								  __out_bcount_part(nSize, *lpNumberOfBytesRead) LPVOID lpBuffer,
								  __in      SIZE_T nSize,
								  __out_opt SIZE_T * lpNumberOfBytesRead
								  )
{
	BOOL bRet = ::ReadProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesRead);
	if (bRet) {
		return TRUE;
	} else {
		DWORD dwOldProtect;
		bRet = ::VirtualProtectEx(hProcess, (PVOID)lpBaseAddress, nSize, PAGE_EXECUTE_READWRITE, &dwOldProtect);
		if (bRet) {
			bRet = ::ReadProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesRead);
			::VirtualProtectEx(hProcess, (PVOID)lpBaseAddress, nSize, dwOldProtect, &dwOldProtect);
			return bRet;
		} else {
			return -1;
		}
	}
}

// 读进程内存 - 多级指针。
BOOL __stdcall ReadProcessMemory_MulLevelPtrBytes(
	__in HANDLE hProcess, 
	__in ULONG_PTR aBaseAddress[], 
	__in int arraySize, 
	__out LPVOID lpBuffer, 
	__in SIZE_T nSize, 
	__out_opt SIZE_T * lpNumberOfBytesRead /* = NULL */)
{
	ULONG_PTR address = 0;
	int iBufSize = sizeof(aBaseAddress[0]);
	// 循环读取地址数组中的内存
	// 原理如：[[[00b15f50]+1c]+268]
	for (int i = 0; i < arraySize; i++)
	{
		address += aBaseAddress[i];
		if ((arraySize - 1) == i) {
			if ( FALSE == ReadProcessMemory_(hProcess, (LPVOID)(address), lpBuffer, nSize, lpNumberOfBytesRead) ){
				return FALSE;
			}
		} else {
			if ( FALSE == ReadProcessMemory_(hProcess, (LPVOID)(address), &address, iBufSize, lpNumberOfBytesRead) ) {
				return FALSE;
			}
		}
	}

	return TRUE;
}

// 远程拷贝。源操作数和目的操作数都在远程进程中。
BOOL __stdcall RemoteCopy ( 
						   __in HANDLE hProcess, 
						   __in ULONG_PTR aBaseAddress_des[], 
						   __in int arraySize_des, 
						   __in ULONG_PTR aBaseAddress_src[], 
						   __in int arraySize_src, 
						   __in SIZE_T nSize, 
						   __out_opt SIZE_T * lpNumberOfBytesWritten /* = NULL */)
{
	BYTE *lpBuffer = new BYTE[nSize];
	if ( NULL == lpBuffer ) {
		SetLastError(MRLEE_ERROR_MEMORY_ALLOTTED_FAILURE);	// 内存分配失败。
		return FALSE;
	}
	ZeroMemory(lpBuffer, nSize);

	BOOL bRet = FALSE;

	// 读源地址处的数据。
	SIZE_T nNumberOfBytesRead = 0;
	if ( ReadProcessMemory_MulLevelPtrBytes(hProcess, aBaseAddress_src, arraySize_src, lpBuffer, nSize, &nNumberOfBytesRead) ) {
		if ( nNumberOfBytesRead != nSize )
			goto _ret;
	} else {
		goto _ret;
	}

	// 将原地址处的数据写入目的地址。
	if ( FALSE == WriteProcessMemory_MulLevelPtrBytes(hProcess, aBaseAddress_des, arraySize_des, lpBuffer, nSize, lpNumberOfBytesWritten) ) {
		goto _ret;
	}

	bRet = TRUE;

_ret:
	delete []lpBuffer;
	return bRet;
}

// 调用远程进程中的函数。
BOOL __stdcall RemoteFunctionCall ( 
	__in HANDLE hProcess, 
	__in LPTHREAD_START_ROUTINE pFunAddr, 
	__in BOOL isWait /* = TRUE */, 
	__out_opt LPDWORD pFunRet /* = NULL */, 
	__in_opt LPVOID lpParamBuffer /* = NULL */, 
	__in SIZE_T paramSize /* = 0 */ )
{
	LPVOID lpBufferRemote = NULL;
	HANDLE hThread = NULL;
	BOOL bRet = TRUE;
	DWORD dwRet;

	if (NULL != lpParamBuffer && 0 != paramSize) {
		// 将参数内容写入远程进程。
		lpBufferRemote = WriteProcessMemory_(hProcess, lpParamBuffer, paramSize);
		if (NULL == lpBufferRemote) {
			TRACE("【错误】RemoteFunctionCall_CopyTo() - 将参数内容写入远程进程失败。\n");
			bRet = FALSE;
			goto _return;
		}
	} else {
		TRACE("【提示】CInjectDll_RemoteLoad::InjectDll() - 参数lpBuffer或bufferSize为0。");
	}

	// 在远程进程中调用写入的函数。
	hThread = ::CreateRemoteThread(hProcess, 
		NULL, 
		0,	
		(LPTHREAD_START_ROUTINE)pFunAddr, 
		lpBufferRemote, 
		0, 
		NULL);
	// Failed
	if(NULL == hThread) {
		TRACE("【错误】CInjectDll_RemoteLoad::InjectDll() - 创建远程线程，加载动态链接库失败。\n");
		bRet = FALSE;
		goto _return;
	}

	if ( isWait ) {
		// 等待线程中的 LoadLibrary() 函数执行完毕，线程返回。
		::WaitForSingleObject(hThread, INFINITE);
		::GetExitCodeThread(hThread, &dwRet);
	}

_return:
	if (lpBufferRemote)
		::VirtualFreeEx(hProcess, lpBufferRemote, paramSize, MEM_RELEASE);
	if (hThread)
		::CloseHandle(hThread);
	if ( pFunRet && isWait )
		*pFunRet = dwRet;
	return bRet;
}

// 在远程线程中调用当前进程的函数。
BOOL RemoteFunctionCall_CopyTo( 
							   __in HANDLE hProcess, 
							   __in LPTHREAD_START_ROUTINE pFunAddr, 
							   __in SIZE_T funSize, 
							   __in BOOL isWait /* = TRUE */, 
							   __out_opt LPDWORD pFunRet /* = NULL */, 
							   __in_opt LPVOID lpParamBuffer /* = NULL */, 
							   __in SIZE_T paramSize /* = 0 */ )
{
	LPVOID lpFunAddrRemote = NULL;
	LPVOID lpBufferRemote = NULL;
	HANDLE hThread = NULL;
	BOOL bRet = TRUE;
	DWORD dwRet;

	// 将函数内容写入远程进程。
	lpFunAddrRemote = WriteFuncToProcessMemory(hProcess, pFunAddr, funSize);
	if (NULL == lpFunAddrRemote) {
		TRACE("【错误】RemoteFunctionCall_CopyTo() - 将函数内容写入远程进程失败。\n");
		return FALSE;
	}

	if (NULL != lpParamBuffer && 0 != paramSize) {
		// 将参数内容写入远程进程。
		lpBufferRemote = WriteProcessMemory_(hProcess, lpParamBuffer, paramSize);
		if (NULL == lpBufferRemote) {
			TRACE("【错误】RemoteFunctionCall_CopyTo() - 将参数内容写入远程进程失败。\n");
			bRet = FALSE;
			goto _return;
		}
	} else {
		TRACE("【提示】CInjectDll_RemoteLoad::InjectDll() - 参数lpBuffer或bufferSize为0。");
	}

	// 在远程进程中调用写入的函数。
	hThread = ::CreateRemoteThread(hProcess, 
		NULL, 
		0,	
		(LPTHREAD_START_ROUTINE)lpFunAddrRemote, 
		lpBufferRemote, 
		0, 
		NULL);
	// Failed
	if(NULL == hThread) {
		TRACE("【错误】CInjectDll_RemoteLoad::InjectDll() - 创建远程线程，加载动态链接库失败。\n");
		bRet = FALSE;
		goto _return;
	}

	if ( isWait ) {
		// 等待线程中的 LoadLibrary() 函数执行完毕，线程返回。
		::WaitForSingleObject(hThread, INFINITE);
		::GetExitCodeThread(hThread, &dwRet);
	}


_return:
	::VirtualFreeEx(hProcess, lpFunAddrRemote, funSize, MEM_RELEASE);
	if (lpBufferRemote)
		::VirtualFreeEx(hProcess, lpBufferRemote, paramSize, MEM_RELEASE);
	if (hThread)
		::CloseHandle(hThread);
	if ( pFunRet && isWait )
		*pFunRet = dwRet;
	return bRet;
}

// 【慎用】恢复进程运行(其实是恢复进程中的所有线程)。
BOOL __stdcall ResumeProcess(__in DWORD dwProcessId)
{
	return SuspendProcessSwitch(dwProcessId, false);
}

// 设置映射路径。
BOOL SetImagePath(__in HANDLE hProcess, __in UNICODE_STRING &szImagePath)
{
	PEB_* pPeb = FS_GetPEBPtr(hProcess);	// 获得PEB指针。

	if (pPeb) {
		//////////////////////////////////////////////////////////////////////////
		//ULONG_PTR addrArray[10] = {0};
		//addrArray[0] = (ULONG_PTR)pPeb + FIELD_OFFSET(PEB, ProcessParameters);	// 读取PPEB->ProcessParameters处的数据。
		//addrArray[1] = FIELD_OFFSET(RTL_USER_PROCESS_PARAMETERS_, ImagePathName) + FIELD_OFFSET(UNICODE_STRING, Buffer);// 读取[PPEB->ProcessParameters].ImagePathName.Buffer处的数据。
		//addrArray[2] = 0;	// 读取[[PPEB->ProcessParameters].ImagePathName.Buffer]处的数据。
		//wchar_t imagePath[MAX_PATH] = {L'\0'};
		//ReadProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 3, imagePath, sizeof(imagePath));
		//OutputDebugStringW(imagePath);

		ULONG_PTR addrArray1[10] = {0};
		addrArray1[0] = (ULONG_PTR)pPeb + FIELD_OFFSET(PEB_, ProcessParameters);	// 读取PPEB->ProcessParameters处的数据。
		addrArray1[1] = FIELD_OFFSET(RTL_USER_PROCESS_PARAMETERS_, ImagePathName);

		LPVOID lpBuffer = VirtualAllocEx_(hProcess, szImagePath.MaximumLength);
		WriteProcessMemory_(hProcess, lpBuffer, szImagePath.Buffer, szImagePath.MaximumLength, NULL);	// 将新映射路径写入目标进程。
		szImagePath.Buffer = (PWSTR)lpBuffer;
		WriteProcessMemory_MulLevelPtrBytes(hProcess, addrArray1, 2, (LPCVOID)&szImagePath, sizeof(UNICODE_STRING));
		//printf("lpBuffer=0x%08X\n", lpBuffer);
		//printf("ImagePathName Addr: 0x%08X\nszImagePath.Buffer: 0x%08X\n", &pPeb->ProcessParameters->ImagePathName, szImagePath.Buffer);
		return TRUE;
	} else {
		return FALSE;
	}
}

// 设置映射路径。
BOOL SetImagePath(__in DWORD dwPID, __in UNICODE_STRING &szImagePath) {
	HANDLE hProcess = OpenProcess_(dwPID);
	if (hProcess) {
		BOOL bRet = SetImagePath(hProcess, szImagePath);
		CloseHandle(hProcess);
		return bRet;
	} else {
		return FALSE;
	}
}

// 设置当前进程的映射路径。
BOOL SetImagePath_CurProc(__in UNICODE_STRING &szImagePath) {
	PEB_* pPeb = FS_GetPEBPtr(GetCurrentProcess());	// 获得PEB指针。
	if (pPeb) {
		//printf("ImagePathName Addr: 0x%08X\nszImagePath.Buffer: 0x%08X\n", &pPeb->ProcessParameters->ImagePathName, szImagePath.Buffer);
		pPeb->ProcessParameters->ImagePathName.Buffer = szImagePath.Buffer;
		pPeb->ProcessParameters->ImagePathName.Length = szImagePath.Length;
		pPeb->ProcessParameters->ImagePathName.MaximumLength = szImagePath.MaximumLength;
		return TRUE;
	} else {
		return FALSE;
	}
}

// 终止进程。
BOOL TerminateProcess_ (__in DWORD dwPID) {
	HANDLE handle = OpenProcess_(dwPID);
	if (NULL == handle) {
		TRACE("【错误】TerminateProcess_() - 打开进程失败。\n");
		return FALSE;
	}
	BOOL bRet = ::TerminateProcess(handle, 0);
	CloseHandle(handle);
	return bRet;
}

// 将函数写入进程内存。
PVOID  __stdcall WriteFuncToProcessMemory (
	__in HANDLE hProcess, 
	__in_bcount(nSize) LPCVOID lpBuffer,
	__in SIZE_T dwSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten /* = NULL */
	)
{
	return WriteProcessMemory_(hProcess, WinSys::GetJmpTableFuncAddr((PVOID)lpBuffer), dwSize, lpNumberOfBytesWritten);
}

// 写进程内存。
BOOL __stdcall WriteProcessMemory_(
								   __in      HANDLE hProcess,
								   __in      LPVOID lpBaseAddress,
								   __in_bcount(nSize) LPCVOID lpBuffer,
								   __in      SIZE_T nSize,
								   __out_opt SIZE_T * lpNumberOfBytesWritten
								   )
{
	DWORD dwOldProtect;
	DWORD dwLastError = ERROR_SUCCESS;
	BOOL bRet = ::VirtualProtectEx(hProcess, (PVOID)lpBaseAddress, nSize, PAGE_EXECUTE_READWRITE, &dwOldProtect);
	if (bRet) {
		bRet = ::WriteProcessMemory(hProcess, lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesWritten);
		if ( FALSE == bRet ) {
			dwLastError = GetLastError();
		}
		::VirtualProtectEx(hProcess, (PVOID)lpBaseAddress, nSize, dwOldProtect, &dwOldProtect);
		SetLastError(dwLastError);
		return bRet;
	} else {
		return FALSE;
	}
}

// 写进程内存。
PVOID __stdcall WriteProcessMemory_(
									__in HANDLE hProcess, 
									__in_bcount(nSize) LPCVOID lpBuffer,
									__in SIZE_T dwSize, 
									__out_opt SIZE_T * lpNumberOfBytesWritten /* = NULL */
									)
{
	// Allocate memory in the remote process to store the szLibPath string
	PVOID pRemote = ::VirtualAllocEx(hProcess, NULL, dwSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (NULL == pRemote) {
		TRACE("【错误】WriteProcessMemory_() - 在远程进程中分配内存失败。\n");
		return NULL;
	}

	// 将dll路径拷贝到远程进程中。
	if (!::WriteProcessMemory(hProcess, pRemote, lpBuffer, dwSize, lpNumberOfBytesWritten)) {
		TRACE("【错误】WriteProcessMemory_() - 写远程进程内存失败。\n");
		::VirtualFreeEx(hProcess, pRemote, dwSize, MEM_RELEASE);
		return NULL;
	}

	return pRemote;
}

// 写进程内存 - 多级指针。
BOOL WriteProcessMemory_MulLevelPtrBytes(
	__in HANDLE hProcess, __in ULONG_PTR aBaseAddress[], 
	__in int arraySize, __in LPCVOID lpBuffer, __in SIZE_T nSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten /* = NULL */)
{
	ULONG_PTR address = 0;
	if ( 1 == arraySize ) {
		return WriteProcessMemory_(hProcess, (LPVOID)(aBaseAddress[0]), lpBuffer, nSize, lpNumberOfBytesWritten);
	}

	SIZE_T nNumberOfBytesRead = 0;
	if ( ReadProcessMemory_MulLevelPtrBytes(hProcess, aBaseAddress, arraySize - 1, &address, sizeof(address), &nNumberOfBytesRead) ) {
		if ( sizeof(address) != nNumberOfBytesRead ) {
			return FALSE;
		}
		return WriteProcessMemory_(hProcess, (LPVOID)(address + aBaseAddress[arraySize - 1]), lpBuffer, nSize, lpNumberOfBytesWritten);
	} else {
		return FALSE;
	}
}

// 暂停进程开关（慎用）。
BOOL SuspendProcessSwitch(__in DWORD dwProcessId, __in BOOL bSuppend)
{
	// 获得系统中所有线程的列表。
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, dwProcessId);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		THREADENTRY32 te = {sizeof(te)};
		BOOL bOK = (BOOL)::Thread32Next(hSnapshot, &te);
		for (; bOK; bOK = (BOOL)::Thread32Next(hSnapshot, &te))
		{
			// 判断线程的进程Id是否等于dwProcessId。
			if (te.th32OwnerProcessID == dwProcessId)
			{
				// 打开线程，获得线程句柄。
				HANDLE hThread = ::OpenThread(THREAD_SUSPEND_RESUME, FALSE, te.th32ThreadID);
				if (hThread != NULL)
				{
					if (bSuppend)
						SuspendThread(hThread);
					else
						ResumeThread(hThread);
				}
				CloseHandle(hThread);
			}
		}
		CloseHandle(hSnapshot);
		return true;
	} 
	else
	{
		return false;
	}
}

// 【慎用】暂停进程(其实是暂停进程中的所有线程)。
BOOL SuspendProcess(__in DWORD dwProcessId)
{
	return SuspendProcessSwitch(dwProcessId, true);
}

//////////////////////////////////////////////////////////////////////////
CProcessBase::CProcessBase( __in DWORD dwProcessId )
: m_Pid(dwProcessId), m_hProcess(NULL)
{
	SuperPrivileges_EnableDebugPrivilege(TRUE);	// 提高当前进程特权。
	m_hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
}
// 析构函数。
CProcessBase::~CProcessBase(){
	// 关闭进程句柄。
	if (m_hProcess)
		CloseHandle(m_hProcess);
}

// 获得进程句柄。
HANDLE CProcessBase::GetProcessHandle() {
	return m_hProcess;
}

// 返回打开进程是否成功。
BOOL CProcessBase::IsOpenSuccess() {
	return (m_hProcess == NULL) ? FALSE : TRUE;
}

// 读取进程内存中的一个字节数组。
int CProcessBase::ReadMemoryBytes(__in LPCVOID lpBaseAddress, __out BYTE* data, __in SIZE_T nSize, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return ReadProcessMemory_(m_hProcess, lpBaseAddress, data, nSize, lpNumberOfBytesRead);
}

// 读取进程内存中的一个32位整数值。
BOOL CProcessBase::ReadMemoryInt32(__in LPCVOID lpBaseAddress, __out INT32* data, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return ReadProcessMemory_(m_hProcess, lpBaseAddress, data, 4, lpNumberOfBytesRead);
}

// 读取进程内存中的一个64位整数值。
BOOL CProcessBase::ReadMemoryInt64(__in LPCVOID lpBaseAddress, __out INT64* data, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return ReadProcessMemory_(m_hProcess, lpBaseAddress, data, 8, lpNumberOfBytesRead);
}

// 将一个字节数组写入进程内存。
BOOL CProcessBase::WriteMemoryBytes(__in LPVOID lpBaseAddress, __in BYTE* data, __in SIZE_T nSize, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return WriteProcessMemory_(m_hProcess, lpBaseAddress, data, nSize, lpNumberOfBytesRead);
}

// 将一个32位整数写入进程内存。
BOOL CProcessBase::WriteMemoryInt32(__in LPVOID lpBaseAddress, __in INT32 data, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return WriteProcessMemory_(m_hProcess, lpBaseAddress, (LPCVOID)(&data), 4, lpNumberOfBytesRead);
}

// 将一个64位整数写入进程内存。
int CProcessBase::WriteMemoryInt64(__in LPVOID lpBaseAddress, __in INT64 data, __out_opt SIZE_T * lpNumberOfBytesRead /* = 0 */) {
	return WriteProcessMemory_(m_hProcess, lpBaseAddress, (LPCVOID)(&data), 8, lpNumberOfBytesRead);
}