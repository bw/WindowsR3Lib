#include "stdafx.h"
#include "PeLoaderBase.h"

using namespace Mrlee_R3::File;

CPeLoaderBase::CPeLoaderBase()
	: m_ImageCodeOffset(0), 
	  m_ImageDataOffset(0), 
	  m_DataDirectory(NULL), 
	  m_DataDirectoryCount(0), 
	  m_DosHeader(NULL), 
	  m_FileHeader(NULL), 
	  m_FileAlignmentSize(0), 
	  m_FilePoint(NULL), 
	  m_FileSize(0), 
	  m_FileTypeMagic(0), 
	  m_IsLoad(FALSE), 
	  m_IsPE64(FALSE), 
	  m_MemoryAlignmentSize(0), 
	  m_NumberOfSections(0), 
	  m_PeHeaderSize(0), 
	  m_SectionHeader(NULL)
{
}

CPeLoaderBase::~CPeLoaderBase() {
}

// 将数据在文件中的地址转换成内存中的RVA。
long CPeLoaderBase::FOAToRVA(__in DWORD fileAddress)
{
	const IMAGE_SECTION_HEADER* ish = GetImageSectionHeader();// 获得节表起始地址
	// 遍历节表
	for (int i = 0; i < m_NumberOfSections; i++)
	{
		// 判断是文件地址否在当前节的范围中
		if (fileAddress >= ish[i].PointerToRawData && fileAddress < (ish[i].PointerToRawData + ish[i].SizeOfRawData))
		{
			DWORD fileOffset = fileAddress - ish[i].PointerToRawData;	// 获得参数对于当前节的地址偏移
			return fileOffset + ish[i].VirtualAddress;		// 偏移再加上相对于文件的偏移
		}
	}
	return -1;// 遍历所有节后，还未找到传入的地址，则返回-1
}

// 根据指定对齐粒度，计算对其后的大小。
DWORD CPeLoaderBase::GetAlignedSize(__in DWORD dwCurSize, __in DWORD dwAlignment) {
	DWORD dwResult = 0;
	// 如果PE中的数据在文件中占用的空间，没有刚好与内存中的对齐粒度相符合，
	// 那么数据在映射入内存的时候就要按照内存的对齐粒度去扩展。
	// 数据是不会变的，只是简单的将对齐粒度内，未占用的内存补0。
	if (dwCurSize % dwAlignment == 0)
	{
		dwResult = dwCurSize;
	}
	else
	{
		int val = dwCurSize / dwAlignment;
		val++;	// 因为 dwVirtualSize % dwMemoryAlignment != 0 所以要加一
		dwResult = val * dwAlignment;
	}
	return dwResult;
}

// 获得Dos头大小。
UINT CPeLoaderBase::GetDosHeaderSize() {
	return sizeof(IMAGE_DOS_HEADER);
}

// 获得文件指针。
PVOID CPeLoaderBase::GetFilePoint() {
	return m_FilePoint;
}

// 获得导出函数名地址。
DWORD* CPeLoaderBase::GetAddressOfExportNames(__out_opt DWORD *numberOfNames) {
	const IMAGE_EXPORT_DIRECTORY *exportDirectory = GetImageExportDirectory();	// 获得导出表地址。
	if (NULL == exportDirectory) {
		OutputDebugString(TEXT("【错误】CPeLoaderBase::GetAddressOfExportNames() - 获得导出表指针失败。\n"));
		return NULL;
	}

	long foa = RVAToFOA(exportDirectory->AddressOfNames);	// 获得导出函数名的偏移。
	if (-1 == foa) {
		OutputDebugString(TEXT("【错误】CPeLoaderBase::GetAddressOfExportNames() - RVA转换FOA失败。\n"));
		return NULL;
	}

	if (NULL != numberOfNames) {
		*numberOfNames = exportDirectory->NumberOfNames;
	}
	return (DWORD*)((BYTE*)m_FilePoint + (DWORD)foa);
}

// 获得文件大小。
LONGLONG CPeLoaderBase::GetFileSize_() {
	return m_FileSize;
}

// 获得文件映射入内存后的大小。
const IMAGE_BASE_RELOCATION_* CPeLoaderBase::GetImageBaseRelocation () {
	const IMAGE_DATA_DIRECTORY* data = GetImageDataDirectory(PeDataDirectory_BaseRelocationTable);
	long foa = RVAToFOA(data->VirtualAddress);
	if (-1 == foa) {
		return NULL;
	} else {
		return (IMAGE_BASE_RELOCATION_*)((BYTE*)m_FilePoint + (DWORD)foa);
	}
}

// 获得重定位表项的个数。
DWORD CPeLoaderBase::GetImageBaseRelocationItemNum (__in const IMAGE_BASE_RELOCATION_ *relocationTable, __out_opt DWORD *size) {
	// sizeof(IMAGE_BASE_RELOCATION_) == 8。
	DWORD relocationItemCount = (relocationTable->SizeOfBlock - 8) / 2;	// 计算表项个数。
	if (size) {
		// sizeof(IMAGE_BASE_RELOCATION_) == 8。
		*size = 8 + 2 * relocationItemCount;
	}
	return relocationItemCount;
}

// 获得映射代码的偏移。
PVOID CPeLoaderBase::GetImageCodeOffset() {
	return m_ImageCodeOffset;
}

// 获得指向数据目录起始位置的指针。数据目录的结构体是IMAGE_DATA_DIRECTORY。
const IMAGE_DATA_DIRECTORY* CPeLoaderBase::GetImageDataDirectory() {
	return m_DataDirectory;
}

// 获得指向特定数据目录结构的指针。数据目录的结构体是IMAGE_DATA_DIRECTORY。
const IMAGE_DATA_DIRECTORY* CPeLoaderBase::GetImageDataDirectory(__in PeDataDirectory peDataDirectory) {
	return &(m_DataDirectory[peDataDirectory]);
}

// 获得映射数据的偏移。
PVOID CPeLoaderBase::GetImageDataOffset() {
	return m_ImageDataOffset;
}

// 获得延迟加载导入表描述符指针。
const IMAGE_DELAY_IMPORT_DESCRIPTOR* CPeLoaderBase::GetImageDelayImportDescriptor() {
	long foa = RVAToFOA(GetImageDataDirectory(PeDataDirectory_DelayImportDescriptor)->VirtualAddress);
	if (-1 == foa) {
		OutputDebugString(TEXT("【错误】CPeLoaderBase::GetImageExportDirectory() - RVA转换FOA失败。\n"));
		return NULL;
	} else {
		return (const IMAGE_DELAY_IMPORT_DESCRIPTOR*)((BYTE*)m_FilePoint + (DWORD)foa);
	}
}

// 获得指向IMAGE_DOS_HEADER的指针。
const IMAGE_DOS_HEADER* CPeLoaderBase::GetImageDosHeader() {
	return m_DosHeader;
}

// 获得导出目录指针。
const IMAGE_EXPORT_DIRECTORY* CPeLoaderBase::GetImageExportDirectory() {
	long foa = RVAToFOA(GetImageDataDirectory(PeDataDirectory_ExportTable)->VirtualAddress);
	if (-1 == foa) {
		OutputDebugString(TEXT("【错误】CPeLoaderBase::GetImageExportDirectory() - RVA转换FOA失败。\n"));
		return NULL;
	} else {
		return (const IMAGE_EXPORT_DIRECTORY*)((BYTE*)m_FilePoint + (DWORD)foa);
	}
}

// 获得指向IMAGE_FILE_HEADER的指针。
const IMAGE_FILE_HEADER* CPeLoaderBase::GetImageFileHeader() {
	return m_FileHeader;
}

// 获得文件映射入内存后的大小。
DWORD CPeLoaderBase::GetImageFileSize() {
	DWORD dwResult = 0;	// 返回的结果

	// 获得对其后的尺寸
	dwResult += GetAlignedSize(m_PeHeaderSize, m_MemoryAlignmentSize);

	DWORD dwVirtualSize = 0;
	// 遍历所有的节，累加节的大小
	for (DWORD i = 0; i < m_NumberOfSections; i++)
	{
		// 获得节在文件中的长度
		dwVirtualSize = m_SectionHeader[i].Misc.VirtualSize;
		if (dwVirtualSize)
		{
			// 获得对其后的尺寸
			dwResult += GetAlignedSize(dwVirtualSize, m_MemoryAlignmentSize);
		}
	}
	return dwResult;
}

// 获得资源目录地址。
const IMAGE_RESOURCE_DIRECTORY* CPeLoaderBase::GetImageResourceDirectory() {
	long foa = RVAToFOA(GetImageDataDirectory(PeDataDirectory_ResourceTable)->VirtualAddress);
	if (-1 == foa) {
		return NULL;
	} else {
		return (const IMAGE_RESOURCE_DIRECTORY*)((BYTE*)m_FilePoint + foa);
	}
}

// 获得特定的资源目录项。
const IMAGE_RESOURCE_DIRECTORY_ENTRY* CPeLoaderBase::GetImageResourceDirectoryEntry1(__in DWORD type, __out_opt IMAGE_RESOURCE_DIRECTORY **resDir2 /* = NULL */) {
	const IMAGE_RESOURCE_DIRECTORY *resDir = GetImageResourceDirectory();
	if (NULL == resDir) {
		return NULL;	// 获得资源目录失败，则返回NULL。
	}
	DWORD typeTemp = 0;
	BOOL isStandardType = (type & 0x80000000) == 0;	// 是否是标准类型。高位为1，表示不是标准类型。高位为0，表示是标准类型。
	DWORD dwResNum = resDir->NumberOfNamedEntries + resDir->NumberOfIdEntries;
	IMAGE_RESOURCE_DIRECTORY_ENTRY *resDirEntry = (IMAGE_RESOURCE_DIRECTORY_ENTRY *)((BYTE*)resDir + sizeof(IMAGE_RESOURCE_DIRECTORY));	// 获得数据项。
	int i = 0;
	while (dwResNum > 0) {
		typeTemp = resDirEntry[i].Name & 0x80000000;
		// 最高位为0则说明是标准类型。
		if (0 == typeTemp && isStandardType) {
			// 判断是否是菜单资源。
			if ((WORD)type == (WORD)(resDirEntry[i].Name)) {
				break;
			}
		} else if ( (0x80000000 == typeTemp) && (FALSE == isStandardType) ){
			// 非标准类型。
			IMAGE_RESOURCE_DIR_STRING_U *irds = (IMAGE_RESOURCE_DIR_STRING_U *)((BYTE*)resDir + (type & 0x7FFFFFFF));			// 参数1所指向的类型地址。
			IMAGE_RESOURCE_DIR_STRING_U *irdsTemp = (IMAGE_RESOURCE_DIR_STRING_U *)((BYTE*)resDir + (typeTemp & 0x7FFFFFFF));	// 当前资源项的类型字符串。
			
			if (irds->Length == irdsTemp->Length) {
				int j = 0;
				for (; j < irds->Length; j++) {
					if (irds->NameString[j] != irdsTemp->NameString[j]) {
						break;
					}
				}
				if (j == irds->Length) {
					break; 
				}
			}
		}
		i++;
		dwResNum--;
	}

	if (dwResNum <= 0) {
		return NULL;
	}

	const IMAGE_RESOURCE_DIRECTORY_ENTRY *resDirEntryRet = (const IMAGE_RESOURCE_DIRECTORY_ENTRY*)(&resDirEntry[i]);	// 获得资源目录项。
	if (NULL != resDir2) {
		DWORD OffsetToDirectory = resDirEntryRet->OffsetToData & 0x7FFFFFFF;	// 求其他目录项。
		*resDir2 = (IMAGE_RESOURCE_DIRECTORY *)((BYTE*)resDir + OffsetToDirectory);	// 获得菜单数据目录。
	}
	return resDirEntryRet;
}

// 获得对话框的资源目录项。
const IMAGE_RESOURCE_DIRECTORY_ENTRY* CPeLoaderBase::GetImageResourceDirectoryEntryOfDialog(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfDialog2 /* = NULL */) {
	return GetImageResourceDirectoryEntry1((DWORD)RT_DIALOG, resDirOfDialog2);
}

// 获得图标的资源目录项。
const IMAGE_RESOURCE_DIRECTORY_ENTRY* CPeLoaderBase::GetImageResourceDirectoryEntryOfIcon(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfIcon2 /* = NULL */) {
	return GetImageResourceDirectoryEntry1((DWORD)RT_ICON, resDirOfIcon2);
}

// 获得图标组的资源目录项。
const IMAGE_RESOURCE_DIRECTORY_ENTRY* CPeLoaderBase::GetImageResourceDirectoryEntryOfIconGroup(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfIconGroup2 /* = NULL */) {
	return GetImageResourceDirectoryEntry1((DWORD)RT_GROUP_ICON, resDirOfIconGroup2);
}

// 获得菜单的资源目录项。
const IMAGE_RESOURCE_DIRECTORY_ENTRY* CPeLoaderBase::GetImageResourceDirectoryEntryOfMenu(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfMenu2 /* = NULL */) {
	return GetImageResourceDirectoryEntry1((DWORD)RT_MENU, resDirOfMenu2);
}

// 获得PE文件中节的个数。
DWORD CPeLoaderBase::GetNumberOfSections() {
	return m_NumberOfSections;
}

// 获得指向节表起始位置的指针。节表的数据结构是IMAGE_SECTION_HEADER。
const IMAGE_SECTION_HEADER* CPeLoaderBase::GetImageSectionHeader() {
	return m_SectionHeader;
}

// 获得Pe头大小。
UINT CPeLoaderBase::GetPeHeaderSize() {
	// m_FileHeader.SizeOfOptionalHeader 指定结构IMAGE_OPTIONAL_HEADER的长度。
	// 默认情况下这个值为0x00E0，如果是64位PE文件该结构的默认大小为0x00F0。
	return m_PeHeaderSize;
}

// 获得Pe文件的类型。
PeFileType CPeLoaderBase::GetPeType() {
	if (0x010B == m_FileTypeMagic) {
		return PeFileType_Pe32;	// 32位。
	} else if (0x0107 == m_FileTypeMagic) {
		return PeFileType_ROM;
	} else if (0x020B == m_FileTypeMagic) {
		return PeFileType_PE64;
	} else {
		return PeFileType_Unknown;	// 未知类型。
	}
}

// 获得资源类型名。
int CPeLoaderBase::GetResourceTypeName(__in DWORD typeId, __in DWORD directoryLevel, __out CStringW &typeName) {
	static const IMAGE_RESOURCE_DIRECTORY *resourceDirectory = GetImageResourceDirectory();
	// 最高位为1吗？
	if ((typeId & 0x80000000) == 0x80000000) {
		// 非标准类型。
		IMAGE_RESOURCE_DIR_STRING_U *irds = (IMAGE_RESOURCE_DIR_STRING_U *)((BYTE*)resourceDirectory + (typeId & 0x7FFFFFFF));
		typeName.SetString(irds->NameString, irds->Length);
		return -1;
	} else {
		// 标准类型。
		WORD id = (WORD)typeId;
		if (1 == directoryLevel) {
			switch (id)
			{
			case RT_CURSOR:		// 光标。
				typeName = L"光标";
				break;
			case RT_BITMAP:		// 位图。
				typeName = L"位图";
				break;
			case RT_ICON:		// 图标。
				typeName = L"图标";
				break;
			case RT_MENU:		// 菜单。
				typeName = L"菜单";
				break;
			case RT_DIALOG:		// 对话框。
				typeName = L"对话框";
				break;
			case RT_STRING:		// 字符串。
				typeName = L"字符串";
				break;
			case RT_FONTDIR:	// 字体目录。
				typeName = L"字体目录";
				break;
			case RT_FONT:		// 字体。
				typeName = L"字体";
				break;
			case RT_ACCELERATOR:// 加速键。
				typeName = L"加速键";
				break;
			case RT_RCDATA:		// 未格式化资源。
				typeName = L"未格式化资源";
				break;
			case RT_MESSAGETABLE:// 消息表。
				typeName = L"消息表";
				break;
			case RT_GROUP_CURSOR:// 光标组。
				typeName = L"光标组";
				break;
			case RT_GROUP_ICON:	// 图标组。
				typeName = L"图标组";
				break;
			case RT_VERSION:	// 版本。
				typeName = L"版本";
				break;
			default:
				return -1;
			}
		} else if (2 == directoryLevel) {
			typeName.Format(L"%d", id);	// id是标准的命名，这个整数标识符ID来定义名字。
		} else if (3 == directoryLevel) {
			// 标准语言代码。
			switch (id)
			{
			case 2052:
				typeName = L"简体中文";
				break;
			default:
				typeName.Format(L"未识别的语言代码 - %d", id);
				break;
			}
		}
		return 1;
	}
}

// 获得资源类型名。
int CPeLoaderBase::GetResourceTypeName(__in const IMAGE_RESOURCE_DIRECTORY_ENTRY *resourceDirectoryEntry, __in DWORD directoryLevel, __out CStringW &typeName) {
	return GetResourceTypeName(resourceDirectoryEntry->Name, directoryLevel, typeName);
}

// 是否是32为应用程序。
int CPeLoaderBase::Is32BitApplication () {
	// 在32位平台上运行？
	if (IMAGE_FILE_MACHINE_I386 == m_FileHeader->Machine) {
		return 1;
	}

	// 在64位平台上运行？
	if (IMAGE_FILE_MACHINE_IA64 == m_FileHeader->Machine || IMAGE_FILE_MACHINE_AMD64 == m_FileHeader->Machine) {
		return 0;
	}

	return -1;
}

// 是否加载成功。
BOOL CPeLoaderBase::IsLoad() {
	return m_IsLoad;
}

BOOL CPeLoaderBase::IsPE(__in PVOID lpVoid)
{
	IMAGE_DOS_HEADER* pDosHeader = (IMAGE_DOS_HEADER*)(lpVoid);
	if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		return FALSE;
	IMAGE_NT_HEADERS* pNtHeader = (IMAGE_NT_HEADERS*)((LONG_PTR)pDosHeader->e_lfanew + (LONG_PTR)pDosHeader);
	if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
		return FALSE;
	return TRUE;
}

// 加载Pe文件。
BOOL CPeLoaderBase::LoadPe(__in TCHAR* szFilePath) {
	// 判断文件是否存在。
	if (IsFile(szFilePath)) {
		m_IsLoad = FALSE;
		// 以读的方式打开文件。
		HANDLE hFile = CreateFile(szFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hFile) {
			OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 打开文件失败！\n"));
			return m_IsLoad;
		}

		HANDLE hMapFile = NULL;
		__try {
			//返回值size_high,size_low分别表示文件大小的高32位/低32位
			DWORD size_low,size_high;
			size_low = GetFileSize(hFile,&size_high); 
			m_FileSize = ((0xFFFFFFFF & (LONGLONG)size_high) << 32) + (0xFFFFFFFF & (LONGLONG)size_low);	// 保存文件大小。

			//创建文件的内存映射文件。   
			hMapFile = CreateFileMapping(hFile, NULL,
				PAGE_READONLY,			//对映射文件进行读写
				size_high, size_low,	//这两个参数共64位，所以支持的最大文件长度为16EB
				NULL);   
			if(NULL == hMapFile) {
				OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 创建文件的内存映射文件失败。\n"));
				__leave;
			}

			// 把文件数据映射到进程的地址空间
			m_FilePoint = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);
			// 如果映射失败，或目标文件不是PE文件。
			if (NULL == m_FilePoint || !IsPE(m_FilePoint)) {
				OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 把文件数据映射到进程的地址空间失败。\n"));
				if (NULL != m_FilePoint)
					UnmapViewOfFile(m_FilePoint);
				__leave;
			}

			m_DosHeader = (IMAGE_DOS_HEADER*)m_FilePoint;	// 获得IMAGE_DOS_HEADER指针。
			if (!IsPE(m_DosHeader)) {
				OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 此文件不是PE文件。\n"));
				UnmapViewOfFile(m_FilePoint);
				__leave;
			}

			IMAGE_NT_HEADERS *NtHeader = (IMAGE_NT_HEADERS*)((LONG_PTR)m_DosHeader->e_lfanew + (LONG_PTR)(m_DosHeader));
			m_FileTypeMagic = NtHeader->OptionalHeader.Magic;	// 获得文件类型魔术字。
			m_FileHeader = &(NtHeader->FileHeader);				// 获得IMAGE_FILE_HEADER指针。
			int platform = Is32BitApplication();	// 是否是32位应用程序。
			if (1 == platform) {
				m_IsPE64 = FALSE;
				m_DataDirectory = ((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.DataDirectory;	// 数据目录指针。
				m_DataDirectoryCount = ((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.NumberOfRvaAndSizes;	// 数据目录数组元素个数。
				m_SectionHeader = (IMAGE_SECTION_HEADER*)((LONG_PTR)NtHeader + sizeof(IMAGE_NT_HEADERS32));	// 节头指针。
				m_PeHeaderSize = ((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.SizeOfHeaders;		// 所有头+节表按照文件对齐粒度后的大小（即含补足的0）。
				m_MemoryAlignmentSize = ((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.SectionAlignment;	// 获得内存中节的对齐粒度。
				m_FileAlignmentSize = ((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.FileAlignment;		// 获得文件中节的对齐粒度。
				m_ImageCodeOffset = (PVOID)(((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.BaseOfCode);	// 代码偏移。
				m_ImageDataOffset = (PVOID)(((IMAGE_NT_HEADERS32*)NtHeader)->OptionalHeader.BaseOfData);	// 数据偏移。
			} else if (0 == platform){
				m_IsPE64 = TRUE;
				m_DataDirectory = ((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.DataDirectory;
				m_DataDirectoryCount = ((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.NumberOfRvaAndSizes;	// 数据目录数组元素个数。
				m_SectionHeader = (IMAGE_SECTION_HEADER*)((LONG_PTR)NtHeader + sizeof(IMAGE_NT_HEADERS64));
				m_PeHeaderSize = ((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.SizeOfHeaders;
				m_MemoryAlignmentSize = ((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.SectionAlignment;
				m_FileAlignmentSize = ((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.FileAlignment;
				m_ImageCodeOffset = (PVOID)(((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.BaseOfCode);
				//m_ImageDataOffset = (PVOID)(((IMAGE_NT_HEADERS64*)NtHeader)->OptionalHeader.BaseOfData);	// IMAGE_NT_HEADERS64不存在BaseOfData字段。
			} else {
				UnmapViewOfFile(m_FilePoint);
				OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 无法确认程序运行的平台。\n"));
				throw TEXT("无法确认程序运行的平台。");
				__leave;
			}

			m_Path = szFilePath;	// 保存文件名。
			m_NumberOfSections = m_FileHeader->NumberOfSections;	// 获得节的个数。
			m_IsLoad = TRUE;
		} __finally {
			if (hMapFile) {
				CloseHandle(hMapFile);
			}
			CloseHandle(hFile);
		}

		return m_IsLoad;
	} else {
		OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 文件不存在！！！\n"));
		return FALSE;
	}
}

// 获得页面保护类型。
DWORD CPeLoaderBase::PageProtectType(__in DWORD dwCharacteristics) {
	int mapping[] = {PAGE_NOACCESS, PAGE_EXECUTE, PAGE_READONLY, 
		PAGE_EXECUTE_READ, PAGE_READWRITE, PAGE_EXECUTE_READWRITE, 
		PAGE_READWRITE, PAGE_EXECUTE_READWRITE};
	return mapping[dwCharacteristics>>29];
}

// 将内存中的RVA转换成文件偏移。
long CPeLoaderBase::RVAToFOA(__in DWORD dwRVA)
{
	const IMAGE_SECTION_HEADER* ish = GetImageSectionHeader();// 获得节表起始地址
	// 遍历节表
	for (int i = 0; i < m_NumberOfSections; i++)
	{
		// 判断RVA地址是否在当前节的范围中
		if (dwRVA >= ish[i].VirtualAddress && dwRVA < (ish[i].VirtualAddress + ish[i].SizeOfRawData))
		{
			DWORD rva = dwRVA - ish[i].VirtualAddress;	// 获得参数对于当前节的地址偏移
			return rva + ish[i].PointerToRawData;		// 偏移再加上相对于文件的偏移
		}
	}
	return -1;// 遍历所有节后，还未找到传入的地址，则返回-1
}

// 卸载Pe文件。
void CPeLoaderBase::UnLoadPe() {
	if (m_IsLoad) {
		// 从进程的地址空间撤消文件数据映像
		UnmapViewOfFile(m_FilePoint);
		m_IsLoad = FALSE;
	}
}