#include "stdafx.h"
#include "PE.h"

using namespace Mrlee_R3;
using namespace Mrlee_R3::File;

CPE::CPE()
{
	Init();
}

CPE::CPE(__in TCHAR *szFilePath)
{
	Init();	// 初始化成员变量。

	// 解析PE文件。
	//VERIFY((m_IsSuc = Parse(szFilePath)));
}

CPE::~CPE(){}

// 初始化成员变量。
void CPE::Init() {
	m_IsSuc = FALSE;
}

// 解析文件。
//BOOL CPE::Parse(__in TCHAR *szFilePath) {
//	if ( m_IsSuc ) {
//		m_ErrorInfo = TEXT("【错误】CPE::Parse(TCHAR*) - 已经成功解析了文件，不要重复解析。");
//		return FALSE;
//	}
//	// 判断是否是一个文件。
//	if ( FALSE == IsFile(szFilePath) ) {
//		m_ErrorInfo = TEXT("【错误】CPE::Parse(TCHAR*) - 这不是一个文件。");
//		return FALSE;
//	}
//
//	// 以只读的方式打开文件。
//	HANDLE hFile = CreateFile(szFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//	if (INVALID_HANDLE_VALUE == hFile) {
//		m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 打开文件失败。");
//		return FALSE;
//	}
//
//	//返回值size_high,size_low分别表示文件大小的高32位/低32位
//	DWORD size_low,size_high;
//	size_low = GetFileSize(hFile,&size_high); 
//	m_FileSize = ((0xFFFFFFFF & (ULONGLONG)size_high) << 32) + (0xFFFFFFFF & (ULONGLONG)size_low);	// 保存文件大小。
//
//	HANDLE hMapFile = NULL;
//	BOOL bRet = FALSE;
//
//	__try {
//		//创建文件的内存映射文件。   
//		hMapFile = CreateFileMapping(hFile, NULL,
//									 PAGE_READONLY,			//对映射文件进行读写
//									 size_high, size_low,	//这两个参数共64位，所以支持的最大文件长度为16EB
//									 NULL);   
//		if(NULL == hMapFile) {
//			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 创建文件的内存映射文件失败。");
//			__leave;
//		}
//
//		// 把文件数据映射到进程的地址空间
//		m_FilePoint = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);
//		// 如果映射失败，或目标文件不是PE文件。
//		if (NULL == m_FilePoint || !IsPE(m_FilePoint)) {
//			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 把文件数据映射到进程的地址空间失败。");
//			if (NULL != m_FilePoint)
//				UnmapViewOfFile(m_FilePoint);
//			__leave;
//		}
//
//		m_pImageDosHeader = (IMAGE_DOS_HEADER*)m_FilePoint;	// 获得IMAGE_DOS_HEADER指针。
//		if (!IsPE(m_pImageDosHeader)) {
//			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 此文件不是PE文件。");
//			UnmapViewOfFile(m_FilePoint);
//			__leave;
//		}
//
//		IMAGE_NT_HEADERS *m_pImageNtHeader32 = (IMAGE_NT_HEADERS*)((LONG_PTR)m_pImageDosHeader->e_lfanew + (LONG_PTR)(m_pImageDosHeader));
//		m_FileTypeMagic = m_pImageNtHeader32->OptionalHeader.Magic;	// 获得文件类型魔术字。
//		m_pFileHeader = &(m_pImageNtHeader32->FileHeader);				// 获得IMAGE_FILE_HEADER指针。
//
//		m_FilePath = szFilePath;	// 保存文件名。
//
//		bRet = TRUE;
//	} __finally {
//		if (hMapFile) {
//			CloseHandle(hMapFile);
//		}
//		CloseHandle(hFile);
//	}
//
//	return bRet;
//}

//////////////////////////////////////////////////////////////////////////
//

CPE32::CPE32()
	: CPEBase()
{}

CPE32::CPE32(__in TCHAR *szFilePath)
	: CPEBase(szFilePath)
{}

BOOL CPE32::Parse(__in TCHAR *szFilePath) {
	if ( m_IsSuc ) {
		m_ErrorInfo = TEXT("【错误】CPE::Parse(TCHAR*) - 已经成功解析了文件，不要重复解析。");
		return FALSE;
	}
	// 判断是否是一个文件。
	if ( FALSE == IsFile(szFilePath) ) {
		m_ErrorInfo = TEXT("【错误】CPE::Parse(TCHAR*) - 这不是一个文件。");
		return FALSE;
	}

	// 以只读的方式打开文件。
	HANDLE hFile = CreateFile(szFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hFile) {
		m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 打开文件失败。");
		return FALSE;
	}

	//返回值size_high,size_low分别表示文件大小的高32位/低32位
	DWORD size_low,size_high;
	size_low = GetFileSize(hFile,&size_high); 
	m_FileSize = ((0xFFFFFFFF & (ULONGLONG)size_high) << 32) + (0xFFFFFFFF & (ULONGLONG)size_low);	// 保存文件大小。

	HANDLE hMapFile = NULL;
	BOOL bRet = FALSE;

	__try {
		//创建文件的内存映射文件。   
		hMapFile = CreateFileMapping(hFile, NULL,
			PAGE_READONLY,			//对映射文件进行读写
			size_high, size_low,	//这两个参数共64位，所以支持的最大文件长度为16EB
			NULL);   
		if(NULL == hMapFile) {
			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 创建文件的内存映射文件失败。");
			__leave;
		}

		// 把文件数据映射到进程的地址空间
		m_FilePoint = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);
		// 如果映射失败，或目标文件不是PE文件。
		if (NULL == m_FilePoint || !IsPE(m_FilePoint)) {
			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 把文件数据映射到进程的地址空间失败。");
			if (NULL != m_FilePoint)
				UnmapViewOfFile(m_FilePoint);
			__leave;
		}

		m_pImageDosHeader = (IMAGE_DOS_HEADER*)m_FilePoint;	// 获得IMAGE_DOS_HEADER指针。
		if (!IsPE(m_pImageDosHeader)) {
			m_ErrorInfo = TEXT("【错误】CPE32::Parse(TCHAR*) - 此文件不是PE文件。");
			UnmapViewOfFile(m_FilePoint);
			__leave;
		}

		// 获得NT头。
		m_pImageNtHeader32 = (IMAGE_NT_HEADERS32*)((LONG_PTR)m_pImageDosHeader->e_lfanew + (LONG_PTR)(m_pImageDosHeader));
		m_FileTypeMagic = m_pImageNtHeader32->OptionalHeader.Magic;	// 获得文件类型魔术字。
		// 判断是否是32位可执行文件。
		if ( IMAGE_NT_OPTIONAL_HDR32_MAGIC != m_FileTypeMagic ) {
			m_ErrorInfo.Format(TEXT("【错误】CPE32::Parse(TCHAR*) - 此文件不是32位可执行文件。文件类型魔术字=0x%X。"), m_FileTypeMagic);
			m_ErrorInfo = TEXT("");
			UnmapViewOfFile(m_FilePoint);
			__leave;
		}

		m_pImageFileHeader = &(m_pImageNtHeader32->FileHeader);								// File头。
		m_pImageOptionalHeader32 = &(m_pImageNtHeader32->OptionalHeader);					// 可选头。

		m_pImageDataDirectory = m_pImageNtHeader32->OptionalHeader.DataDirectory;			// 数据目录。
		m_ImageDataDirectoryCount = m_pImageNtHeader32->OptionalHeader.NumberOfRvaAndSizes;	// 数据目录数组元素个数。

		m_pImageSectionHeader = (IMAGE_SECTION_HEADER*)((LONG_PTR)m_pImageNtHeader32 + sizeof(IMAGE_NT_HEADERS32));	// 节头指针。
		m_SizeOfHeaders = m_pImageNtHeader32->OptionalHeader.SizeOfHeaders;					// 所有头+节表按照文件对齐粒度后的大小（即含补足的0）。
		m_MemoryAlignment = m_pImageNtHeader32->OptionalHeader.SectionAlignment;			// 获得内存中节的对齐粒度。
		//m_FileAlignmentSize = m_pImageNtHeader32->OptionalHeader.FileAlignment;		// 获得文件中节的对齐粒度。
		//m_ImageCodeOffset = (PVOID)((m_pImageNtHeader32)->OptionalHeader.BaseOfCode);	// 代码偏移。
		//m_ImageDataOffset = (PVOID)((m_pImageNtHeader32)->OptionalHeader.BaseOfData);	// 数据偏移。

		m_FilePath = szFilePath;	// 保存文件名。

		bRet = TRUE;
	} __finally {
		if (hMapFile) {
			CloseHandle(hMapFile);
		}
		CloseHandle(hFile);
	}

	return bRet;
}

//////////////////////////////////////////////////////////////////////////

// 验证PE文件是否有效。
BOOL IsPE(__in PVOID lpVoid)
{
	IMAGE_DOS_HEADER* pDosHeader = (IMAGE_DOS_HEADER*)(lpVoid);
	if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		return FALSE;
	IMAGE_NT_HEADERS* pNtHeader = (IMAGE_NT_HEADERS*)((LONG_PTR)pDosHeader->e_lfanew + (LONG_PTR)pDosHeader);
	if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
		return FALSE;
	return TRUE;
}