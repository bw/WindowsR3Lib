#include "stdafx.h"
#include "PeLoader32.h"

using namespace Mrlee_R3::File;

CPeLoader32::CPeLoader32()
	: CPeLoaderBase()
{
}

CPeLoader32::~CPeLoader32() {
}

// 获得程序装入内存的建议地址。
DWORD CPeLoader32::GetImageBase() {
	return m_NtHeader->OptionalHeader.ImageBase;
}

// 获得指向IMAGE_NT_HEADERS的指针。
const IMAGE_NT_HEADERS32* CPeLoader32::GetImageNtHeader() {
	return m_NtHeader;
}

// 获得指向IMAGE_OPTIONAL_HEADER的指针。
const IMAGE_OPTIONAL_HEADER32* CPeLoader32::GetImageOptionHeader() {
	return &(m_NtHeader->OptionalHeader);
}

// 获得线程局部存储目录指针。
const IMAGE_TLS_DIRECTORY32* CPeLoader32::GetImageTlsDirectory() {
	const IMAGE_DATA_DIRECTORY *DataDir = GetImageDataDirectory(PeDataDirectory_ThreadLocalStorageTable);
	if (0 == DataDir->VirtualAddress) {
		return NULL;
	}
	long foa = RVAToFOA(DataDir->VirtualAddress);
	if (-1 == foa) {
		return NULL;
	}
	return (const IMAGE_TLS_DIRECTORY32*)((BYTE*)GetFilePoint() + (DWORD)foa);
}

// 获得程序执行入口偏移。
DWORD CPeLoader32::GetOEPOffset() {
	return (( IMAGE_NT_HEADERS32*)m_NtHeader)->OptionalHeader.AddressOfEntryPoint;
}

// 加载Pe文件。
BOOL CPeLoader32::LoadPe(__in TCHAR* szFilePath) {
	BOOL bRet = CPeLoaderBase::LoadPe(szFilePath);
	if (bRet) {
		if (m_IsPE64) {
			UnLoadPe();
			OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 此文件是64位PE文件。"));
			return FALSE;
		} else {
			m_NtHeader = (IMAGE_NT_HEADERS32*)((LONG_PTR)m_DosHeader->e_lfanew + (LONG_PTR)(m_DosHeader));
		}
		return TRUE;
	} else {
		OutputDebugString(TEXT("【错误】CPeLoader32::LoadPe() - 加载Pe文件失败。\n"));
		return FALSE;
	}
}