#include "stdafx.h"
#include "FileHelper.h"

CString GetFileExtension (__in TCHAR *strFilePath, __in_opt BOOL isName /* = FALSE */) {
	if ( FALSE == isName ) {
		if ( FALSE == IsFile(strFilePath) ) {
			return TEXT("");// 如果不是文件，则返回空串。
		}
	}

	TCHAR *strExt = StrRChr(strFilePath, NULL, TEXT('.'));
	if ( strExt ) {
		return strExt + 1;
	} else {
		SetLastError(MRLEE_ERROR_CHARACTER_NOT_FOUND);
		return TEXT("");	// 未找到扩展名。
	}
}

// 获得文件版本信息。
BOOL GetFileVerInfo(__in PCTSTR szFileName, __out FileVersionInfo &fileVersionInfo) {
	BOOL bRet;
	CString strSubBlock,strTranslation,strTemp;
	DWORD dwSize = ::GetFileVersionInfoSize(szFileName,NULL);	// 获得文件版本信息大小。
	LPVOID pBlock = malloc(dwSize);
	if (!pBlock)
		return FALSE;

	bRet = ::GetFileVersionInfo(szFileName,0,dwSize,pBlock);	// 获得文件版本信息。
	if (!bRet)
		goto _ret;

	char* pVerValue = NULL;
	UINT nSize = 0;
	bRet = ::VerQueryValue(pBlock,TEXT("\\VarFileInfo\\Translation"), (LPVOID*)&pVerValue,&nSize);
	if (!bRet)
		goto _ret;

	strTemp.Format(TEXT("000%x"),*((unsigned short int *)pVerValue));
	strTranslation = strTemp.Right(4);
	strTemp.Format(TEXT("000%x"),*((unsigned short int *)&pVerValue[2]));
	strTranslation += strTemp.Right(4);
	//080404b0为中文，040904E4为英文

	// 
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\Comments"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.Comments.Format(TEXT("%s"), pVerValue);
	
	// 内部名称。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\InternalName"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.InternalName.Format(TEXT("%s"), pVerValue);

	// 产品名称。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\ProductName"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.ProductName.Format(TEXT("%s"), pVerValue);

	// 公司名称。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\CompanyName"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.CompanyName.Format(TEXT("%s"), pVerValue);

	// 法律版权所有。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\LegalCopyright"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.LegalCopyright.Format(TEXT("%s"), pVerValue);

	// 产品版本。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\ProductVersion"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.ProductVersion.Format(TEXT("%s"), pVerValue);

	// 文件描述。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\FileDescription"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.FileDescription.Format(TEXT("%s"), pVerValue);

	// 法律商标。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\LegalTrademarks"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.LegalTrademarks.Format(TEXT("%s"), pVerValue);

	// 
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\PrivateBuild"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.PrivateBuild.Format(TEXT("%s"), pVerValue);

	// 文件版本。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\FileVersion"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.FileVersion.Format(TEXT("%s"), pVerValue);

	// 原始文件名。
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\OriginalFilename"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.OriginalFilename.Format(TEXT("%s"), pVerValue);

	// 
	strSubBlock.Format(TEXT("\\StringFileInfo\\%s\\SpecialBuild"),strTranslation);
	::VerQueryValue(pBlock,strSubBlock.GetBufferSetLength(MAX_PATH * sizeof(TCHAR)),(LPVOID*)&pVerValue,&nSize);
	strSubBlock.ReleaseBuffer();
	fileVersionInfo.SpecialBuild.Format(TEXT("%s"), pVerValue);

_ret:
	free(pBlock);
	return bRet;
}

// 是否是文件。
BOOL IsFile(__in TCHAR *szFilePath) {
	// 判断是否是目录。
	if (PathIsDirectory(szFilePath)) {
		SetLastError(MRLEE_ERROR_IS_DIRECTORY);
		return FALSE;
	}
	// 判断是否是文件。
	if (PathFileExists(szFilePath))
		return TRUE;
	else
		return FALSE;
}
