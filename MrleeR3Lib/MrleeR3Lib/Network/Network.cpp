#include "stdafx.h"
#include "Network.h"

using namespace Mrlee_R3::Network;

// 网络是否连接。
BOOL CNetwork::IsConnected() {
	DWORD   flags;//上网方式 
	BOOL   isOnline=TRUE;//是否在线  
	isOnline = InternetGetConnectedState(&flags,0);   
	if(isOnline) {
		/*if ((flags & INTERNET_CONNECTION_MODEM) ==INTERNET_CONNECTION_MODEM)
		{
			cout<<"在线：拨号上网\n";
		}
		if ((flags & INTERNET_CONNECTION_LAN) ==INTERNET_CONNECTION_LAN)
		{
			cout<<"在线：通过局域网\n";
		}
		if ((flags & INTERNET_CONNECTION_PROXY) ==INTERNET_CONNECTION_PROXY)
		{
			cout<<"在线：代理\n";
		}
		if ((flags & INTERNET_CONNECTION_MODEM_BUSY) ==INTERNET_CONNECTION_MODEM_BUSY)
		{
			cout<<"MODEM被其他非INTERNET连接占用\n";
		}*/
		return TRUE;
	} else {
		//cout<<"不在线\n";
		return FALSE;
	}
}

// 网络是否连接。
BOOL CNetwork::IsConnected2() {
	DWORD   flags;//上网方式 
	BOOL   bOnline=TRUE;//是否在线  
	bOnline=IsNetworkAlive(&flags);   
	if(bOnline)//在线   
	{   
		/*if ((flags & NETWORK_ALIVE_LAN) ==NETWORK_ALIVE_LAN)
		{
			cout<<"在线：NETWORK_ALIVE_LAN\n";
		}
		if ((flags & NETWORK_ALIVE_WAN) ==NETWORK_ALIVE_WAN)
		{
			cout<<"在线：NETWORK_ALIVE_WAN\n";
		}
		if ((flags & NETWORK_ALIVE_AOL) ==NETWORK_ALIVE_AOL)
		{
			cout<<"在线：NETWORK_ALIVE_AOL\n";
		}*/
		return TRUE;
	}
	else {
		//cout<<"不在线\n";
		return FALSE;
	}
}

// 打开网页。
BOOL CNetwork::OpenWeb(__in TCHAR *website) {
	if ((DWORD)ShellExecute(NULL, TEXT("open"), website, NULL, NULL, SW_SHOWNORMAL) > 32) {
		return TRUE;
	} else {
		TRACE("【错误】WinSys::OpenWeb() - ShellExecute()执行失败。\n");
		return FALSE;
	}
}