#include "stdafx.h"
#include "Driver.h"

using namespace Mrlee_R3::Driver;

CDriver::CDriver()
	: m_IsLoadSuc(FALSE)
{
}

CDriver::~CDriver(){
	//// 如果类卸载以后依旧有驱动正在加载，则卸载它。
	//if (m_IsLoadSuc) {
	//	UnLoadNtDriver();
	//}
}

// 加载NT式驱动。
BOOL CDriver::LoadNtDriver (__in CString &driverPath) {
	// 获得驱动名。
	int n = driverPath.ReverseFind('\\');
	CString driverName = driverPath.Right(driverPath.GetLength() - 1 - n);
	n = driverName.ReverseFind('.');
	driverName = driverName.Left(n);
	return LoadNtDriver(driverName, driverPath);
}

// 加载NT式驱动。
BOOL CDriver::LoadNtDriver (__in TCHAR *driverName, __in TCHAR *driverPath)
{
	if (TRUE == m_IsLoadSuc) {
		OutputDebugString(TEXT("【错误】CDriver::LoadNtDriver() - 已经有驱动正在加载，请先卸载已加载的驱动。\n"));
		return FALSE;
	}
	SC_HANDLE hServiceDDK=NULL;
	SC_HANDLE hServiceSCR=NULL;
	//	A、OpenSCManager
	hServiceSCR=OpenSCManager(
		NULL,	                 //	__in          LPCTSTR lpMachineName,
		NULL,                	//	__in          LPCTSTR lpDatabaseName,
		SC_MANAGER_ALL_ACCESS	//	__in          DWORD dwDesiredAccess
		);
	if (NULL==hServiceSCR)
	{
		OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - OpenSCManager()调用失败。\n"));
		goto BExit;
	}

	//	B、CreateService
	hServiceDDK= CreateService(
		hServiceSCR,			//__in          SC_HANDLE hSCManager,
		driverName,	//__in          LPCTSTR lpServiceName,
		driverName,	//__in          LPCTSTR lpDisplayName,
		SERVICE_ALL_ACCESS,		//__in          DWORD dwDesiredAccess,
		SERVICE_KERNEL_DRIVER,	//__in          DWORD dwServiceType,
		SERVICE_DEMAND_START,	//__in          DWORD dwStartType,
		SERVICE_ERROR_IGNORE,	//__in          DWORD dwErrorControl,
		driverPath,//__in          LPCTSTR lpBinaryPathName,
		NULL,					//__in          LPCTSTR lpLoadOrderGroup,
		NULL,					//__out         LPDWORD lpdwTagId,
		NULL,					//__in          LPCTSTR lpDependencies,
		NULL,					//__in          LPCTSTR lpServiceStartName,
		NULL					//__in          LPCTSTR lpPassword
		);



	//	C、OpenService	
	DWORD dwRtn;
	if (hServiceDDK==NULL)
	{
		dwRtn=GetLastError();
		if (dwRtn!=ERROR_IO_PENDING && dwRtn!=ERROR_SERVICE_EXISTS)
		{
			OutputDebugString(TEXT("【错误】CDriver::LoadNtDriver函数 - CreateService()调用失败。\n"));
			goto BExit;

		}
		else
		{
			OutputDebugString(TEXT("【提示】CDriver::LoadNtDriver函数 - CreateService()已经创建过了。\n"));
		}
		//驱动程序已经加载，只需打开
		hServiceDDK= OpenService(
			hServiceSCR,			//__in          SC_HANDLE hSCManager,
			driverName,	//__in          LPCTSTR lpServiceName,
			SC_MANAGER_ALL_ACCESS	//__in          DWORD dwDesiredAccess
			);
		if (hServiceDDK==NULL)
		{
			dwRtn=GetLastError();
			OutputDebugString(TEXT("【错误】CDriver::LoadNtDriver函数 - OpenService()调用失败。\n"));
			goto BExit;
		}
	}

	//	D、StartService
	m_IsLoadSuc= StartService(
		hServiceDDK,				//__in          SC_HANDLE hService,
		NULL,				//__in          DWORD dwNumServiceArgs,
		NULL				//__in          LPCTSTR* lpServiceArgVectors
		);

	// 注释了有好处。
	if (!m_IsLoadSuc)
	{
		DWORD dwRtn=GetLastError();
		if (dwRtn!=ERROR_IO_PENDING && dwRtn!=ERROR_SERVICE_ALREADY_RUNNING)
		{
			OutputDebugString(TEXT("【提示】CDriver::LoadNtDriver函数 - StartService()已经在执行了。\n"));
			/*m_IsLoadSuc=TRUE;
			goto BExit;*/
		}
		else
		{
			if (dwRtn==ERROR_IO_PENDING)
			{
				OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - 设备被挂起 error_io_pending。\n"));
				m_IsLoadSuc=FALSE;
				goto BExit;
			}
			else
			{
				OutputDebugString(TEXT("【提示】CDriver::LoadNtDriver函数 - 服务已开启 error_service_already_running。\n"));
				/*m_IsLoadSuc=TRUE;
				goto BExit;*/
			}
		}
	}

	m_DriverName = driverName;
	m_DriverPath = driverPath;
	m_IsLoadSuc=TRUE;
	//	E、CloseServiceHandle

BExit:
	if(hServiceDDK)
	{
		CloseServiceHandle(hServiceDDK);
	}
	if(hServiceSCR)
	{
		CloseServiceHandle(hServiceSCR);
	}
	return m_IsLoadSuc;
}

// 加载NT式驱动。
BOOL CDriver::LoadNtDriver (__in CString &driverName, __in CString &driverPath)
{
	return LoadNtDriver(driverName.GetBuffer(), driverPath.GetBuffer());
}

// 卸载NT式驱动。
BOOL CDriver::UnLoadNtDriver()
{
	if (FALSE == m_IsLoadSuc) {
		OutputDebugString(TEXT("【警告】CDriver::UnLoadNtDriver() - 还未加载过驱动。\n"));
		return FALSE;
	}
	BOOL bRet=FALSE;
	SC_HANDLE hServiceDDK=NULL;
	SC_HANDLE hServiceSCR=NULL;
	SERVICE_STATUS SvrSta;
	hServiceSCR=OpenSCManager(
		NULL,	                 //	__in          LPCTSTR lpMachineName,
		NULL,                	//	__in          LPCTSTR lpDatabaseName,
		SC_MANAGER_ALL_ACCESS	//	__in          DWORD dwDesiredAccess
		);
	if (NULL==hServiceSCR)
	{
		OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - 打开scm管理器失败。\n"));
		bRet=FALSE;
		goto BExit;
	}


	hServiceDDK= OpenService(
		hServiceSCR,			//__in          SC_HANDLE hSCManager,
		m_DriverName.GetBuffer(),	//__in          LPCTSTR lpServiceName,
		SC_MANAGER_ALL_ACCESS	//__in          DWORD dwDesiredAccess
		);
	if (hServiceDDK==NULL)
	{
		DWORD dwErrorCode = GetLastError();
		OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - 打开驱动对应的服务失败。\n"));
		bRet=FALSE;
		goto BExit;
	}

	if (!ControlService(hServiceDDK,SERVICE_CONTROL_STOP,&SvrSta))
	{
		OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - ControlService()函数调用失败。\n"));
	}

	if (!DeleteService(hServiceDDK))
	{
		OutputDebugString(TEXT("CDriver::LoadNtDriver函数 - 驱动卸载失败。\n"));
	}

	bRet=TRUE;
	m_IsLoadSuc = FALSE;	// 卸载成功。

BExit:
	if(hServiceDDK)
	{
		CloseServiceHandle(hServiceDDK);
	}
	if(hServiceSCR)
	{
		CloseServiceHandle(hServiceSCR);
	}
	return bRet;
}