#include "stdafx.h"
#include "BitConverter.h"

using namespace Mrlee_R3;

// 将数值转换为字节数组。
void BitConverter::GetBytes(__in int value, __out BYTE bytes[4]) {
	GetBytes((unsigned int)value, bytes);
}

// 将数值转换为字节数组。
void BitConverter::GetBytes(__in unsigned int value, __out BYTE bytes[4]) {
	unsigned int uValue = value;
	int i = 0;
	while (0 != uValue) {
		bytes[i] = uValue & 0xFF;
		uValue >>= 8;
		i++;
	}
}

// 将数值转换为字节数组。
void BitConverter::GetBytes(__in LONGLONG value, __out BYTE bytes[8]) {
	GetBytes((ULONGLONG)value, bytes);
}

// 将数值转换为字节数组。
void BitConverter::GetBytes(__in ULONGLONG value, __out BYTE bytes[8]) {
	ULONGLONG uValue = value;
	int i = 0;
	while (0 != uValue) {
		bytes[i] = uValue & 0xFF;
		uValue >>= 8;
		i++;
	}
}

// 中文转字节码。
void BitConverter::中文转字节码(__in char *Chinese, __out UINT16 *number) {
	*number = (BYTE)(*Chinese);
	*number += (BYTE)(*(Chinese + 1)) << 8;
}

// 中文转字节码。
void BitConverter::中文转字节码(__in wchar_t *Chinese, __out UINT16 *number) {
	*number = (USHORT)(*Chinese);
}