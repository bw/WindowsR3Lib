#include "stdafx.h"
#include "CWindowBase.h"

using namespace Mrlee_R3;
using namespace Mrlee_R3::Process;

/**	枚举窗口找到相应的窗口句柄，此函数被FindWindow函数调用。
 *	@param[in] 窗口句柄。
 *	@param[in] 参数。
 *	@return 此函数返回值总为true。
 */
BOOL CALLBACK EnumWindowsProc_FindTopLevelWindow(__in HWND hwnd, __in LPARAM lparam);

BOOL CALLBACK EnumWindowsProc_FindTopLevelWindow(__in HWND hwnd, __in LPARAM lparam)
{
	FindWindowInfo *pFindWindowInfo = (FindWindowInfo*)lparam;
	TCHAR lpWinTitle[MAX_PATH] = {TEXT('\0')}; 
	// 获得窗口标题。
	GetWindowText(hwnd,lpWinTitle,MAX_PATH - sizeof(TCHAR));
	CString strTitle = lpWinTitle;
	// 判断窗口标题是否符合。
	if (pFindWindowInfo->isContain)
	{
		// 包含。
		if (strTitle.Find(pFindWindowInfo->m_WindowName) != -1)
		{
			if (IsWindow(hwnd))
			{
				pFindWindowInfo->hWindow = hwnd;
			}
		}
	} 
	else
	{
		// 必须相等。
		if(strTitle == pFindWindowInfo->m_WindowName) 
		{
			if (IsWindow(hwnd))
			{
				pFindWindowInfo->hWindow = hwnd;
			}
		}
	}

	return TRUE ;
}

// 消息框。
int MsgBox (__in LPCTSTR lpText, __in_opt LPCTSTR lpCaption /* = NULL */, __in_opt UINT uType /* = MB_OK */, __in_opt HWND hWnd /* = NULL */)
{
	if ( NULL == hWnd ) {
		HWND hWndTop;
		hWnd = CWnd::GetSafeOwner_(NULL, &hWndTop);
	}
	return MsgBox_ND(lpText, lpCaption, uType, hWnd);
}

// 消息框。
int MsgBox_ND(__in LPCTSTR lpText, __in_opt LPCTSTR lpCaption /* = NULL */, __in_opt UINT uType /* = MB_OK */, __in_opt HWND hWnd /* = NULL */) {
	return ::MessageBox(hWnd, lpText, lpCaption == NULL ? TEXT("信息") : lpCaption, uType);
}

// 批量设置窗口内容。
void BatchSetWinContent(__in CArray<CWnd*> *ctrls, __in const CWnd* excludes, __in TCHAR *content) {
	INT_PTR length = (*ctrls).GetSize();
	for (INT_PTR i = 0; i < length; i++)
	{
		if ((*ctrls)[i] == excludes)
			continue;
		(*ctrls)[i]->SetWindowText(content);
		(*ctrls)[i]->UpdateData();
	}
}

// 批量设置窗口内容。
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in TCHAR *content) {
	std::map<int, CWnd*>::iterator i_begin = (*ctrls).begin();
	std::map<int, CWnd*>::iterator i_end = (*ctrls).end();
	for (; i_begin != i_end; i_begin++)
	{
		i_begin->second->SetWindowText(content);
		i_begin->second->UpdateData();
	}
}

// 批量设置窗口内容。
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in CWnd* excludes, __in TCHAR *content) {
	std::map<int, CWnd*>::iterator i_begin = (*ctrls).begin();
	std::map<int, CWnd*>::iterator i_end = (*ctrls).end();
	for (; i_begin != i_end; i_begin++)
	{
		if (i_begin->second == excludes)
			continue;
		i_begin->second->SetWindowText(content);
		i_begin->second->UpdateData();
	}
}

// 批量设置窗口内容。
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in std::map<int, CWnd*> *excludes, __in TCHAR *content) {
	std::map<int, CWnd*>::iterator i_begin = (*ctrls).begin();
	std::map<int, CWnd*>::iterator i_end = (*ctrls).end();

	std::map<int, CWnd*>::iterator i_ex_begin = (*excludes).begin();
	std::map<int, CWnd*>::iterator i_ex_end = (*excludes).end();

	for (; i_begin != i_end; i_begin++)
	{
		for (; i_ex_begin != i_ex_end; i_ex_begin++)
		{
			if (i_begin->second == i_ex_begin->second)
				continue;
		}
		
		i_begin->second->SetWindowText(content);
		i_begin->second->UpdateData();
	}
}

// 单选框组设置选中。
void RadioGroupSetCheck(__in CArray<CButton*> &radioGroup, __in int ctlId) {
	INT_PTR count = radioGroup.GetCount();
	for (INT_PTR i = 0; i < count; i++)
	{
		if (radioGroup[i]->GetDlgCtrlID() == ctlId) {
			radioGroup[i]->SetCheck(TRUE);
		} else {
			radioGroup[i]->SetCheck(FALSE);
		}
	}
}

// 判断传入的控件Id是否是选中状态。
BOOL IsCheckInRadioGroup(__in CArray<CButton*> &radioGroup, __in int ctlId) {
	INT_PTR count = radioGroup.GetCount();
	for (INT_PTR i = 0; i < count; i++)
	{
		if (radioGroup[i]->GetDlgCtrlID() == ctlId) {
			return radioGroup[i]->GetCheck();
		}
	}
	return FALSE;
}

// 获得窗口句柄。
HWND FindWin(__in_opt LPTSTR lpClassName, __in_opt LPTSTR lpWindowName) {
	HWND hWnd;
	hWnd = ::FindWindow(lpClassName, lpWindowName);
	if (!hWnd) {
		hWnd = FindTopWindow(lpWindowName, TRUE);
	}
	return hWnd;
}

// 查找顶层窗口句柄。此函数与系统的FindWindow函数实现原理不同，若FindWindow函数找不到窗口句柄，可以试试此函数。
HWND FindTopWindow(__in LPTSTR windowName, __in BOOL isContain)
{
	if ( NULL == windowName ) {
		return NULL;
	}

	FindWindowInfo findWindowInfo;	// 查找窗口信息。
	findWindowInfo.hWindow = NULL;
	findWindowInfo.m_WindowName = windowName;
	findWindowInfo.isContain = isContain;
	// 枚举所有窗口。
	::EnumWindows(EnumWindowsProc_FindTopLevelWindow, (LPARAM)&findWindowInfo);
	if ( NULL == findWindowInfo.hWindow ) {
		SetLastError(ERROR_INVALID_WINDOW_HANDLE);
	}
	return findWindowInfo.hWindow;
}

// 子窗口填充整个父窗口。
void FillTheParentWindow(__in HWND hChild) {
	HWND hParent = ::GetParent(hChild);
	// 选项卡控件的大小随窗口大小而改变。
	CRect clientRect;
	::GetClientRect(hParent, &clientRect);
	::MoveWindow(hChild, clientRect.left, clientRect.top, clientRect.Width(), clientRect.Height(), TRUE);
}

// 允许/禁止窗口关闭按钮。
BOOL EnableWindowClose (__in HWND hWnd, __in BOOL bEnable) {
	HMENU hSysMenu = ::GetSystemMenu(hWnd, FALSE);	// 获得系统菜单句柄。
	if (NULL == hSysMenu) {
		TRACE("【错误】EnableWindowClose() - 获得系统菜单句柄失败。\n");
		return FALSE;
	}

	if (TRUE == bEnable) {
		// 启用关闭按钮。
		if (FALSE == ::EnableMenuItem(hSysMenu, SC_CLOSE, MF_BYCOMMAND)) {
			TRACE("【错误】EnableWindowClose() - 启用关闭按钮失败。\n");
			return FALSE;
		}
	} else {
		// 禁止关闭按钮。
		if (FALSE == ::EnableMenuItem(hSysMenu, SC_CLOSE, MF_BYCOMMAND | MF_GRAYED)) {
			TRACE("【错误】EnableWindowClose() - 禁止关闭按钮失败。\n");
			return FALSE;
		}
	}

	/*RECT rect;
	if (FALSE == ::GetWindowRect(hWnd, &rect)) {
		TRACE("【错误】EnableWindowClose() - 获得窗口矩形失败。\n");
		return FALSE;
	}

	if (FALSE == ::RedrawWindow(hWnd, &rect, NULL, 1 | 128 | 256)) {
		TRACE("【错误】EnableWindowClose() - 重画窗口失败。\n");
		return FALSE;
	}*/

	return TRUE;
}

// 窗口是否在最前。
BOOL IsWindowForeground(__in void *lpVoid, __in IsWindowForegroundFlag isWindowForegroundFlag) {
	HWND hForegroundWindow;
	DWORD ProcessId = 0;
	CString processName;
	BOOL bRet;
	TCHAR name[150] = {TEXT('\0')};

	hForegroundWindow = GetForegroundWindow();
	if (NULL == hForegroundWindow) {
		TRACE("【错误】IsWindowForeground() - 没有前台窗口。\n");
		return FALSE;
	}

	switch (isWindowForegroundFlag) {
		case IsWindowForegroundFlag_Pid:
			GetWindowThreadProcessId(hForegroundWindow, &ProcessId);
			if (0 == ProcessId) {
				TRACE("【错误】IsWindowForeground() - 获得进程Id失败。\n");
				return FALSE;
			}
			if (ProcessId == (DWORD)lpVoid) {
				return TRUE;
			} else {
				TRACE("【提示】IsWindowForeground() - Pid_不为前台窗口。\n");
				return FALSE;
			}

		case IsWindowForegroundFlag_ProcessName:
			bRet = GetProcessName(hForegroundWindow, processName);
			if (FALSE == bRet) {
				TRACE("【错误】IsWindowForeground() - 获得进程名失败。\n");
				return FALSE;
			}

			if (0 == _tcsncmp (processName.GetBuffer(), (TCHAR*)lpVoid, MAX_PATH)) {
				return TRUE;
			} else {
				TRACE("【提示】IsWindowForeground() - ProcessName_不为前台窗口。\n");
				return FALSE;
			}

		case IsWindowForegroundFlag_WindowName:
			::GetWindowText(hForegroundWindow, name, 149);

			if (0 == _tcsncmp (name, (TCHAR*)lpVoid, MAX_PATH)) {
				return TRUE;
			} else {
				TRACE("【提示】IsWindowForeground() - WindowName_不为前台窗口。\n");
				return FALSE;
			}

		case IsWindowForegroundFlag_ClassName:
			::GetClassName(hForegroundWindow, name, 149);
			if (0 == _tcsncmp (name, (TCHAR*)lpVoid, MAX_PATH)) {
				return TRUE;
			} else {
				TRACE("【提示】IsWindowForeground() - ClassName_不为前台窗口。\n");
				return FALSE;
			}

		default:
			TRACE("【错误】IsWindowForeground() - 参数isWindowForegroundFlag的值未定义。\n");
			return FALSE;
	}
}

// 窗口透明。
//BOOL TransparentWindow(__in HWND hWnd, __in BOOL isTransparent) {
//	if (transparentRange < 0) {
//		transparentRange = 0;
//	} else if (transparentRange > 255) {
//		transparentRange = 255;
//	}
//
//	/*static HWND hWndStatic = NULL;
//	static LONG styleStatic = ::GetWindowLong(hWnd, GWL_EXSTYLE);
//
//	if (hWndStatic != hWnd) {
//		hWndStatic = hWnd;
//	}
//
//	if (isMouseThrough) {
//		LONG styleTemp = ::GetWindowLong(hWnd, GWL_EXSTYLE);
//		
//		SetWindowLong (hWnd, GWL_EXSTYLE,  | WS_EX_TRANSPARENT | WS_EX_LAYERED);
//	} else {
//		SetWindowLong (hWnd, GWL_EXSTYLE, WS_EX_LAYERED);
//	}*/
//	
//	return SetLayeredWindowAttributes(hWnd, 0, transparentRange, LWA_ALPHA);
//}

BOOL TransparentWindow(__in HWND hWnd, __in BOOL isTransparent) {
	/*if (isTransparent) {
		return SetLayeredWindowAttributes(hWnd, 0, 0, LWA_ALPHA);
	} else {
		return SetLayeredWindowAttributes(hWnd, 0, 255, LWA_ALPHA);
	}*/

	int transparentRange;
	if (isTransparent) {
		transparentRange = 0;
	} else {
		transparentRange = 255;
	}

	LONG styleTemp = ::GetWindowLong(hWnd, GWL_EXSTYLE);

	//if (isMouseThrough) {
	if (isTransparent) {
		// styleTemp | WS_EX_TRANSPARENT | WS_EX_LAYERED 是设置鼠标穿透。
		//if (FALSE == SetWindowLong (hWnd, GWL_EXSTYLE, styleTemp | WS_EX_TRANSPARENT | WS_EX_LAYERED)) {
		if (FALSE == SetWindowLong (hWnd, GWL_EXSTYLE, WS_EX_LAYERED)) {
			TRACE("【错误】TransparentWindow() - 设置窗口风格失败。\n");
			return FALSE;
		}
		//SetWindowLong (hWnd, GWL_EXSTYLE, WS_EX_TRANSPARENT);
	} else {
		if (FALSE == SetWindowLong (hWnd, GWL_EXSTYLE, styleTemp & (~WS_EX_TRANSPARENT))) {
			TRACE("【错误】TransparentWindow() - 设置窗口风格失败。\n");
			return FALSE;
		}
	}
	
	return SetLayeredWindowAttributes(hWnd, 0, transparentRange, LWA_ALPHA);
}

// 关闭窗口。
void CloseWindow_ (__in HWND hWnd) {
	::PostMessage(hWnd, WM_CLOSE, 0, 0);
}

// 窗口最大化。
void MaximizeWindow (__in HWND hWnd) {
	::PostMessage(hWnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
}

// 窗口最小化。
void MinimizedWindow (__in HWND hWnd) {
	::PostMessage(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
}

// 窗口置顶。
void WindowTopmost (__in HWND hWnd, __in BOOL isTopmost) {
	if (isTopmost) {
		::SetWindowPos (hWnd, HWND_TOPMOST, 0, 0, 0, 0, 3);	// 3 == SWP_NOSIZE | SWP_NOMOVE，保留当前大小和位置。
	} else {
		::SetWindowPos (hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, 3);
	}
}

// 获得所有窗口所属的线程。
void GetAllWindowThread (__out std::vector<DWORD> &threads) {
	HWND hTemp = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
	while (hTemp != NULL) {
		threads.push_back(::GetWindowThreadProcessId(hTemp, NULL));
		hTemp = ::GetWindow(hTemp, GW_HWNDNEXT);
	}
}

// 窗口抖动。
BOOL 窗口抖动 (__in HWND hWnd, __in int count /* = 3 */, __in int speed /* = 10 */) {
	RECT rect = {0};
	BOOL bRet;
	bRet = ::GetWindowRect(hWnd, &rect);
	if (FALSE == bRet) {
		TRACE("【错误】窗口抖动() - 获得窗口外边框失败。\n");
		return FALSE;
	}

	if (count < 3)
		count = 3;
	if (speed < 10)
		speed = 10;

	// 开始得瑟。
	for (int i = 0; i < count; i++) {
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left - 3, rect.top - 3, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left - 5, rect.top, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left - 3, rect.top + 3, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left, rect.top + 5, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left + 3, rect.top + 3, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left + 5, rect.top, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left + 3, rect.top - 3, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		WinSys::Delay(speed, TimeUnit_Millisecond);
		::MoveWindow(hWnd, rect.left, rect.top - 5, rect.right - rect.left, rect.bottom - rect.top, TRUE);
	}

	::MoveWindow(hWnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, TRUE);
	return TRUE;
}