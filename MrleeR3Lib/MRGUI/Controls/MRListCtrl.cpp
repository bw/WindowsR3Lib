#include "stdafx.h"
#include "MRListCtrl.h"

IMPLEMENT_DYNAMIC(CMRListCtrl, CMRListCtrlBase)

CMRListCtrl::CMRListCtrl()
	: m_IsEnableColumnAutoSize(FALSE), 
	  m_InitFlag(FALSE), 
	  m_IsScroll(FALSE), 
	  CMRListCtrlBase()
{
	//CListCtrl::WindowProc(0, 0, 0);
	//SetExtendedStyle(GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
}

CMRListCtrl::~CMRListCtrl()
{
}

// 自动调整列的宽度。
void CMRListCtrl::EnableColumnAutoSize(BOOL bEnable /* = TRUE */) {
	m_IsEnableColumnAutoSize = bEnable;
}

// 固定列宽度。
void CMRListCtrl::FixedColumnWidth(TCHAR* lpszColumnNames[], DWORD num) {
	for ( DWORD i = 0; i < num; i++  ) {
		POSITION pos;
		INT_PTR iRet = FindColumn(lpszColumnNames[i], &pos);
		if ( -1 == iRet ) {
			continue;
		}
		((CNode *)pos)->data.IsFix = TRUE;
	}
}

int CMRListCtrl::GetColumnCount() {
	return GetHeaderCtrl()->GetItemCount();
}

// 获得一个单元格的文本。
CString CMRListCtrl::GetItemText(int nRowIndex, int nColIndex) const {
	return CMRListCtrlBase::GetItemText(nRowIndex, nColIndex);
}

// 获得一个单元格的文本。
CString CMRListCtrl::GetItemText(int nRowIndex, LPCTSTR lpszColumnName) {
	INT_PTR columnIndex = FindColumn(lpszColumnName);
	// 列不存在，则返回-1。
	if ( -1 == columnIndex ) {
		return TEXT("");
	}
	return CMRListCtrlBase::GetItemText(nRowIndex, columnIndex);
}

int CMRListCtrl::GetItemText(int nItem, int nSubItem, __out_ecount_part_z(nLen, return + 1) LPTSTR lpszText, int nLen) const {
	return CMRListCtrlBase::GetItemText(nItem, nSubItem, lpszText, nLen);
}

// 插入列。向后插入新列。
int CMRListCtrl::InsertColumn(
	LPCTSTR lpszColumnCaption, 
	int nWidth /* = -1 */, 
	int nFormat /* = LVCFMT_LEFT */, 
	int nSubItem /* = -1 */) {
	return InsertColumn(GetHeaderCtrl()->GetItemCount(), lpszColumnCaption, nWidth, nFormat, nSubItem);
}

// 插入列。向后插入新列。
int CMRListCtrl::InsertColumn(
	LPCTSTR lpszColumnName, 
	LPCTSTR lpszColumnCaption, 
	int nWidth /* = -1 */, 
	int nFormat /* = LVCFMT_LEFT */, 
	int nSubItem /* = -1 */) {
	return InsertColumn(GetHeaderCtrl()->GetItemCount(), lpszColumnName, lpszColumnCaption, nWidth, nFormat, nSubItem);
}

// 插入列。
int CMRListCtrl::InsertColumn(
	int nColIndex, 
	LPCTSTR lpszColumnCaption, 
	int nWidth /* = -1 */, 
	int nFormat /* = LVCFMT_LEFT */, 
	int nSubItem /* = -1 */) {
	TCHAR columnName[12] = TEXT("Column");
	StringCchPrintf(columnName + 6, 6, TEXT("%d"), GetColumnCount());
	return InsertColumn(nColIndex, columnName, lpszColumnCaption, nWidth, nFormat, nSubItem);
}

// 插入列。
int CMRListCtrl::InsertColumn(
	int nColIndex, 
	LPCTSTR lpszColumnName, 
	LPCTSTR lpszColumnCaption, 
	int nWidth /* = -1 */, 
	int nFormat /* = LVCFMT_LEFT */, 
	int nSubItem /* = -1 */)
{
	if ( FALSE == m_InitFlag ) {
		m_InitFlag = TRUE;
		SetExtendedStyle(GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	}
	INT_PTR columnIndex = FindColumn(lpszColumnName);
	// 相应列已经存在，则返回-2。
	if ( -1 != columnIndex ) {
		return -2;
	}
	// 插入列。
	int iRet = CMRListCtrlBase::InsertColumn(nColIndex, lpszColumnCaption, nFormat, nWidth, nSubItem);
	// 插入列成功，则在m_Columns中添加一个值。
	if ( -1 != iRet ) {
		CListColumn column;
		column.ColumnIndex = iRet;
		column.ColumnName = lpszColumnName;
		column.ColumnCaption = lpszColumnCaption;
		column.Width = nWidth;
		column.IsFix = FALSE;
		column.IsFix = FALSE;
		// 判断是在结尾插入还是在中间插入。
		if ( m_Columns.GetCount() == iRet ) {
			m_Columns.InsertAfter(m_Columns.FindIndex(iRet), column);
		} else if ( iRet < m_Columns.GetCount() ) {
			m_Columns.InsertBefore(m_Columns.FindIndex(iRet), column);
		} else {
			throw TEXT("CMRListCtrl::InsertColumn() - 列计算出错。\n");
		}
	}
	return iRet;
}

// 设置一个单元格的文本。
BOOL CMRListCtrl::SetItemText(int nRowIndex, int nColIndex, LPCTSTR lpszText) {
	BOOL bRet;
	if ( 0 == nColIndex ) {
		bRet = CMRListCtrlBase::InsertItem(nRowIndex, lpszText) != -1 ? TRUE : FALSE;
		if ( bRet && m_IsScroll ) {
			CMRListCtrlBase::EnsureVisible(nRowIndex, FALSE);	// 滚动到最后一行。
		}
		return bRet;
	} else {
		return CMRListCtrlBase::SetItemText(nRowIndex, nColIndex, lpszText);
	}
}

// 设置最后一行的某一个单元格的文本。
BOOL CMRListCtrl::SetItemText(LPCTSTR lpszColumnName, LPCTSTR lpszText) {
	return SetItemText(GetItemCount(), lpszColumnName, lpszText);
}

// 设置一个单元格的文本。
int CMRListCtrl::SetItemText(int nRowIndex, LPCTSTR lpszColumnName, LPCTSTR lpszText) {
	INT_PTR columnIndex = FindColumn(lpszColumnName);
	// 列不存在，则返回-1。
	if ( -1 == columnIndex ) {
		return -1;
	}

	return SetItemText(nRowIndex, columnIndex, lpszText);
}

// 设置滚动。
void CMRListCtrl::SetScroll(BOOL isScroll) {
	m_IsScroll = isScroll;
}

// 重载窗口过程。
LRESULT CMRListCtrl::WindowProc(UINT nMsg, WPARAM wParam, LPARAM lParam) {
	switch ( nMsg )
	{
	case WM_SIZE:
		ResizeColumns();	// 调整列。
		//return 0;
		break;

	case WM_CREATE:
		SetExtendedStyle(GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
		break;
	}
	return CMRListCtrlBase::WindowProc(nMsg, wParam, lParam);
}

//////////////////////////////////////////////////////////////////////////

// 查找列。
INT_PTR CMRListCtrl::FindColumn(LPCTSTR lpszColumnName, POSITION *pPos /* = NULL */) {
	POSITION pos = m_Columns.GetHeadPosition();
	INT_PTR count = m_Columns.GetCount();
	INT_PTR i=0;
	for ( ; i < count; i++ ) {
		if ( ((CNode *)pos)->data.ColumnName == lpszColumnName ) {
			if ( pPos ) {
				*pPos = pos;
			}
			return i;
		}
		m_Columns.GetNext(pos);
	}
	if ( 0 == i || count == i ) {
		return -1;
	} else {
		TCHAR errorInfo[100] = {TEXT('\0')};
		StringCchPrintf(errorInfo, 50, TEXT("CMRListCtrl::FindColumn() - 这种情况不应该存在#i=%d。"), i);
		throw errorInfo;
	}
}

// 固定列。
void CMRListCtrl::ResizeColumns() {
	POSITION pos = m_Columns.GetHeadPosition();
	INT_PTR count = m_Columns.GetCount();
	int j = 0;	// 需要自动调整宽度的列的个数。
	ULONG_PTR *poses = (ULONG_PTR *)calloc(count, sizeof(ULONG_PTR));
	DWORD totalFixColWidth = 0;	// 需要固定的列的总宽度。
	
	// 固定列宽度。
	for ( INT_PTR i=0; i < count; i++ ) {
		// 列宽度是否固定？
		if ( ((CNode *)pos)->data.IsFix ) {
			SetColumnWidth(((CNode *)pos)->data.ColumnIndex, ((CNode *)pos)->data.Width);	// 固定列宽度。
			totalFixColWidth += ((CNode *)pos)->data.Width;
		} else {
			// 是否需要自动调整列宽度？
			if ( m_IsEnableColumnAutoSize ) {
				poses[j] = (ULONG_PTR)pos;
				j++;
			} else {
				((CNode *)pos)->data.Width = GetColumnWidth(((CNode *)pos)->data.ColumnIndex);	// 更新列宽度。
			}
		}
		m_Columns.GetNext(pos);
	}

	RECT rc;
	//  是否需要自动调整列宽度？
	if ( m_IsEnableColumnAutoSize ) {
		GetClientRect(&rc);
		//GetWindowRect(&rc);
		int averageWidth = (rc.right - rc.left - totalFixColWidth - 1) / j;
		for ( int i = 0; i < j; i++ ) {
			CNode *node = (CNode *)poses[i];
			node->data.Width = averageWidth;
			SetColumnWidth(node->data.ColumnIndex, node->data.Width);	// 设置宽度。
		}
	}

	free(poses);
}

BEGIN_MESSAGE_MAP(CMRListCtrl, CMRListCtrlBase)
ON_NOTIFY(HDN_ENDTRACKA, 0, &CMRListCtrl::OnHdnEndtrack)
ON_NOTIFY(HDN_ENDTRACKW, 0, &CMRListCtrl::OnHdnEndtrack)
END_MESSAGE_MAP()

void CMRListCtrl::OnHdnEndtrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}
