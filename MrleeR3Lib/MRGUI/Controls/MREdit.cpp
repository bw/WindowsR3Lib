// CMREdit.cpp : 实现文件
//

#include "stdafx.h"
#include "MREdit.h"


// CMREdit

IMPLEMENT_DYNAMIC(CMREdit, CEdit)

CMREdit::CMREdit()
	: CEdit()
	, m_TextColor(RGB(0, 0, 0))	// 默认文本颜色为黑色。
{

}

CMREdit::~CMREdit()
{
}


BEGIN_MESSAGE_MAP(CMREdit, CEdit)
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



// CMREdit 消息处理程序



HBRUSH CMREdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr = CEdit::OnCtlColor(pDC, GetParent(), nCtlColor);

	// TODO:  在此更改 DC 的任何属性
	pDC->SetTextColor(m_TextColor);

	// TODO:  如果不应调用父级的处理程序，则返回非空画笔
	
	return hbr;
}

// 获得文本颜色。
COLORREF CMREdit::GetTextColor() {
	return m_TextColor;
}

// 设置文字颜色。
void CMREdit::SetTextColor(__in COLORREF textColor) {
	m_TextColor = textColor;
}

// 设置文字颜色。
void CMREdit::SetTextColor(__in MColor color) {
	switch ( color )
	{
	case M_Red:
		m_TextColor = RGB(255, 0, 0);
		break;
	case M_Green:
		m_TextColor = RGB(0, 255, 0);
		break;
	case M_Blue:
		m_TextColor = RGB(0, 0, 255);
		break;
	}
}