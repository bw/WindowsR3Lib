#include "stdafx.h"
#include "MRTabCtrl.h"

using namespace std;

IMPLEMENT_DYNAMIC(CMRTabCtrl, CTabCtrl)

BEGIN_MESSAGE_MAP(CMRTabCtrl, CTabCtrl)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, &CMRTabCtrl::OnTcnSelchange)
END_MESSAGE_MAP()

CMRTabCtrl::CMRTabCtrl() 
	: CTabCtrl(), 
	  m_CurSelTab(0)
{
}

CMRTabCtrl::~CMRTabCtrl()
{

}

LRESULT CMRTabCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) {
	switch ( message )
	{
	case WM_SIZE:
		AllPopulateControl();
		break;
	}
	return CTabCtrl::WindowProc(message, wParam, lParam);
}

// 添加标签页。
LONG CMRTabCtrl::InsertItem(__in int nItem, __in LPCTSTR lpszItem, __in CDialog *pDialog) {
	int iRet = CTabCtrl::InsertItem(nItem, lpszItem);
	if (-1 == iRet) {
		return iRet;
	} else {
		list<CDialog*>::iterator findNode;
		CDialog* dialog_Temp;
		try
		{
			dialog_Temp = m_pTabDialogs.at(nItem);	// 查找这一项。

			// 找到的情况。
			m_pTabDialogs.insert(m_pTabDialogs.begin() + nItem, pDialog);
		}
		catch (...)
		{
			m_pTabDialogs.push_back(pDialog);
		}

		if (pDialog)
			pDialog->SetParent(this);

		if ( 0 == nItem ) {
			SetCurFocus(0);
			pDialog->ShowWindow(SW_SHOW);
		} else {
			pDialog->ShowWindow(SW_HIDE);
		}

		PopulateControl(nItem);		// 填充对话框。

		return iRet;
	}
}

// 添加标签页。
LONG CMRTabCtrl::InsertItem(__in int nItem, __in LPCTSTR lpszItem, __in CDialog *pDialog, __in UINT nDialogId) {
	pDialog->Create(nDialogId, this);
	return InsertItem(nItem, lpszItem, pDialog);
}

// 获得当前标签页的对话框指针。
CDialog* CMRTabCtrl::GetCurrentDialog() {
	try
	{
		return this->m_pTabDialogs.at(m_CurSelTab);
	}
	catch (...)
	{
		return NULL;
	}
}

// 设置显示的标签页。
void CMRTabCtrl::SetTabPage(__in DWORD index) {
	int count = m_pTabDialogs.size();
	if ( 0 == count ) {
		return;
	}
	if ( ( index + 1 ) > count ) {
		index = 0;
	}
	CDialog *dialog = m_pTabDialogs[index];
	if (dialog) {
		m_pTabDialogs[m_CurSelTab]->ShowWindow(SW_HIDE);// 隐藏前一个对话框。
		m_CurSelTab = index;
		dialog->ShowWindow(SW_SHOW);// 显示当前选择的标签页。
	}
	
	this->SetCurFocus(index);
}

// 当前对话框填充。
void CMRTabCtrl::PopulateControl() {
	// 设定在Tab内显示的范围。
	PopulateControl(m_CurSelTab);
}

// 填充对话框。
void CMRTabCtrl::PopulateControl(__in DWORD index) {
	CDialog *dialog = m_pTabDialogs[index];
	if ( dialog ) {
		CRect rc;
		this->GetClientRect(rc);
		rc.top += 25;
		rc.bottom -= 8;
		rc.left += 8;
		rc.right -= 8;

		dialog->MoveWindow(&rc);
		dialog->RedrawWindow();
	}
}

// 所有标签页中的窗口都进行填充。
void CMRTabCtrl::AllPopulateControl() {
	// 设定在Tab内显示的范围。
	CRect rc;
	this->GetClientRect(rc);
	rc.top += 25;
	rc.bottom -= 8;
	rc.left += 8;
	rc.right -= 8;

	size_t size = this->m_pTabDialogs.size();
	for (size_t i = 0; i < size; i++)
	{
		this->m_pTabDialogs[i]->MoveWindow(&rc);
		this->m_pTabDialogs[i]->RedrawWindow();	// 这条语句很重要，一定要添加上。
	}
}

// 销毁标签页中的所有窗口。
void CMRTabCtrl::DestroyAllWindow() {
	size_t size = m_pTabDialogs.size();
	for (size_t i = 0; i < size; i++)
	{
		if (NULL != m_pTabDialogs[i] && NULL != m_pTabDialogs[i]->m_hWnd) {
			m_pTabDialogs[i]->DestroyWindow();
		}
	}
	m_pTabDialogs.clear();
}

// 关闭标签页中的所有窗口。
//void CMRTabCtrl::CloseAllWindow() {
//	size_t size = m_pTabDialogs.size();
//	for (size_t i = 0; i < size; i++)
//	{
//		if (NULL != m_pTabDialogs[i] && NULL != m_pTabDialogs[i]->m_hWnd) {
//			m_pTabDialogs[i]->CloseWindow();
//		}
//	}
//	m_pTabDialogs.clear();	// 争议！
//}

// 选择标签页。
void CMRTabCtrl::OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	SetTabPage(GetCurSel());
}
