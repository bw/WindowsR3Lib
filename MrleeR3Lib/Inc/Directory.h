#pragma once

namespace Mrlee_R3
{
	namespace IO
	{
		class CDirectory
		{
		public:
			/**	目录拷贝。
			 *	@param[in] 目的目录。
			 *	@param[in] 源目录。
			 *	@param[in] 如果文件存在，是否覆盖。TRUE:覆盖。FALSE:不覆盖。
			 */
			static BOOL Copy(__in CString &strDesPath, __in CString &strSrcPath, __in BOOL isOverlay);

			/**	创建目录。
			 *	@param[in] 路径。
			 *	@return 创建成功，则返回TRUE。创建失败，则返回FALSE。
			 */
			static BOOL CreateDirectory_(__in TCHAR *strPath);

			/**	创建多级目录。
			 *	@param[in] 路径。
			 *	@return 创建成功，则返回TRUE。创建失败，则返回FALSE。
			 */
			static BOOL CreatedMultipleDirectory (TCHAR* strPath);

			/**	获得文件名。
			 *	@param[in] 路径。
			 */
			static void GetFileName (__in TCHAR *path, __out CString &fileName);
		};
	}
}