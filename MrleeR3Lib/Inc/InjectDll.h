#pragma once

#include "IInject.h"

namespace Mrlee_R3
{
	namespace Security
	{
		namespace Inject
		{
			#define CInjectBase IInject
			/**	注入Dll。
			 *	注入方法：将加载Dll的代码写入远程线程中，然后在远程线程中加载Dll。
			 */
			class CInjectDll_RemoteLoad : public CInjectBase
			{
			protected:
				HMODULE m_hLibModule;	// 加载的Dll的句柄。

			public:
				/**	构造函数。
				 */
				CInjectDll_RemoteLoad();

				/**	析构函数。
				 */
				virtual ~CInjectDll_RemoteLoad();

				/**	将Dll注入到进程中。
				 *	@param[in] Pid。
				 *	@param[in] dll路径。
				 *	@param[in] 【可选】函数名。
				 *	@param[in] 【可选】是否等待函数执行完成。TRUE：等待远程线程中的函数执行完毕。FALSE：不等待远程线程中的函数执行完毕，参数pFunRet将是无效的。
				 *	@param[in] 【可选】参数buffer地址。
				 *	@param[in] 【可选】参数大小。
				 *	@param[out] 【可选】注入的函数的返回值。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_RemoteLoad::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Inject(
					__in DWORD dwPID, 
					__in CString& szDllPath, 
					__in BOOL isWait = TRUE, 
					__in_opt TCHAR *szFunName = NULL, 
					__in_opt LPVOID lpParamBuffer = NULL, 
					__in_opt SIZE_T paramSize = 0, 
					__out_opt LPDWORD pFunRet = NULL );

				/**	将Dll注入到进程中。
				 *	@param[in] 窗口名。查找到的窗口标题只要包含strWindowName_Contain字符串即可。
				 *	@param[in] dll路径。
				 *	@param[in] 【可选】函数名。
				 *	@param[in] 【可选】是否等待函数执行完成。TRUE：等待远程线程中的函数执行完毕。FALSE：不等待远程线程中的函数执行完毕，参数pFunRet将是无效的。
				 *	@param[in] 【可选】参数buffer地址。
				 *	@param[in] 【可选】参数大小。
				 *	@param[out] 【可选】注入的函数的返回值。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_RemoteLoad::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Inject(
					__in TCHAR *strWinName_Contain, 
					__in CString& szDllPath, 
					__in BOOL isWait = TRUE, 
					__in_opt TCHAR *szFunName = NULL, 
					__in_opt LPVOID lpParamBuffer = NULL, 
					__in_opt SIZE_T paramSize = 0, 
					__out_opt LPDWORD pFunRet = NULL);

				/**	将Dll注入到进程中。
				 *	@param[in] 【可选】窗口名。
				 *	@param[in] 【可选】类名。
				 *	@param[in] dll路径。
				 *	@param[in] 【可选】函数名。
				 *	@param[in] 【可选】是否等待函数执行完成。TRUE：等待远程线程中的函数执行完毕。FALSE：不等待远程线程中的函数执行完毕，参数pFunRet将是无效的。
				 *	@param[in] 【可选】参数buffer地址。
				 *	@param[in] 【可选】参数大小。
				 *	@param[out] 【可选】注入的函数的返回值。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_RemoteLoad::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Inject(
					__in_opt TCHAR *strWinName, 
					__in_opt TCHAR *className, 
					__in CString& szDllPath, 
					__in BOOL isWait = TRUE, 
					__in_opt TCHAR *szFunName = NULL, 
					__in_opt LPVOID lpParamBuffer = NULL, 
					__in_opt SIZE_T paramSize = 0, 
					__out_opt LPDWORD pFunRet = NULL);

				/**	卸载注入到进程中的Dll。
				 *	@return 卸载成功，则返回TRUE。卸载失败，返回FALSE。
				 *	@note 如果卸载失败，可以通过CInjectDll_RemoteLoad::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Eject();
			};

			/**	将Dll注入到窗口中。
			 *	注入方法：使用SetWindowsHookEx函数进行注入。
			 */
			class CInjectDll_SWHE : public CInjectBase
			{
			protected:
				HHOOK m_hHook;

			public:
				/**	构造函数。
				 */
				CInjectDll_SWHE();

				/**	析构函数。
				 */
				virtual ~CInjectDll_SWHE();

				/**	安装线程钩子。
				 *  @param[in] 线程ID。
				 *  @param[in] dll路径。
				 *  @param[in] dll中导出函数的名称。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_SWHE::GetErrorInfo()查看失败信息。
				 */
				/*BOOL Inject(
					__in DWORD	dwThreadId, 
					__in TCHAR* strDllPath, 
					__in TCHAR* strDllFunName);*/

				/**	安装线程钩子。将钩子安装到指定窗口所在的线程中。
				 *  @param[in] 窗口句柄。
				 *  @param[in] dll路径。
				 *  @param[in] dll中导出函数的名称。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_SWHE::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Inject(
					__in HWND	hWnd, 
					__in TCHAR* strDllPath, 
					__in TCHAR* strDllFunName);

				/**	安装线程钩子。将钩子安装到指定窗口所在的线程中。
				 *  @param[in] 窗口标题。查找到的窗口标题只要包含strWindowName_Contain字符串即可。
				 *  @param[in] dll路径。
				 *  @param[in] dll中导出函数的名称。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_SWHE::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Inject(
					__in TCHAR* strWindowName_Contain, 
					__in TCHAR* strDllPath, 
					__in TCHAR* strDllFunName);

				/**	安装线程钩子。将钩子安装到指定窗口所在的线程中。
				 *  @param[in] 【可选】窗口标题。
				 *	@param[in] 【可选】窗口类名。
				 *  @param[in] dll路径。
				 *  @param[in] dll中导出函数的名称。
				 *	@return 注入成功，则返回TRUE。注入失败，返回FALSE。
				 *	@note 如果注入失败，可以通过CInjectDll_SWHE::GetErrorInfo()查看失败信息。
				 */
				BOOL Inject(
					__in_opt TCHAR* strWindowName, 
					__in_opt TCHAR* strClassName, 
					__in TCHAR* strDllPath, 
					__in TCHAR* strDllFunName);

				/**	卸载注入到进程中的Dll。
				 *	@return 卸载成功，则返回TRUE。卸载失败，返回FALSE。
				 *	@note 如果卸载失败，可以通过CInjectDll_SWHE::GetErrorInfo()查看失败信息。
				 */
				virtual BOOL Eject();
			};
		}
	}
}