#pragma once

// 注入参数。
typedef struct _InjectParams {
	GetProcAddress_Ptr	pGetProcAddress;// GetProcAddress函数地址。
	HMODULE				hDllHandle;		// DLL句柄。
	char*				pFunName;		// 函数名。
} InjectParams, *PInjectParams;

//#pragma optimize( "ts", off )
//#pragma check_stack( off )

/**	为注入所写的GetProcAddress()函数。
 *	@param[in] 传入的Buffer代表系统API-GetProcAddress()的两个参数。
 *	@return 返回值与API-GetProcAddress()的返回值一样。
 */
FARPROC __stdcall GetProcAddressForInject (InjectParams *pInjectParams);

int __stdcall AfterGetProcAddressForInject(void);

//#pragma check_stack( on )
//#pragma optimize( "", on )