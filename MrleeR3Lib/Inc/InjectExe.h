#pragma once

#include "IInject.h"
#include "PeLoader32.h"

namespace Mrlee_R3
{
	namespace Security
	{
		namespace Inject
		{
			/**	注入可执行文件。
			 */
			class CInjectExe : public IInject
			{
			private:
				/// 进程信息
				//PROCESS_INFORMATION m_ProcInfo;
				BOOL m_IsSuc;

			public:
				CInjectExe();
				virtual ~CInjectExe();
				/**	开始注入。
				 *	@param[in] 被注入的Exe文件路径。此函数将创建这个进程，并将这个进程中的代码替换。
				 *	@param[in] 需要替换的代码。
				 *	@return 注入成功，则返回TRUE。否则，返回FALSE。
				 */
				virtual BOOL Inject(__in TCHAR* pDesFileName, __in Mrlee_R3::File::CPeLoader32 *pCPeLoaderEx);

				/**	终止注入。
				 */
				virtual BOOL Eject();
			};
		}
	}
}