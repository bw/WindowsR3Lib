#pragma once

namespace Mrlee_R3
{
	class BitConverter
	{
	public:
		/**	将数值转换为字节数组。
		 *	@param[in] 值。
		 *	@param[out] 输出字节数组。
		 */
		static void GetBytes(__in int value, __out BYTE bytes[4]);

		/**	将数值转换为字节数组。
		 *	@param[in] 值。
		 *	@param[out] 输出字节数组。
		 */
		static void GetBytes(__in unsigned int value, __out BYTE bytes[4]);

		/**	将数值转换为字节数组。
		 *	@param[in] 值。
		 *	@param[out] 输出字节数组。
		 */
		static void GetBytes(__in LONGLONG value, __out BYTE bytes[8]);

		/**	将数值转换为字节数组。
		 *	@param[in] 值。
		 *	@param[out] 输出字节数组。
		 */
		static void GetBytes(__in ULONGLONG value, __out BYTE bytes[8]);

		/**	中文转字节码。
		 *	@param[in] 中文字符。如：小。
		 *	@param[out] 返回转换后的数字，这个数字是内存中表示文字的数字。
		 */
		static void 中文转字节码(__in char *Chinese, __out UINT16 *number);

		/**	中文转字节码。
		 *	@param[in] 中文字符。如：小。
		 *	@param[out] 返回转换后的数字，这个数字是内存中表示文字的数字。
		 */
		static void 中文转字节码(__in wchar_t *Chinese, __out UINT16 *number);
	};
}