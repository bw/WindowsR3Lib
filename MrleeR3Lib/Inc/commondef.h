#pragma once

#include <Windows.h>
#include <winternl.h>
#include <vector>

/** 获得数组长度 */
#define arraysize(p) (sizeof(p)/sizeof((p)[0]))

#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)
#endif

#ifdef MrleeR3Lib_EXPORTS
#define MrleeR3Lib_API __declspec(dllexport)
#else
#define MrleeR3Lib_API __declspec(dllimport)
#endif

typedef std::vector<CString> MyCStringArray;
typedef std::vector<MyCStringArray> CStringArrayTwo;

//////////////////////////////////////////////////////////////////////////

typedef
VOID
(NTAPI *PPS_POST_PROCESS_INIT_ROUTINE_) (
	VOID
	);

typedef void (*PPEBLOCKROUTINE)(PVOID PebLock);

typedef struct _IMAGE_BASE_RELOCATION_ {
	DWORD   VirtualAddress;
	DWORD   SizeOfBlock;
	WORD    TypeOffset[1];
} IMAGE_BASE_RELOCATION_;
typedef IMAGE_BASE_RELOCATION_ UNALIGNED * PIMAGE_BASE_RELOCATION_;

typedef struct _CURDIR_{
	UNICODE_STRING DosPath;
	PVOID Handle;
} CURDIR_, *PCURDIR_;

typedef struct _PEB_LDR_DATA_ {
	ULONG		Length;
	BOOLEAN		Initialized;
	PVOID		SsHandle;
	LIST_ENTRY	InLoadOrderModuleList;
	LIST_ENTRY  InMemoryOrderModuleList;
	LIST_ENTRY	InInitializationOrderModuleList;
} PEB_LDR_DATA_, *PPEB_LDR_DATA_;

typedef struct _LDR_DATA_TABLE_ENTRY_ {
	LIST_ENTRY InLoadOrderLinks;
	LIST_ENTRY InMemoryOrderLinks;
	LIST_ENTRY InInitializationOrderLinks;
	PVOID DllBase;
	PVOID EntryPoint;
	ULONG SizeOfImage;
	UNICODE_STRING FullDllName;
	UNICODE_STRING BaseDllName;
	ULONG Flags;
	WORD LoadCount;
	WORD TlsIndex;
	union
	{
		LIST_ENTRY HashLinks;
		struct
		{
			PVOID SectionPointer;
			ULONG CheckSum;
		};
	};
	union
	{
		ULONG TimeDateStamp;
		PVOID LoadedImports;
	};
	//struct _ACTIVATION_CONTEXT * EntryPointActivationContext;
	PVOID EntryPointActivationContext;
	PVOID PatchInformation;
	LIST_ENTRY ForwarderLinks;
	LIST_ENTRY ServiceTagLinks;
	LIST_ENTRY StaticLinks;
} LDR_DATA_TABLE_ENTRY_, *PLDR_DATA_TABLE_ENTRY_;

typedef struct _RTL_USER_PROCESS_PARAMETERS_ {
	UINT MaximumLength;
	UINT Length;
	UINT Flags;
	UINT DebugFlags;
	PVOID ConsoleHandle;
	UINT ConsoleFlags;
	PVOID StandardInput;
	PVOID StandardOutput;
	PVOID StandardError;
	CURDIR_ CurrentDirectory;
	UNICODE_STRING DllPath;
	UNICODE_STRING ImagePathName;
	UNICODE_STRING CommandLine;
	PVOID Environment;
} RTL_USER_PROCESS_PARAMETERS_, *PRTL_USER_PROCESS_PARAMETERS_;

// 结构体PEB_FREE_BLOCK在PEB（进程环境块）中使用。 structure for describe free blocks in memory allocated for PEB。
typedef struct _PEB_FREE_BLOCK {
	struct _PEB_FREE_BLOCK *Next;	// Pointer to next free block. 
	ULONG					Size;	// Size of block, in bytes. 
} PEB_FREE_BLOCK, *PPEB_FREE_BLOCK;

typedef struct _PEB_ {
	BOOLEAN							InheritedAddressSpace;
	BOOLEAN							ReadImageFileExecOptions;
	BOOLEAN							BeingDebugged;					// 进程是否在被调试状态。
	BOOLEAN							Spare;
	HANDLE							Mutant;
	PVOID							ImageBaseAddress;				// 进程映像基址。
	PPEB_LDR_DATA_					Ldr;							// 加载的其他模块信息(LoaderData)。
	PRTL_USER_PROCESS_PARAMETERS_	ProcessParameters;
	PVOID							SubSystemData;
	PVOID							ProcessHeap;
	PVOID							FastPebLock;
	PPEBLOCKROUTINE					FastPebLockRoutine;
	PPEBLOCKROUTINE					FastPebUnlockRoutine;
	ULONG							EnvironmentUpdateCount;
	PVOID							*KernelCallbackTable;
	PVOID							EventLogSection;
	PVOID							EventLog;
	PPEB_FREE_BLOCK					FreeList;
	ULONG							TlsExpansionCounter;			// TLS索引计数。
	PVOID							TlsBitmap;						// TLS位图指针。
	ULONG							TlsBitmapBits[0x2];				// TLS进程标志位。
	PVOID							ReadOnlySharedMemoryBase;
	PVOID							ReadOnlySharedMemoryHeap;
	PVOID*							ReadOnlyStaticServerData;
	PVOID							AnsiCodePageData;
	PVOID							OemCodePageData;
	PVOID							UnicodeCaseTableData;
	ULONG							NumberOfProcessors;
	ULONG							NtGlobalFlag;					// 全局标志。
	BYTE							Spare2[0x4];
	LARGE_INTEGER					CriticalSectionTimeout;
	ULONG							HeapSegmentReserve;
	ULONG							HeapSegmentCommit;
	ULONG							HeapDeCommitTotalFreeThreshold;	// 
	ULONG							HeapDeCommitFreeBlockThreshold;
	ULONG							NumberOfHeaps;
	ULONG							MaximumNumberOfHeaps;
	PVOID							**ProcessHeaps;
	PVOID							GdiSharedHandleTable;
	PVOID							ProcessStarterHelper;
	PVOID							GdiDCAttributeList;
	PVOID							LoaderLock;
	ULONG							OSMajorVersion;
	ULONG							OSMinorVersion;
	ULONG							OSBuildNumber;
	ULONG							OSPlatformId;
	ULONG							ImageSubSystem;
	ULONG							ImageSubSystemMajorVersion;
	ULONG							ImageSubSystemMinorVersion;
	ULONG							GdiHandleBuffer[0x22];
	ULONG							PostProcessInitRoutine;
	ULONG							TlsExpansionBitmap;
	BYTE							TlsExpansionBitmapBits[0x80];
	ULONG							SessionId;
} PEB_, *PPEB_;

typedef struct {
	HANDLE UniqueProcess;
	HANDLE UniqueThread;
} CLIENT_ID;

typedef struct _TEB_ {
	NT_TIB                  Tib;
	PVOID                   EnvironmentPointer;
	CLIENT_ID               Cid;
	PVOID                   ActiveRpcInfo;
	PVOID                   ThreadLocalStoragePointer;		// 合并后的TLS副本指针。
	PPEB                    Peb;							// 指向所属进程的PEB。
	ULONG                   LastErrorValue;
	ULONG                   CountOfOwnedCriticalSections;
	PVOID                   CsrClientThread;
	PVOID                   Win32ThreadInfo;
	ULONG                   Win32ClientInfo[0x1F];
	PVOID                   WOW32Reserved;
	ULONG                   CurrentLocale;
	ULONG                   FpSoftwareStatusRegister;
	PVOID                   SystemReserved1[0x36];
	PVOID                   Spare1;
	ULONG                   ExceptionCode;
	ULONG                   SpareBytes1[0x28];
	PVOID                   SystemReserved2[0xA];
	ULONG                   GdiRgn;
	ULONG                   GdiPen;
	ULONG                   GdiBrush;
	CLIENT_ID               RealClientId;
	PVOID                   GdiCachedProcessHandle;
	ULONG                   GdiClientPID;
	ULONG                   GdiClientTID;
	PVOID                   GdiThreadLocaleInfo;
	PVOID                   UserReserved[5];
	PVOID                   GlDispatchTable[0x118];
	ULONG                   GlReserved1[0x1A];
	PVOID                   GlReserved2;
	PVOID                   GlSectionInfo;
	PVOID                   GlSection;
	PVOID                   GlTable;
	PVOID                   GlCurrentRC;
	PVOID                   GlContext;
	NTSTATUS                LastStatusValue;
	UNICODE_STRING          StaticUnicodeString;
	WCHAR                   StaticUnicodeBuffer[0x105];
	PVOID                   DeallocationStack;
	PVOID                   TlsSlots[0x40];					// 线程的TLS存储槽。
	LIST_ENTRY              TlsLinks;
	PVOID                   Vdm;
	PVOID                   ReservedForNtRpc;
	PVOID                   DbgSsReserved[0x2];
	ULONG                   HardErrorDisabled;
	PVOID                   Instrumentation[0x10];
	PVOID                   WinSockData;
	ULONG                   GdiBatchCount;
	ULONG                   Spare2;
	ULONG                   Spare3;
	ULONG                   Spare4;
	PVOID                   ReservedForOle;
	ULONG                   WaitingOnLoaderLock;
	PVOID                   StackCommit;
	PVOID                   StackCommitMax;
	PVOID                   StackReserved;
} TEB_, *PTEB_;

typedef struct _PROCESS_BASIC_INFORMATION_ {
	PVOID ExitStatus;					// 进程终止状态。
	PPEB_ PebBaseAddress;				// 进程环境块地址。
	PVOID AffinityMask;					// 进程关联掩码。
	PVOID BasePriority;					// 进程的优先级类。
	ULONG_PTR UniqueProcessId;			// 进程Id。
	PVOID InheritedFromUniqueProcessId;	// 父进程Id。
} PROCESS_BASIC_INFORMATION_;

//////////////////////////////////////////////////////////////////////////
// 异常相关。

//typedef struct _DISPATCHER_CONTEXT DISPATCHER_CONTEXT;
typedef struct _EXCEPTION_REGISTRATION_RECORD EXCEPTION_REGISTRATION_RECORD;

// 异常运行时函数。
typedef
EXCEPTION_DISPOSITION
(*PEXCEPTION_ROUTINE) (
    IN EXCEPTION_RECORD *pExceptionRecord,
    IN EXCEPTION_REGISTRATION_RECORD *pEstablisherFrame,
    IN OUT struct _CONTEXT *pContextRecord,
    //IN OUT DISPATCHER_CONTEXT *pDispatcherContext
	IN OUT PVOID *pDispatcherContext
    );

typedef struct _EXCEPTION_REGISTRATION_RECORD {
	struct _EXCEPTION_REGISTRATION_RECORD *Next;	// 下一个异常注册记录结构。
	PEXCEPTION_ROUTINE Handler;						// 异常处理函数指针。
} EXCEPTION_REGISTRATION_RECORD, *PEXCEPTION_REGISTRATION_RECORD;

//typedef struct _DISPATCHER_CONTEXT {
//	ULONG64 ControlPc;
//	ULONG64 ImageBase;
//	//PRUNTIME_FUNCTION FunctionEntry;
//	PVOID FunctionEntry;
//	ULONG64 EstablisherFrame;
//	ULONG64 TargetIp;
//	PCONTEXT ContextRecord;
//	PEXCEPTION_ROUTINE LanguageHandler;
//	PVOID HandlerData;
//} DISPATCHER_CONTEXT, *PDISPATCHER_CONTEXT;

//////////////////////////////////////////////////////////////////////////

// NtQueryInformationProcess。
typedef NTSTATUS (WINAPI* NtQueryInformationProcess_Ptr)(
	HANDLE ProcessHandle,
	PROCESSINFOCLASS ProcessInformationClass,
	PVOID ProcessInformation,
	ULONG ProcessInformationLength,
	PULONG ReturnLength
	);

// ZwUnmapViewOfSection。
typedef NTSTATUS (WINAPI *ZwUnmapViewOfSection_Ptr)(
	__in      HANDLE ProcessHandle,
	__in_opt  PVOID BaseAddress
	);

// GetProcAddress。
typedef FARPROC (WINAPI *GetProcAddress_Ptr) (
	__in HMODULE hModule,
	__in LPCSTR lpProcName
	);