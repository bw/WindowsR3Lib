#pragma once

namespace Mrlee_R3
{
	namespace Driver
	{
		class CDriver
		{
		protected:
			CString m_DriverName;	/// 驱动名。
			CString m_DriverPath;	/// 驱动路径。
			BOOL m_IsLoadSuc;		/// 是否加载成功。

		public:
			CDriver();
			virtual ~CDriver();

			/** 加载NT式驱动。
			 *  @param[in] 指向驱动所在路径的指针。
			 *  @return 返回BOOL。
			 *  TRUE  加载驱动成功。
			 *  FALSE 加载驱动失败！
			 *	@note 此函数经测试可用于的系统：Win XP。
			 */
			BOOL LoadNtDriver (__in CString &driverPath);

			/** 加载NT式驱动。
			 *  @param[in] 指向驱动名称的指针。
			 *  @param[in] 指向驱动所在路径的指针。
			 *  @return 返回BOOL。
			 *  TRUE  加载驱动成功。
			 *  FALSE 加载驱动失败！
			 *	@note 此函数经测试可用于的系统：Win XP。
			 */
			BOOL LoadNtDriver (__in TCHAR *driverName, __in TCHAR *driverPath);
			/** 加载NT式驱动。
			 *  @param[in] 指向驱动名称的指针。
			 *  @param[in] 指向驱动所在路径的指针。
			 *  @return 返回BOOL。
			 *  TRUE  加载驱动成功。
			 *  FALSE 加载驱动失败！
			 *	@note 此函数经测试可用于的系统：Win XP。
			 */
			BOOL LoadNtDriver (__in CString &driverName, __in CString &driverPath);

			/** 卸载NT式驱动。
			 *  @return 返回BOOL。
			 *  TRUE  卸载驱动成功。
			 *  FALSE 卸载驱动失败！
			 *	@note 此函数经测试可用于的系统：Win XP。
			 */
			BOOL UnLoadNtDriver ();
		};
	}
}