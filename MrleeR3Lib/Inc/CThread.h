#pragma once

namespace Mrlee_R3
{
	namespace Thread
	{
		class CThread
		{
		public:
			/** 创建远程线程。
			 *  @param[in] 进程句柄。
			 *  @param[in] 远程线程执行起始地址。
			 *  @param[in] 【可选】远程线程执行的函数的参数地址。
			 *	@param[out] 【可选】返回创建的线程Id。
			 *  @return 如果成功，则返回线程句柄。如果失败，则返回 0。
			 */
			static HANDLE CreateRemoteThread(
				__in HANDLE hProcess, __in LPTHREAD_START_ROUTINE lpStartAddress, 
				__in_opt LPVOID lpParameter = NULL, __out_opt LPDWORD lpThreadId = NULL);

			/**	创建线程。
			 *	@param[in] 线程函数。
			 *	@param[in] 【可选】参数。
			 *	@param[out] 【可选】返回创建的线程Id。
			 *	@return 创建成功，则返回线程句柄。创建失败，则返回NULL。
			 */
			static HANDLE CreateThead_(__in LPTHREAD_START_ROUTINE lpStartAddress, __in_opt  LPVOID lpParameter, __out_opt LPDWORD lpThreadId);
		};
	}
}