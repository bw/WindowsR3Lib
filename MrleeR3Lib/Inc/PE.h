#pragma once

namespace Mrlee_R3
{
	namespace File
	{
		#define CPEBase CPE

		/**	PE文件解析基类。
		 */
		class CPE : public CObjectBase
		{
		protected:
			/// 解析是否成功。
			BOOL		m_IsSuc;
			/// 文件路径。
			CString		m_FilePath;
			/// 文件大小。
			ULONGLONG	m_FileSize;
			/// 映射文件在内存空间的基址指针。
			PVOID		m_FilePoint;
			/// 文件类型魔术字。见《Windows PE权威指南》第78页，3.5.3节IMAGE_NT_HEADERS32.Magic的介绍。
			WORD		m_FileTypeMagic;
			/// 数据目录数组中元素的个数。一般来说为16。
			DWORD		m_ImageDataDirectoryCount;
			/// 所有头+节表按照文件对齐粒度后的大小（即含补足的0）。
			DWORD		m_SizeOfHeaders;
			/// 内存中节的对齐粒度。
			/// 内存中的数据存取以页面为单位。Win32的页面大小为4KB，所以Win32 PE中节的内存对齐粒度一般都选择4KB。
			DWORD		m_MemoryAlignment;

			/// IMAGE_DOS_HEADER的指针。
			IMAGE_DOS_HEADER		*m_pImageDosHeader;
			/// IMAGE_NT_HEADERS32的指针。
			IMAGE_NT_HEADERS32		*m_pImageNtHeader32;
			/// IMAGE_NT_HEADERS64的指针。
			IMAGE_NT_HEADERS64		*m_pImageNtHeader64;
			/// IMAGE_FILE_HEADER的指针。
			IMAGE_FILE_HEADER		*m_pImageFileHeader;
			/// IMAGE_OPTIONAL_HEADER32的指针。
			IMAGE_OPTIONAL_HEADER32 *m_pImageOptionalHeader32;
			/// IMAGE_OPTIONAL_HEADER64的指针。
			IMAGE_OPTIONAL_HEADER64 *m_pImageOptionalHeader64;
			/// IMAGE_DATA_DIRECTORY的指针。
			IMAGE_DATA_DIRECTORY	*m_pImageDataDirectory;
			/// IMAGE_SECTION_HEADER的指针。
			IMAGE_SECTION_HEADER	*m_pImageSectionHeader;

		protected:
			CPE();
			/**	构造函数。
			 *	@param[in] 文件路径。
			 *	@note 函数中会解析PE文件，如果解析失败将会抛出异常。可以调用CPE::GetErrorInfo()函数获得异常信息。
			 */
			CPE(__in TCHAR *szFilePath);
			virtual ~CPE();

			/**	解析文件。
			 *	@param[in] 文件路径。
			 *	@return 解析成功，则返回TRUE。解析失败，则返回FALSE。
			 */
			virtual BOOL Parse(__in TCHAR *szFilePath) = 0;

		private:
			/** 初始化成员变量。*/
			void Init();
		};

		/**	解析PE格式。
		 */
		class CPE32 : public CPEBase
		{
		public:
			CPE32();
			/**	构造函数。
			 *	@param[in] 文件路径。
			 *	@note 函数中会解析PE文件，如果解析失败将会抛出异常。可以调用CPE32::GetErrorInfo()函数获得异常信息。
			 */
			CPE32(__in TCHAR *szFilePath);
			virtual ~CPE32();

			/**	解析文件。
			 *	@param[in] 文件路径。
			 *	@return 解析成功，则返回TRUE。解析失败，则返回FALSE。
			 */
			virtual BOOL Parse(__in TCHAR *szFilePath);
		};
	}
}

/** 验证PE文件是否有效。
 *  @param[in] PE文件载入内存后的基址。
 *  @return 返回BOOL。
 *  TRUE  PE文件有效。
 *  FALSE PE文件无效。
 */
BOOL IsPE(__in PVOID pPeBaseAddr);