#pragma once

namespace Mrlee_R3
{
	namespace Security
	{
		namespace Hook
		{
			class CInlineHook
			{
			protected:
				DWORD m_Pid;			// 进程Id。
				PVOID m_TargetAddr;		// 目标地址。
				DWORD m_Offset;			// inline hook相对于目标地址的偏移。
				PVOID m_FinalAddr;		// 最终地址。
				PVOID m_ProxyFun;		// 代理函数的地址。
				BYTE m_OrigInsts[30];	// 原始指令。
				DWORD m_RealInstsLength;// 原始指令的实际长度。
				BOOL m_IsSuc;			// Hook是否成功。
				//DWORD m_HookInstsLength;// hook指令长度。

			public:
				CInlineHook();
				/**	Inline Hook。
				 *	@param[in] 目标地址。
				 *	@param[in] Hook函数地址。
				 *	@param[in] inline hook相对于目标地址的偏移。
				 *	@return Inline Hook成功，则返回true。否则返回false。
				 */
				CInlineHook(__in DWORD dwPID, __in PVOID TargetAddr, __in PVOID ProxyFun, __in DWORD dwOffset = 0/*, __in DWORD hookInstsLength = 5*/);

				/**	Hook。
				 */
				BOOL Hook_();

				BOOL UnHook();
			};

			class CInlineHookEx
			{
			protected:
				DWORD m_Pid;			// 进程Id。
				PVOID m_TargetAddr;		// 目标地址。
				DWORD m_Offset;			// inline hook相对于目标地址的偏移。
				PVOID m_FinalAddr;		// 最终地址。
				PVOID m_ProxyFun;		// 代理函数的地址。
				BYTE m_OrigInsts[30];	// 原始指令。
				DWORD m_RealInstsLength;// 原始指令的实际长度。
				BOOL m_IsSuc;			// Hook是否成功。
				//DWORD m_HookInstsLength;// hook指令长度。

			public:
				CInlineHookEx();
				/**	Inline Hook。
				 *	@param[in] 目标地址。
				 *	@param[in] Hook函数地址。Hook函数请最少保留Hook起始处的6个字节。
				 *	@param[in] inline hook相对于目标地址的偏移。
				 *	@return Inline Hook成功，则返回true。否则返回false。
				 */
				CInlineHookEx(__in DWORD dwPID, __in PVOID TargetAddr, __in PVOID ProxyFun, __in DWORD dwOffset = 0/*, __in DWORD hookInstsLength = 5*/);

				/**	Hook。
				 */
				BOOL Hook_();

				BOOL UnHook();
			};
		}
	}
}