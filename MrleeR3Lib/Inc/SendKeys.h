#pragma once

namespace Mrlee_R3
{
	namespace IO
	{
		class CSendKeys
		{
		private:
			bool m_bWait;
			bool m_bUsingParens;	// 是否使用了括号。
			bool m_bShiftDown;		// Shift键是否按下。true: 按下。false: 不按下。
			bool m_bAltDown;		// Alt键是否按下。
			bool m_bControlDown;	// Ctrl键是否按下。
			bool m_bWinDown;		// Win键是否按下。
			//DWORD m_nDelayAlways;
			//DWORD m_nDelayNow;

			static const WORD INVALIDKEY;	// 无效键，0xFFFF。

			typedef BYTE KEYBOARDSTATE_t[256];
			struct EnumWindow {
				LPTSTR str;	// 窗口名。
				HWND hwnd;	// 窗口句柄。
			};

			WORD StringToVKey(__in LPCTSTR KeyString, __out int &idx);

			/**	释放所有转移键。
			 */
			void PopUpShiftKeys();

		public:
			/**	应用程序激活。
			 *	@param[in] 窗口句柄。
			 *	@return 激活成功，则返回true。否则，返回false。
			 */
			bool AppActivate(__in HWND hWnd);

			/**	应用程序激活。
			 *	@param[in] 【可选】窗口类名。
			 *	@param[in] 【可选】窗口标题。此窗口标题可以是窗口标题的一部分。
			 *	@return 激活成功，则返回true。否则，返回false。
			 */
			bool AppActivate(__in_opt LPTSTR lpWindowName, __in_opt LPTSTR lpClassName = 0);

			/**	将一个或多个按键消息发送到活动窗口。
			 *	@param[in] 按键字符串。
			 *	@param[in] 【可选】指定等待方式的值。如果为 false（缺省值），则控件在按键发送出去之后立刻返回到过程；如果为 true，则按键消息必须在控件返回到过程之前加以处理。说明每个按键由一个或多个字符表示。
			 *	@return 发送成功，则返回true。否则，返回false。
			 */
			bool SendKeys(__in LPCTSTR KeysString, __in_opt bool Wait = false);
		};
	}
}