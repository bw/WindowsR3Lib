#pragma once

// 文件版本信息。
typedef struct _FileVersionInfo {
	CString Comments;
	CString InternalName;	// 内部名称。
	CString ProductName;	// 产品名称。
	CString CompanyName;
	CString LegalCopyright;
	CString ProductVersion;
	CString FileDescription;
	CString LegalTrademarks;
	CString PrivateBuild;
	CString FileVersion;
	CString OriginalFilename;
	CString SpecialBuild;
} FileVersionInfo, *PFileVersionInfo;

/**	获得文件扩展名。
 *	@param[in] 当isName为TRUE时，表示文件名。当isName为FALSE时，表示文件完整路径。
 *	@param[in] 【可选】当isName为TRUE时，strFilePath表示文件名。当isName为FALSE时，strFilePath表示文件完整路径。默认为FALSE。
 *	@return 如果获得成功，则返回扩展名字符串，如exe、dll、jpg等。如果获得失败，返回的字符串长度为0。
 */
CString GetFileExtension(__in TCHAR *strFilePath, __in_opt BOOL isName = FALSE);

/**	获得文件版本信息。
 *	@param[in] 文件名。
 *	@param[out] 输出文件版本信息。
 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
 */
BOOL GetFileVerInfo(__in PCTSTR szFileName, __out FileVersionInfo &fileVersionInfo);

/**	是否是文件。
 *	@param[in]  文件路径。
 *	@return 是文件，则返回TRUE。否则，返回FALSE。
 */
BOOL IsFile(__in TCHAR *szFilePath);