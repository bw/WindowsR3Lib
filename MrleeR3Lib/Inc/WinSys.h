#pragma once

#include <winternl.h>

#define MAKE_MICROSECOND ((LONGLONG)-10)			// 微秒。
#define MAKE_MILLISECOND (MAKE_MICROSECOND * 1000)	// 毫秒。
#define MAKE_SECOND (MAKE_MILLISECOND * 1000)		// 秒。
#define MAKE_MINUTE (MAKE_SECOND * 60)				// 分钟。
#define MAKE_HOUR (MAKE_MINUTE * 60)				// 小时。
#define MAKE_DAY (MAKE_HOUR * 24)					// 天。

/**	时间单位。
 */
typedef enum _TimeUnit {
	TimeUnit_Microseconds, // 微秒。
	TimeUnit_Millisecond,	// 毫秒。
	TimeUnit_Second,		// 秒。
	TimeUnit_Minute,		// 分钟。
	TimeUnit_Hour,			// 小时。
	TimeUnit_Day,			// 天。
} TimeUnit;

namespace Mrlee_R3
{
	typedef enum _URLShortKey {
		URLShortKey_None = 0, 
		URLShortKey_A = 1601, 
		URLShortKey_B, 
		URLShortKey_C, 
		URLShortKey_D, 
		URLShortKey_E, 
		URLShortKey_F, 
		URLShortKey_G, 
		URLShortKey_H, 
		URLShortKey_I, 
		URLShortKey_J, 
		URLShortKey_K, 
		URLShortKey_L, 
		URLShortKey_M, 
		URLShortKey_N, 
		URLShortKey_O, 
		URLShortKey_P, 
		URLShortKey_Q, 
		URLShortKey_R, 
		URLShortKey_S, 
		URLShortKey_T, 
		URLShortKey_U, 
		URLShortKey_V, 
		URLShortKey_W, 
		URLShortKey_X, 
		URLShortKey_Y, 
		URLShortKey_Z, 
	} URLShortKey;
	class WinSys
	{
	public:

		/**	检测Tcp端口是否被占用。
		 *	@param[in] num 端口号。
		 *	@return 端口被占用，则返回TRUE。端口未被占用，则返回FALSE。
		 */
		static bool CheckTcpPortState( IN unsigned num );

		/**	检测Udp端口是否被占用。
		 *	@param[in] num 端口号。
		 *	@return 端口被占用，则返回TRUE。端口未被占用，则返回FALSE。
		 */
		static bool CheckUdpPortState( IN unsigned num );

		/**	拷贝指定窗口中的文本。
		 *	@param[in] 句柄。
		 */
		static void CopyText (__in HWND hWnd);

		/**	延时。
		 *	@param[in] 延时时间。
		 *	@param[in] 时间单位。TimeUnit枚举中的值。
		 */
		static void Delay(__in LONGLONG delay, __in TimeUnit timeUnit);

		/**	模拟VB的DoEvents()函数。
		 */
		static void DoEvents();

		/**	提取图标。
		 *	@param[in] 保存路径。如：X:\XX\，生成的文件名为MrleeExtIcoX.ico，X表示数字，从1开始。如果文件存在，将覆盖。
		 *	@param[in] 提取的文件路径。
		 *	@note 当前只支持exe文件。
		 */
		static BOOL ExtractIcon_(__in CString &savePath, __in CString &filePath);

		/**	获得剪贴板文本。
		 *	@param[in] 窗口句柄。如果此参数为NULL，打开的剪贴板与当前任务相关联。
		 *	@param[out] 返回剪贴板文本。
		 */
		static BOOL GetClipboardText(__in HWND hWnd, __out CString &text);

		/**	获得CPU序列号。
		 *	@return 返回CPU序列号字符串。
		 */
		static CString GetCPUID();

		/**	获得当前桌面路径。
		 *	@return 返回当前桌面路径。
		 */
		static CString GetDesktopPath();

		/**	获得当前桌面路径。
		 *	@param[out] 返回当前桌面路径。
		 */
		static void GetDesktopPath(__out CString &DesktopPath);

		/**	计算函数大小。
		 *	@param[in] 函数指针。
		 *	@param[in] 【可选】限定函数最大大小，默认为0x1000。
		 *	限定最大大小有两个考虑：
		 *	1. 为了防止逻辑写的有问题而造成死循环。
		 *	2. 为了防止逻辑写的有问题而造成计算的函数大小比实际大小大很多。
		 *	@note 计算成功，则返回函数大小。计算失败，则返回0。目前只比较了C2和C3。
		 *		  函数失败可以通过GetLastError()函数查询失败原因。
		 */
		static DWORD GetFuncSize (__in PVOID pFunc, __in_opt DWORD dwMax = 0x1000);

		/**	获得跳转表的实际函数地址。
		 *	@param[in] 跳转命令地址。
		 *	@return 如果判断是跳转表，则进行计算，如果判断不是跳转表，则返回参数1。
		 */
		static PVOID GetJmpTableFuncAddr (__in PVOID pFunc);

		/**	当前操作系统是否是64位。
		 *	@return 当前操作系统是64位，则返回TRUE。当前操作系统不是64位，则返回FALSE。
		 */
		static BOOL Is64bitSystem();

		/**	Windows注销。
		 *	@param[in] 是否强制执行。true:强制执行。false:不强制执行。
		 */
		static void LogOff(__in bool isForced);

		/**	根据错误信息码，获得错误信息。
		 *	@param[in] 错误码。通过GetLastError()获得。
		 *	@param[out] 返回对应的错误信息。
		 */
		static CString Mrlee_ErrorMessage (__in DWORD dwLastError);

		/**	根据错误信息码，获得错误信息。
		 *	@param[in] 错误码。通过GetLastError()获得。
		 *	@param[out] 返回对应的错误信息。
		 */
		static void Mrlee_ErrorMessage (__in DWORD dwLastError, __out CString &message);

		/**	根据错误信息码，获得错误信息。
		 *	@param[in] 错误码。通过GetLastError()获得。
		 *	@param[out] 返回对应的错误信息。
		 *	@note 对系统错误消息的进一步理解。如ERROR_INVALID_WINDOW_HANDLE，
		 *		  系统解释为“无效的窗口句柄。”，而这个函数会解释为“未找到窗口。”。如果要系统的解释，可调用Mrlee_ErrorMessage函数。
		 *		  此函数的解释还有待完善，如果遇到无法解释的错误码，将使用系统的解释。
		 */
		static CString Mrlee_ErrorMessage_MY (__in DWORD dwLastError);

		/**	根据错误信息码，获得错误信息。
		 *	@param[in] 错误码。通过GetLastError()获得。
		 *	@param[out] 返回对应的错误信息。
		 *	@note 对系统错误消息的进一步理解。如ERROR_INVALID_WINDOW_HANDLE，
		 *		  系统解释为“无效的窗口句柄。”，而这个函数会解释为“未找到窗口。”。如果要系统的解释，可调用Mrlee_ErrorMessage函数。
		 *		  此函数的解释还有待完善，如果遇到无法解释的错误码，将使用系统的解释。
		 */
		static void Mrlee_ErrorMessage_MY(__in DWORD dwLastError, __out CString &message);

		/**	将文本粘贴到指定窗口中。
		 *	@param[in] 句柄。
		 */
		static void PasteText (__in HWND hWnd);

		/**	投递文本。
		 *	@param[in] 句柄。
		 *	@param[in] 文本。
		 */
		static void PostText (__in HWND hWnd, __in TCHAR *text);

		/**	电脑重启。
		 *	@param[in] 是否强制执行。true:强制执行。false:不强制执行。
		 */
		static void Reboot(__in bool isForced);

		/**	替换图标。
		 *	@param[in] icon文件的路径。
		 *	@param[in] 被替换图标的文件路径。
		 *	@return 替换成功，则返回TRUE。替换失败，则返回FALSE。
		 *	@note 当前只支持exe文件的图标替换。
		 */
		static BOOL ReplaceIcon(__in TCHAR *iconFilePath, __in TCHAR *filePath);

		/**	替换文本。
		 *	@param[in] 编辑框句柄。
		 *	@param[in] 文本。
		 */
		static void ReplaceText_ (__in HWND hWnd, __in TCHAR *text);

		/**	发送文本。
		 *	@param[in] 编辑框句柄。
		 *	@param[in] 文本。
		 */
		static void SendText (__in HWND hWnd, __in TCHAR *text);

		/**	文本全选。
		 *	@param[in] 句柄。
		 */
		static void SelectText (__in HWND hWnd);

		/**	设置剪贴板文本。
		 *	@param[in] 窗口句柄。如果此参数为NULL，打开的剪贴板与当前任务相关联。
		 *	@param[in] 要设置的文本。
		 */
		static BOOL SetClipboardText(__in HWND hWnd, __in char *text);

		/**	设置剪贴板文本。
		 *	@param[in] 窗口句柄。如果此参数为NULL，打开的剪贴板与当前任务相关联。
		 *	@param[in] 要设置的文本。
		 */
		static BOOL SetClipboardText(__in HWND hWnd, __in wchar_t *text);

		/**	设置开机启动。
		 *	@param[in] 应用程序路径。
		 *	@param[in] 应用程序名(不一定非要与应用程序名字一样)。
		 *	@return 返回BOOL。
		 *  TRUE  设置成功。
		 *  FALSE 设置失败。
		 */
		static BOOL SetToRunOnStartup(__in CString& strAppPath, __in CString& strAppName);

		/** 通过GetLastError的返回值，获得错误信息。
		 *  @param[in] 错误编号。可以通过GetLastError()获得。
		 *  @param[in] 接收字符数组。
		 *  @param[in] 接收字符数组长度。
		 *	@note 通过GetLastError的返回值，获得错误信息。此函数经测试可用于的系统：Win XP。
		 */
		static void ShowErrorInfo(IN DWORD errorCode, OUT LPTSTR lpInfo, IN int length);

		/**	电脑关机。
		 *	@param[in] 是否强制执行。true:强制执行。false:不强制执行。
		 */
		static void Shutdown(__in bool isForced);

		/**	获得鼠标点处的窗口标题。
		 *	@param[out] 返回窗口标题字符串。
		 */
		static BOOL WindowCaptionFromPoint (__out CString &windowCaption);

		
	};
}

/**	其实是一段音乐。
 */
void 鬼叫();

//////////////////////////////////////////////////////////////////////////
// 提权函数。

#ifdef __cplusplus
extern "C++" {

/** 启用/禁用特权。
 *  @param[in] 表示所要查看的特权信息的名称，定义在winnt.h中，具体指请MSDN索引"windows nt privileges"。
 *  @param[in] 启用/禁用特权。
 *  TRUE  启用。
 *  FALSE 禁用。
 *  @return 返回BOOL。
 *  TRUE  启用特权成功。
 *  FALSE 启用特权失败。
 *	@note 此函数经测试可用于的系统：Win XP。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL SuperPrivileges_EnablePrivilege(__in LPTSTR lpszPrivilegeName, __in BOOL bEnable);

/** 提高进程权限。若不提高进程权限，在调用OpenProcess()函数时可能NULL。
 *  @param[in] 启用/禁用特权。
 *  TRUE  启用。
 *  FALSE 禁用。
 *  @return 返回BOOL。
 *  TRUE  提高进程权限成功。
 *  FALSE 提高进程权限失败。
 *	@note 默认为SE_DEBUG_NAME特权。此函数经测试可用于的系统：Win XP。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL SuperPrivileges_EnableDebugPrivilege(__in BOOL bEnable);

/**	设置映射路径。
 *	@param[in] 进程句柄。
 *	@param[in] 映射路径。
 *	@note 如果替换的是系统进程的映射路径，则可过 HS 保护。\n
 *	详情参考: ring3下伪装进程名绕过HS 部分Inline hook ，有码\n
 *	http://bbs.pediy.com/showthread.php?t=153146
 *  @return 如果成功，返回 1。如果失败，则返回 0。
 */
BOOL SuperPrivileges_SetImagePath (__in HANDLE hProcess, __in UNICODE_STRING &szImagePath);

/**	设置映射路径。
 *	@param[in] 进程Id。
 *	@param[in] 映射路径。
 *	@note 如果替换的是系统进程的映射路径，则可过 HS 保护。\n
 *	详情参考: ring3下伪装进程名绕过HS 部分Inline hook ，有码\n
 *	http://bbs.pediy.com/showthread.php?t=153146
 *  @return 如果成功，返回 1。如果失败，则返回 0。
 */
BOOL SuperPrivileges_SetImagePath (__in DWORD dwPID, __in UNICODE_STRING &szImagePath);

/**	设置当前进程的映射路径。
 *	@note 
 */
BOOL SuperPrivileges_SetImagePath_CurProc (__in UNICODE_STRING &szImagePath);

}
#endif