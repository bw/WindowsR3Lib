//////////////////////////////////////////////////////////////////////////
// 最高基类。

#pragma once

namespace Mrlee_R3
{
	class CObjectBase
	{
	protected:
		CObjectBase();
		virtual ~CObjectBase();
		CString m_ErrorInfo;	// 错误信息。

	public:
		/**	获得类的错误信息。
		 *	@return 返回类的错误信息。
		 */
		virtual CString GetErrorInfo();
	};
}
