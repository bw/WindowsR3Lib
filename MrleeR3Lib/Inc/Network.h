#pragma once

/* 4字节的IP地址 */
typedef struct IPv4_Address{
	u_char byte1;
	u_char byte2;
	u_char byte3;
	u_char byte4;
}IPv4_Address;

namespace Mrlee_R3
{
	namespace Network
	{
		class CNetwork
		{
		public:
			/**	网络是否连接。
			 *	@return 网络连接，则返回TRUE。否则，返回FALSE。
			 */
			static BOOL IsConnected();

			/**	网络是否连接。
			 *	@return 网络连接，则返回TRUE。否则，返回FALSE。
			 */
			static BOOL IsConnected2();

			/**	打开网页。
			 *	@param[in] 网址。
			 *	@return 打开成功，则返回TRUE。打开失败，则返回FALSE。
			 */
			static BOOL OpenWeb(__in TCHAR *website);
		};
	}
}