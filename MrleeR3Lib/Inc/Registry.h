#pragma once

namespace Mrlee_R3
{
	namespace IO
	{
		enum CRegistryFlag
		{
			CRegistryFlag_ERROR = 0,		// 失败。
			CRegistryFlag_SUCCESS = 1,		// 成功。
			CRegistryFlag_NOT_FIND = 2,		// 未找到相应的键。
		};

		class CRegistry
		{
		public:
			/**	设置注册表项中的值。
			 *	@param[in]  一个已打开项的句柄，或指定一个标准项名。
			 *				可参照RegOpenKey函数的第一个参数。
			 *	@param[in]  子项名。可参照RegOpenKey函数的第二个参数。
			 *	@param[in]  欲删除值的名字。可参照RegDeleteValue函数的第二个参数。
			 *	@param[in]	欲删除值的类型。可参照RegQueryValueEx函数的第四个参数。
			 *	@return 返回CRegistry枚举类型。
			 *  设置成功返回CRegistry_SUCCESS。
			 *  设置失败返回CRegistry_ERROR。
			 *	如果子项或欲删除的值不存在返回CRegistry_NOT_FIND。
			 */
			static CRegistryFlag DeleteRegistryValue(
				__in HKEY hKey, 
				__in CString& lpSubKey, 
				__in CString& lpValueName, 
				__in DWORD dwType);

			static CRegistryFlag GetRegistryValue(
				__in HKEY hKey,
				__in_opt LPCWSTR lpSubKey,
				__in_opt LPCWSTR lpValueName,
				__out_opt LPDWORD lpType,
				__out_bcount_opt(*lpcbData) LPBYTE lpData,
				__inout_opt LPDWORD lpcbData
				);

			/**	设置注册表项中的值。
			 *	@param[in]  一个已打开项的句柄，或指定一个标准项名。
			 *				可参照RegCreateKey函数的第一个参数。
			 *	@param[in]  【可选】欲创建的新子项。可同时创建多个项，只需用反斜杠将它们分隔开即可。例如level1\level2\newkey。
			 *				可参照RegCreateKey函数的第二个参数。
			 *	@param[in]  【可选】要设置值的名字。
			 *				可参照RegSetValueEx函数的第二个参数。
			 *	@param[in]	要设置的值的类型。
			 *				可参照RegSetValueEx函数的第四个参数。
			 *	@param[in]	【可选】包含数据的缓冲区中的第一个字节。
			 *				可参照RegSetValueEx函数的第五个参数。
			 *	@param[in]	【可选】lpData缓冲区的长度。
			 *				可参照RegSetValueEx函数的第六个参数。
			 *	@return 返回CRegistry枚举类型。
			 *  设置成功返回CRegistry_SUCCESS。
			 *  设置失败返回CRegistry_ERROR。
			 *	@note 如指定的项已经存在，那么函数会打开现有的项。如果指定的项不存在，将会创建一个新项。
			 */
			static CRegistryFlag SetRegistryValue(
				__in HKEY hKey, 
				__in_opt CString& lpSubKey, 
				__in_opt CString& lpValueName, 
				__in DWORD dwType, 
				__in_bcount_opt(cbData) CONST BYTE* lpData, 
				__in DWORD cbData);
		};
	}
}