#pragma once

#include "ObjectBase.h"

namespace Mrlee_R3
{
	namespace Security
	{
		namespace Inject
		{
			class IInject : public CObjectBase
			{
			protected:
				/// 注入是否成功。
				CString m_DllPath;
				CString m_strInjectType;
				BOOL m_IsSuc;
				DWORD m_Pid;

			public:
				IInject();
				virtual ~IInject();

				virtual BOOL Eject();

				/** 获得注入类型字符串。 */
				CString GetInjectTypeString();

				/** 获得被注入进程的ID。 */
				DWORD GetPID();

				/**	注入是否成功。
				 *	@return 注入成功，则返回TRUE。否则，返回FALAE。
				 */
				BOOL IsInjectSuccess();
			};
		}
	}
}