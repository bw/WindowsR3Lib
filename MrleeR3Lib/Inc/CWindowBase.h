#pragma once

#include <map>
#include <vector>

#ifdef __cplusplus
extern "C++" {

typedef struct _FindWindowInfo
{
	HWND hWindow;				// 窗口句柄。
	CString m_WindowName;		// 窗口名称。
	BOOL isContain;				// 窗口名称是否是只要包含即可。
} FindWindowInfo, *PFindWindowInfo;

typedef enum _IsWindowForegroundFlag {
	IsWindowForegroundFlag_Pid,			// 进程Id。
	IsWindowForegroundFlag_ProcessName,	// 进程名。
	IsWindowForegroundFlag_WindowName,	// 窗口名。
	IsWindowForegroundFlag_ClassName,	// 窗口类名。
} IsWindowForegroundFlag;

/**	消息框。
 *	@param[in] 消息文本。
 *	@param[in] 【可选】消息框标题。
 *	@param[in] 【可选】消息框显示按钮类型。参照MessageBox的按钮类型。
 *	@param[in] 【可选】消息框所属窗口句柄。
 *	@return 返回值参照MessageBox的返回值。
 *	@note 当hWnd参数不传入的时候，创建的消息框是模式对话框。
 */
int MsgBox (__in LPCTSTR lpText, __in_opt LPCTSTR lpCaption = NULL, __in_opt UINT uType = MB_OK, __in_opt HWND hWnd = NULL);

/**	消息框。
 *	@param[in] 消息文本。
 *	@param[in] 【可选】消息框标题。
 *	@param[in] 【可选】消息框显示按钮类型。参照MessageBox的按钮类型。
 *	@param[in] 【可选】消息框所属窗口句柄。
 *	@return 返回值参照MessageBox的返回值。
 *	@note 当hWnd参数不传入的时候，创建的消息框不是模式对话框。
 */
int MsgBox_ND (__in LPCTSTR lpText, __in_opt LPCTSTR lpCaption = NULL, __in_opt UINT uType = MB_OK, __in_opt HWND hWnd = NULL);

/**	批量设置窗口内容。
 *	@param[in] 批量设置的控件。
 *	@param[in] 不进行设置的控件。
 *	@param[in] 要设置的内容。
 */
void BatchSetWinContent(__in CArray<CWnd*> *ctrls, __in const CWnd* excludes, __in TCHAR *content);

/**	批量设置窗口内容。
 *	@param[in] 批量设置的控件。
 *	@param[in] 不进行设置的控件。
 *	@param[in] 要设置的内容。
 */
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in TCHAR *content);

/**	批量设置窗口内容。
 *	@param[in] 批量设置的控件。
 *	@param[in] 不进行设置的控件。
 *	@param[in] 要设置的内容。
 */
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in CWnd* excludes, __in TCHAR *content);

/**	批量设置窗口内容。
 *	@param[in] 批量设置的控件。
 *	@param[in] 不进行设置的控件。
 *	@param[in] 要设置的内容。
 */
void BatchSetWinContent(__in std::map<int, CWnd*> *ctrls, __in std::map<int, CWnd*> *excludes, __in TCHAR *content);

/**	单选框组设置选中。
 *	@param[in] 单选框控件组。
 *	@param[in] 设置选中的控件Id。
 */
void RadioGroupSetCheck(__in CArray<CButton*> &radioGroup, __in int ctlId);

/**	判断传入的控件Id是否是选中状态。
 *	@param[in] 单选框控件组。
 *	@param[in] 要判断的控件Id。
 *	@return 如果选中，则返回TRUE。否则返回FALSE。
 */
BOOL IsCheckInRadioGroup(__in CArray<CButton*> &radioGroup, __in int ctlId);

/**	子窗口填充整个父窗口。
 *	@param[in] 子窗口句柄。
 */
void FillTheParentWindow(__in HWND hChild);

/**	获得窗口句柄。
 *	@param[in] 【可选】窗口类名。
 *	@param[in] 【可选】窗口标题。此窗口标题可以是窗口标题的一部分。
 *	@return 找到对应窗口，则返回窗口句柄。未找到对应窗口，则返回NULL。
 */
HWND FindWin (__in_opt LPTSTR lpClassName, __in_opt LPTSTR lpWindowName);

/**	查找顶层窗口句柄。此函数与系统的FindWindow函数实现原理不同，若FindWindow函数找不到窗口句柄，可以试试此函数。
 *	@param[in] 需要查找的窗口标题。
 *	@param[in] TRUE,查找到的窗口标题只要包含windowName字符串，就返回窗口句柄。
 *			   FALSE,查找到的窗口标题必须与windowName字符串完全相等，才返回窗口句柄。
 *	@return 如果找到相应的窗口则返回此窗口的句柄。如果未找到则返回NULL。
 */
HWND FindTopWindow (__in LPTSTR windowName, __in BOOL isContain);

/**	允许/禁止窗口关闭按钮。
 *	@param[in] 窗口句柄。
 *	@param[in] TRUE:启用。FALSE:禁用。
 *	@return 执行成功，则返回TRUE。执行失败，则返回FALSE。
 */
BOOL EnableWindowClose (__in HWND hWnd, __in BOOL bEnable);

/**	窗口是否在最前。
 *	@param[in] 根据参数lpVoid传入的数据来判断窗口是否在最前。
 *	@param[in] 当传入IsWindowForegroundFlag_Pid时，参数lpVoid应传入进程Id。
 *			   当传入IsWindowForegroundFlag_ProcessName时，参数lpVoid应传入进程名。
 *			   当传入IsWindowForegroundFlag_WindowName时，参数lpVoid应传入窗口名。
 *			   当传入IsWindowForegroundFlag_ClassName时，参数lpVoid应传入窗口类名。
 *	@return 窗口在最前，则返回TRUE。窗口不在最前，则返回FALSE。
 */
BOOL IsWindowForeground(__in void *lpVoid, __in IsWindowForegroundFlag isWindowForegroundFlag);

/**	窗口透明。
 *	@param[in] 窗口句柄。
 *	@param[in] TRUE:窗口透明。FALSE:窗口不透明。
 *	@return 设置成功，则返回TRUE。设置失败，则返回FALSE。
 */
BOOL TransparentWindow(__in HWND hWnd, __in BOOL isTransparent);

/**	关闭窗口。
 *	@param[in] 窗口句柄。
 */
void CloseWindow_ (__in HWND hWnd);

/**	窗口最大化。
 *	@param[in] 窗口句柄。
 */
void MaximizeWindow (__in HWND hWnd);

/**	窗口最小化。
 *	@param[in] 窗口句柄。
 */
void MinimizedWindow (__in HWND hWnd);

/**	窗口置顶。
 *	@param[in] 窗口句柄。
 *	@param[in] TRUE:窗口置顶。FALSE:取消窗口置顶。
 */
void WindowTopmost (__in HWND hWnd, __in BOOL isTopmost);

/**	获得所有窗口所属的线程。
 *	@param[in] 窗口线程数组。
 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
 */
void GetAllWindowThread (__out std::vector<DWORD> &threads);

/**	窗口抖动。
 *	@param[in] 窗口句柄。
 *	@param[in] 抖动次数。默认为3，且不能小于3。
 *	@param[in] 抖动频率。默认为10，且不能小于10。
 *	@return 成功，则返回TRUE。失败，则返回FALSE。
 */
BOOL 窗口抖动(__in HWND hWnd, __in int count = 3, __in int speed = 10);

}
#endif