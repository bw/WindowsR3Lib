#pragma once

#include <Windows.h>

#define MRLEE_ERROR_TYPE_SUCCESS	0
#define MRLEE_ERROR_TYPE_INFO		1
#define MRLEE_ERROR_TYPE_WARN		2
#define MRLEE_ERROR_TYPE_ERROR		3
#define MRLEE_ERROR_MAKE(errorType,expCode) ((((errorType<<30)|0x20000000)|((256+1)<<16))+expCode)
#define IsMrleeError(expCode) ((expCode&0x20000000)>0?1:0)

// 重复调用。
#define MRLEE_ERROR_CALL_REPEATED MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,1)

// 调用未卸载。
#define MRLEE_ERROR_CALL_NOT_UNLOAD MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,2)

// 路径是目录。
#define MRLEE_ERROR_IS_DIRECTORY MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,3)

// 进程未找到。
#define MRLEE_ERROR_PROCESS_NOT_FOUND MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,4)

// 字符未找到。
#define MRLEE_ERROR_CHARACTER_NOT_FOUND MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,5)

// 内存分配失败。
#define MRLEE_ERROR_MEMORY_ALLOTTED_FAILURE MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,6)

// 远程调用失败。
#define MRLEE_ERROR_REMOTE_CALL_FAILURE MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,7)

// 计算函数大小失败。
#define MRLEE_ERROR_CALC_FUN_SIZE_FAILURE MRLEE_ERROR_MAKE(MRLEE_ERROR_TYPE_ERROR,8)