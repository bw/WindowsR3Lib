#pragma once

namespace Mrlee_R3
{
	namespace File
	{
		#define CPEBase CPE

		/**	PE文件解析基类。
		 */
		class CPE : CObjectBase
		{
		protected:
			BOOL m_IsSuc;			// 解析是否成功。
			CString m_FilePath;		// 文件路径。
			ULONGLONG m_FileSize;	// 文件大小。

		protected:
			CPE();
			/**	构造函数。
			 *	@param[in] 文件路径。
			 */
			CPE(__in TCHAR *szFilePath);
			virtual ~CPE();

			/**	解析文件。
			 *	@param[in] 文件路径。
			 *	@return 解析成功，则返回TRUE。解析失败，则返回FALSE。
			 */
			BOOL ParseFile(__in TCHAR *szFilePath);

		private:
			/** 初始化成员变量。*/
			void Init();
		};

		/**	解析PE格式。
		 */
		class CPE32 : CPEBase
		{
		public:
			CPE32();
			virtual ~CPE32();
		};
	}
}