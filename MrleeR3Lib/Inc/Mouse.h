#pragma once

typedef enum _CMouseKey {
	CMouseKey_LeftButton,	// 左键。
	CMouseKey_MiddleButton,	// 中键。
	CMouseKey_RightButton, 	// 右键。
} CMouseKey;

typedef enum _CMouseControl {
	CMouseControl_Up,			// 鼠标弹起。
	CMouseControl_Down,			// 鼠标按下。
	CMouseControl_Click,		// 鼠标单击。
	CMouseControl_DoubleClick,	// 鼠标双击。
	CMouseControl_ScrollUp,		// 鼠标滚轮向上滚动。
	CMouseControl_ScrollDown,	// 鼠标滚轮向下滚动。
} CMouseControl;

namespace Mrlee_R3
{
	namespace IO
	{
		class CMouse
		{
		public:
			/**	鼠标移动。
			 *	@param[in] 水平坐标。
			 *	@param[in] 垂直坐标。
			 *	@param[in] 是否是绝对坐标。true:绝对坐标。false:相对坐标。
			 *	@note 绝对坐标：鼠标原点为，当前屏幕的0,0点。相对坐标：鼠标原点为，当前鼠标的坐标位置。
			 */
			static void MouseMove(__in LONG dx, __in LONG dy, __in bool isAbsoluteMove);

			/**	鼠标移动。
			 *	@param[in] 窗口句柄。
			 *			   如果传入NULL，则鼠标以桌面0,0点为原点进行移动。
			 *			   如果传入窗口句柄，则鼠标以窗口客户区0,0点为原点进行移动。
			 *	@param[in] 水平坐标。
			 *	@param[in] 垂直坐标。
			 */
			static BOOL MouseMove(__in HWND hWnd, __in LONG dx, __in LONG dy);

			/**	鼠标左键弹起。
			 */
			static void LeftButtonUp();

			/**	鼠标左键按下。
			 */
			static void LeftButtonDown();

			/**	鼠标左键单击。
			 *	@param[in] x坐标。
			 *	@param[in] y坐标。
			 *	@param[in] 是否是绝对坐标。true:绝对坐标。false:相对坐标。
			 *	@note 绝对坐标：鼠标原点为当前屏幕的0,0点。相对坐标：鼠标原点为当前坐标位置。
			 *		  【注意】当点击一个按钮时，执行CMouse::LeftButtonClick(10, 0, false);代码。鼠标移动的长度一般来说要超出10像素，这是为什么哪？
			 *		  这时的鼠标会移动10个像素然后单击，如果这个“击点”依旧在这个按钮上，那么会出现什么情况？
			 *		  出现的情况就是，会再一次执行鼠标单击响应函数！
			 */
			static void LeftButtonClick(__in LONG dx, __in LONG dy, __in bool isAbsoluteMove);

			/**	鼠标捕获。
			 *	@param[in] 窗口句柄。
			 *	@return 窗口句柄。
			 */
			static void MouseCapture(__in HWND hWnd);

			/**	限制鼠标活动范围。
			 *	@param[in] 左边距。
			 *	@param[in] 上边距。
			 *	@param[in] 右边距。
			 *	@param[in] 下边距。
			 *	@return 返回句柄，将这个句柄值，传入 恢复鼠标活动范围() 函数，将恢复鼠标的活动范围。
			 *	@note 与 恢复鼠标活动范围() 函数配合使用。
			 */
			static HANDLE 限制鼠标活动范围 (__in LONG left, __in LONG top, __in LONG right, __in LONG bottom);

			/**	恢复鼠标活动范围。
			 *	@param[in] 恢复鼠标范围事件。
			 *	@note 与 限制鼠标活动范围() 函数配合使用。
			 */
			static void 恢复鼠标活动范围 (__in HANDLE hRestoreEvent);

			/**	获得鼠标位置相对于窗口顶点的坐标。
			 *	@param[in] 窗口句柄。如果转入NULL，表示获得鼠标在屏幕中的坐标。否则获得鼠标相对于窗口的坐标。
			 *	@param[out] 返回坐标。
			 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
			 *	@note 这里的窗口顶点指的是窗口客户区顶点。
			 */
			static BOOL 鼠标位置相对于窗口顶点的坐标(__in HWND hWnd, __out POINT &point);
		};
	}
}