#pragma once

#include "PeLoaderBase.h"

namespace Mrlee_R3
{
	namespace File
	{
		/**	Pe加载器1。
		 */
		class CPeLoader32 : public CPeLoaderBase
		{
		private:
			/// 指向IMAGE_NT_HEADERS的指针
			IMAGE_NT_HEADERS32* m_NtHeader;

		public:
			/** 构造函数，初始化成员变量并打开文件映射。
			 *  @param[in] 文件路径。
			 */
			CPeLoader32();
			/**	析构函数。释放资源。 */
			virtual ~CPeLoader32();

			/** 获得程序装入内存的建议地址。
			 *  @return 获得程序装入内存的建议地址。
			 *	@note 建议地址，就是程序装入内存的基址。一般装入内存后的基址就是此函数的返回值。如，最常见的0x400000。
			 *	但既然叫“建议地址”，意思是有可能不按这个地址装入。\n
			 *	一般来说应用程序是按照这个地址来装入的，因为应用程序独占4GB内存空间，没有程序与之冲突，
			 *	而DLL则不然，可能装入的时候目标地址已经占用。\n
			 *	不清楚应用程序加壳后，这个建议地址是否也会变。
			 */
			DWORD GetImageBase();

			/** 获得指向IMAGE_NT_HEADERS的指针。
			 *  @return 返回IMAGE_NT_HEADERS的指针。
			 */
			const IMAGE_NT_HEADERS* GetImageNtHeader();

			/** 获得指向IMAGE_OPTIONAL_HEADER32的指针。
			 *  @return 返回IMAGE_OPTIONAL_HEADER32的指针。
			 */
			const IMAGE_OPTIONAL_HEADER32* GetImageOptionHeader();

			/**	获得线程局部存储目录指针。
			 *	@return 返回线程局部存储目录指针。
			 */
			const IMAGE_TLS_DIRECTORY32* GetImageTlsDirectory();

			/** 获得程序执行入口偏移。
			 *  @return 程序执行入口偏移。
			 *	@note 程序在内存中执行入口的地址，相对于基址的偏移。
			 */
			DWORD GetOEPOffset();

			/**	加载Pe文件。
			 *	@param[in] 文件路径。
			 *	@return 加载成功，返回TRUE。否则，返回FALSE。
			 *	@note 当不再需要映射Pe文件时，需要调用UnLoadPe()函数来释放映射。
			 */
			virtual BOOL LoadPe(__in TCHAR* szFilePath);
		};
	}
}