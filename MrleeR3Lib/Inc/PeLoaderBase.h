#pragma once

#include "commondef.h"

namespace Mrlee_R3
{
	namespace File
	{
		/**	Pe文件的数据目录项。可参考《Windows PE权威指南》第72页，表3-1 数据目录项描述。*/
		typedef enum _PeDataDirectory {
			PeDataDirectory_ExportTable = 0, 				/// 导出表。
			PeDataDirectory_ImportTable = 1,				/// 导入表。
			PeDataDirectory_ResourceTable = 2,				/// 资源表。
			PeDataDirectory_ExceptionTable = 3,				/// 异常表。
			PeDataDirectory_CertificateTable = 4,			/// 属性证书表。
			PeDataDirectory_BaseRelocationTable = 5,		/// 基址重定位表。
			PeDataDirectory_DebuggingInformation = 6,		/// 调试信息起始地址。
			PeDataDirectory_ArchitectureSpecificData = 7,	/// 预留为0。
			PeDataDirectory_GlobalPointerRegister = 8,		/// 指向全局指针寄存器。
			PeDataDirectory_ThreadLocalStorageTable = 9,	/// 线程局部存储(TLS)表。
			PeDataDirectory_LoadConfigurationTable = 10,	/// 加载配置表。
			PeDataDirectory_BoundImportTable = 11,			/// 绑定导入表。
			PeDataDirectory_ImportAddressTable = 12,		/// 导入函数地址表。
			PeDataDirectory_DelayImportDescriptor = 13,		/// 延迟导入表。
			PeDataDirectory_CLRRuntimeHeader = 14,			/// CLR运行时头部数据。
			PeDataDirectory_Reserved = 15,					/// 系统保留。
		} PeDataDirectory;

		/**	Pe文件类型。*/
		typedef enum _PeFileType {
			PeFileType_Unknown,	/// 未知。
			PeFileType_Pe32,	/// 32位文件。
			PeFileType_ROM,		/// ROM映像。
			PeFileType_PE64,	/// 64位文件。
		} PeFileType;

		/**	菜单资源由一个菜单头加一个菜单项的序列组成。菜单项有两种：弹出式（POPUP）菜单项和普通菜单项。
		 *	详见：《Windows PE权威指南》第217页，菜单资源数据结构。
		 */
		/** 菜单项标记。*/
		typedef enum _MenuItemFlag {
			MenuItemFlag_Grayed			 = 0x0001,	// 灰色。
			MenuItemFlag_Inactive		 = 0x0002,	// 未激活。
			MenuItemFlag_Bitmap			 = 0x0004,	// 位图。
			MenuItemFlag_OwnerDraw		 = 0x0100,	// 自画。
			MenuItemFlag_Checked		 = 0x0008,	// 已选中。
			MenuItemFlag_Popup			 = 0x0010,	// 弹出菜单。
			MenuItemFlag_MenuBarBreak	 = 0x0020,	// 菜单条分隔。
			MenuItemFlag_MenuBreak		 = 0x0040,	// 菜单分隔。
			MenuItemFlag_EndMenu		 = 0x0080,	// 菜单结束。
		} MenuItemFlag;
		/** 菜单头结构。*/
		typedef struct _MenuHeader {
			WORD Version;	// 版本号。暂时取值为0。
			WORD HeaderSize;// 头大小。暂时取值为0。
		} MenuHeader;

		/** 普通菜单项。菜单分隔符MENUITEM SEPARATE也是普通菜单项，不过其名称为空，ID为0。*/
		typedef struct _NormalMenuItem {
			WORD ItemFlags;		// 菜单标志。
			WORD MenuId;		// 菜单ID。
			wchar_t szItemText[1];	// Unicode格式字符串，以0结尾，不确定长度。
		} NormalMenuItem;

		/** 弹出式菜单。*/
		typedef struct _PopupMenuItem {
			WORD ItemFlags;		// 菜单标志。
			wchar_t szItemText[1];	// Unicode格式字符串，以0结尾，不确定长度。
		} PopupMenuItem;

		// 设置对齐方式为一个字节。
		// 如果不设置以一个字节对齐，编译器可能将ICON_DIR_ENTRY_InExe结构的大小按照四个字节的大小进行对齐。
		// 那么ICON_DIR_ENTRY_InExe结构的大小将为16字节，而不是14字节。
		#pragma  pack (push,1)
		/** ICON文件结构。
		 *	详见：《Windows PE权威指南》第219页。
		 */
		/** ico文件的图标目录项。
		 */
		typedef struct _ICON_DIR_ENTRY {
			BYTE Width;			// 宽度。
			BYTE Height;		// 高度。
			BYTE ColorCount;	// 颜色数。
			BYTE Reserved;		// 保留字，必须为0。
			WORD Planes;		// 调色板。
			WORD BitCount;		// 每个像素的位数。
			DWORD BytesInRes;	// 资源长度。
			DWORD ImageOffset;	// 资源在文件的偏移。
		} ICON_DIR_ENTRY;
		/**	Exe文件的图标目录项。
		 */
		typedef struct _ICON_DIR_ENTRY_FORPE {
			BYTE Width;			// 宽度。
			BYTE Height;		// 高度。
			BYTE ColorCount;	// 颜色数。
			BYTE Reserved;		// 保留字，必须为0。
			WORD Planes;		// 调色板。
			WORD BitCount;		// 每个像素的位数。
			DWORD BytesInRes;	// 资源长度。
			WORD Id;			// 图标资源Id。对应图标资源中的资源Id。
		} ICON_DIR_ENTRY_InExe;

		/** ICON文件头部结构。*/
		typedef struct _ICON_DIR_HEADER {
			WORD Reserved;	// 保留字，必须为0。
			WORD Type;		// 资源类别，如果为1表示为ICON文件。
			WORD Count;		// 图标数量。
		} ICON_DIR_HEADER;
		// 图标目录头紧跟着图标目录项数组。数组元素各位为ICON_DIR_HEADER.Count。
		#pragma pack(pop)	// 恢复对齐状态。

		/**	延迟加载导入表。
		 *	结构体见《Windows PE权威指南》第248页。
		 */
		typedef struct _IMAGE_DELAY_IMPORT_DESCRIPTOR {
			DWORD Attributes;	// 属性，必须为0。
			DWORD Name;			// 指向DLL名称的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD ModuleHandle;	// DLL模块句柄的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD DelayIAT;		// 延迟加载导入IAT的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD DelayINT;		// 延迟加载导入INT的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD BoundDelayIT;	// 绑定延迟加载导入地址表的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD UnloadDelayIT;// 卸载延迟加载导入地址表的RVA。要获得FOA需要RVA - IMAGE_OPTIONAL_HEADER.ImageBase，再计算FOA。
			DWORD TimeStamp;	// 此映像绑定到DLL的时间戳。
		} IMAGE_DELAY_IMPORT_DESCRIPTOR;

		/** 对话框资源数据结构。*/
		/** 对话框头。*/
		//typedef struct _DialogBoxHeader {
		//	DWORD Style;			// 标准窗口样式。
		//	DWORD ExtendedStyle;	// 扩展窗口样式。
		//	WORD NumberOfItems;		// 控件数量。
		//	WORD X;					// 起点坐标X。
		//	WORD Y;					// 起点坐标Y。
		//	WORD CX;				// 宽度。
		//	WORD CY;				// 高度。
		//	[名称或序数]MenuName;	// 菜单名或ID。若第一个字为0xFFFF，则第二个字就是一个序数ID，如果第一个字为0x0000，则表示一个空字符串。
		//	[名称或序数]ClassName;	// 类名获得ID。若第一个字为0xFFFF，则第二个字就是一个序数ID，如果第一个字为0x0000，则表示一个空字符串。
		//	wchar_t Caption[];		// 对话框标题Unicode字符。
		//	WORD PointSize;			// 字体大小（如果有）。
		//	wchar_t FontName[];		// 字体名（如果有）。
		//} DialogBoxHeader;
		/** 控件数据。*/
		//typedef struct _ControlData {
		//	DWORD Style;			// 标准窗口样式。
		//	DWORD ExtendedStyle;	// 扩展窗口样式。
		//	WORD X;					// 起点坐标X。
		//	WORD Y;					// 起点坐标Y。
		//	WORD CX;				// 宽度。
		//	WORD CY;				// 高度。
		//	WORD Id;				// 控件ID。
		//	[名称或序数]ClassID;	// 控件类型。详见：《Windows PE权威指南》第225页。
		//	[名称或序数]Text;		// 控件类型名称，Unicode字符。
		//	WORD ExtraStuff;		// 附加信息。
		//} ControlData;

		/**	Pe加载器基类。
		 */
		class CPeLoaderBase : public CObjectBase
		{
		protected:
			/// 代码偏移。
			PVOID m_ImageCodeOffset;
			/// 数据偏移。
			PVOID m_ImageDataOffset;
			/// 数据目录指针。
			IMAGE_DATA_DIRECTORY *m_DataDirectory;
			/// 数据目录数组中元素的个数。一般来说为16。
			DWORD m_DataDirectoryCount;
			/// 指向IMAGE_DOS_HEADER的指针。
			IMAGE_DOS_HEADER *m_DosHeader;
			/// 指向IMAGE_FILE_HEADER的指针。
			IMAGE_FILE_HEADER *m_FileHeader;
			/// 文件中节的对齐粒度。
			DWORD m_FileAlignmentSize;
			/// 映射文件在内存空间的基址指针。
			PVOID m_FilePoint;
			/// 文件大小
			LONGLONG m_FileSize;
			/// 文件类型魔术字。见《Windows PE权威指南》第78页，3.5.3节IMAGE_NT_HEADERS32.Magic的介绍。
			WORD m_FileTypeMagic;
			/// 加载Pe文件是否成功。
			BOOL m_IsLoad;
			/// 是否是64位PE文件。
			BOOL m_IsPE64;
			/// 内存中节的对齐粒度。
			/// 内存中的数据存取以页面为单位。Win32的页面大小为4KB，所以Win32 PE中节的内存对齐粒度一般都选择4KB。
			DWORD m_MemoryAlignmentSize;
			/// 打开Pe文件映射是否成功。在OpenFileMap函数中设置。TRUE成功。FALSE失败。
			/// 节的个数。
			/// Windows XP中，可以有0个节，但数值不能小于1，也不能超过96。
			/// 如果将其设置为0，则操作系统装载时会提示不是有效的Win32程序。
			/// 如果想在PE中增加或删除节，必须变更此处的值。
			/// 节的个数既不能比实际内存中存在的节多，也不能比它少，否则装载会发生错误，提示不是有效的Win32应用程序。
			WORD m_NumberOfSections;
			/// 所有头+节表按照文件对齐粒度后的大小（即含补足的0）。
			DWORD m_PeHeaderSize;
			/// 文件完整路径。
			CString m_Path;
			/// 指向节表起始位置的指针
			IMAGE_SECTION_HEADER *m_SectionHeader;

		public:
			CPeLoaderBase();
			virtual ~CPeLoaderBase();

			/** 将数据在文件中的地址转换成内存中的RVA。
			 *  @return 转换成功则返回RVA。转换失败返回-1。
			 */
			long FOAToRVA(__in DWORD fileAddress);

			/** 根据指定对齐粒度，计算对其后的大小。
			 *	@param[in] 对其前的大小。
			 *	@param[in] 对齐粒度。
			 *  @return 根据指定对齐粒度，计算对其后的大小。
			 */
			static DWORD GetAlignedSize(__in DWORD dwCurSize, __in DWORD dwAlignment);

			/**	获得Dos头大小。
			 *	@return 返回Dos头大小。
			 *	@note 此函数内只有一行代码：return sizeof(IMAGE_DOS_HEADER);。只要PE文件格式不做改变，那么IMAGE_DOS_HEADER结构的大小64字节。
			 */
			UINT GetDosHeaderSize();

			/**	获得文件指针。
			 *	@return 返回文件指针。
			 *	@note 映射文件在内存空间的基址指针。
			 */
			PVOID GetFilePoint();

			/**	获得导出函数名地址。
			 *	@param[in] 【可选】导出函数名个数。
			 *	@return 返回导出函数名地址。
			 *  @par 示例:
			 *	@code
				CPeLoader32 pe;
				pe.LoadPe(TEXT("X:\\XX\\XXX.dll"));
				PVOID filePoint = pe.GetFilePoint();
				DWORD numberOfNames = 0;
				DWORD* nameAddress = pe.GetAddressOfExportNames(&numberOfNames);
				for (int i = 0; i < numberOfNames; i++)
				{
					long nameFOA = (DWORD)pe.RVAToFOA(nameAddress[i]);
					// 打印导出函数。
					TRACE("Name=%s\n", (BYTE*)filePoint + nameFOA);
				}
				pe.UnLoadPe();
			 *  @endcode
			 */
			DWORD* GetAddressOfExportNames(__out_opt DWORD *numberOfNames);

			/**	获得文件大小。
			 *	@return 返回文件大小。
			 */
			LONGLONG GetFileSize_();

			/**	获得重定位表指针。
			 *	@return 获得成功，则返回重定位表指针。获得失败，则返回NULL。
			 *	@note IMAGE_BASE_RELOCATION_.TypeOffset的高四位含义可参考《Windows PE权威指南》191页，表6-6 IMAGE_BASE_RELOCATION_.SizeOfBlock高四位含义表。
			 *  @par 示例:
			 *	@code
				// 遍历重定位表。
				CPeLoader32 pe;
				pe.LoadPe(TEXT("X:\\XX\\XXX.exe"));
				const IMAGE_BASE_RELOCATION_* relocationTable = pe.GetImageBaseRelocation();
				int j = 1;
				while(relocationTable->VirtualAddress) {
					DWORD size = 0;
					DWORD relocationItemCount = pe.GetImageBaseRelocationItemNum(relocationTable, &size);
					TRACE("重定位信息：0x%08X-%d\n", relocationTable->VirtualAddress, relocationItemCount);
					for (int i = 0; i < relocationItemCount; i++)
					{
						WORD temp = relocationTable->TypeOffset[i] & 0xF000;	// 获得高四位。
						DWORD address = 0;
						// 重定位地址指向的双字的32位都需要休正？
						if (0x3000 == temp) {
							temp = relocationTable->TypeOffset[i] & 0x0FFF;
							address = relocationTable->VirtualAddress + (DWORD)temp;	// 得到修正以前的偏移，该偏移加上装入时的基址就是绝对地址。
						} else {
							address = -1;	// -1表示该重定位项无意义，不需要重定位，仅用来作为对齐。
						}
						TRACE("\t%02d-0x%08X\n", j++, address);
					}
					relocationTable = (const IMAGE_BASE_RELOCATION_*)((BYTE*)relocationTable + size);
				}
				pe.UnLoadPe();
			 *  @endcode
			 */
			const IMAGE_BASE_RELOCATION_* GetImageBaseRelocation();

			/**	获得重定位表项的个数。
			 *	@param[in] 重定位表。
			 *	@param[out] 【可选】返回这个重定位表带上表项，总共的大小。
			 *	@return 获得成功，则返回表项个数。获得失败，则返回0。
			 */
			DWORD GetImageBaseRelocationItemNum (__in const IMAGE_BASE_RELOCATION_ *relocationTable, __out_opt DWORD *size);

			/**	获得映射代码的偏移。
			 *	@return 返回映射代码的偏移。
			 *	@note IMAGE_OPTIONAL_HEADER.BaseOfCode。
			 */
			PVOID GetImageCodeOffset();

			/** 获得指向数据目录起始位置的指针。数据目录的结构体是IMAGE_DATA_DIRECTORY。
			 *  @return 返回IMAGE_DATA_DIRECTORY的指针。
			 */
			const IMAGE_DATA_DIRECTORY* GetImageDataDirectory();

			/** 获得指向特定数据目录结构的指针。数据目录的结构体是IMAGE_DATA_DIRECTORY。
			 *	@param[in] 想要获得哪种数据目录的指针。
			 *  @return 返回IMAGE_DATA_DIRECTORY的指针。
			 */
			const IMAGE_DATA_DIRECTORY* GetImageDataDirectory(__in PeDataDirectory peDataDirectory);

			/**	获得映射数据的偏移。
			 *	@return 返回映射数据的偏移。
			 *	@note IMAGE_OPTIONAL_HEADER.BaseOfData。
			 */
			PVOID GetImageDataOffset();

			/**	获得延迟加载导入表描述符指针。
			 *	@return 获得成功，则返回延迟加载导入表描述符指针。获得失败，则返回NULL。
			 */
			const IMAGE_DELAY_IMPORT_DESCRIPTOR* GetImageDelayImportDescriptor();

			/** 获得指向IMAGE_DOS_HEADER的指针。
			 *  @return 返回IMAGE_DOS_HEADER的指针。
			 */
			const IMAGE_DOS_HEADER* GetImageDosHeader();

			/**	获得导出目录指针。
			 *	@return 获得成功，则返回导出目录指针。获得失败，则返回NULL。
			 */
			const IMAGE_EXPORT_DIRECTORY* GetImageExportDirectory();

			/** 获得指向IMAGE_FILE_HEADER的指针。
			 *  @return 返回IMAGE_FILE_HEADER的指针。
			 */
			const IMAGE_FILE_HEADER* GetImageFileHeader();

			/** 获得文件映射入内存后的大小。
			 *  @return 成功: 获得文件映射入内存后的大小。失败: 0。
			 */
			DWORD GetImageFileSize();

			/**	获得资源目录地址。
			 *	@return 获得成功，则返回资源目录地址。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY* GetImageResourceDirectory();

			/**	获得特定的资源目录项指针。
			 *	@param[in] 资源类型。
			 *			   获得第一层子目录的目录项，如：传入RT_DIALOG，就是获得对话框类型的一级子目录项。
			 *			   或 IMAGE_RESOURCE_DIRECTORY_ENTRY.Name的高位为1，则表示不是标准的类型，传入(IMAGE_RESOURCE_DIRECTORY_ENTRY.Name & 0x7FFFFFFF)则将去查找指定偏移位置的字符串进行比较。
			 *	@param[out] 【可选】返回二级子目录地址。二级子目录按照资源的ID分类。
			 *	@return 获得成功，则返回资源目录项指针。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY_ENTRY* GetImageResourceDirectoryEntry1(__in DWORD type, __out_opt IMAGE_RESOURCE_DIRECTORY **resDir2 = NULL);

			/**	获得对话框的资源目录项指针。
			 *	@param[out] 【可选】返回对话框的二级子目录地址。二级子目录按照资源的ID分类。
			 *	@return 获得成功，则返回对话框的资源目录项指针。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY_ENTRY* GetImageResourceDirectoryEntryOfDialog(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfDialog2 = NULL);

			/**	获得图标的资源目录项指针。
			 *	@param[out] 【可选】返回图标的二级子目录地址。二级子目录按照资源的ID分类。
			 *	@return 获得成功，则返回图标的资源目录项指针。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY_ENTRY* GetImageResourceDirectoryEntryOfIcon(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfIcon2 = NULL);

			/**	获得图标组的资源目录项指针。
			 *	@param[out] 【可选】返回图标组的二级子目录地址。二级子目录按照资源的ID分类。
			 *	@return 获得成功，则返回图标组的资源目录项指针。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY_ENTRY* GetImageResourceDirectoryEntryOfIconGroup(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfIconGroup2 = NULL);

			/**	获得菜单的资源目录项指针。
			 *	@param[out] 【可选】返回菜单的二级子目录地址。二级子目录按照资源的ID分类。
			 *	@return 获得成功，则返回菜单的资源目录项指针。获得失败，则返回NULL。
			 */
			const IMAGE_RESOURCE_DIRECTORY_ENTRY* GetImageResourceDirectoryEntryOfMenu(__out_opt IMAGE_RESOURCE_DIRECTORY **resDirOfMenu2 = NULL);

			/** 获得指向节表起始位置的指针。节表的数据结构是IMAGE_SECTION_HEADER。
			 *  @return 返回IMAGE_SECTION_HEADER的指针。
			 */
			const IMAGE_SECTION_HEADER* GetImageSectionHeader();

			/** 获得PE文件中节的个数。
			 *  @return 返回节的个数。
			 *	@note Windows XP中，可以有0个节，但数值不能小于1，也不能超过96。
			 *		  如果将其设置为0，则操作系统装载时会提示不是有效的Win32程序。
			 *		  如果想在PE中增加或删除节，必须变更此处的值。
			 *		  节的个数既不能比实际内存中存在的节多，也不能比它少，否则装载会发生错误，提示不是有效的Win32应用程序。
			 */
			DWORD GetNumberOfSections();

			/**	获得Pe头大小。
			 *	@return 返回Pe头大小。
			 *	@note 所有头+节表按照文件对齐粒度后的大小（即含补足的0）。
			 *	m_FileHeader.SizeOfOptionalHeader 指定结构IMAGE_OPTIONAL_HEADER的长度。
			 *	默认情况下这个值为0x00E0，如果是64位PE文件该结构的默认大小为0x00F0。
			 */
			UINT GetPeHeaderSize();

			/**	获得Pe文件的类型。
			 *	@return 返回Pe文件类型。
			 *	@note 见《Windows PE权威指南》第78页，3.5.3节IMAGE_NT_HEADERS32.Magic的介绍。
			 */
			PeFileType GetPeType();

			/**	获得资源类型名。
			 *	@param[in] 类型Id。传入IMAGE_RESOURCE_DIRECTORY_ENTRY.Name，将第一层目录的Name字段传入才能得到正确的值。
			 *	@param[in] 第几层目录。
			 *	@param[out] 类型名。如果类型Id是标准的类型，则返回标准类型名。如果类型Id是非标准的类型，则返回“非标准类型”。如果类型既不是标准类型，也不是非标准类型，则返回“非法类型”。
			 *	@return 如果类型Id是标准的类型，则返回1。如果类型Id是非标准的类型，则返回-1。如果类型既不是标准类型，也不是非标准类型，则返回0。
			 */
			int GetResourceTypeName(__in DWORD typeId, __in DWORD directoryLevel, __out CStringW &typeName);

			/**	获得资源类型名。
			 *	@param[in] IMAGE_RESOURCE_DIRECTORY_ENTRY类型的指针。将第一层目录项的指针传入才能得到正确的值。
			 *	@param[in] 第几层目录。
			 *	@param[out] 类型名。如果类型Id是标准的类型，则返回标准类型名。如果类型Id是非标准的类型，则返回“非标准类型”。如果类型既不是标准类型，也不是非标准类型，则返回“非法类型”。
			 *	@return 如果类型Id是标准的类型，则返回1。如果类型Id是非标准的类型，则返回-1。如果类型既不是标准类型，也不是非标准类型，则返回0。
			 */
			int GetResourceTypeName(__in const IMAGE_RESOURCE_DIRECTORY_ENTRY *resourceDirectoryEntry, __in DWORD directoryLevel, __out CStringW &typeName);

			/**	是否是32为应用程序。
			 *	@return 32位应用程序，则返回1。64位应用程序，则返回0。无法确定，则返回-1。
			 *	@note 在《Windows PE权威指南》第76页，表3-2中，有详细介绍IMAGE_FILE_HEADER.Machine的值。
			 */
			int Is32BitApplication();

			/**	是否加载成功。
			 *	@return 加载成功，返回TRUE。加载失败，返回FALSE。
			 */
			BOOL IsLoad();

			/** 验证PE文件是否有效。
			 *  @param[in] PE文件载入内存后的基址。
			 *  @return 返回BOOL。
			 *  TRUE  PE文件有效。
			 *  FALSE PE文件无效。
			 */
			static BOOL IsPE(__in PVOID pPeBaseAddr);

			/**	加载Pe文件。
			 *	@param[in] 文件路径。
			 *	@return 加载成功，返回TRUE。否则，返回FALSE。
			 *	@note 当不再需要映射Pe文件时，需要调用UnLoadPe()函数来释放映射。
			 */
			virtual BOOL LoadPe(__in TCHAR* szFilePath);

			/**	获得页面保护类型。
			 *	@param[in] 传入 IMAGE_SECTION_HEADER.Characteristics。
			 *  @return 返回类似 PAGE_XXXX 宏的值。
			 */
			static DWORD PageProtectType(__in DWORD dwCharacteristics);

			/** 将内存中的RVA转换成文件偏移。
			 *  @return 转换成功则返回文件偏移。转换失败返回-1。
			 */
			long RVAToFOA(__in DWORD dwRVA);

			/**	卸载Pe文件。
			 */
			virtual void UnLoadPe();
		};
	}
}