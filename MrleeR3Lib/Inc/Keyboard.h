#pragma once

#ifndef MAPVK_VK_TO_VSC
#define MAPVK_VK_TO_VSC		(0)
#endif
#ifndef MAPVK_VSC_TO_VK
#define MAPVK_VSC_TO_VK		(1)
#endif
#ifndef MAPVK_VK_TO_CHAR
#define MAPVK_VK_TO_CHAR	(2)
#endif
#ifndef MAPVK_VSC_TO_VK_EX
#define MAPVK_VSC_TO_VK_EX	(3)
#endif
#ifndef MAPVK_VK_TO_VSC_EX
#define MAPVK_VK_TO_VSC_EX	(4)
#endif

namespace Mrlee_R3
{
	namespace IO
	{
		typedef enum _CKeyboardFlag {
			CKeyboardFlag_KeyDelay,		// 按键时延时。
			CKeyboardFlag_KeyDown,		// 键盘按下。
			CKeyboardFlag_KeyUp,		// 键盘弹起。
			CKeyboardFlag_KeyChar,		// 键盘字符。一般对应WM_CHAR消息。
			CKeyboardFlag_KeyDownUp,	// 对应WM_KEYDOWN、WM_KEYUP。
			CKeyboardFlag_SYSKeyDownUp	// 对应WM_SYSKEYDOWN、WM_SYSKEYUP。
		} CKeyboardFlag;

		enum
		{
			MaxExtendedVKeys = 10
		};

		/**	Shift键是否按下。
		 *	@return Shift键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsShiftPressed()  ((GetKeyState(VK_SHIFT) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	Ctrl键是否按下。
		 *	@return Ctrl键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsCtrlPressed()  ((GetKeyState(VK_CONTROL) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	Alt键是否按下。
		 *	@return Alt键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsAltPressed()  ((GetKeyState(VK_MENU) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	Caps Lock键是否按下。
		 *	@return Caps Lock键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsCapsLockPressed()  ((GetKeyState(VK_CAPITAL) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	Num Lock键是否按下。
		 *	@return Num Lock键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsNumLockPressed()  ((GetKeyState(VK_NUMLOCK) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	Scroll Lock键是否按下。
		 *	@return Scroll Lock键按下，则返回TRUE，否则返回FALSE。
		 */
		#define IsScrollLockPressed()  ((GetKeyState(VK_SCROLL) & (1<<(sizeof(SHORT)*8-1))) != 0)

		/**	键盘。
		 */
		class CKeyboard
		{
		public:
			static const UINT_PTR ExtendedVKeys[MaxExtendedVKeys];	// 扩展键数组。

			/**	检查虚拟键值是否是一个扩展键。
			 *	@param[in] 虚拟键值。
			 *	@return 如果是一个扩展键，则返回true。否则返回false。
			 */
			static bool IsVkExtended(__in UINT_PTR VKey);

			/**	发送按键。
			 *	@param[in] 虚拟键值。
			 *	@param[in] 按键延时。
			 */
			static void SendKey (__in BYTE bVk, __in_opt LONGLONG delay = 0);

			/**	弹起键。
			 *	@param[in] 虚拟键值。
			 */
			static void SendKeyUp (__in BYTE bVk);

			/**	按下键。
			 *	@param[in] 虚拟键值。
			 */
			static void SendKeyDown (__in BYTE bVk);

			/**	模拟按键。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 键盘状态。接收：CKeyboardFlag_KeyDown、CKeyboardFlag_KeyUp、CKeyboardFlag_KeyDownUp、CKeyboardFlag_KeyDelay。
			 *	@param[in] 当参数2设置CKeyboardFlag_KeyDelay标志时，此参数才有效。
			 *	@note 使用keybd_event()函数模拟按键。不能模拟小键盘关闭Num Lock后的按键。
			 *  @par 示例:
			 *	@code
				// 按Alt + T键。
				CKeyboard::模拟按键_KeyEvent(VK_MENU, CKeyboardFlag_KeyDown);	// 按下Alt键。
				CKeyboard::模拟按键_KeyEvent(CKey_T, CKeyboardFlag_KeyDown);	// 按下T键。
				CKeyboard::模拟按键_KeyEvent(CKey_T, CKeyboardFlag_KeyUp);		// 弹起T键。
				CKeyboard::模拟按键_KeyEvent(VK_MENU, CKeyboardFlag_KeyUp);		// 弹起Alt键。

				// 按Alt + T键。
				CKeyboard::模拟按键_KeyEvent(VK_MENU, CKeyboardFlag_KeyDown);	// 按下Alt键。
				CKeyboard::模拟按键_KeyEvent(CKey_T, CKeyboardFlag_KeyDownUp);	// 按下+弹起T键。
				CKeyboard::模拟按键_KeyEvent(VK_MENU, CKeyboardFlag_KeyUp);		// 弹起Alt键。

				// CKeyboardFlag_KeyDelay的用法。
				DWORD delay = 20;
				HWND hWnd = ::FindWindow(NULL, TEXT("Notepad.txt - 记事本"));	// 获得窗口名为“Notepad.txt - 记事本”的窗口。
				if (!hWnd) {
				::MessageBox(NULL, TEXT("窗口未找到。"), TEXT("提示"), MB_OK);
				return;
				}
				::SendMessage(hWnd, WM_SYSCOMMAND, SC_HOTKEY, (LPARAM) hWnd);
				::SendMessage(hWnd, WM_SYSCOMMAND, SC_RESTORE, (LPARAM) hWnd);

				::ShowWindow(hWnd, SW_SHOW);
				::SetForegroundWindow(hWnd);	// 将窗口设置到最前。
				::SetFocus(hWnd);
				CKeyboard::模拟按键_SendInput('F', CKeyboardFlag_KeyDelay, delay);		// 发送'F'到窗口。这个'F'在目标窗口中将显示小写。不要传入'f'，如果传入'f'在目标窗口中将不会显示为f。
				CKeyboard::模拟按键_SendInput(VK_DELETE, CKeyboardFlag_KeyDelay, delay);// 发送delete键消息到目标窗口。
				CKeyboard::模拟按键_SendInput(CKey_F, CKeyboardFlag_KeyDelay, delay);	// CKey_F代表的就是'F'。

				// nihao，如果对应的窗口打开了输入法，那么将显示“你好”两个字。
				CKeyboard::模拟按键_SendInput(CKey_N, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_I, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_H, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_A, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_O, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(VK_SPACE, CKeyboardFlag_KeyDelay, 20);// 空格。
				CKeyboard::模拟按键_SendInput(13, CKeyboardFlag_KeyDownUp);	// 回车。
				CKeyboard::模拟按键_SendInput(10, CKeyboardFlag_KeyDownUp);	// 换行。
			 *  @endcode
			 */
			//static void 模拟按键_KeyEvent(__in BYTE bVk, __in CKeyboardFlag keyboardFlag, __in_opt LONGLONG delay = 0);

			/**	模拟按键。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 键盘状态。接收：CKeyboardFlag_KeyDown、CKeyboardFlag_KeyUp、CKeyboardFlag_KeyDownUp、CKeyboardFlag_KeyDelay。
			 *	@param[in] 当参数2设置CKeyboardFlag_KeyDelay标志时，此参数才有效。
			 *	@note 使用SendInput()函数模拟按键。不能模拟小键盘关闭Num Lock后的按键。
			 *  @par 示例:
			 *	@code
				// 按Alt + T键。
				CKeyboard::模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyDown);	// 按下Alt键。
				CKeyboard::模拟按键_SendInput(CKey_T, CKeyboardFlag_KeyDown);	// 按下T键。
				CKeyboard::模拟按键_SendInput(CKey_T, CKeyboardFlag_KeyUp);		// 弹起T键。
				CKeyboard::模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyUp);	// 弹起Alt键。

				// 按Alt + T键。
				CKeyboard::模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyDown);	// 按下Alt键。
				CKeyboard::模拟按键_SendInput(CKey_T, CKeyboardFlag_KeyDownUp);	// 按下+弹起T键。
				CKeyboard::模拟按键_SendInput(VK_MENU, CKeyboardFlag_KeyUp);	// 弹起Alt键。

				// CKeyboardFlag_KeyDelay的用法。
				DWORD delay = 20;
				HWND hWnd = ::FindWindow(NULL, TEXT("Notepad.txt - 记事本"));	// 获得窗口名为“Notepad.txt - 记事本”的窗口。
				if (!hWnd) {
					::MessageBox(NULL, TEXT("窗口未找到。"), TEXT("提示"), MB_OK);
					return;
				}
				::SendMessage(hWnd, WM_SYSCOMMAND, SC_HOTKEY, (LPARAM) hWnd);
				::SendMessage(hWnd, WM_SYSCOMMAND, SC_RESTORE, (LPARAM) hWnd);

				::ShowWindow(hWnd, SW_SHOW);
				::SetForegroundWindow(hWnd);	// 将窗口设置到最前。
				::SetFocus(hWnd);
				CKeyboard::模拟按键_SendInput('F', CKeyboardFlag_KeyDelay, delay);		// 发送'F'到窗口。这个'F'在目标窗口中将显示小写。不要传入'f'，如果传入'f'在目标窗口中将不会显示为f。
				CKeyboard::模拟按键_SendInput(VK_DELETE, CKeyboardFlag_KeyDelay, delay);// 发送delete键消息到目标窗口。
				CKeyboard::模拟按键_SendInput(CKey_F, CKeyboardFlag_KeyDelay, delay);	// CKey_F代表的就是'F'。

				// nihao，如果对应的窗口打开了输入法，那么将显示“你好”两个字。
				CKeyboard::模拟按键_SendInput(CKey_N, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_I, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_H, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_A, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(CKey_O, CKeyboardFlag_KeyDelay, 20);
				CKeyboard::模拟按键_SendInput(VK_SPACE, CKeyboardFlag_KeyDelay, 20);// 空格。
				CKeyboard::模拟按键_SendInput(13, CKeyboardFlag_KeyDownUp);	// 回车。
				CKeyboard::模拟按键_SendInput(10, CKeyboardFlag_KeyDownUp);	// 换行。
			 *  @endcode
			 */
			static void 模拟按键_SendInput(__in BYTE bVk, __in CKeyboardFlag keyboardFlag, __in_opt LONGLONG delay = 0);

			/**	发送按键消息。
			 *	@param[in] 窗口句柄。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 键盘状态。接收：CKeyboardFlag_KeyChar、CKeyboardFlag_SYSKeyDownUp、CKeyboardFlag_KeyDown、CKeyboardFlag_KeyUp、CKeyboardFlag_KeyDownUp。
			 *	@param[in] 【可选】是否将按键消息发送到所有子窗口。true: 将按键消息发送到所有的子窗口中。false: 不将按键消息发送到所有的子窗口。
			 *  @par 示例:
			 *	@code
				CKeyboard::PostKeyMessage(this->m_hWnd, 'A', CKeyboardFlag_KeyDown);		// 按下
				CKeyboard::PostKeyMessage(this->m_hWnd, 'B', CKeyboardFlag_KeyUp);			// 弹起。
				CKeyboard::PostKeyMessage(this->m_hWnd, 'C', CKeyboardFlag_KeyDownUp);		// 按下+弹起。
				CKeyboard::PostKeyMessage(this->m_hWnd, 'E', CKeyboardFlag_SYSKeyDownUp);	// 按下+弹起。
				CKeyboard::PostKeyMessage(this->m_hWnd, 'D', CKeyboardFlag_KeyChar);		// 字符消息。

				// 将按键的按下弹起消息发送到所有的子控件中。
				CKeyboard::PostKeyMessage(this->m_hWnd, 'E', CKeyboardFlag_SYSKeyDownUp, true);
			 *  @endcode
			 */
			static void PostKeyMessage(__in HWND hWnd, __in UINT_PTR virtualKey, __in CKeyboardFlag keyboardFlag, __in_opt bool isPostToChildWindow = false);

			/**	按下或弹起控件。
			 *	@param[in] 控件句柄。
			 *	@param[in] 0: 对应BM_CLICK消息，表示点击控件。1: 对应WM_LBUTTONDOWN消息，表示按下控件。2: 对应WM_LBUTTONUP，表示弹起控件。
			 *  @par 示例:
			 *	@code
				CKeyboard::PressOrReleaseControl(GetDlgItem(IDC_BUTTON_ShowMessage)->m_hWnd, 0);	// 点击控件。

				// 按下+弹起控件。
				CKeyboard::PressOrReleaseControl(GetDlgItem(IDC_BUTTON_ShowMessage)->m_hWnd, 1);	// 按下。
				CKeyboard::PressOrReleaseControl(GetDlgItem(IDC_BUTTON_ShowMessage)->m_hWnd, 2);	// 弹起。
			 *  @endcode
			 */
			static void PressOrReleaseControl(__in HWND hWnd, __in UINT flag);

			/**	按组合键。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 功能键码1。
			 *	@param[in] 【可选】功能键码2。
			 *	@param[in] 【可选】功能键码3。
			 *	@note 使用keybd_event()函数模拟按键。
			 *  @par 示例:
			 *	@code
				CKeyboard::按组合键_KeyEvent('S', VK_MENU, VK_LSHIFT, VK_LCONTROL);	// Alt + Shift + Ctrl + S
			 *  @endcode
			 */
			//static void 按组合键_KeyEvent(__in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in UINT_PTR functionKey2 = 0, __in UINT_PTR functionKey3 = 0);

			/**	按组合键。
			 *	@param[in] 延时。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 功能键码1。
			 *	@param[in] 【可选】功能键码2。
			 *	@param[in] 【可选】功能键码3。
			 *	@note 使用keybd_event()函数模拟按键。
			 *  @par 示例:
			 *	@code
				CKeyboard::按组合键_KeyEvent(0, 'S', VK_MENU, VK_LSHIFT, VK_LCONTROL);	// Alt + Shift + Ctrl + S
			 *  @endcode
			 */
			static void 按组合键_KeyEvent(__in LONGLONG delay, __in UINT virtualKey, __in UINT functionKey1, __in_opt UINT functionKey2 = 0, __in_opt UINT functionKey3 = 0);

			/**	按组合键。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 功能键码1。
			 *	@param[in] 【可选】功能键码2。
			 *	@param[in] 【可选】功能键码3。
			 *	@param[in] 【可选】是否将按键消息发送到所有子窗口。true: 将按键消息发送到所有的子窗口中。false: 不将按键消息发送到所有的子窗口。
			 *	@note 通过发送按键消息模拟按键。
			 *  @par 示例:
			 *	@code
				CKeyboard::按组合键_KeyMessage(this->m_hWnd, 'T', VK_MENU, 0, 0, true);	// Alt + T
			 *  @endcode
			 */
			//static void 按组合键_KeyMessage(__in HWND hWnd, __in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in UINT_PTR functionKey2 = 0, __in UINT_PTR functionKey3 = 0, __in bool isPostToChildWindow = false);

			/**	按组合键。
			 *	@param[in] 窗口句柄。
			 *	@param[in] 延时。
			 *	@param[in] 虚拟键码。
			 *	@param[in] 功能键码1。
			 *	@param[in] 【可选】功能键码2。
			 *	@param[in] 【可选】功能键码3。
			 *	@param[in] 【可选】是否将按键消息发送到所有子窗口。true: 将按键消息发送到所有的子窗口中。false: 不将按键消息发送到所有的子窗口。
			 *	@note 通过发送按键消息模拟按键。
			 *  @par 示例:
			 *	@code
				CKeyboard::按组合键_KeyMessage(this->m_hWnd, 0, 'T', VK_MENU, 0, 0, true);	// Alt + T
			 *  @endcode
			 */
			static void 按组合键_KeyMessage(__in HWND hWnd, __in LONGLONG delay, __in UINT_PTR virtualKey, __in UINT_PTR functionKey1, __in_opt UINT_PTR functionKey2 = 0, __in_opt UINT_PTR functionKey3 = 0, __in bool isPostToChildWindow = false);

		private:
			/**	检查位设置。
			 *	
			 */
			static bool BitSet(__in BYTE BitTable, __in UINT BitMask);

			
		};
	}
}
