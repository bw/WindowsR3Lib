#pragma once

#include "commondef.h"

// 设置这个宏可参见
// “无法定位程输入点K32GetModuleBaseNameA于动态链接库KERNEL32.dll上” “http://hi.baidu.com/violetwangy/item/c1b50ce756bcb7aec00d752c”
#define PSAPI_VERSION 1

#ifdef __cplusplus
extern "C++" {

/**	创建进程。
 *	@param[in] 进程路径。
 *	@param[in] 【可选】返回进程信息。
 *	@return 创建成功，则返回TRUE。创建失败，则返回FALSE。
 */
BOOL CreateProcess_ (__in TCHAR *processPath, __out_opt LPPROCESS_INFORMATION lpProcessInformation);

/**	查找进程模块。
 *	@param[in] 进程ID。
 *	@param[in] 模块路径。
 *	@param[in] 【可选】如果只查找模块名，则传入TRUE。如果查找完整的模块路径，则传入FALSE。
 *	@return 找到模块，则返回TRUE。未找到，则返回FALSE。
 */
BOOL FindProcessModule (__in DWORD dwPID, __in TCHAR *strModulePath, __in_opt BOOL isName = FALSE);

/**	获得PEB结构指针。
 *	@param[in] 进程句柄。如果传入NULL，则是获得当前进程的PEB指针。
 *	@return 获得成功，则返回PEB指针。否则，返回NULL。
 *	@note 获得指定进程的PEB指针。如果获得的不是本进程的PEB指针，而是其他进程的，那么读取PEB指针中的内容需要用ReadProcessMemory()函数读取。
 */
PEB_* FS_GetPEBPtr (__in HANDLE hProcess);

/** 获得父进程Id。
 *  @param[in] 进程Id。
 *  @return 如果成功，则返回父进程Pid。如果失败，则返回 -1。
 */
HANDLE GetParentProcessId(__in DWORD dwPID);

/** 获得进程ID。
 *  @param[in] 进程名(不区分大小写)。
 *  @return 如果成功，则返回Pid。如果失败，则返回 -1。
 *	@note 为什么失败返回-1哪？因为进程Idle进程的进程Id为0，虽然R3层无法获得Idle的进程Id，但是为了消除歧义，所以获得失败将返回-1。
 */
long GetProcessIdOfProcessName(__in TCHAR *strProcessName);

/** 获得进程ID。
 *	@param[in] 窗口标题。查找到的窗口标题只要包含strWinName_Contain，就返回窗口所属进程的ID。
 *  @return 如果成功，则返回Pid。如果失败，则返回 -1。
 *	@note 为什么失败返回-1哪？因为进程Idle进程的进程Id为0，虽然R3层无法获得Idle的进程Id，但是为了消除歧义，所以获得失败将返回-1。
 */
long GetProcessId(__in TCHAR *strWinName_Contain);

/** 获得进程ID。
 *	@param[in] 【可选】窗口标题。
 *	@param[in] 【可选】窗口类名。
 *  @return 如果成功，则返回Pid。如果失败，则返回 -1。
 *	@note 为什么失败返回-1哪？因为进程Idle进程的进程Id为0，虽然R3层无法获得Idle的进程Id，但是为了消除歧义，所以获得失败将返回-1。
 */
long GetProcessId(__in_opt TCHAR *strWinName, __in_opt TCHAR *strClassName);

/**	获得进程名。
 *	@param[in] 进程Id。
 *	@param[out] 返回进程名。
 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
 */
BOOL GetProcessName (__in DWORD dwPID, __out CString &processName);

/**	获得进程名。
 *	@param[in] 窗口句柄。
 *	@param[out] 返回进程名。
 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
 */
BOOL GetProcessName (__in HWND hWnd, __out CString &processName);

/**	获得进程名。
 *	@param[in] 【可选】窗口标题。
 *	@param[in] 【可选】窗口类名。
 *	@param[out] 返回进程名。
 *	@return 获得成功，则返回TRUE。获得失败，则返回FALSE。
 */
BOOL GetProcessName (__in_opt TCHAR *strWinName, __in_opt TCHAR *strClassName, __out CString &processName);

/** 获得进程路径。
 *  @param[in] 进程Id。
 *	@param[out] 返回进程路径。
 *  @return 如果获得进程路径成功，则返回复制到字符串缓冲区的长度。否则，返回FALSE。
 */
DWORD GetProcessPath(__in DWORD dwPID, __out CString *procPath);

/**	隐藏模块。隐藏当前进程中的模块。
 *	@param[in] 模块完整路径。
 *	@return 隐藏成功，则返回TRUE。隐藏失败，则返回FALSE。
 *	@note 函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL __stdcall HideModule(__in TCHAR *strModulePath);

/**	隐藏模块。
 *	@param[in] 进程Id。
 *	@param[in] 模块完整路径。
 *	@return 隐藏成功，则返回TRUE。隐藏失败，则返回FALSE。
 *	@note 函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL __stdcall HideModule(__in DWORD dwPID, __in TCHAR *strModulePath);

/** 打开进程。
 *  @param[in] 进程Id。
 *  @return 如果成功，则返回进程句柄。如果失败，则返回 0。
 *	@note 此函数与系统API - OpenProcess的唯一区别是：在本函数中，提升了调用进程的权限为Debug权限。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
HANDLE OpenProcess_ (__in DWORD dwPID);

/** 通过进程名打开进程。
 *  @param[in] 进程名字符串。
 *  @return 如果成功，则返回进程句柄。如果失败，则返回 0。
 *	@note 函数失败可以通过GetLastError()函数查询失败原因。
 */
HANDLE OpenProcess_ (__in TCHAR *processName);

/**	读进程内存。
 *	@param 参数与系统API-ReadProcessMemory()的参数一样。
 *	@return 读取进程内存成功，返回TRUE。读取进程内存失败，返回FALSE。
 *	@note 此函数中先调用系统API-ReadProcessMemory()函数，如果读取失败，则调用VirtualProtectEx()设置对应内存块的内存保护，然后再次调用系统API-ReadProcessMemory()函数。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL __stdcall ReadProcessMemory_ (
	__in      HANDLE hProcess,
	__in      LPCVOID lpBaseAddress,
	__out_bcount_part(nSize, *lpNumberOfBytesRead) LPVOID lpBuffer,
	__in      SIZE_T nSize,
	__out_opt SIZE_T * lpNumberOfBytesRead
	);

/** 读进程内存 - 多级指针。
 *  @param[in] 进程句柄。
 *  @param[in] 地址数组。
 *  @param[in] 数组元素个数
 *  @param[out] 数据缓冲区。用以返回读取的数据。
 *	@param[in] 缓冲区字节数。
 *	@return 读取进程内存成功，返回TRUE。读取进程内存失败，返回FALSE。
 *	@note 此函数可解决游戏基址+偏移量的读取。经测试可用于的系统：Win XP。
 *  @par 示例:
 *  @code
	PEB* pPeb = FS_GetPEBPtr(hProcess);	// 通过进程句柄获得对应进程的PEB结构地址。
	if (pPeb) {
		ULONG_PTR addrArray[10] = {0};
		addrArray[0] = (ULONG_PTR)pPeb + FIELD_OFFSET(PEB, ProcessParameters);	// 读取pPeb->ProcessParameters处的数据。
		addrArray[1] = FIELD_OFFSET(RTL_USER_PROCESS_PARAMETERS_, ImagePathName) + FIELD_OFFSET(UNICODE_STRING, Buffer);// 读取[pPeb->ProcessParameters].ImagePathName.Buffer处的数据。
		addrArray[2] = 0;	// 读取[[pPeb->ProcessParameters].ImagePathName.Buffer + 0]处的数据。
		wchar_t imagePath[MAX_PATH] = {L'\0'};
		CProcessBase::ReadProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 3, imagePath, sizeof(imagePath));
		OutputDebugStringW(imagePath);	// 调试输出进程映射路径。
		return TRUE;
	} else {
		return FALSE;
	}
 *  @endcode
 */
BOOL __stdcall ReadProcessMemory_MulLevelPtrBytes (
	__in HANDLE hProcess, 
	__in ULONG_PTR aBaseAddress[], 
	__in int arraySize, 
	__out LPVOID lpBuffer, 
	__in SIZE_T nSize, 
	__out_opt SIZE_T * lpNumberOfBytesRead = NULL);

/**	远程拷贝。源操作数和目的操作数都在远程进程中。将源地址中的数据拷贝到目的地址中。
 *	@param[in] 进程句柄。
 *  @param[in] 目的地址数组。
 *  @param[in] 目的地址数组元素个数。
 *  @param[in] 源地址数组。
 *  @param[in] 源地址数组元素个数。
 *	@param[in] 拷贝字节数。
 *	@param[out] 【可选】实际写入的字节数。
 *	@return 远程拷贝成功，则返回TRUE。远程拷贝失败，则返回FALSE。
 *	@note 函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL __stdcall RemoteCopy ( 
	__in HANDLE hProcess, 
	__in ULONG_PTR aBaseAddress_des[], 
	__in int arraySize_des, 
	__in ULONG_PTR aBaseAddress_src[], 
	__in int arraySize_src, 
	__in SIZE_T nSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten = NULL);

/**	调用远程进程中的函数。
 *	@param[in] 进程句柄。
 *	@param[in] 远程进程中的函数地址。
 *	@param[in] 【可选】是否等待函数执行完成。true：等待远程线程中的函数执行完毕。false：不等待远程线程中的函数执行完毕，参数pFunRet将是无效的。
 *	@param[out] 【可选】函数执行完毕后的返回值。
 *	@param[in] 【可选】函数调用所需的参数地址。这个地址是当前进程中的地址。
 *	@param[in] 参数的长度。
 *	@return 如果调用成功，则返回TRUE。否则，返回FALSE。
 */
BOOL __stdcall RemoteFunctionCall (
	__in HANDLE hProcess, 
	__in LPTHREAD_START_ROUTINE pFunAddr, 
	__in BOOL isWait = TRUE, 
	__out_opt LPDWORD pFunRet = NULL, 
	__in_opt LPVOID lpParamBuffer = NULL, 
	__in SIZE_T paramSize = 0
	);

/**	在远程线程中调用当前进程的函数。
 *	@param[in] 进程句柄。
 *	@param[in] 当前进程中函数的地址。
 *	@param[in] 函数的长度。
 *	@param[in] 【可选】是否等待函数执行完成。true：等待远程线程中的函数执行完毕。false：不等待远程线程中的函数执行完毕，参数pFunRet将是无效的。
 *	@param[out] 【可选】返回线程id。
 *	@param[out] 【可选】函数的返回值。
 *	@param[in] 【可选】函数调用所需的参数地址。这个地址是当前进程中的地址。
 *	@param[in] 参数的长度。
 *	@return 如果调用成功，则返回TRUE。否则，返回FALSE。
 *	@note 将当前进程的函数拷贝到远程进程中，然后调用。
 */
BOOL RemoteFunctionCall_CopyTo (
	__in HANDLE hProcess, 
	__in LPTHREAD_START_ROUTINE pFunAddr, 
	__in SIZE_T funSize, 
	__in BOOL isWait = TRUE, 
	__out_opt LPDWORD pFunRet = NULL, 
	__in_opt LPVOID lpParamBuffer = NULL, 
	__in SIZE_T paramSize = 0
	);

/** 【慎用】恢复进程运行(其实是恢复进程中的所有线程)。
 *  @param[in] 进程Id。
 *  @return 如果成功，则返回true。如果失败，则返回false。
 *	@note 【慎用】在枚举线程的时候会有新的线程被创建，也可能有线程被销毁。因此在调用CreateToolhelp32Snapshot之后，
 *	目标进程中可能会出现新的线程，此函数无法对其实施恢复操作。在恢复线程时，他会恢复一个从未挂起的线程。
 *	更糟糕的是，在枚举线程ID时，可能会销毁一个已有线程，创建一个新的线程，而这两个线程恰好ID相同。
 *	这样的话，函数将恢复任意一个线程（可能属于目标进程之外的进程），当然这种情况可能性很小。
 */
BOOL __stdcall ResumeProcess(__in DWORD dwProcessId);

/**	终止进程。
 *	@param[in] 进程Id。
 *	@return 终止成功，则返回TRUE。终止失败，则返回FALSE。
 */
BOOL TerminateProcess_ (__in DWORD dwPID);

/**	将函数写入进程内存。
 *	@param[in] 进程句柄。
 *	@param[in] 要写入的内容。
 *	@param[in] 写入的长度。
 *	@param[in] 【可选】实际写入长度。
 *	@return 写入成功，则返回写入的地址。写入失败，则返回NULL。
 *	@note 如果写入成功，返回的内存地址需要使用VirtualFreeEx()进行释放内存。
 *		  为什么不在函数中将内存释放哪？比如将一个函数写入远程进程中，那么将这块内存释放掉，那么还如何运行写入的函数。
 *		  函数中做了判断，判断传入的函数地址是否是跳转表地址。
 */
PVOID __stdcall WriteFuncToProcessMemory (
	__in HANDLE hProcess, 
	__in_bcount(nSize) LPCVOID lpBuffer,
	__in SIZE_T dwSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten = NULL
	);

/**	写进程内存。
 *	@param 参数与系统API-WriteProcessMemory()的参数一样。
 *	@return 写进程内存成功，返回1。写进程内存失败，返回0。设置进程内存保护失败，返回-1。
 *	@note 此函数与系统API的区别在于，在此函数内部调用了VirtualProtectEx()设置了对应内存块的内存保护。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
BOOL __stdcall WriteProcessMemory_ (
	__in      HANDLE hProcess,
	__in      LPVOID lpBaseAddress,
	__in_bcount(nSize) LPCVOID lpBuffer,
	__in      SIZE_T nSize,
	__out_opt SIZE_T * lpNumberOfBytesWritten
	);

/**	写进程内存。
 *	@param[in] 进程句柄。
 *	@param[in] 要写入的内容。
 *	@param[in] 写入的长度。
 *	@param[in] 【可选】实际写入长度。
 *	@return 写入成功，则返回写入的地址。写入失败，则返回NULL。
 *	@note 如果写入成功，返回的内存地址需要使用VirtualFreeEx()进行释放内存。
 *		  为什么不在函数中将内存释放哪？比如将一个函数写入远程进程中，那么将这块内存释放掉，那么还如何运行写入的函数。
 *		  函数失败可以通过GetLastError()函数查询失败原因。
 */
PVOID __stdcall WriteProcessMemory_ (
	__in HANDLE hProcess, 
	__in_bcount(nSize) LPCVOID lpBuffer,
	__in SIZE_T dwSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten = NULL
	);

/** 写进程内存 - 多级指针。
 *  @param[in] 进程句柄。
 *  @param[in] 地址数组。
 *  @param[in] 数组元素个数
 *  @param[in] 数据缓冲区。
 *	@param[in] 缓冲区字节数。
 *	@param[out] 【可选】实际写入的字节数。
 *	@note 此函数可解决游戏基址+偏移量的写入。经测试可用于的系统：Win XP。
 *  @par 示例:
 *  @code
	wchar_t *strFullDllName0 = NULL;
	strFullDllName0 = (wchar_t *)calloc(us.Length + 2, 1);
	// 构造地址数组。
	ULONG_PTR addrArray[2] = {0};
	addrArray[0] = (ULONG_PTR)pldte + FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_, FullDllName) + FIELD_OFFSET(UNICODE_STRING, Buffer);
	addrArray[1] = 0;
	// 读取UNICODE_STRING.Buffer。
	if ( FALSE == ReadProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 2, strFullDllName0, us.Length) ) {
		free(strFullDllName0);
		return FALSE;
	}
	strFullDllName0[4] = L'S';

	// 写UNICODE_STRING.Buffer中的内容。
	if ( FALSE == WriteProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 2, strFullDllName0, us.Length, NULL) ) {
		AfxMessageBox(TEXT("TMD写错了。"));
	}

	ZeroMemory(strFullDllName0, us.Length + 2);
	// 读取UNICODE_STRING.Buffer，查看是否正确写入。
	if ( FALSE == ReadProcessMemory_MulLevelPtrBytes(hProcess, addrArray, 2, strFullDllName0, us.Length) ) {
		AfxMessageBox(TEXT("TMD写错了。"));
	}
	free(strFullDllName0);
 *  @endcode
 */
BOOL WriteProcessMemory_MulLevelPtrBytes(
	__in HANDLE hProcess, __in ULONG_PTR aBaseAddress[], 
	__in int arraySize, __in LPCVOID lpBuffer, __in SIZE_T nSize, 
	__out_opt SIZE_T * lpNumberOfBytesWritten = NULL);

/** 【慎用】暂停进程开关。
 *  @param[in] 进程Id。
 *  @param[in] 传入true，则暂停进程的所有线程。传入false，则恢复进程中所有线程的执行。
 *  @return 如果成功，则返回true。如果失败，则返回false。
 *	@note 【慎用】在枚举线程的时候会有新的线程被创建，也可能有线程被销毁。因此在调用CreateToolhelp32Snapshot之后，
 *	目标进程中可能会出现新的线程，此函数无法对其实施挂起、恢复操作。在恢复线程时，他会恢复一个从未挂起的线程。
 *	更糟糕的是，在枚举线程ID时，可能会销毁一个已有线程，创建一个新的线程，而这两个线程恰好ID相同。
 *	这样的话，函数将挂起或恢复任意一个线程（可能属于目标进程之外的进程），当然这种情况可能性很小。
 */
BOOL SuspendProcessSwitch(__in DWORD dwProcessId, __in BOOL bSuppend);

/** 【慎用】暂停进程(其实是暂停进程中的所有线程)。
 *  @param[in] 进程Id。
 *  @return 如果成功，则返回true。如果失败，则返回false。
 *	@note 【慎用】在枚举线程的时候会有新的线程被创建，也可能有线程被销毁。因此在调用CreateToolhelp32Snapshot之后，
 *	目标进程中可能会出现新的线程，此函数无法对其实施挂起操作。
 *	更糟糕的是，在枚举线程ID时，可能会销毁一个已有线程，创建一个新的线程，而这两个线程恰好ID相同。
 *	这样的话，函数将挂起任意一个线程（可能属于目标进程之外的进程），当然这种情况可能性很小。
 */
BOOL SuspendProcess(__in DWORD dwProcessId);

}
#endif

namespace Mrlee_R3
{
	namespace Process
	{
		class CProcessBase
		{
		protected:
			static HMODULE m_hNtdll;
			static NtQueryInformationProcess_Ptr m_NtQueryInformationProcess;

		public:
		protected:
			DWORD m_Pid;
			HANDLE m_hProcess;	// 进程句柄。

		public:
			/**	构造函数。
			 *	@param[in] pid。
			 */
			CProcessBase( __in DWORD dwProcessId );

			/**	析构函数。
			 */
			virtual ~CProcessBase();

			/**	获得进程句柄。
			 *	@return 返回进程句柄。
			 */
			HANDLE GetProcessHandle();

			/**	返回打开进程是否成功。
			 *	@return 打开进程成功，则返回TRUE。否则，返回FALSE。
			 */
			BOOL IsOpenSuccess();

			/**	读取进程内存中的一个字节数组。
			 *	@param[in] 读取的内存地址。
			 *	@param[out] 返回的数据。
			 *	@param[in] 要读取的字节数。
			 *	@param[out] 【可选】返回实际读取的字节数。
			 *	@return 读取进程内存成功，返回1。读取进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int ReadMemoryBytes(__in LPCVOID lpBaseAddress, __out BYTE* data, __in SIZE_T nSize, __out_opt SIZE_T * lpNumberOfBytesRead = 0);

			/**	读取进程内存中的一个32位整数值。
			 *	@param[in] 读取的内存地址。
			 *	@param[out] 返回的数据。
			 *	@param[out] 【可选】返回实际读取的字节数。
			 *	@return 读取进程内存成功，返回1。读取进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int ReadMemoryInt32(__in LPCVOID lpBaseAddress, __out INT32* data, __out_opt SIZE_T * lpNumberOfBytesRead = 0);

			/**	读取进程内存中的一个64位整数值。
			 *	@param[in] 读取的内存地址。
			 *	@param[out] 返回的数据。
			 *	@param[out] 【可选】返回实际读取的字节数。
			 *	@return 读取进程内存成功，返回1。读取进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int ReadMemoryInt64(__in LPCVOID lpBaseAddress, __out INT64* data, __out_opt SIZE_T * lpNumberOfBytesRead = 0);

			/**	将一个字节数组写入进程内存。
			 *	@param[in] 写入的内存地址。
			 *	@param[in] 写入的数据。
			 *	@param[in] 要写入数据的字节数。
			 *	@param[out] 【可选】返回实际写入的字节数。
			 *	@return 写进程内存成功，返回1。写进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int WriteMemoryBytes(__in LPVOID lpBaseAddress, __in BYTE* data, __in SIZE_T nSize, __out_opt SIZE_T * lpNumberOfBytesRead = 0);

			/**	将一个32位整数写入进程内存。
			 *	@param[in] 写入的内存地址。
			 *	@param[in] 写入的数据。
			 *	@param[out] 【可选】返回实际写入的字节数。
			 *	@return 写进程内存成功，返回1。写进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int WriteMemoryInt32(__in LPVOID lpBaseAddress, __in INT32 data, __out_opt SIZE_T * lpNumberOfBytesRead = 0);

			/**	将一个64位整数写入进程内存。
			 *	@param[in] 写入的内存地址。
			 *	@param[in] 写入的数据。
			 *	@param[out] 【可选】返回实际写入的字节数。
			 *	@return 写进程内存成功，返回1。写进程内存失败，返回0。设置进程内存保护失败，返回-1。
			 */
			int WriteMemoryInt64(__in LPVOID lpBaseAddress, __in INT64 data, __out_opt SIZE_T * lpNumberOfBytesRead = 0);
		};
	}
}