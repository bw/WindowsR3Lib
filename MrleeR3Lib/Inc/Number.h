#pragma once

namespace Mrlee_R3
{
	class CNumber
	{
	public:
		/**	将LONGLONG类型转换为十六进制字符串。
		 *	@param[in] LONGLONG类型。
		 *	@param[out] 返回转换后的字符串。
		 *	@return 转换成功，返回TRUE。转换失败，返回FALSE。
		 */
		static void LONGLONGToHexStr(__in LONGLONG llData, __out CString *hexStr);

		/**	将ULONGLONG类型转换为十六进制字符串。
		 *	@param[in] LONGLONG类型。
		 *	@param[out] 返回转换后的字符串。
		 *	@return 转换成功，返回TRUE。转换失败，返回FALSE。
		 */
		static void ULONGLONGToHexStr(__in ULONGLONG llData, __in CString *hexStr);

		//static BOOL HexArrayToBytesArray

		/** 将十六进制数字数组转换成数字。
		 *  @param[in] 十六进制数字数组。
		 *	@param[in] 数字的字节。
		 *  @return 返回转换后的数字。
		 *  @par 示例:
		 *  @code
			BYTE bytes[] = {0x1,0x2,0x3,0x4};
			LONGLONG num = HexByteArrayToNumber(bytes, Number_32);
			num: 0x0000000004030201

			BYTE bytes[] = {0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8};
			LONGLONG num = HexByteArrayToNumber(bytes, Number_64);
			num: 0x0807060504030201
		 *	@endcode
		 */
		static BOOL HexBytesArrayToNumber(__in BYTE szHex[], __in SIZE_T length, __out LONGLONG *outData);

		/**	将数值转换成字节数组。
		 *	@param[in] 数值。
		 *	@param[out] 输出十六进制转换后的字节数组。
		 *	@param[in] 数组长度。
		 */
		static void NumberToBytesArray(__in LONGLONG &hex, __out BYTE szHex[], __in SIZE_T length);

		/** 判断一个数字是否是偶数。
		 *  @param[in] 整形数字。
		 *  @return 如果是偶数，则返回TRUE。如果是奇数，则返回FALSE。
		 */
		static BOOL IsEvenNum(__in LONGLONG dwNum);
	};
}