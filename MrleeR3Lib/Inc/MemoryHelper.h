#pragma once

#ifdef __cplusplus
extern "C++" {

/** 在进程空间中分配内存，此函数分配的内存拥有完全权限并且分配了实际的物理内存。
 *  @param[in] 进程句柄。
 *  @param[in] 分配大小。
 *  @return 如果成功，则返回申请的内存地址。如果失败，则返回NULL。
 */
LPVOID VirtualAllocEx_(__in HANDLE hProcess, __in DWORD dwSize);

/** 释放在堆中的内存。按 VirtualAllocEx 审请时的大小全部释放。
 *  @param[in] 进程句柄。
 *  @param[in] 指向要释放的虚拟内存空间首地址的指针。
 *  @return 如果成功，则返回TRUE。如果失败，则返回FALSE。
 */
BOOL VirtualAllocEx_(__in HANDLE hProcess, __in LPVOID lpAddress);

}
#endif