#pragma once

/**	相加。
 */
int add(__in int a, __in int b);

namespace Mrlee_R3
{
	

	/*#ifdef UNICODE
	#define FindString FindStringW
	#define FindStringI FindStringIW
	#else
	#define FindString FindStringA
	#define FindStringI FindStringIA
	#endif*/

	class StringHelper
	{
	public:
		/**	char 转 wchar_t。
		 *	@param[in] char类型。
		 *	@param[in] wchar_t类型。
		 *	@note 参数des使用完毕后，需要释放内存。
		 */
		static void CharToWChar (__in char *src, __in wchar_t **des);

		/**	wchar_t 转 char。
		 *	@param[in] wchar_t类型。
		 *	@param[in] char类型。
		 *	@note 参数des使用完毕后，需要释放内存。
		 */
		static void WCharToChar (__in wchar_t *src, __in char **des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数不忽略大小写。
		 */
		static int FindString (__in TCHAR *src, __in TCHAR *des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数不忽略大小写。
		 */
		static int FindStringA (__in char *src, __in char *des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数不忽略大小写。
		 */
		static int FindStringW (__in wchar_t *src, __in wchar_t *des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数忽略大小写。
		 */
		static int FindStringI (__in TCHAR *src, __in TCHAR *des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数忽略大小写。
		 */
		static int FindStringIA (__in char *src, __in char *des);

		/**	从左边开始查找字符串。返回找到的第一个字符串的偏移。
		 *	@param[in] 被比较的字符串。
		 *	@param[in] 要查找的字符串。
		 *	@return 如果查找到，则返回偏移，这个偏移是数组元素的偏移。如果未找到，则返回-1。
		 *	@note 此函数忽略大小写。
		 */
		static int FindStringIW (__in wchar_t *src, __in wchar_t *des);

		/** 十六进制字符串转换成十六进制数组。
		 *	@par 示例:
		 *  @code
			CString hexStr = "1234567";
			BYTE bytes[MAX_PATH] = {0};
			int count = StringHelper::HexStrToByteArray(m_SearchValueOfMem.Trim(), bytes);
			结果：count值为数组中实际赋值的元素个数，即4。bytes中的值为：0x01, 0x23, 0x45, 0x67。

			CString hexStr = "12 34 56 7";
			BYTE bytes[MAX_PATH] = {0};
			int count = StringHelper::HexStrToByteArray(m_SearchValueOfMem.Trim(), bytes);
			结果：count值为数组中实际赋值的元素个数，即4。bytes中的值为：0x01, 0x23, 0x45, 0x67。
		 *	@endcode
		 */
		static int HexStrToByteArray(__in CString& hexStr, __out BYTE outBytes[]);

		/**	保存十六进制的字符串转换成LONGLONG类型。
		 *	@param[in] 十六进制字符串。
		 *	@param[in] 返回转换后的数值。
		 *	@return 转换成功，返回TRUE。否则，返回FALSE。
		 */
		static BOOL HexStrToLONGLONG(__in CString& hexStr, __out LONGLONG *hex);

		/**	保存十六进制的数组转换成十进制。
		 *	@param[in] 十六进制字符串。
		 *	@param[in] 返回转换后的数值。
		 *	@return 转换成功，返回TRUE。否则，返回FALSE。
		 */
		static BOOL HexStrToLONGLONG(__in CStringArray& hexStrArray, __out LONGLONG *hex);

		/**	十六进制字符串转换成字节表现形式。
		 *	@par 示例:
		 *  @code
			CString hexStr = "1234567";
			CString bytes;
			HexStrToBytes(hexStr, &bytes);
			结果：bytes的值将为67452301。
		 *	@endcode
		 */
		static void HexStrToBytes(__in CString& hexStr, __out CString *bytes);

		/**	分割字符串。
		 *	@param[in] 原字符串。
		 *	@param[in] 分隔字符串。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@param[in] 【可选】如果分割字符串在当前的原字符串的起始位置，是否保留分割字符串前面的空串。TRUE：保留。FALSE：不保留。
		 */
		static INT_PTR SplitString(__in LPCTSTR lpszStr, __in LPCTSTR lpszSplit, __out CStringArray& rArrString, __in BOOL bAllowNullString = FALSE);

		/**	分割字符串。
		 *	@param[in] 原字符串。
		 *	@param[in] 分隔字符。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@return 拆分后的子串个数。
		 */
		static INT_PTR SplitCString( IN CString& strIn, IN TCHAR division, OUT CStringArray& strAryRe );

		/**	分割字符串。
		 *	@param[in] 原字符串。
		 *	@param[in] 分隔字符。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@return 拆分后的子串个数。
		 */
		static INT_PTR SplitCString( IN CString& strIn, IN LPCTSTR division, OUT CStringArray& strAryRe );

		/**	分割字符串，分割字符为重复的字符串。
		 *	@param[in] 原字符串。
		 *	@param[in] 分隔字符。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@return 拆分后的子串个数。
		 */
		static INT_PTR SplitCStringForRepeat( IN CString& strIn, IN TCHAR division, OUT CStringArray& strAryRe );

		/**	以行为单位分隔字符串。
		 *	@param[in] 原字符串。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@return 拆分后的子串个数。
		 *	@note 换行符："\r\n", "\n"。
		 */
		static INT_PTR SplitCStringByLine( IN CString& strIn, OUT CStringArray& strAryRe );

		/**	通过空白符分割字符串。
		 *	@param[in] 原字符串。
		 *	@param[out] 返回分割后的字符串数组。
		 *	@return 拆分后的子串个数。
		 *	@note 空白符：空格、水平制表符、垂直制表符。
		 */
		static INT_PTR SplitCStringBySpace( IN CString& strIn, OUT CStringArray& strAryRe );

		/**	繁体中文转换简体中文。
		 *	@param[in] 繁体中文。
		 *	@return 返回简体中文字符串指针。
		 *	@note 返回的字符串使用完后，需要释放内存。
		 */
		static TCHAR* 繁体转简体 (__in TCHAR *Traditional);

		/**	简体中文转换繁体中文。
		 *	@param[in] 简体中文。
		 *	@return 返回繁体中文字符串指针。
		 *	@note 返回的字符串使用完后，需要释放内存。
		 */
		static TCHAR* 简体转繁体 (__in TCHAR *Simplified);
	};
}