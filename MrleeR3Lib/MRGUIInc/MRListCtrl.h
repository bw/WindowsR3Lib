#pragma once

#include <afx.h>
#include <afxcmn.h>
#include <afxtempl.h>

#define CMRListCtrlBase CListCtrl

typedef struct _CListColumn {
	int		ColumnIndex;	// 列索引。
	CString ColumnName;		// 列名。
	CString ColumnCaption;	// 列标题。
	int		Width;			// 列宽度。
	BOOL	IsFix;			// 是否固定。TRUE，固定。FALSE，不固定。
} CListColumn;

class CMRListCtrl : public CMRListCtrlBase
{
	DECLARE_DYNAMIC(CMRListCtrl)

protected:
	struct CNode
	{
		CNode* pNext;
		CNode* pPrev;
		CListColumn data;
	};
	CList<CListColumn, CListColumn&> m_Columns;
	BOOL m_IsEnableColumnAutoSize;	// 是否自动调整列的宽度。
	BOOL m_InitFlag;				// 初始化标记。
	BOOL m_IsScroll;				// TRUE:滚动。FALSE:不滚动。

public:
	CMRListCtrl();
	virtual ~CMRListCtrl();

public:
	/**	自动调整列的宽度。
	 *	@param[in] 【可选】TRUE：启动。FALSE：不启动。
	 */
	void EnableColumnAutoSize(BOOL bEnable = TRUE);

	/**	固定列宽度。
	 *	@param[in] 列名数组。
	 *	@param[in] 数组中元素的个数。
	 *	@note 如果列名数组中有没有找到的列名，则不固定其宽度。
	 *		  先调用此还是固定某些列的宽度，然后调用EnableColumnAutoSize确保控件放大或缩小时那些没有固定的列，按比例缩放。
	 */
	virtual void FixedColumnWidth(TCHAR* lpszColumnNames[], DWORD num);

	/**	获得列的个数。
	 *	@return 返回列的个数。
	 */
	virtual int GetColumnCount();

	/**	获得一个单元格的文本。
	 *	@param[in] 行的索引，从零开始。
	 *	@param[in] 列索引。
	 *	@return 返回单元格中的内容。
	 */
	virtual CString GetItemText(int nRowIndex, int nColIndex) const;

	/**	获得一个单元格的文本。
	 *	@param[in] 行的索引，从零开始。
	 *	@param[in] 列名。
	 *	@return 返回单元格中的内容。如果列名不存在，则返回空串。
	 */
	virtual CString GetItemText(int nRowIndex, LPCTSTR lpszColumnName);

	virtual int GetItemText(int nItem, int nSubItem, __out_ecount_part_z(nLen, return + 1) LPTSTR lpszText, int nLen) const;

	/**	插入列。向后插入新列。
	 *	@param[in] 列标题。
	 *	@param[in] 【可选】列的宽度，单位为像素。如果设置为-1，表示不设置列的宽度。
	 *	@param[in] 【可选】列的对齐方式。可以是以下值：LVCFMT_LEFT, LVCFMT_RIGHT, or LVCFMT_CENTER。
	 *	@param[in] 【可选】与列关联的子项的索引。如果设置为-1，表示没有子项与列关联。
	 *	@return 插入成功，则返回新列的索引。插入失败，则返回-1。
	 *	@note 如果不输入列名，则会创建一个默认的列名"ColumnX"，其中X代表从零开始的序号，这个序号与列的顺序无关，与创建的顺序有关。
	 */
	virtual int InsertColumn(LPCTSTR lpszColumnCaption, int nWidth = -1, int nFormat = LVCFMT_LEFT, int nSubItem = -1);

	/**	插入列。向后插入新列。
	 *	@param[in] 列名。用以标识列的唯一性，可以理解为列的Id。
	 *	@param[in] 列标题。
	 *	@param[in] 【可选】列的宽度，单位为像素。如果设置为-1，表示不设置列的宽度。
	 *	@param[in] 【可选】列的对齐方式。可以是以下值：LVCFMT_LEFT, LVCFMT_RIGHT, or LVCFMT_CENTER。
	 *	@param[in] 【可选】与列关联的子项的索引。如果设置为-1，表示没有子项与列关联。
	 *	@return 插入成功，则返回新列的索引。列名（参数2）冲突，则返回-2。插入失败，则返回-1。
	 */
	virtual int InsertColumn(LPCTSTR lpszColumnName, LPCTSTR lpszColumnCaption, 
		int nWidth = -1, int nFormat = LVCFMT_LEFT, int nSubItem = -1);

	/**	插入列。
	 *	@param[in] 插入的索引位置。
	 *	@param[in] 列标题。
	 *	@param[in] 【可选】列的宽度，单位为像素。如果设置为-1，表示不设置列的宽度。
	 *	@param[in] 【可选】列的对齐方式。可以是以下值：LVCFMT_LEFT, LVCFMT_RIGHT, or LVCFMT_CENTER。
	 *	@param[in] 【可选】与列关联的子项的索引。如果设置为-1，表示没有子项与列关联。
	 *	@return 插入成功，则返回新列的索引。插入失败，则返回-1。
	 *	@note 如果不输入列名，则会创建一个默认的列名"ColumnX"，其中X代表从零开始的序号，这个序号与列的顺序无关，与创建的顺序有关。
	 */
	virtual int InsertColumn(int nColIndex, LPCTSTR lpszColumnCaption, 
		int nWidth = -1, int nFormat = LVCFMT_LEFT, int nSubItem = -1);
	
	/**	插入列。
	 *	@param[in] 插入的索引位置。
	 *	@param[in] 列名。用以标识列的唯一性，可以理解为列的Id。
	 *	@param[in] 列标题。
	 *	@param[in] 【可选】列的宽度，单位为像素。如果设置为-1，表示不设置列的宽度。
	 *	@param[in] 【可选】列的对齐方式。可以是以下值：LVCFMT_LEFT, LVCFMT_RIGHT, or LVCFMT_CENTER。
	 *	@param[in] 【可选】与列关联的子项的索引。如果设置为-1，表示没有子项与列关联。
	 *	@return 插入成功，则返回新列的索引。列名（参数2）冲突，则返回-2。插入失败，则返回-1。
	 */
	virtual int InsertColumn(int nColIndex, LPCTSTR lpszColumnName, LPCTSTR lpszColumnCaption, 
		int nWidth = -1, int nFormat = LVCFMT_LEFT, int nSubItem = -1);

	/**	设置一个单元格的文本。
	 *	@param[in] 行的索引，从零开始。
	 *	@param[in] 列的索引，从零开始。
	 *	@param[in] 要设置的内容。
	 *	@return 设置成功，则返回TRUE。设置失败，则返回FALSE。
	 */
	virtual BOOL SetItemText(int nRowIndex, int nColIndex, LPCTSTR lpszText);

	/** 设置最后一行的某一个单元格的文本。
	 *	@param[in] 列名。
	 *	@param[in] 要设置的内容。
	 *	@return 设置成功，则返回TRUE。设置失败，则返回FALSE。
	 */
	virtual BOOL SetItemText(LPCTSTR lpszColumnName, LPCTSTR lpszText);

	/**	设置一个单元格的文本。
	 *	@param[in] 行的索引，从0开始。
	 *	@param[in] 列名。
	 *	@param[in] 要设置的内容。
	 *	@return 列名不存在，则返回-1。设置成功，则返回TRUE。设置失败，则返回FALSE。
	 */
	virtual int SetItemText(int nRowIndex, LPCTSTR lpszColumnName, LPCTSTR lpszText);

	/**	设置滚动。
	 *	@param[in] TRUE：添加一行数据后，自动滚动。FALSE：添加一行数据后，不滚动。
	 */
	virtual void SetScroll(BOOL isScroll);

protected:
	/**	查找列。
	 *	@param[in] 列名。
	 *	@param[out] 【可选】返回CList中的位置。
	 *	@return 查找到，则返回列索引。未查到到，则返回-1。
	 */
	virtual INT_PTR FindColumn(LPCTSTR lpszColumnName, POSITION *pPos = NULL);

	/**	调整列。
	 */
	virtual void ResizeColumns();

	/**	重载窗口过程。
	 */
	virtual LRESULT WindowProc(UINT nMsg, WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnHdnEndtrack(NMHDR *pNMHDR, LRESULT *pResult);
};