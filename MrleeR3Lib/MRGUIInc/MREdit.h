#pragma once

typedef enum _MColor {
	M_Red, 
	M_Green, 
	M_Blue, 
} MColor;

// CMREdit

class CMREdit : public CEdit
{
	DECLARE_DYNAMIC(CMREdit)

public:
	CMREdit();
	virtual ~CMREdit();

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF m_TextColor;	/// 文本颜色。
public:
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	/**	获得文本颜色。
	 *	@return 返回文本颜色。
	 */
	COLORREF GetTextColor();

	/**	设置文字颜色。
	 *	@param[in] 颜色值。可以使用RGB(,,)宏生成。
	 */
	void SetTextColor(__in COLORREF textColor);

	/**	设置文字颜色。
	 *	@param[in] 颜色值。MColor类型的枚举  。
	 */
	void SetTextColor(__in MColor color);
};


