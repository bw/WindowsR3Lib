#pragma once

#include <afx.h>
#include <afxwin.h>
#include <afxcmn.h>
#include <list>
#include <vector>

class CMRTabCtrl : public CTabCtrl
{
	DECLARE_DYNAMIC(CMRTabCtrl)

public:
	CMRTabCtrl();
	virtual ~CMRTabCtrl();
	
	//////////////////////////////////////////////////////////////////////////

	/**	添加标签页。
	 *	@param[in] 第几项。
	 *	@param[in] 项的名称。
	 *	@param[in] 添加到标签页中的窗口指针。
	 *	@return 如果插入成功，则返回插入标签的索引。如果插入失败，则返回-1。索引起始为零。
	 *	@note 调用这个函数传入对话框指针之前，需要先创建对话框。
	 *		  例：对话框.Create(对话框Id, &CMRTabCtrl);
	 */
	virtual LONG InsertItem(__in int nItem, __in LPCTSTR lpszItem, __in CDialog *pDialog);

	/**	添加标签页。
	 *	@param[in] 第几项。
	 *	@param[in] 项的名称。
	 *	@param[in] 添加到标签页中的窗口指针。
	 *	@return 如果插入成功，则返回插入标签的索引。如果插入失败，则返回-1。索引起始为零。
	 *	@note 调用这个函数传入对话框指针之前，【不要】创建(Create)对话框。在此函数内部会自动创建。
	 */
	virtual LONG InsertItem(__in int nItem, __in LPCTSTR lpszItem, __in CDialog *pDialog, __in UINT nDialogId);
	
	/**	获得当前标签页的对话框指针。
	 */
	virtual CDialog* GetCurrentDialog();

	/**	设置显示的标签页。
	 */
	virtual void SetTabPage(__in DWORD index);

	/**	销毁标签页中的所有窗口。
	 */
	virtual void DestroyAllWindow();

	/**	关闭标签页中的所有窗口。
	 */
	//virtual void CloseAllWindow();

protected:
	/*typedef struct _DialogInfo {
		CDialog *pDialog;
		BOOL	isFree;
	} DialogInfo;*/
	int m_CurSelTab;		// 当前选择的标签页。
	BOOL m_isSizeChanged;	// 控件大小是否被改变。
	std::vector<CDialog*> m_pTabDialogs;
	//std::vector<CDialog*> m_pTabDialogs_Free;	// 要释放内存的对话框。

	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	/**	选择标签页。
	 */
	afx_msg void OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()

	/**	所有标签页中的窗口都进行填充。
	 */
	virtual void AllPopulateControl();

	/**	当前对话框填充。
	 */
	virtual void PopulateControl();

	/**	填充对话框。
	 *	@param[in] 从零开始的标签页索引。
	 */
	virtual void PopulateControl(__in DWORD index);
};